<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *  Class model unit ranting
 */
class Unitranting extends Model {

	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = 'unitranting';

	/**
	 * Fillable fields
	 * 
	 * @var Array
	 */
	protected $fillable = ['unitcabang_id','nama_unitranting','created_by','updated_by'];

	/**
	 * Define inverse one to many relationship with Unit cabang model
	 * 
	 * @return Model
	 */
	public function unitcabang()
	{
		return $this->belongsTo('Certification\Models\Unitcabang');
	} 

	/**
	 * Query scope for query condition with given unit ranting id
	 * 
	 * @param  Query $query query
	 * @param  Integer $id  unit ranting Id
	 * @return Query
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	/**
	 * Query scope with all relationship
	 * 
	 * @param  query $query query
	 * @return Query
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('unitcabang');
	}

}
