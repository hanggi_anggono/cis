<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Biayapelaksanaan extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'biayapelaksanaan';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['penjadwalan_id', 'komponenbiaya_id', 'nosurat_tagihan', 'biaya'];

	/**
	 * [komponenbiaya description]
	 * 
	 * @return [type] [description]
	 */
	public function komponenbiaya()
	{
		return $this->belongsTo('Certification\Models\Komponenbiaya');
	}

	/**
	 * [penjadwalan description]
	 * 
	 * @return [type] [description]
	 */
	public function penjadwalan()
	{
		return $this->belongsTo('Certification\Models\Penjadwalan');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id', '=', $id);
	}

	/**
	 * [scopeWherePenjadwalanId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWherePenjadwalanId($query, $id)
	{
		return $query->where('penjadwalan_id', '=', $id);
	}

	/**
	 * [scopeWhereKomponenbiayaId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereKomponenbiayaId($query, $id)
	{
		return $query->where('komponenbiaya_id', '=', $id);
	}

	/**
	 * [scopeWhereNosuratTagihan description]
	 * 
	 * @param  [type] $query   [description]
	 * @param  [type] $nosurat [description]
	 * @return [type]          [description]
	 */
	public function scopeWhereNosuratTagihan($query, $nosurat)
	{
		return $query->where('nosurat_tagihan', '=', $nosurat);
	}

	/**
	 * [scopeNoFilter description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeNoFilter($query)
	{
		return $query;
	}

}
