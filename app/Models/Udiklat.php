<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *  Class model Unit diklat
 */
class Udiklat extends Model {

	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = 'udiklat';

	/**
	 * Fillable fields
	 * 
	 * @var array
	 */
	protected $fillable = ['nama_udiklat'];

	/**
	 * Define one to many relationship with Unit induk model
	 * 
	 * @return Model
	 */
	public function unitinduk()
	{
		return $this->hasMany('Certification\Models\Unitinduk');
	}

	/**
	 * [penjadwalan description]
	 * 
	 * @return [type] [description]
	 */
	public function penjadwalan()
	{
		return $this->hasMany('Certification\Models\Penjadwalan');
	}

	/**
	 * Query scope for query condition with given udiklat id
	 * 
	 * @param  Query $query query
	 * @param  Integer $id  udiklat Id
	 * @return Query
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	/**
	 * Query scope with all relationship
	 * 
	 * @param  Query $query query
	 * @return Query
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('unitinduk');
	}

}
