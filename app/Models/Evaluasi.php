<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluasi extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'evaluasi';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['kompetensipermohonan_id', 'kompetensipermohonansdm_id', 'peserta_id', 'hasil', 'info', 'status_penjadwalan', 'updated_by', 'tanggal_evaluasi'];

	/**
	 * [kompetensipermohonanpeserta description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensipermohonanpeserta()
	{
		return $this->belongsTo('Certification\Models\Kompetensipermohonanpeserta');
	}

	/**
	 * [peserta description]
	 * 
	 * @return [type] [description]
	 */
	public function peserta()
	{
		return $this->belongsTo('Certification\Models\Peserta');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id', '=', $id);
	}

	/**
	 * [scopeAccepted description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeAccepted($query)
	{
		return $query->where('hasil', '=', 1);
	}

	/**
	 * [scopeRejected description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeRejected($query)
	{
		return $query->where('hasil', '=', 0);
	}

	/**
	 * [scopeIsScheduled description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeIsScheduled($query)
	{
		return $query->where('status_penjadwalan', '=', 1);
	}

	/**
	 * [scopeIsNotScheduled description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeIsNotScheduled($query)
	{
		return $query->where('status_penjadwalan', '=', 0);
	}

	/**
	 * [scopeUpdatedBy description]
	 * 
	 * @param  [type] $query   [description]
	 * @param  [type] $adminId [description]
	 * @return [type]          [description]
	 */
	public function scopeUpdatedBy($query, $adminId)
	{
		return $query->where('updated_by', '=', $adminId);
	}

	/**
	 * [scopeWithRelationship description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('kompetensipermohonanpeserta');
	}

	/**
	 * [scopeJoinWithPermohonan description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeJoinWithPermohonan($query)
	{
		return $query->join('kompetensipermohonan', 'evaluasi.kompetensipermohonan_id', '=', 'kompetensipermohonan.id')
					 ->join('permohonan', 'kompetensipermohonan.permohonan_id', '=', 'permohonan.id');
	}

}
