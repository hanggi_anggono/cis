<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Kompetensipermohonansdm extends Model {

    /**
     * [$table description]
     * 
     * @var string
     */
	protected $table = 'kompetensipermohonansdm';

    /**
     * [peserta description]
     * 
     * @return [type] [description]
     */
    public function peserta()
    {
    	return $this->belongsToMany('Certification\Models\Peserta', 'kompetensipermohonansdmpeserta')->withTimestamps();
    }

    /**
     * [kompetensi description]
     * 
     * @return [type] [description]
     */
    public function kompetensi()
    {
    	return $this->belongsTo('Certification\Models\Kompetensi');
    }

    /**
     * [permohonan description]
     * 
     * @return [type] [description]
     */
    public function permohonansdm()
    {
        return $this->belongsTo('Certification\Models\Permohonansdm');
    }

    /**
     * [permohonan description]
     * 
     * @return [type] [description]
     */
    public function evaluasi()
    {
        return $this->hasMany('Certification\Models\Evaluasi');
    }

    /**
     * [scopeWithRelationship description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeWithRelationship($query)
    {
    	return $query->with('peserta','kompetensi', 'permohonansdm', 'evaluasi');
    }

    /**
     * [scopeWherePermohonanId description]
     * 
     * @param  [type] $query [description]
     * @param  [type] $id    [description]
     * @return [type]        [description]
     */
    public function scopeWherePermohonanSdmId($query, $id)
    {
        return $query->where('permohonansdm_id', '=', $id);
    }

    /**
     * [scopeWhereKompetensiId description]
     * 
     * @param  [type] $query [description]
     * @param  [type] $id    [description]
     * @return [type]        [description]
     */
    public function scopeWhereKompetensiId($query, $id)
    {
        return $query->where('kompetensi_id', '=', $id);
    }

    /**
     * [scopeIsKompetensiPermohonanExist description]
     * 
     * @param  [type] $query        [description]
     * @param  [type] $permohonanid [description]
     * @param  [type] $kompetensiId [description]
     * @return [type]               [description]
     */
    public function scopeIsKompetensiPermohonanSdmExist($query, $permohonansdmid, $kompetensiId)
    {
        return $query->where('permohonansdm_id', '=', $permohonansdmid)->where('kompetensi_id', '=', $kompetensiId);
    }

}
