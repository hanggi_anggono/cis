<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lembaga extends Model {

	use SoftDeletes;

	protected $table = 'lembaga';
    protected $dates = ['deleted_at'];

	protected $fillable = ['nama_lembaga'];

}
