<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Unitnonpln extends Model {

	protected $table    = 'unitnonpln';
	protected $fillable = ['nama_unitnonpln', 'alamat', 'created_by', 'edited_by'];

	public function administrator()
	{
		return $this->hasMany('Certification\Models\Administrator');
	}

	public function asesor()
	{
		return $this->hasMany('Certification\Models\Asesor');
	}

	public function peserta()
	{
		return $this->hasMany('Certification\Models\Peserta');
	}

	/**
	 * Query scope for query condition with given unit induk id
	 * 
	 * @param  Query $query query
	 * @param  Integer $id  unit induk Id
	 * @return Query
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	public function scopeWithRelationship($query)
	{
		return $query->with('administrator', 'asesor', 'peserta');
	}

}
