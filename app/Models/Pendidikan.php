<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Model for table Pendidikan
 */
class Pendidikan extends Model {

	use SoftDeletes;

	protected $table    = 'pendidikan';
	protected $fillable = ['nama_pendidikan'];
	protected $dates    = ['deleted_at'];

}
