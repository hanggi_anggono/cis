<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class model unit cabang
 */
class Unitcabang extends Model {

	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = 'unitcabang';

	/**
	 * Fillable fields
	 * 
	 * @var Array
	 */
	protected $fillable = ['unitinduk_id','nama_unitcabang','created_by','edited_by'];

	/**
	 * Define inverse one to many relationship with unit induk
	 * 
	 * @return Model
	 */
	public function unitinduk()
	{
		return $this->belongsTo('Certification\Models\Unitinduk');
	}

	/**
	 * Define one to many relationship with unit ranting
	 * 
	 * @return Model
	 */
	public function unitranting()
	{
		return $this->hasMany('Certification\Models\Unitranting');
	}

	/**
	 * Query scope for query condition with given unt cabang id
	 * 
	 * @param  Query $query query
	 * @param  Integer $id  unt cabang Id
	 * @return Query
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	/**
	 * Query scope to include relationship model
	 * 
	 * @return Query
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('unitinduk','unitranting');
	}

}
