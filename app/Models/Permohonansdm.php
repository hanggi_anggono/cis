<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Permohonansdm extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'permohonansdm';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['tanggal_mohon','unitinduk_id','keterangan'];

	/**
	 * [unitinduk description]
	 * 
	 * @return [type] [description]
	 */
	public function unitinduk()
	{
		return $this->belongsTo('Certification\Models\Unitinduk');
	}

	/**
	 * [kompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensi()
	{
		return $this->belongsToMany('Certification\Models\Kompetensi', 'kompetensipermohonansdm')->withTimestamps();
	}

	/**
	 * [peserta description]
	 * 
	 * @return [type] [description]
	 */
	public function peserta()
	{
		return $this->belongsToMany('Certification\Models\Peserta', 'kompetensipermohonansdmpeserta')->withPivot('kompetensipermohonansdm_id')->withTimestamps();
	}

	/**
	 * [kompetensipermohonan description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensipermohonansdm()
	{
		return $this->hasMany('Certification\Models\Kompetensipermohonansdm');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id', '=', $id);
	}

	/**
	 * [scopeWhereUnitInduk description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereUnitInduk($query, $id)
	{
		return $query->where('unitinduk_id', '=', $id);
	}

	/**
	 * [scopeOrderByCreated description]
	 * 
	 * @param  [type] $query [description]
	 * @param  string $sort  [description]
	 * @return [type]        [description]
	 */
	public function scopeOrderByCreated($query, $sort = 'desc')
	{
		return $query->orderBy('permohonansdm.created_at',$sort);
	}

	/**
	 * [scopeOrderByTanggalSurat description]
	 * 
	 * @param  [type] $query [description]
	 * @param  string $sort  [description]
	 * @return [type]        [description]
	 */
	public function scopeOrderByTanggalMohon($query, $sort = 'desc')
	{
		return $query->orderBy('tanggal_mohon', $sort);
	}

	/**
	 * [scopeWithRelationship description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('unitinduk','kompetensi','peserta','kompetensipermohonansdm');
	}
	
}
