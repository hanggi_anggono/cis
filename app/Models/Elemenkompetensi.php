<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Database Elemen kompetensi Model
 */
class Elemenkompetensi extends Model {

	/**
	 * Database table name
	 * 
	 * @var string
	 */
	protected $table = 'elemenkompetensi';
	
	/**
	 * Database table fillable fields for mass assignment
	 * 
	 * @var Array
	 */
	protected $fillable = ['nama_elemen','kompetensi_id', 'nama_elemen_english'];

	/**
	 * Define inverse one to many relationship with Kompetensi model
	 * 
	 * @return Relationship
	 */
	public function kompetensi()
	{
		return $this->belongsTo('Certification\Models\Kompetensi');
	}

	/**
	 * Query scope condition in given elemen kompetensi id
	 * 
	 * @param  Query   $query Query
	 * @param  Integer $id    Elemen kompetensi id
	 * @return Query
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=', $id);
	}

	/**
	 * Query scope condition in given kompetensi id;
	 * 
	 * @param  Query $query Query
	 * @param  Integer $kompetensi_id Kompetensi id
	 * @return Query
	 */
	public function scopeByKompetensi($query, $kompetensi_id)
	{
		return $query->where('kompetensi_id', '=', $kompetensi_id);
	}

}
