<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Master Bidang Model Class
 */
class Bidang extends Model {

	/**
	 * Database table name
	 * 
	 * @var string
	 */
	protected $table = 'bidang';
	
	/**
	 * Database table fillable field
	 * @var array
	 */
	protected $fillable = ['nama_bidang', 'nama_bidang_english'];

	/**
	 * Define one to many relationship with sub bidang model
	 * 
	 * @return Relationship
	 */
	public function subbidang()
	{
		return $this->hasMany('Certification\Models\Subbidang');
	}

	/**
	 * Query scope get collection with given id
	 * 
	 * @param  Query $query query
	 * @param  Integer $id  Bidang id
	 * @return Query
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

}
