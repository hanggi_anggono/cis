<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Database Kompetensi Model
 */
class Kompetensi extends Model {

	/**
	 * Database Table name
	 * 
	 * @var string
	 */
	protected $table = 'kompetensi';
	
	/**
	 * Database table fillable fields
	 * 
	 * @var Array
	 */
	protected $fillable = ['subbidang_id', 'nama_kompetensi', 'kodedjk', 'kodeskkni', 'nama_kompetensi_english'];

	/**
	 * Define inverse one to many relationship with subbidang Model
	 * 
	 * @return Relationship
	 */
	public function subbidang()
	{
		return $this->belongsTo('Certification\Models\Subbidang');
	}

	/**
	 * Define one to many relationship with Elemenkompetensi Model
	 * 
	 * @return Relationship
	 */
	public function elemenKompetensi()
	{
		return $this->hasMany('Certification\Models\Elemenkompetensi');
	}

	/**
	 * Define many to many relationship with Asesor Model
	 * 
	 * @return Relationship
	 */
	public function asesor()
	{
		return $this->belongsToMany('Certification\Models\Asesor', 'kompetensiasesor')->withTimestamps();
	}

	/**
	 * Define many to many relationship with Administrator Model
	 * 
	 * @return Relationship
	 */
	public function Administrator()
	{
		return $this->belongsToMany('Certification\Models\Administrator', 'kompetensiadministrator')->withTimestamps();
	}

	/**
	 * Define many to many relationship with permohonan Model
	 * 
	 * @return Relationship
	 */
	public function permohonan()
	{
		return $this->belongsToMany('Certification\Models\Permohonan', 'kompetensipermohonan')->withTimestamps();
	}	

	/**
	 * Query scope for query condition with given Kompetensi id
	 * 
	 * @param  Query $query query
	 * @param  Integer $id  Kompetensi Id
	 * @return Query
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

}
