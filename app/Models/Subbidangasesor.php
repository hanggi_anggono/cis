<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Subbidangasesor extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'subbidangasesor';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['subbidang_id', 'asesor_id'];

}
