<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Master Sub bidang model class
 */
class Subbidang extends Model {

	/**
	 * Database table name
	 * 
	 * @var string
	 */
	protected $table = 'subbidang';
	
	/**
	 * Database table fillable field
	 * 
	 * @var Array
	 */
	protected $fillable = ['nama_subbidang','bidang_id', 'nama_subbidang_english'];

	/**
	 * Define inverse one to many relationship with Bidang model
	 * 
	 * @return Relationship
	 */
	public function bidang()
	{
		return $this->belongsTo('Certification\Models\Bidang');
	}

	/**
	 * Define one to many relationship with Bidang kompetensi
	 * 
	 * @return Relationship
	 */
	public function kompetensi()
	{
		return $this->hasMany('Certification\Models\Kompetensi');
	}

	/**
	 * [asesor description]
	 * 
	 * @return [type] [description]
	 */
	public function asesor()
	{
		return $this->belongsToMany('Certification\Models\Asesor', 'subbidangasesor')->withTimestamps();
	}

	/**
	 * Query scope get collection with given id
	 * 
	 * @param  Query $query query
	 * @param  Integer $id  Bidang id
	 * @return Query
	 */
	public function scopeByBidang($query, $bidang_id)
	{
		return $query->where('bidang_id','=',$bidang_id);
	}

}
