<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Register extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'registers';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['nip','pendidikan_id','jenjangjabatan_id','grade_id','unitinduk_id','unitcabang_id','unitranting_id','unitnonpln_id','nama','no_identitas','jenis_kelamin','kewarganegaraan','tempat_lahir','tanggal_lahir','golongan_darah','alamat','email','no_hp','no_telp','no_telp_perusahaan','jabatan','photo','status','created_by','edited_by','tipe_peserta','jurusan', 'kompetensi_id', 'is_valid', 'validated_by', 'kondisi_khusus', 'keterangan_kondisi_khusus', ];

	/**
	 * [pendidikan description]
	 * 
	 * @return [type] [description]
	 */
	public function pendidikan()
	{
		return $this->belongsTo('Certification\Models\Pendidikan');
	}

	/**
	 * [jenjangjabatan description]
	 * 
	 * @return [type] [description]
	 */
	public function jenjangjabatan()
	{
		return $this->belongsTo('Certification\Models\Jenjang');
	}

	/**
	 * [grade description]
	 * 
	 * @return [type] [description]
	 */
	public function grade()
	{
		return $this->belongsTo('Certification\Models\Grade');
	}

	/**
	 * [unitinduk description]
	 * 
	 * @return [type] [description]
	 */
	public function unitinduk()
	{
		return $this->belongsTo('Certification\Models\Unitinduk');
	}

	/**
	 * [unitcabang description]
	 * 
	 * @return [type] [description]
	 */
	public function unitcabang()
	{
		return $this->belongsTo('Certification\Models\Unitcabang');
	}

	/**
	 * [unitranting description]
	 * 
	 * @return [type] [description]
	 */
	public function unitranting()
	{
		return $this->belongsTo('Certification\Models\Unitranting');
	}

	/**
	 * [unitranting description]
	 * 
	 * @return [type] [description]
	 */
	public function unitnonpln()
	{
		return $this->belongsTo('Certification\Models\Unitnonpln');
	}

	/**
	 * [kompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensi()
	{
		return $this->belongsTo('Certification\Models\Kompetensi');
	}
	/**
	 * [scopeNoFilter description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeNoFilter($query)
	{
		return $query;
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('registers.id', '=', $id);
	}

	/**
	 * [scopeWithRelationship description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('pendidikan','jenjangjabatan','grade','unitinduk','unitcabang','unitranting', 'kompetensi', 'unitnonpln');
	}

}
