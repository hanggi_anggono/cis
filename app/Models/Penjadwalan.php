<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Penjadwalan extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'penjadwalan';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['tanggal_mulai', 'tanggal_selesai', 'udiklat_id','unitinduk_id', 'unitcabang_id', 'unitranting_id', 'lembagasertifikasi_id', 'administrator_id', 'no_undanganpeserta', 'status', 'subbidang_id'];

	/**
	 * [unitdiklat description]
	 * 
	 * @return [type] [description]
	 */
	public function udiklat()
	{
		return $this->belongsTo('Certification\Models\Udiklat');
	}

	/**
	 * [unitinduk description]
	 * 
	 * @return [type] [description]
	 */
	public function unitinduk()
	{
		return $this->belongsTo('Certification\Models\Unitinduk');
	}

	/**
	 * [unitcabang description]
	 * 
	 * @return [type] [description]
	 */
	public function unitcabang()
	{
		return $this->belongsTo('Certification\Models\Unitcabang');
	}

	/**
	 * [unitranting description]
	 * 
	 * @return [type] [description]
	 */
	public function unitranting()
	{
		return $this->belongsTo('Certification\Models\Unitranting');
	}

	/**
	 * [lembagasertifikasi description]
	 * 
	 * @return [type] [description]
	 */
	public function lembagasertifikasi()
	{
		return $this->belongsTo('Certification\Models\Lembagasertifikasi');
	}

	/**
	 * [administrator description]
	 * 
	 * @return [type] [description]
	 */
	public function administrator()
	{
		return $this->belongsTo('Certification\Models\Administrator');
	}

	/**
	 * [penjadwalanasesor description]
	 * 
	 * @return [type] [description]
	 */
	public function asesors()
	{
		return $this->belongsToMany('Certification\Models\Asesor', 'penjadwalanasesor')->withPivot('posisi');
	}

	/**
	 * [kompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensi()
	{
		return $this->belongsToMany('Certification\Models\Kompetensi', 'kompetensipenjadwalan')->withPivot('id')->withTimestamps();
	}

	/**
	 * [kompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function peserta()
	{
		return $this->belongsToMany('Certification\Models\Peserta', 'pesertakompetensipenjadwalan')->withPivot('id', 'kompetensipenjadwalan_id', 'evaluasi_id')->withTimestamps();
	}

	/**
	 * [kompetensipenjadwalan description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensipenjadwalan()
	{
		return $this->hasMany('Certification\Models\Kompetensipenjadwalan');
	}

	/**
	 * [biayapelaksanaan description]
	 * 
	 * @return [type] [description]
	 */
	public function biayapelaksanaan()
	{
		return $this->hasMany('Certification\Models\Biayapelaksanaan');
	}

	/**
	 * [subbidang description]
	 * 
	 * @return [type] [description]
	 */
	public function subbidang()
	{
		return $this->belongsTo('Certification\Models\Subbidang');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id', '=', $id);
	}

	/**
	 * [scopeWhereAdministratorId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereAdministratorId($query, $id)
	{
		return $query->where('administrator_id', '=', $id);
	}

	/**
	 * [scopeWhereLembagasertifikasiId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereLembagasertifikasiId($query, $id)
	{
		return $query->where('lembagasertifikasi_id', '=', $id);
	}

	/**
	 * [scopeWithRelationships description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWithRelationships($query)
	{
		return $query->with('unitinduk', 'administrator', 'lembagasertifikasi', 'asesors', 'kompetensi', 'udiklat', 'subbidang');
	}

	/**
	 * [scopeNoFilter description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeNoFilter($query)
	{
		return $query;
	}
}
