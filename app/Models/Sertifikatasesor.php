<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class Sertifikatasesor extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'sertifikatasesor';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['no_registrasi','no_sertifikat','lembaga_id', 'asesor_id', 'tanggal_terbit','masa_berlaku'];

	/**
	 * [lembaga description]
	 * 
	 * @return [type] [description]
	 */
	public function lembaga()
	{
		return $this->belongsTo('Certification\Models\Lembaga');
	}

	/**
	 * [lembaga description]
	 * 
	 * @return [type] [description]
	 */
	public function asesor()
	{
		return $this->belongsTo('Certification\Models\Asesor');
	}	

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	/**
	 * [scopeWithRelationship description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('lembaga','asesor');
	}

	/**
	 * Query scope for attributes
	 * 
	 * @param  Query Builder $query
	 * @param  Integer $asesor_id     Asesor Id
	 * @param  Ineteger $lembaga_id Lembaga Id
	 * @return QueryBuilder
	 */
	public function scopeWhereAttributes($query, $asesor_id, $lembaga_id)
	{
		return $query->where('lembaga_id','=',$lembaga_id)
					 ->where('asesor_id','=',$asesor_id);
	}

	/**
	 * Query scope for attributes
	 * 
	 * @param  Query Builder $query
	 * @param  Integer $asesor_id     Asesor Id
	 * @return QueryBuilder
	 */
	public function scopeByAsesor($query, $asesor_id)
	{
		return $query->where('asesor_id','=',$asesor_id);
	}	

}
