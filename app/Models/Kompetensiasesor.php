<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Database Kompetensi Asesor Model
 */
class Kompetensiasesor extends Model {

	/**
	 * Database Table name
	 * 
	 * @var string
	 */
	protected $table = 'kompetensiasesor';
	
	/**
	 * Database table fillable fields
	 * 
	 * @var Array
	 */
	protected $fillable = ['kompetensi_id','asesor_id'];	

	/**
	 * Query scope for attributes
	 * 
	 * @param  Query Builder $query
	 * @param  Integer $asesor_id     Asesor Id
	 * @param  Ineteger $kompetensi_id Kompetensi Id
	 * @return QueryBuilder
	 */
	public function scopeWhereAttributes($query, $asesor_id, $kompetensi_id)
	{
		return $query->where('kompetensi_id','=',$kompetensi_id)
					 ->where('asesor_id','=',$asesor_id);
	}


}
