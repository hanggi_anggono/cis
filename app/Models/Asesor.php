<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class Asesor extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'asesor';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['nip','pendidikan_id','jenjangjabatan_id','grade_id','unitinduk_id','unitcabang_id','unitranting_id','nama','no_identitas','jenis_kelamin','kewarganegaraan','tempat_lahir','tanggal_lahir','golongan_darah','alamat','email','no_hp','no_telp','no_telp_perusahaan','jabatan','photo','status','created_by','edited_by', 'tipe_asesor', 'unitnonpln_id'];

	/**
	 * [pendidikan description]
	 * 
	 * @return [type] [description]
	 */
	public function pendidikan()
	{
		return $this->belongsTo('Certification\Models\Pendidikan');
	}

	/**
	 * [jenjangjabatan description]
	 * 
	 * @return [type] [description]
	 */
	public function jenjangjabatan()
	{
		return $this->belongsTo('Certification\Models\Jenjang');
	}

	/**
	 * [grade description]
	 * 
	 * @return [type] [description]
	 */
	public function grade()
	{
		return $this->belongsTo('Certification\Models\Grade');
	}

	/**
	 * [unitinduk description]
	 * 
	 * @return [type] [description]
	 */
	public function unitinduk()
	{
		return $this->belongsTo('Certification\Models\Unitinduk');
	}

	/**
	 * [unitcabang description]
	 * 
	 * @return [type] [description]
	 */
	public function unitcabang()
	{
		return $this->belongsTo('Certification\Models\Unitcabang');
	}

	/**
	 * [unitranting description]
	 * 
	 * @return [type] [description]
	 */
	public function unitranting()
	{
		return $this->belongsTo('Certification\Models\Unitranting');
	}

	/**
	 * [unitranting description]
	 * 
	 * @return [type] [description]
	 */
	public function unitnonpln()
	{
		return $this->belongsTo('Certification\Models\Unitnonpln');
	}

	/**
	 * Define many to many relationship with Kompetensi Model
	 * 
	 * @return Relationship
	 */
	public function kompetensi()
	{
		return $this->belongsToMany('Certification\Models\Kompetensi', 'kompetensiasesor')->withTimestamps();
	}

	/**
	 * [subbidang description]
	 * 
	 * @return [type] [description]
	 */
	public function subbidang()
	{
		return $this->belongsToMany('Certification\Models\Subbidang', 'subbidangasesor')->withTimestamps();
	}

	/**
	 * Define many to many relationship with Lembaga sertifikasi
	 * 
	 * @return Relationship
	 */
	public function kelompok()
	{
		return $this->belongsToMany('Certification\Models\Lembagasertifikasi', 'kelompokasesor')->withPivot('tanggalpenetapan')->withTimestamps();
	}

	/**
	 * [pengajaran description]
	 * 
	 * @return [type] [description]
	 */
	public function pengajaran()
	{
		return $this->hasMany('Certification\Models\Penjadwalanasesor');
	}

	/**
	 * [scopeGroupPln description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeGroupPln($query)
	{
		return $query->where('tipe_asesor', '=', 'pln');
	}

	/**
	 * [scopeGroupNonPln description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeGroupNonPln($query)
	{
		return $query->where('tipe_asesor', '=', 'nonpln');
	}		

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	/**
	 * [scopeWhereNip description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $nip   [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereNip($query, $nip)
	{
		return $query->where('nip','=',$nip);
	}

	/**
	 * [scopeWithRelationship description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('pendidikan','jenjangjabatan','grade','unitinduk','unitcabang','unitranting','kompetensi','kelompok','unitnonpln', 'pengajaran', 'subbidang');
	}

	/**
	 * [scopeNoFilter description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeNoFilter($query)
	{
		return $query;
	}

}
