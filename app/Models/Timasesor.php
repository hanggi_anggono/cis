<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class model tim asesor
 */
class Timasesor extends Model {

	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = 'timasesor';
	
	/**
	 * Fillable fields
	 * 
	 * @var array
	 */
	protected $fillable = ['jabatantim'];

	/**
	 * Query scope for where id condition
	 * 
	 * @param  Query $query query
	 * @param  Integer $id    integer
	 * @return Query
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

}
