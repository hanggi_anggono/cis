<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'peserta';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['nip','pendidikan_id','jenjangjabatan_id','grade_id','unitinduk_id','unitcabang_id','unitranting_id','unitnonpln_id','nama','no_identitas','jenis_kelamin','kewarganegaraan','tempat_lahir','tanggal_lahir','golongan_darah','alamat','email','no_hp','no_telp','no_telp_perusahaan','jabatan','photo','status','created_by','edited_by','tipe_peserta','atasan_id','jurusan'];

	/**
	 * [pendidikan description]
	 * 
	 * @return [type] [description]
	 */
	public function pendidikan()
	{
		return $this->belongsTo('Certification\Models\Pendidikan');
	}

	/**
	 * [jenjangjabatan description]
	 * 
	 * @return [type] [description]
	 */
	public function jenjangjabatan()
	{
		return $this->belongsTo('Certification\Models\Jenjang');
	}

	/**
	 * [grade description]
	 * 
	 * @return [type] [description]
	 */
	public function grade()
	{
		return $this->belongsTo('Certification\Models\Grade');
	}

	/**
	 * [unitinduk description]
	 * 
	 * @return [type] [description]
	 */
	public function unitinduk()
	{
		return $this->belongsTo('Certification\Models\Unitinduk');
	}

	/**
	 * [unitcabang description]
	 * 
	 * @return [type] [description]
	 */
	public function unitcabang()
	{
		return $this->belongsTo('Certification\Models\Unitcabang');
	}

	/**
	 * [unitranting description]
	 * 
	 * @return [type] [description]
	 */
	public function unitranting()
	{
		return $this->belongsTo('Certification\Models\Unitranting');
	}

	/**
	 * [unitranting description]
	 * 
	 * @return [type] [description]
	 */
	public function unitnonpln()
	{
		return $this->belongsTo('Certification\Models\Unitnonpln');
	}

	/**
	 * [kompetensipermohonan description]
	 * 
	 * @return [type] [description]
	 */
	public function permohonan()
	{
		return $this->belongsToMany('Certification\Models\Permohonan', 'kompetensipermohonanpeserta')->withTimestamps();
	}

	/**
	 * [permohonansdm description]
	 * 
	 * @return [type] [description]
	 */
	public function permohonansdm()
	{
		return $this->belongsToMany('Certification\Models\Permohonansdm', 'kompetensipermohonanspeserta')->withTimestamps();
	}

	/**
	 * [kompetensipermohonanpeserta description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensipermohonanpeserta()
	{
		return $this->hasMany('Certification\Models\Kompetensipermohonanpeserta');
	}

	/**
	 * [evaluasi description]
	 * 
	 * @return [type] [description]
	 */
	public function evaluasi()
	{
		return $this->hasMany('Certification\Models\Evaluasi');
	}	

	/**
	 * [penjadwalan description]
	 * 
	 * @return [type] [description]
	 */
	public function penjadwalan()
	{
		return $this->belongsToMany('Certification\Models\Penjadwalan', 'pesertakompetensipenjadwalan')->withPivot('id', 'kompetensipenjadwalan_id')->withTimestamps();
	}

	/**
	 * [realisasipeserta description]
	 * 
	 * @return [type] [description]
	 */
	public function realisasipeserta()
	{
		return $this->hasMany('Certification\Models\Realisasipeserta');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	/**
	 * [scopeWhereNip description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $nip   [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereNip($query, $nip)
	{
		return $query->where('nip','=',$nip);
	}

	/**
	 * [scopeWithRelationship description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('pendidikan','jenjangjabatan','grade','unitinduk','unitcabang','unitranting', 'evaluasi','permohonan','kompetensipermohonanpeserta', 'unitnonpln');
	}

	/**
	 * [scopeOrderedByName description]
	 * 
	 * @param  [type] $query   [description]
	 * @param  string $ascdesc [description]
	 * @return [type]          [description]
	 */
	public function scopeOrderedByName($query, $ascdesc = 'asc')
	{
		return $query->orderBy('nama', $ascdesc);
	}

	/**
	 * [scopeOrderedByCreated description]
	 * 
	 * @param  [type] $query   [description]
	 * @param  string $ascdesc [description]
	 * @return [type]          [description]
	 */
	public function scopeOrderedByCreated($query, $ascdesc = 'desc')
	{
		return $query->orderBy('created_at', $ascdesc);
	}

}
