<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Kompetensipermohonan extends Model {

    /**
     * [$table description]
     * 
     * @var string
     */
	protected $table = 'kompetensipermohonan';

    /**
     * [peserta description]
     * 
     * @return [type] [description]
     */
    public function peserta()
    {
    	return $this->belongsToMany('Certification\Models\Peserta', 'kompetensipermohonanpeserta')->withTimestamps();
    }

    /**
     * [kompetensi description]
     * 
     * @return [type] [description]
     */
    public function kompetensi()
    {
    	return $this->belongsTo('Certification\Models\Kompetensi');
    }

    /**
     * [permohonan description]
     * 
     * @return [type] [description]
     */
    public function permohonan()
    {
        return $this->belongsTo('Certification\Models\Permohonan');
    }

    /**
     * [permohonan description]
     * 
     * @return [type] [description]
     */
    public function evaluasi()
    {
        return $this->hasMany('Certification\Models\Evaluasi');
    }

    /**
     * [scopeWithRelationship description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeWithRelationship($query)
    {
    	return $query->with('peserta','kompetensi', 'permohonan', 'evaluasi');
    }

    /**
     * [scopeWherePermohonanId description]
     * 
     * @param  [type] $query [description]
     * @param  [type] $id    [description]
     * @return [type]        [description]
     */
    public function scopeWherePermohonanId($query, $id)
    {
        return $query->where('permohonan_id', '=', $id);
    }

    /**
     * [scopeWhereKompetensiId description]
     * 
     * @param  [type] $query [description]
     * @param  [type] $id    [description]
     * @return [type]        [description]
     */
    public function scopeWhereKompetensiId($query, $id)
    {
        return $query->where('kompetensi_id', '=', $id);
    }

    /**
     * [scopeIsKompetensiPermohonanExist description]
     * 
     * @param  [type] $query        [description]
     * @param  [type] $permohonanid [description]
     * @param  [type] $kompetensiId [description]
     * @return [type]               [description]
     */
    public function scopeIsKompetensiPermohonanExist($query, $permohonanid, $kompetensiId)
    {
        return $query->where('permohonan_id', '=', $permohonanid)->where('kompetensi_id', '=', $kompetensiId);
    }

}
