<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Realisasipeserta extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'realisasipeserta';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['peserta_id', 'kompetensipenjadwalan_id', 'kompetensi_id', 'kehadiran', 'kelulusan', 'no_sertifikat', 'tanggal_cetak', 'tanggal_awal', 'tanggal_akhir', 'status_sertifikat', 'information'];

	/**
	 * [peserta description]
	 * 
	 * @return [type] [description]
	 */
	public function peserta()
	{
		return $this->belongsTo('Certification\Models\Peserta');
	}

	/**
	 * [kompetensipenjadwalan description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensipenjadwalan()
	{
		return $this->belongsTo('Certification\Models\Kompetensipenjadwalan');
	}

	/**
	 * [kompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensi()
	{
		return $this->belongsTo('Certification\Models\Kompetensi');
	}

	/**
	 * [scopeAttended description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeAttended($query)
	{
		return $query->where('kehadiran', '=', 'y');
	}

	/**
	 * [scopeNotAttended description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeNotAttended($query)
	{
		return $query->where('kehadiran', '=', 'n');
	}

	/**
	 * [scopePasses description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopePasses($query)
	{
		return $query->where('kelulusan', '=', 'y');
	}

	/**
	 * [scopeNotPasses description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeNotPasses($query)
	{
		return $query->where('kelulusan', '=', 'n');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id', '=', $id);
	}

	/**
	 * [scopeJoinPenjadwalan description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeJoinPenjadwalan($query)
	{
		return $query->join('kompetensipenjadwalan', 'kompetensipenjadwalan.id', '=', 'realisasipeserta.kompetensipenjadwalan_id')->join('penjadwalan', 'penjadwalan.id', '=', 'kompetensipenjadwalan.penjadwalan_id');
	}

	/**
	 * [scopeWithRelationships description]
	 * 
	 * @param  [type] $query    [description]
	 * @param  [type] $relation [description]
	 * @return [type]           [description]
	 */
	public function scopeWithRelationships($query, $relation)
	{
		return $query->with($relation);
	}

	/**
	 * [scopeNoFilter description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeNoFilter($query)
	{
		return $query;
	}

}
