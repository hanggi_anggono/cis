<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Permohonan extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'permohonan';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['nosurat','unitinduk_id','tanggal_surat','status','keterangan'];

	/**
	 * [unitinduk description]
	 * 
	 * @return [type] [description]
	 */
	public function unitinduk()
	{
		return $this->belongsTo('Certification\Models\Unitinduk');
	}

	/**
	 * [kompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensi()
	{
		return $this->belongsToMany('Certification\Models\Kompetensi', 'kompetensipermohonan')->withTimestamps();
	}

	/**
	 * [peserta description]
	 * 
	 * @return [type] [description]
	 */
	public function peserta()
	{
		return $this->belongsToMany('Certification\Models\Peserta', 'kompetensipermohonanpeserta')->withPivot('kompetensipermohonan_id')->withTimestamps();
	}

	/**
	 * [kompetensipermohonan description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensipermohonan()
	{
		return $this->hasMany('Certification\Models\Kompetensipermohonan');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id', '=', $id);
	}

	/**
	 * [scopeWhereUnitInduk description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereUnitInduk($query, $id)
	{
		return $query->where('unitinduk_id', '=', $id);
	}

	/**
	 * [scopeOrderByCreated description]
	 * 
	 * @param  [type] $query [description]
	 * @param  string $sort  [description]
	 * @return [type]        [description]
	 */
	public function scopeOrderByCreated($query, $sort = 'desc')
	{
		return $query->orderBy('permohonan.created_at',$sort);
	}

	/**
	 * [scopeOrderByTanggalSurat description]
	 * 
	 * @param  [type] $query [description]
	 * @param  string $sort  [description]
	 * @return [type]        [description]
	 */
	public function scopeOrderByTanggalSurat($query, $sort = 'desc')
	{
		return $query->orderBy('tanggal_surat', $sort);
	}

	/**
	 * [scopeWithRelationship description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('unitinduk','kompetensi','peserta','kompetensipermohonan');
	}

}
