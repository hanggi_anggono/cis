<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Penjadwalanasesor extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'penjadwalanasesor';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['penjadwalan_id', 'asesor_id', 'posisi'];

	/**
	 * [penjadwalan description]
	 * 
	 * @return [type] [description]
	 */
	public function penjadwalan()
	{
		return $this->belongsTo('Certification\Models\Penjadwalan');
	}

	/**
	 * [asesor description]
	 * 
	 * @return [type] [description]
	 */
	public function asesor()
	{
		return $this->belongsTo('Certification\Models\Asesor');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id', '=', $id);
	}

	/**
	 * [scopeWhereAsesorId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereAsesorId($query, $id)
	{
		return $query->where('asesor_id', '=', $id);
	}

	/**
	 * [scopeWherePosisi description]
	 * 
	 * @param  [type] $query  [description]
	 * @param  [type] $posisi [description]
	 * @return [type]         [description]
	 */
	public function scopeWherePosisi($query, $posisi)
	{
		return $query->where('posisi', 'LIKE', $posisi);
	}
}
