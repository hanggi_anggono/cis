<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Sertifikat extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'sertifikat';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['peserta_id', 'penjadwalan_id', 'kompetensi_id', 'lembagasertifikasi_id', 'no_sertifikat', 'tanggal_mulai_berlaku', 'tanggal_akhir_berlaku', 'tanggal_cetak', 'status'];

	public function kompetensi()
	{
		return $this->belongsTo('Certification\Models\Kompetensi');
	}

	public function peserta()
	{
		return $this->belongsTo('Certification\Models\Peserta');
	}

	public function lembagasertifikasi()
	{
		return $this->belongsTo('Certification\Models\Lembagasertifikasi');
	}

	public function penjadwalan()
	{
		return $this->belongsTo('Certification\Models\Penjadwalan');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('sertifikat.id', '=', $id);
	}

	/**
	 * [scopeNoFilter description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeNoFilter($query)
	{
		return $query;
	}
}
