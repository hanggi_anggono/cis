<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kompetensipermohonansdmpeserta extends Model {

    /**
     * [$table description]
     * 
     * @var string
     */
	protected $table = 'kompetensipermohonansdmpeserta';

    /**
     * [peserta description]
     * 
     * @return [type] [description]
     */
    public function peserta()
    {
    	return $this->belongsToMany('Certification\Models\Peserta', 'kompetensipermohonansdmpeserta')->withTimestamps();
    }

    /**
     * [kompetensipermohonan description]
     * 
     * @return [type] [description]
     */
    public function kompetensipermohonansdm()
    {
    	return $this->belongsToMany('Certification\Models\Kompetensipermohonan', 'kompetensipermohonansdm')->withTimestamps();
    }

    /**
     * [evaluasi description]
     * 
     * @return [type] [description]
     */
    public function evaluasi()
    {
        return $this->hasMany('Certification\Models\Evaluasi');
    }

    /**
     * [scopeWithRelationship description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeWithRelationship($query)
    {
    	return $query->with('kompetensipermohonansdm','evaluasi');
    }

    /**
     * [scopeJoinWithPeserta description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeJoinWithPeserta($query)
    {
        return $query->join('peserta', function($join)
                        {
                            $join->on('peserta.id', '=', 'kompetensipermohonansdmpeserta.peserta_id');
                        });
    }

     /**
     * [scopeJoinWithPeserta description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeJoinWithKompetensipermohonansdm($query)
    {
        return $query->join('kompetensipermohonansdm', function($join)
                    {
                        $join->on('kompetensipermohonansdm.id', '=', 'kompetensipermohonansdmpeserta.kompetensipermohonansdm_id');
                    });
    }

     /**
     * [scopeJoinWithPeserta description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeJoinWithKompetensi($query)
    {
        return $query->join('kompetensi', function($join)
                    {
                        $join->on('kompetensipermohonansdm.kompetensi_id', '=', 'kompetensi.id');
                    });
    }

    /**
     * [scopeJoinWithPeserta description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeJoinWithEvaluasi($query)
    {
        return $query->leftJoin('evaluasi', function($join)
                    {
                        $join->on('kompetensipermohonansdm.id', '=', 'evaluasi.kompetensipermohonansdm_id')->on('peserta.id', '=', 'evaluasi.peserta_id');
                    });
    }

    /**
     * [scopeJoinWithPeserta description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeJoinWithUnitinduk($query)
    {
        return $query->leftJoin('unitinduk', function($join)
                     {
                        $join->on('unitinduk.id', '=', 'peserta.unitinduk_id');
                     });
    }

    /**
     * [scopeJoinWithPeserta description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeJoinWithPermohonan($query)
    {
        return $query->join('permohonansdm', function($join)
                    {
                        $join->on('permohonansdm.id', '=', 'kompetensipermohonansdm.permohonansdm_id');
                    });
    }

    /**
     * [scopeJoinWithPenjadwalan description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeJoinWithPenjadwalan($query)
    {
        return $query->leftJoin('pesertakompetensipenjadwalan', function($join)
                        {
                            $join->on('evaluasi.peserta_id', '=', 'pesertakompetensipenjadwalan.peserta_id');
                        });
    }

    /**
     * [scopeSelectedField description]
     * 
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeSelectedField($query)
    {
        return $query->select('kompetensipermohonansdmpeserta.kompetensipermohonansdm_id','nip','nama_unitinduk','nama_kompetensi','nosurat','tanggal_evaluasi', 'hasil','evaluasi.id as id_evaluasi', 'peserta.id as id_peserta','peserta.nama','evaluasi.info', 'status_penjadwalan', 'kodedjk', 'kodeskkni', DB::raw('IF("id_evaluasi" IS NOT NULL, "id_evaluasi", 1000000)'));
    }

    /**
     * [scopeIsNotEmpty description]
     * 
     * @param  [type] $query                   [description]
     * @param  [type] $pesertaId               [description]
     * @param  [type] $kompetensipermohonan_id [description]
     * @return [type]                          [description]
     */
    public function scopeIsNotEmpty($query, $pesertaId, $kompetensipermohonansdm_id)
    {
        return $query->where('peserta_id', '=', $pesertaId)->where('kompetensipermohonansdm_id', '=', $kompetensipermohonansdm_id);
    }

}
