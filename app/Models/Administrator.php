<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class Administrator extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'administrator';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['nip','pendidikan_id','jenjangjabatan_id','grade_id','unitinduk_id','unitcabang_id','unitranting_id','nama','no_identitas','jenis_kelamin','kewarganegaraan','tempat_lahir','tanggal_lahir','golongan_darah','email','no_hp','no_telp','jabatan','photo','status','created_by','edited_by'];

	/**
	 * [pendidikan description]
	 * 
	 * @return [type] [description]
	 */
	public function pendidikan()
	{
		return $this->belongsTo('Certification\Models\Pendidikan');
	}

	/**
	 * [jenjangjabatan description]
	 * 
	 * @return [type] [description]
	 */
	public function jenjangjabatan()
	{
		return $this->belongsTo('Certification\Models\Jenjang');
	}

	/**
	 * [grade description]
	 * 
	 * @return [type] [description]
	 */
	public function grade()
	{
		return $this->belongsTo('Certification\Models\Grade');
	}

	/**
	 * [unitinduk description]
	 * 
	 * @return [type] [description]
	 */
	public function unitinduk()
	{
		return $this->belongsTo('Certification\Models\Unitinduk');
	}

	/**
	 * [unitcabang description]
	 * 
	 * @return [type] [description]
	 */
	public function unitcabang()
	{
		return $this->belongsTo('Certification\Models\Unitcabang');
	}

	/**
	 * [unitranting description]
	 * 
	 * @return [type] [description]
	 */
	public function unitranting()
	{
		return $this->belongsTo('Certification\Models\Unitranting');
	}

	/**
	 * Define many to many relationship with Kompetensi Model
	 * 
	 * @return Relationship
	 */
	public function kompetensi()
	{
		return $this->belongsToMany('Certification\Models\Kompetensi', 'kompetensiadministrator')->withTimestamps();
	}	

	/**
	 * Define many to many relationship with Lembagasertifikasi Model
	 * 
	 * @return Relationship
	 */
	public function kelompok()
	{
		return $this->belongsToMany('Certification\Models\Lembagasertifikasi', 'kelompokadministrator')->withPivot('tanggalpenetapan')->withTimestamps();
	}	

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	/**
	 * [scopeWhereNip description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $nip   [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereNip($query, $nip)
	{
		return $query->where('nip','=',$nip);
	}

	/**
	 * [scopeWithRelationship description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('pendidikan','jenjangjabatan','grade','unitinduk','unitcabang','unitranting','kompetensi','kelompok');
	}

}
