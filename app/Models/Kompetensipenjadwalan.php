<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Kompetensipenjadwalan extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table    = 'kompetensipenjadwalan';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['kompetensi_id', 'penjadwalan_id'];

	/**
	 * [peserta description]
	 * 
	 * @return [type] [description]
	 */
	public function peserta()
	{
		return $this->belongsToMany('Certification\Models\Peserta', 'pesertakompetensipenjadwalan')->withTimestamps();
	}

	/**
	 * [kompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function kompetensi()
	{
		return $this->belongsTo('Certification\Models\Kompetensi');
	}

	/**
	 * [scopeIsKompetensiPenjadwalanExist description]
	 * 
	 * @param  [type] $query         [description]
	 * @param  [type] $penjadwalanId [description]
	 * @param  [type] $kompetensiId  [description]
	 * @return [type]                [description]
	 */
	public function scopeIsKompetensiPenjadwalanExist($query, $penjadwalanId, $kompetensiId)
	{
		return $query->where('penjadwalan_id', '=', $penjadwalanId)->where('kompetensi_id', '=', $kompetensiId);
	}
}
