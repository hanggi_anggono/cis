<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Lembagasertifikasi extends Model {

	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = 'lembagasertifikasi';
	
	/**
	 * Fillable field
	 * 
	 * @var [type]
	 */
	protected $fillable = ['nama_lembagasertifikasi','koderegistrasilsp', 'grup', 'jenis_sertifikat'];

	/**
	 * Define many to many relationship with Asesor
	 * 
	 * @return Relationship
	 */
	public function asesor()
	{
		return $this->belongsToMany('Certification\Models\Asesor', 'kelompokasesor')->withPivot('tanggalpenetapan')->withTimestamps();
	}

	/**
	 * Define many to many relationship with Administrator
	 * 
	 * @return Relationship
	 */
	public function administrator()
	{
		return $this->belongsToMany('Certification\Models\Administrator', 'kelompokadministrator')->withPivot('tanggalpenetapan')->withTimestamps();
	}

	/**
	 * Query scope with asesor relationship
	 * 
	 * @param  QueryBuilder $query
	 * @return QueryBuilder
	 */
	public function scopeWithAsesor($query)
	{
		return $query->with('asesor');
	}

	/**
	 * Query scope with asesor relationship
	 * 
	 * @param  QueryBuilder $query
	 * @return QueryBuilder
	 */
	public function scopeWithAdministrator($query)
	{
		return $query->with('administrator');
	}

	/**
	 * Query scope where id condition
	 * 
	 * @param  QueryBuilder $query
	 * @param  Integer $id    this id
	 * @return QueryBuilder
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	/**
	 * Query scope in group condition
	 * 
	 * @param  QueryBuilder $query
	 * @param  string $group grup name
	 * @return QueryBuilder
	 */
	public function scopeWhereGroup($query, $group = 'pln')
	{
		return $query->where('grup', '=', $group);
	}

	/**
	 * Query scope order by created at field condition
	 * 
	 * @param  QueryBuilder $query
	 * @param  string $ascdesc ascending / descending
	 * @return QueryBuilder
	 */
	public function scopeOrderedByCreated($query, $ascdesc = 'desc')
	{
		return $query->orderBy('created_at', $ascdesc);
	}

	/**
	 * Query scope order by nama_lembagasertifikasi field condition
	 * 
	 * @param  QueryBuilder $query
	 * @param  string $ascdesc ascending / descending
	 * @return QueryBuilder
	 */
	public function scopeOrderedByName($query, $ascdesc = 'asc')
	{
		return $query->orderBy('nama_lembagasertifikasi', $ascdesc);
	}

}
