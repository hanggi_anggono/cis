<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class model Unit induk
 */
class Unitinduk extends Model {

	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = 'unitinduk';

	/**
	 * Fillable fields
	 * 
	 * @var Array
	 */
	protected $fillable = ['nama_unitinduk','wilayah_unitinduk','nonpln','edited_by','created_by','udiklat_id'];

	/**
	 * Define inverse one to many relationship with Unit diklat model
	 * 
	 * @return Model
	 */
	public function udiklat()
	{
		return $this->belongsTo('Certification\Models\Udiklat');
	}

	/**
	 * Define one to many relationship with Unit cabang model
	 * 
	 * @return Model
	 */
	public function unitcabang()
	{
		return $this->hasMany('Certification\Models\Unitcabang');
	}

	/**
	 * Define one to many relationship with permohonan model
	 * 
	 * @return Model
	 */
	public function permohonan()
	{
		return $this->hasMany('Certification\Models\Permohonan');
	}

	/**
	 * Query scope for query condition with given unit induk id
	 * 
	 * @param  Query $query query
	 * @param  Integer $id  unit induk Id
	 * @return Query
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

	/**
	 * Query scope include relationship model
	 * 
	 * @param  Query $query query
	 * @return Query
	 */
	public function scopeWithRelationship($query)
	{
		return $query->with('udiklat','unitcabang','permohonan');
	}

}
