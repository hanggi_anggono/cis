<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Pesertakompetensipenjadwalan extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'pesertakompetensipenjadwalan';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['peserta_id', 'penjadwalan_id', 'kompetensipenjadwalan_id', 'evaluasi_id'];

	/**
	 * [peserta description]
	 * 
	 * @return [type] [description]
	 */
	public function peserta()
	{
		return $this->belongsTo('Certification\Models\Peserta');
	}

	/**
	 * [scopeIsPesertaPenjadwalanExist description]
	 * 
	 * @param  [type] $query                   [description]
	 * @param  [type] $kompetensipenjadwalanId [description]
	 * @param  [type] $pesertaId               [description]
	 * @return [type]                          [description]
	 */
	public function scopeIsPesertaPenjadwalanExist($query, $kompetensipenjadwalanId, $pesertaId)
	{
		return $query->where('kompetensipenjadwalan_id', '=', $kompetensipenjadwalanId)->where('peserta_id', '=', $pesertaId);
	}

}
