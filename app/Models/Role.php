<?php namespace Certification\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {

	protected $fillable = ['name', 'display_name', 'description'];

	public function permissions()
	{
		return $this->belongsToMany('Certification\Models\Permission');
	}

}
