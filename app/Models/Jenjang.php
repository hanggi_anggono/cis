<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Jenjang extends Model {

	protected $table = 'jenjangjabatan';
	protected $fillable = ['nama_jenjangjabatan'];

	public function scopeWhereId($query, $id)
	{
		return $query->where('id','=',$id);
	}

}
