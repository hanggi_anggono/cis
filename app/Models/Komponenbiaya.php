<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Komponenbiaya extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'komponenbiaya';

	/**
	 * [$fillable description]
	 * 
	 * @var array
	 */
	protected $fillable = ['nama_komponenbiaya'];

	/**
	 * [biaya description]
	 * 
	 * @return [type] [description]
	 */
	public function biaya()
	{
		return $this->hasMany('Certification\Models\Biayapelaksanaan');
	}

	/**
	 * [scopeWhereId description]
	 * 
	 * @param  [type] $query [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 */
	public function scopeWhereId($query, $id)
	{
		return $query->where('id', '=', $id);
	}

	/**
	 * [scopeNoFilter description]
	 * 
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeNoFilter($query)
	{
		return $query;
	}

}
