<?php namespace Certification\Models;

use Illuminate\Database\Eloquent\Model;

class Sertifikathistory extends Model {

	/**
	 * [$table description]
	 * 
	 * @var string
	 */
	protected $table = 'sertifikathistory';

	/**
	 * [$fillable description]
	 * 
	 * @var [type]
	 */
	protected $fillable = ['sertifikat_id','peserta_id', 'penjadwalan_id', 'kompetensi_id', 'lembagasertifikasi_id', 'no_sertifikat', 'tanggal_mulai_berlaku', 'tanggal_akhir_berlaku', 'tanggal_cetak', 'status', 'information'];
}
