<?php namespace Certification\Providers;

use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerMasterLembagaRepo();
		$this->registerMasterPendidikanRepo();
		$this->registerMasterGradeRepository();
		$this->registerMasterBidangRepository();
		$this->registerMasterKompetensiRepository();
		$this->registerMasterJenjangRepository();
		$this->registerMasterUnitRepository();
		$this->registerMasterTimasesorRepository();
		$this->registerMasterAsesorRepository();
		$this->registerMasterKompetensiasesorRepository();
		$this->registerMasterAdministratorRepository();
		$this->registerMasterLembagasertifikasiRepository();
		$this->registerMasterPesertaRepository();
	}

	private function registerMasterLembagaRepo()
	{
		$this->app->bind(
			'Certification\Repositories\Lembaga\InterfaceLembaga',
			'Certification\Repositories\Lembaga\EloquentDbLembaga'
		);
	}

	private function registerMasterPendidikanRepo()
	{
		$this->app->bind(
			'Certification\Repositories\Pendidikan\InterfacePendidikanRepository',
			'Certification\Repositories\Pendidikan\EloquentDbPendidikan'
		);
	}

	private function registerMasterGradeRepository()
	{
		$this->app->bind(
			'Certification\Repositories\Grade\InterfaceGradeRepository',
			'Certification\Repositories\Grade\EloquentDbGrade'
		);
	}

	private function registerMasterBidangRepository()
	{
		$this->app->bind(
			'Certification\Repositories\Bidang\InterfaceBidangRepository',
			'Certification\Repositories\Bidang\EloquentDbBidang'
		);
		$this->app->bind(
			'Certification\Repositories\Bidang\InterfaceSubbidangRepository',
			'Certification\Repositories\Bidang\EloquentDbSubbidang'
		);
	}

	private function registerMasterKompetensiRepository()
	{
		$this->app->bind(
			'Certification\Repositories\Kompetensi\InterfaceKompetensiRepository',
			'Certification\Repositories\Kompetensi\EloquentDbKompetensi'
		);

		$this->app->bind(
			'Certification\Repositories\Kompetensi\InterfaceElemenkompetensiRepository',
			'Certification\Repositories\Kompetensi\EloquentDbElemenkompetensi'
		);
	}

	private function registerMasterJenjangRepository()
	{
		$this->app->bind(
			'Certification\Repositories\Jenjang\InterfaceJenjangRepository',
			'Certification\Repositories\Jenjang\EloquentDbJenjang'
		);
	}

	private function registerMasterUnitRepository()
	{
		$this->app->bind(
			'Certification\Repositories\Unit\InterfaceUdiklatRepository',
			'Certification\Repositories\Unit\EloquentDbUdiklat'
		);
		$this->app->bind(
			'Certification\Repositories\Unit\InterfaceUindukRepository',
			'Certification\Repositories\Unit\EloquentDbUinduk'
		);
		$this->app->bind(
			'Certification\Repositories\Unit\InterfaceUcabangRepository',
			'Certification\Repositories\Unit\EloquentDbUcabang'
		);
		$this->app->bind(
			'Certification\Repositories\Unit\InterfaceUrantingRepository',
			'Certification\Repositories\Unit\EloquentDbUranting'
		);
	}

	private function registerMasterTimasesorRepository()
	{
		$this->app->bind(
			'Certification\Repositories\Timasesor\InterfaceTimasesorRepository',
			'Certification\Repositories\Timasesor\EloquentDbTimasesor'
		);
	}

	private function registerMasterAsesorRepository()
	{
		$this->app->bind(
			'Certification\Repositories\Asesor\InterfaceAsesorRepository',
			'Certification\Repositories\Asesor\EloquentDbAsesor'
		);
	}

	private function registerMasterAdministratorRepository()
	{
		$this->app->bind(
			'Certification\Repositories\Administrator\InterfaceAdministratorRepository',
			'Certification\Repositories\Administrator\EloquentDbAdministrator'
		);
	}	

	private function registerMasterKompetensiasesorRepository()
	{
		$this->app->bind(
			'Certification\Repositories\KompetensiAsesor\InterfaceKompetensiasesorRepository',
			'Certification\Repositories\KompetensiAsesor\EloquentDbKompetensiasesor'
		);
	}	

	private function registerMasterLembagasertifikasiRepository()
	{
		$this->app->bind(
			'Certification\Repositories\LembagaSertifikasi\InterfaceLembagaSertifikasiRepository',
			'Certification\Repositories\LembagaSertifikasi\EloquentDbLembagaSertifikasi'
		);
	}

	private function registerMasterPesertaRepository()
	{
		$this->app->bind(
			'Certification\Repositories\Peserta\InterfacePesertaRepository',
			'Certification\Repositories\Peserta\EloquentDbPeserta'
		);
	}	
}
