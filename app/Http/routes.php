<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'UserAuthController@index');
Route::get('register', ['as' => 'register.index', 'uses' => 'Registration\RegistrationController@register']);
Route::post('register', ['as' => 'register.post', 'uses' => 'Registration\RegistrationController@storeRegister']);

Route::get('signin', ['as' => 'user.signin.index', 'uses' => 'UserAuthController@index']);
Route::post('signin', ['as' => 'user.signin.post', 'uses' => 'UserAuthController@signingin']);
Route::get('signout', ['as' => 'user.signout', 'uses' => 'UserAuthController@signingout']);

Route::group(['prefix' => 'dashboard','middleware' => 'auth'], function()
{
	Route::get('', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);
	
	// ==================== Password management routes section ======================== //
	Route::group(['prefix' => 'password'], function()
	{
		Route::get('change', ['as' => 'dashboard.password.change', 'uses' => 'UserAuthController@changePassword']);
		Route::put('update', ['as' => 'dashboard.password.update', 'uses' => 'UserAuthController@updatePassword']);
	});


/////////////////////////////////////////////////////////////////////////////////////////////
// =================================== MASTER ROUTES ===================================== //
/////////////////////////////////////////////////////////////////////////////////////////////

	Route::group(['prefix' => 'master'], function()
	{
		//////////////////////////////////////
		// =========ADMINISTRATOR===========//
		//////////////////////////////////////
		Route::resource('administrator', 'MasterAdministratorController', ['except' => 'destroy']);
		Route::group(['prefix' => 'administrator'], function()
		{
			Route::get('{id}/delete', ['as' => 'dashboard.master.administrator.delete', 'uses' => 'MasterAdministratorController@destroy']);	
			/**
			 *  Pivot Kompetensi administrator
			 */
			Route::get('{id}/kompetensi', ['as' => 'dashboard.master.administrator.kompetensi.index','uses' => 'MasterAdministratorController@indexKompetensi']);
			Route::get('{id}/kompetensi/{kompetensi_id}/delete', ['as' => 'dashboard.master.administrator.kompetensi.delete','uses' => 'MasterAdministratorController@deleteKompetensi']);
			Route::get('{id}/kompetensi/create', ['as' => 'dashboard.master.administrator.kompetensi.create','uses' => 'MasterAdministratorController@createKompetensi']);
			Route::post('{id}/kompetensi/store', ['as' => 'dashboard.master.administrator.kompetensi.store','uses' => 'MasterAdministratorController@storeKompetensi']);

			/**
			 *  Pivot Kelompok administrator
			 */
			Route::get('{id}/kelompok', ['as' => 'dashboard.master.administrator.kelompok.index','uses' => 'MasterAdministratorController@indexKelompok']);
			Route::get('{id}/kelompok/{lembagasertifikasi_id}/delete', ['as' => 'dashboard.master.administrator.kelompok.delete','uses' => 'MasterAdministratorController@deleteKelompok']);
			Route::get('{id}/kelompok/create', ['as' => 'dashboard.master.administrator.kelompok.create','uses' => 'MasterAdministratorController@createKelompok']);
			Route::post('{id}/kelompok/store', ['as' => 'dashboard.master.administrator.kelompok.store','uses' => 'MasterAdministratorController@storeKelompok']);
		});

		//////////////////////////////////////
		// =============PESERTA=============//
		//////////////////////////////////////
		Route::resource('peserta', 'MasterPesertaController', ['except' => 'delete']);
		Route::get('peserta/{id}/delete', ['as' => 'dashboard.master.peserta.delete', 'uses' => 'MasterPesertaController@destroy']);

		//////////////////////////////////////
		// ==============ASESOR=============//
		//////////////////////////////////////
		Route::resource('asesor', 'MasterAsesorController', ['except' => 'destroy']);
		Route::group(['prefix' => 'asesor'], function()
		{
			Route::get('{id}/delete', ['as' => 'dashboard.master.asesor.delete', 'uses' => 'MasterAsesorController@destroy']);
			
			Route::group(['prefix' => '{id}/kelompok'], function()
			{
				Route::get('/', ['as' => 'dashboard.master.asesor.kelompok.index','uses' => 'MasterAsesorController@indexkelompok']);
				Route::get('{kelompok_id}/delete', ['as' => 'dashboard.master.asesor.kelompok.delete','uses' => 'MasterAsesorController@deleteKelompok']);
				Route::get('create', ['as' => 'dashboard.master.asesor.kelompok.create','uses' => 'MasterAsesorController@createKelompok']);
				Route::post('store', ['as' => 'dashboard.master.asesor.kelompok.store','uses' => 'MasterAsesorController@storeKelompok']);
			});
			
			Route::get('{id}/testhistory', ['as' => 'dashboard.master.asesor.testhistory.index','uses' => 'MasterAsesorController@indexTest']);

			Route::resource('kompetensiasesor', 'MasterKompetensiasesorController', ['only' => ['create','store']]);
			Route::group(['prefix' => 'kompetensiasesor'], function()
			{	
				Route::get('{asesor_id}', ['as' => 'dashboard.master.kompetensiasesor.asesor.index','uses' => 'MasterKompetensiasesorController@showByAsesor']);
				Route::get('{asesor_id}/create', ['as' => 'dashboard.master.kompetensiasesor.asesor.create','uses' => 'MasterKompetensiasesorController@create']);
				Route::get('detach/{asesor}/{kompetensi}', ['as' => 'dashboard.master.asesor.kompetensiasesor.delete', 'uses' => 'MasterKompetensiasesorController@destroy']);
			});

			// ======================= Route sub bidang asesor ============================ //
			Route::group(['prefix' => '{id}/subbidang'], function()
			{
				Route::get('/', ['as' => 'dashboard.master.asesor.subbidang.index', 'uses' => 'MasterAsesorController@indexSubbidang']);
				Route::get('create', ['as' => 'dashboard.master.asesor.subbidang.create', 'uses' => 'MasterAsesorController@createSubbidang']);
				Route::post('store', ['as' => 'dashboard.master.asesor.subbidang.store', 'uses' => 'MasterAsesorController@storeSubbidang']);
				Route::get('{subbidangId}/delete', ['as' => 'dashboard.master.asesor.subbidang.delete', 'uses' => 'MasterAsesorController@deleteSubbidang']);
			});

			// ========================= Route master Sertifikat Asesor ===================== //
			Route::get('{asesor_id}/sertifikatasesor', ['as' => 'dashboard.master.sertifikatasesor.asesor.index','uses' => 'MasterSertifikatAsesorController@showByAsesor']);
			Route::get('{asesor_id}/sertifikatasesor/create', ['as' => 'dashboard.master.sertifikatasesor.asesor.create','uses' => 'MasterSertifikatAsesorController@create']);
			
			Route::resource('sertifikatasesor', 'MasterSertifikatAsesorController', ['except' => ['destroy','show','index']]);

			Route::group(['prefix' => 'sertifikatasesor'], function()
			{
				Route::get('{id}/delete/{asesor}', ['as' => 'dashboard.master.sertifikatasesor.delete', 'uses' => 'MasterSertifikatAsesorController@destroy']);
				Route::get('{id}/edit', ['as' => 'dashboard.master.sertifikatasesor.edit','uses' => 'MasterSertifikatAsesorController@edit']);
				Route::put('{id}/update/{asesor}', ['as' => 'dashboard.master.sertifikatasesor.update','uses' => 'MasterSertifikatAsesorController@update']);
			});
	
		});

		//////////////////////////////////////
		// ============AKREDITOR============//
		//////////////////////////////////////
		Route::resource('akreditor', 'MasterLembagaController', ['except' => ['destroy','show']]);
		Route::get('akreditor/{id}/delete', ['as' => 'dashboard.master.akreditor.delete', 'uses' => 'MasterLembagaController@destroy']);

		//////////////////////////////////////
		// =======LEMBAGA SERTIFIKASI=======//
		//////////////////////////////////////
		Route::resource('lembagasertifikasi', 'MasterLembagasertifikasiController', ['except' => ['destroy','show']]);
		Route::get('lembagasertifikasi/{id}/delete', ['as' => 'dashboard.master.lembagasertifikasi.delete', 'uses' => 'MasterLembagasertifikasiController@destroy']);

		//////////////////////////////////////
		// ==============BIDANG=============//
		//////////////////////////////////////
		Route::resource('bidang', 'MasterBidangController', ['except' => ['destroy','show']]);
		Route::group(['prefix' => 'bidang'], function()
		{
			Route::get('{id}/delete', ['as' => 'dashboard.master.bidang.delete', 'uses' => 'MasterBidangController@destroy']);

			// =========================== sub bidang section ======================= //
			Route::get('{bidang_id}/subbidang', ['as' => 'dashboard.master.subbidang.bidang.index','uses' => 'MasterSubbidangController@showByBidang']);
			Route::get('{bidang_id}/subbidang/create', ['as' => 'dashboard.master.subbidang.bidang.create','uses' => 'MasterSubbidangController@create']);

			Route::group(['prefix' => 'subbidang'], function()
			{
				Route::get('create', ['as' => 'dashboard.master.subbidang.create','uses' => 'MasterSubbidangController@create']);
				Route::post('store', ['as' => 'dashboard.master.subbidang.store','uses' => 'MasterSubbidangController@store']);

				Route::get('{id}/edit', ['as' => 'dashboard.master.subbidang.edit','uses' => 'MasterSubbidangController@edit']);
				Route::put('{id}/update', ['as' => 'dashboard.master.subbidang.update','uses' => 'MasterSubbidangController@update']);
				Route::get('{id}/delete', ['as' => 'dashboard.master.subbidang.delete', 'uses' => 'MasterSubbidangController@destroy']);
			});
			
		});

		//////////////////////////////////////
		// =========UNIT KOMPETENSI=========//
		//////////////////////////////////////
		Route::resource('kompetensi', 'MasterKompetensiController', ['except' => ['destroy','show']]);

		Route::group(['prefix' => 'kompetensi'], function()
		{
			Route::get('{id}/delete', ['as' => 'dashboard.master.kompetensi.delete', 'uses' => 'MasterKompetensiController@destroy']);

			// ============================== Route master elemen kompetensi ================== //
			Route::resource('elemenkompetensi', 'MasterElemenkompetensiController', ['except' => ['destroy','show','index']]);
			
			Route::group(['prefix' => '{kompetensi_id}/elemenkompetensi'], function()
			{
				Route::get('/', ['as' => 'dashboard.master.elemenkompetensi.kompetensi.index','uses' => 'MasterElemenkompetensiController@showByKompetensi']);
				Route::get('create', ['as' => 'dashboard.master.elemenkompetensi.kompetensi.create','uses' => 'MasterElemenkompetensiController@create']);
			});
			
			Route::get('elemenkompetensi/{id}/delete', ['as' => 'dashboard.master.elemenkompetensi.delete', 'uses' => 'MasterElemenkompetensiController@destroy']);
		});

		//////////////////////////////////////
		// ===============UNIT==============//
		//////////////////////////////////////
		Route::group(['prefix'=>'unit'], function()
		{
			Route::get('/', ['as'=>'dashboard.master.unit.index', 'uses' => 'MasterUnitsController@index']);
			
			Route::resource('diklat', 'MasterUdiklatController', ['except' => 'show','destroy']);
			Route::get('diklat/{id}/delete', ['as' => 'dashboard.master.unit.diklat.delete', 'uses' => 'MasterUdiklatController@destroy']);
			Route::get('diklat/api/induk/{id}', ['dashboard.master.unit.diklat.api.induk', 'uses' => 'MasterUdiklatController@getUnitInduk']);
			
			Route::resource('induk', 'MasterUindukController', ['except' => 'show','destroy']);
			Route::get('induk/{id}/delete', ['as' => 'dashboard.master.unit.induk.delete', 'uses' => 'MasterUindukController@destroy']);
			Route::get('induk/api/cabang/{id}', ['dashboard.master.unit.induk.api.cabang', 'uses' => 'MasterUindukController@getUnitCabang']);

			Route::resource('cabang', 'MasterUcabangController', ['except' => 'show','destroy']);
			Route::get('cabang/{id}/delete', ['as' => 'dashboard.master.unit.cabang.delete', 'uses' => 'MasterUcabangController@destroy']);
			Route::get('cabang/api/ranting/{id}', ['dashboard.master.unit.cabang.api.ranting', 'uses' => 'MasterUcabangController@getUnitRanting']);

			Route::resource('ranting', 'MasterUrantingController', ['except' => 'show','destroy']);
			Route::get('ranting/{id}/delete', ['as' => 'dashboard.master.unit.ranting.delete', 'uses' => 'MasterUrantingController@destroy']);

			Route::resource('nonpln', 'MasterUnitnonplnController', ['except' => 'show','destroy']);
			Route::get('nonpln/{id}/delete', ['as' => 'dashboard.master.unit.nonpln.delete', 'uses' => 'MasterUnitnonplnController@destroy']);
		});

		//////////////////////////////////////
		// ==========JENJANG JABATAN========//
		//////////////////////////////////////
		Route::resource('jenjang', 'MasterJenjangController', ['except' => 'show','destroy']);
		Route::get('jenjang/{id}/delete', ['as' => 'dashboard.master.jenjang.delete', 'uses' => 'MasterJenjangController@destroy']);

		//////////////////////////////////////
		// ============PENDIDIKAN===========//
		//////////////////////////////////////
		Route::resource('pendidikan', 'MasterPendidikanController', ['except' => ['destroy','show']]);
		Route::get('pendidikan/{id}/delete', ['as' => 'dashboard.master.pendidikan.delete', 'uses' => 'MasterPendidikanController@destroy']);

		//////////////////////////////////////
		// ===========JENJANG GRADE=========//
		//////////////////////////////////////
		Route::resource('grade', 'MasterGradeController', ['except' => ['destroy','show']]);
		Route::get('grade/{id}/delete', ['as' => 'dashboard.master.grade.delete', 'uses' => 'MasterGradeController@destroy']);

		//////////////////////////////////////
		// ===============ROLE==============//
		//////////////////////////////////////
		Route::resource('userrole', 'UserRoleController', ['except' => ['destroy', 'show']]);
		Route::group(['prefix' => 'userrole'], function()
		{
			Route::get('{id}/delete', ['as' => 'dashboard.master.userrole.delete', 'uses' => 'UserRoleController@destroy']);
			Route::get('role/{role_id}/permission', ['as' => 'dashboard.master.userrole.role.permission', 'uses' => 'UserRoleController@setPermission']);
			Route::post('role/{role_id}/permission/update', ['as' => 'dashboard.master.userrole.role.permission.update', 'uses' => 'UserRoleController@updatePermission']);
		});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		// ================ Master Komponen biaya route section ========================== //
		Route::resource('komponenbiaya', 'MasterKomponenBiayaController', ['except' => ['destroy','show']]);
		Route::get('komponenbiaya/{id}/delete', ['as' => 'dashboard.master.komponenbiaya.delete', 'uses' => 'MasterKomponenBiayaController@destroy']);

		// ============================= Route master tim asesor ===================== //
		Route::resource('tim', 'MasterTimasesorController', ['except' => 'show','destroy']);
		Route::get('tim/{id}/delete', ['as' => 'dashboard.master.tim.delete', 'uses' => 'MasterTimasesorController@destroy']);

	});

/////////////////////////////////////////////////////////////////////////////////////////////
// ================================= REGISTRASI ROUTES =================================== //
/////////////////////////////////////////////////////////////////////////////////////////////
Route::get('registration', ['as' => 'dashboard.registration.index', 'uses' => 'Registration\RegistrationController@registrationDashboardIndex']);
Route::get('registration/{id}', ['as' => 'dashboard.registration.show', 'uses' => 'Registration\RegistrationController@registrationDashboardShow']);

/////////////////////////////////////////////////////////////////////////////////////////////
// ================================= PERMOHONAN ROUTES =================================== //
/////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////
// ========PERMOHONAN SURAT=========//
//////////////////////////////////////
Route::resource('permohonan', 'PermohonanController', ['except' => 'delete']);
Route::group(['prefix' => 'permohonan'], function()
{
	Route::get('{id}/delete', ['as' => 'dashboard.permohonan.delete', 'uses' => 'PermohonanController@destroy']);

	// ========================= kompetensi permohonan route ========================== //
	Route::group(['prefix' => '{id}/kompetensi'], function()
	{
		Route::get('/', ['as' => 'dashboard.permohonan.kompetensi.index', 'uses' => 'PermohonanController@indexKompetensi']);
		Route::get('create', ['as' => 'dashboard.permohonan.kompetensi.create', 'uses' => 'PermohonanController@createKompetensi']);
		Route::post('store', ['as' => 'dashboard.permohonan.kompetensi.store', 'uses' => 'PermohonanController@storeKompetensi']);
		Route::get('{kompetensiid}/delete', ['as' => 'dashboard.permohonan.kompetensi.delete', 'uses' => 'PermohonanController@deleteKompetensi']);
	});

	// ========================= kompetensi peserta permohonan route =================== //
	Route::group(['prefix' => '{id}/kompetensi/{kompetensiid}/peserta'], function()
	{
		Route::get('/', ['as' => 'dashboard.permohonan.kompetensi.peserta.index', 'uses' => 'PermohonanController@indexKompetensiPeserta']);
		Route::get('create', ['as' => 'dashboard.permohonan.kompetensi.peserta.create', 'uses' => 'PermohonanController@createKompetensiPeserta']);
		Route::post('store', ['as' => 'dashboard.permohonan.kompetensi.peserta.store', 'uses' => 'PermohonanController@storeKompetensiPeserta']);
		Route::get('{pesertaid}/delete', ['as' => 'dashboard.permohonan.kompetensi.peserta.delete', 'uses' => 'PermohonanController@deleteKompetensiPeserta']);
	});
	
});

//////////////////////////////////////
// =========PERMOHONAN SDM==========//
//////////////////////////////////////
Route::resource('permohonansdm', 'PermohonanSdmController', ['except' => 'delete']);
Route::group(['prefix' => 'permohonansdm'], function()
{
	Route::get('{id}/delete', ['as' => 'dashboard.permohonansdm.delete', 'uses' => 'PermohonanSdmController@destroy']);

	Route::group(['prefix' => '{id}/kompetensi'], function()
	{	
		// ========================= kompetensi permohonan SDM route ========================== //
		Route::get('/', ['as' => 'dashboard.permohonansdm.kompetensi.index', 'uses' => 'PermohonanSdmController@indexKompetensi']);
		Route::get('create', ['as' => 'dashboard.permohonansdm.kompetensi.create', 'uses' => 'PermohonanSdmController@createKompetensi']);
		Route::post('store', ['as' => 'dashboard.permohonansdm.kompetensi.store', 'uses' => 'PermohonanSdmController@storeKompetensi']);
		Route::get('{kompetensiid}/delete', ['as' => 'dashboard.permohonansdm.kompetensi.delete', 'uses' => 'PermohonanSdmController@deleteKompetensi']);
	});

		// ========================= kompetensi peserta permohonan SDM route =================== //
	Route::group(['prefix' => '{id}/kompetensi/{kompetensiid}/peserta'], function()
	{
		Route::get('/', ['as' => 'dashboard.permohonansdm.kompetensi.peserta.index', 'uses' => 'PermohonanSdmController@indexKompetensiPeserta']);
		Route::get('create', ['as' => 'dashboard.permohonansdm.kompetensi.peserta.create', 'uses' => 'PermohonanSdmController@createKompetensiPeserta']);
		Route::post('store', ['as' => 'dashboard.permohonansdm.kompetensi.peserta.store', 'uses' => 'PermohonanSdmController@storeKompetensiPeserta']);
		Route::get('{pesertaid}/delete', ['as' => 'dashboard.permohonansdm.kompetensi.peserta.delete', 'uses' => 'PermohonanSdmController@deleteKompetensiPeserta']);
	});
	
});	


/////////////////////////////////////////////////////////////////////////////////////////////
// =================================== EVALUASI ROUTES =================================== //
/////////////////////////////////////////////////////////////////////////////////////////////

Route::resource('evaluasi', 'EvaluasiController', ['only' => 'index']);
Route::group(['prefix' => 'evaluasi'], function()
{
	Route::get('{id}/delete', ['as' => 'dashboard.evaluasi.delete', 'uses' => 'EvaluasiController@destroy']);
	Route::get('detail/{id}/kompetensi/{kompetensipermohonanid}', ['as' => 'dashboard.evaluasi.detail', 'uses' => 'EvaluasiController@detail']);
	Route::get('hasil/{id}/kompetensi/{kompetensipermohonanid}', ['as' => 'dashboard.evaluasi.set.hasil', 'uses' => 'EvaluasiController@setHasil']);
	Route::post('hasil/store', ['as' => 'dashboard.evaluasi.store.hasil', 'uses' => 'EvaluasiController@storeHasil']);
});

/////////////////////////////////////////////////////////////////////////////////////////////
// ================================== PENJADWALAN ROUTES ================================= //
/////////////////////////////////////////////////////////////////////////////////////////////
Route::resource('penjadwalan', 'PenjadwalanController', ['except' => 'destroy']);
Route::group(['prefix' => 'penjadwalan'], function()
{
	Route::get('evaluasi/peserta', ['as' => 'dashboard.penjadwalan.peserta.evaluasipeserta', 'uses' => 'PenjadwalanController@evaluasiPeserta']);

	Route::get('{id}/delete', ['as' => 'dashboard.penjadwalan.delete', 'uses' => 'PenjadwalanController@destroy']);

	// ============ kompetensi penjadwalan route ===================== //
	Route::group(['prefix' => '{id}/kompetensi'], function()
	{
		Route::get('/', ['as' => 'dashboard.penjadwalan.kompetensi.index', 'uses' => 'PenjadwalanController@indexKompetensi']);
		Route::get('create', ['as' => 'dashboard.penjadwalan.kompetensi.create', 'uses' => 'PenjadwalanController@createKompetensi']);
		Route::post('store', ['as' => 'dashboard.penjadwalan.kompetensi.store', 'uses' => 'PenjadwalanController@storeKompetensi']);
		Route::get('{kompetensiId}/delete', ['as' => 'dashboard.penjadwalan.kompetensi.delete', 'uses' => 'PenjadwalanController@destroyKompetensi']);

		// =================== peserta kompetensi penjadwalan route ============= //
		Route::group(['prefix' => '{kompetensiId}/peserta'], function()
		{
			Route::get('{kompetensipenjadwalanId}', ['as' => 'dashboard.penjadwalan.peserta.index', 'uses' => 'PenjadwalanController@indexPeserta']);
			Route::get('{kompetensipenjadwalanId}/create', ['as' => 'dashboard.penjadwalan.peserta.create', 'uses' => 'PenjadwalanController@createPeserta']);
			Route::post('store', ['as' => 'dashboard.penjadwalan.peserta.store', 'uses' => 'PenjadwalanController@storePeserta']);
			Route::get('{pesertaId}/{evaluasiId}/{kompetensipenjadwalanId}/delete', ['as' => 'dashboard.penjadwalan.peserta.delete', 'uses' => 'PenjadwalanController@destroyPeserta']);
		});
	});
	
});

/////////////////////////////////////////////////////////////////////////////////////////////
// ================================= PELAKSANAAN ROUTES ================================== //
/////////////////////////////////////////////////////////////////////////////////////////////

// Route::resource('pelaksanaan', 'PelaksanaanController');
Route::group(['prefix' => 'pelaksanaan'], function()
{
	Route::get('/', ['as' => 'dashboard.pelaksanaan.index', 'uses' => 'PelaksanaanController@index']);
	Route::get('{id}/detail', ['as' => 'dashboard.pelaksanaan.show', 'uses' => 'PelaksanaanController@show']);
	Route::get('{penjadwalanId}/{kompetensiId}/{kompetensipenjadwalanId}/realisasi', ['as' => 'dashboard.pelaksanaan.realisasi', 'uses' => 'PelaksanaanController@realisasi']);
	Route::post('realisasi/store', ['as' => 'dashboard.pelaksanaan.realisasi.store', 'uses' => 'PelaksanaanController@storeRealisasi']);

	// ====================== Biaya pelaksanaan routes ========================== //
	Route::group(['prefix' => '{id}'], function()
	{
		Route::resource('biaya', 'BiayaPelaksanaanController', ['except' => ['destroy', 'show']]);
		Route::get('biaya/{biaya}/delete', ['as' => 'dashboard.pelaksanaan.{id}.biaya.destroy', 'uses' => 'BiayaPelaksanaanController@destroy']);
	});
	
});

/////////////////////////////////////////////////////////////////////////////////////////////
// ================================= SERTIFIKAT ROUTES =================================== //
/////////////////////////////////////////////////////////////////////////////////////////////

Route::group(['prefix' => 'sertifikat'], function()
{
//////////////////////////////////////
// =========SERTIFIKAT BARU=========//
//////////////////////////////////////
	Route::get('baru', ['as' => 'dashboard.sertifikat.baru.index', 'uses' => 'SertifikatController@indexBaru']);
	Route::post('baru/store', ['as' => 'dashboard.sertifikat.baru.store', 'uses' => 'SertifikatController@store']);
	Route::get('baru/multiple/{penjadwalanId}', ['as' => 'dashboard.sertifikat.baru.multiple', 'uses' => 'SertifikatController@generateMultiple']);
	Route::get('baru/bypenjadwalan/{penjadwalanId}', ['as' => 'dashboard.sertifikat.baru.bypenjadwalan.index', 'uses' => 'SertifikatController@detailByPenjadwalan']);

//////////////////////////////////////
// =====SERTIFIKAT PERPANJANGAN=====//
//////////////////////////////////////
	Route::get('perpanjangan', ['as' => 'dashboard.sertifikat.perpanjangan.index', 'uses' => 'SertifikatController@indexPerpanjangan']);
	Route::get('perpanjangan/proses/{sertifikat_id}', ['as' => 'dashboard.sertifikat.perpanjangan.proses', 'uses' => 'SertifikatController@prosesPerpanjangan']);

	Route::put('update/{sertifikat_id}', ['as' => 'dashboard.sertifikat.update', 'uses' => 'SertifikatController@updateSertifikat']);

	Route::get('download/single/{sertifikasi_id}', ['as' => 'dashboard.sertifikat.download.single', 'uses' => 'SertifikatController@downloadSingle']);
	Route::get('download/all/{penjadwalanId}', ['as' => 'dashboard.sertifikat.download.all', 'uses' => 'SertifikatController@downloadAll']);
});

/////////////////////////////////////////////////////////////////////////////////////////////
// ======================================= LAPORAN  ====================================== //
/////////////////////////////////////////////////////////////////////////////////////////////
Route::group(['prefix' => 'report'], function()
{

//////////////////////////////////////
// ===========PELAKSANAAN===========//
//////////////////////////////////////
	Route::get('pelaksanaan', ['as' => 'report.pelaksanaan.index', 'uses' => 'Reports\ReportPelaksanaanController@index']);

//////////////////////////////////////
// ============EVALUASI=============//
//////////////////////////////////////
	Route::get('evaluasi', ['as' => 'report.evaluasi.index', 'uses' => 'Reports\ReportEvaluasiController@index']);

//////////////////////////////////////
// =============ASESOR==============//
//////////////////////////////////////	
	Route::get('asesor', ['as' => 'report.asesor.index', 'uses' => 'Reports\ReportAsesorController@index']);
	Route::get('asesor/{id}/detail', ['as' => 'report.asesor.detail', 'uses' => 'Reports\ReportAsesorController@detail']);

//////////////////////////////////////
// ============KOMPETENSI===========//
//////////////////////////////////////
	Route::get('sebarankompetensi', ['as' => 'report.sebarankompetensi.index', 'uses' => 'Reports\ReportSebarankompetensiController@index']);

//////////////////////////////////////
// ===========KELULUSAN===========//
//////////////////////////////////////
	Route::get('kelulusan', ['as' => 'report.kelulusan.index', 'uses' => 'Reports\ReportKelulusanController@index']);

//////////////////////////////////////
// ==============BIAYA==============//
//////////////////////////////////////
	Route::get('biaya', ['as' => 'report.biaya.index', 'uses' => 'Reports\ReportBiayaController@index']);

});

/////////////////////////////////////////////////////////////////////////////////////////////
// ===================================== API ROUTES ====================================== //
/////////////////////////////////////////////////////////////////////////////////////////////

Route::group(['prefix' => 'api'], function()
{
	Route::get('peserta/detail/{id}', ['as' => 'api.get.detail.peserta', 'uses' => 'MasterPesertaController@apiDetail']);
	Route::get('subbidang/{id}/bidang', ['as' => 'api.get.subbidang.byBidang', 'uses' => 'MasterSubbidangController@apiGetByBidang']);
	Route::get('kompentensi/{id}/subbidang', ['as' => 'api.get.kompetensi.getBySubbidang', 'uses' => 'MasterKompetensiController@apiGetBySubbidang']);
	Route::get('kompetensi/{id}/belongstobidang', ['as' => 'api.get.kompetensi.belongstobidang', 'uses' => 'MasterKompetensiController@apiGetBelongsToBidang']);
	Route::get('asesor/kelompok/{lskid}/{subbidangId}', ['as' => 'api.get.asesor.bykelompok', 'uses' => 'MasterAsesorController@apiByLsk']);
	Route::get('administrator/kelompok/{lskid}', ['as' => 'api.get.administrator.bykelompok', 'uses' => 'MasterAdministratorController@apiByLsk']);
});

Route::group(['prefix' => 'administrator', 'middleware' => 'auth'], function()
{
	Route::get('dashboard', ['as' => 'administrator.dashboard.index', 'uses' => 'Administrator\DashboardController@index']);
});
Route::group(['prefix' => 'asesor', 'middleware' => 'auth'], function()
{
	Route::get('dashboard', ['as' => 'asesor.dashboard.index', 'uses' => 'Asesor\DashboardController@index']);
});
Route::group(['prefix' => 'peserta', 'middleware' => 'auth'], function()
{
	Route::get('dashboard', ['as' => 'peserta.dashboard.index', 'uses' => 'Peserta\DashboardController@index']);
});
Route::group(['prefix' => 'sdm', 'middleware' => 'auth'], function()
{
	Route::get('dashboard', ['as' => 'asesor.dashboard.index', 'uses' => 'SDM\DashboardController@index']);
});

});