<?php namespace Certification\Http\Controllers;

use Certification\Repositories\Asesor\InterfaceAsesorRepository;
use Certification\Repositories\Bidang\InterfaceSubbidangRepository;
use Certification\Repositories\LembagaSertifikasi\InterfaceLembagaSertifikasiRepository;
use Certification\Http\Requests\StoreMasterAsesorRequest as RequestValidator;
use Certification\Services\DataSupportMasterService as DataService;
use Certification\Services\StoreAsesorService as StoreService;
use Certification\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config, Flash;

class MasterAsesorController extends Controller {

	/**
	 * Asesor Repository
	 * 
	 * @var Interface
	 */
	private $asesorRepository;
	
	/**
	 * Module Title
	 * 
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param InterfaceAsesorRepository $asesorRepository asesor repository
	 */
	public function __construct(InterfaceAsesorRepository $asesorRepository)
	{
		$this->asesorRepository = $asesorRepository;
		$this->title            = 'Master Asesor';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master asesor';
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		$data['asesor']            = $this->asesorRepository->all('paginate', $request);
		return view('backend.masterasesor.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(DataService $dataService)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Daftar master asesor';
		$data['support']  = $dataService->forMasterAsesor();
		return view('backend.masterasesor.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request, StoreService $store)
	{
		$affected = $store->save($request->except('_token'));
		$request->flashExcept('_token');
		if ($affected) {
			Flash::success('Data asesor berhasil disimpan');
			return redirect()->route('dashboard.master.asesor.index');
		}
		Flash::error('Data asesor gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Detail master asesor';
		$data['asesor']   = $this->asesorRepository->getById($id);
		return view('backend.masterasesor.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(DataService $dataService, $id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit master asesor';
		$data['asesor']   = $this->asesorRepository->getById($id);
		$data['support']  = $dataService->forMasterAsesor();
		return view('backend.masterasesor.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, StoreService $store, $id)
	{
		$affected = $store->save($request->except('_token','_method'),'update',$id);
		if ($affected) {
			Flash::success('Data asesor berhasil dirubah');
			return redirect()->route('dashboard.master.asesor.index');
		}
		Flash::error('Data asesor gagal dirubah');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->asesorRepository->delete($id);
		Flash::success('Data berhasil dihapus');
		return redirect()->route('dashboard.master.asesor.index');
	}

	/**
	 * Display all resource kelompok
	 * 
	 * @param  Integer $id asesor Id
	 * @return Response
	 */
	public function indexKelompok($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Kelompok master asesor';
		$data['asesor']   = $this->asesorRepository->getById($id);
		return view('backend.masterasesor.indexKelompok', compact('data'));
	}

	/**
	 * Detach kelompok
	 * 
	 * @param  Integer $id         asesor id
	 * @param  Integer $kelompok_id kelompok / lembaga sertifikasi id
	 * @return Response
	 */
	public function deleteKelompok($id, $kelompok_id)
	{
		$this->asesorRepository->deleteKelompok($id, $kelompok_id);
		Flash::success('Data kelompok berhasil dihapus');
		return redirect()->route('dashboard.master.asesor.index');
	}

	/**
	 * Display form create kelompok
	 * 
	 * @param  InterfaceLembagaSertifikasiRepository $lembagasertifikasi
	 * @param  Integer $id Asesor Id
	 * @return Response
	 */
	public function createKelompok(InterfaceLembagaSertifikasiRepository $lembagasertifikasi, $id)
	{
		$data['title']              = $this->title;
		$data['subtitle']           = 'Tambah Kelompok';
		$data['asesor']             = $this->asesorRepository->getById($id);
		$data['lembagasertifikasi'] = $lembagasertifikasi->lists();
		return view('backend.masterasesor.createKelompok', compact('data'));
	}

	/**
	 * Store data kelompok asesor
	 * 
	 * @param  InterfaceLembagaSertifikasiRepository $lembagasertifikasi
	 * @param  Request $request HTTP Request
	 * @param  Integer $id  Asesor ID
	 * @return Response
	 */
	public function storeKelompok(InterfaceLembagaSertifikasiRepository $lembagasertifikasi, Request $request, $id)
	{
		$this->asesorRepository->storeKelompok($id, $request->except('_token'));
		Flash::success('Data kelompok berhasil disimpan');
		return redirect()->route('dashboard.master.asesor.kelompok.index',['id' => $id]);
	}

	/**
	 * Display test history asesor
	 * 
	 * @param  Integer $id Asesor Id
	 * @return Response
	 */
	public function indexTest($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Riwayat menguji';
		$data['history']  = $this->asesorRepository->testHistory($id);
		return view('backend.masterasesor.indexmenguji', compact('data'));
	}

	/**
	 * [indexSubbidang description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function indexSubbidang($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Sub bidang kompetensi asesor';
		$data['asesor']   = $this->asesorRepository->getById($id);
		return view('backend.masterasesor.indexsubbidang', compact('data'));
	}

	/**
	 * [createSubbidang description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function createSubbidang(InterfaceSubbidangRepository $subbidang, $id)
	{
		$data['title']     = $this->title;
		$data['subtitle']  = 'Tambah Subbidang Asesor';
		$data['asesor']    = $this->asesorRepository->getById($id);
		$data['subbidang'] = $subbidang->lists();
		return view('backend.masterasesor.createsubbidang', compact('data'));
	}

	/**
	 * [storeSubbidang description]
	 * 
	 * @param  Request $request [description]
	 * @param  [type]  $id      [description]
	 * @return [type]           [description]
	 */
	public function storeSubbidang(Request $request, $id)
	{
		$affected = $this->asesorRepository->storeSubbidang($id, $request->except('_token'));
		if ($affected) {
			Flash::success('Data sub bidang berhasil disimpan');
			return redirect()->route('dashboard.master.asesor.subbidang.index',['id' => $id]);
		}
		Flash::warning('Data Sub bidang sudah ada');
		return redirect()->back();
		
	}

	/**
	 * [deleteSubbidang description]
	 * 
	 * @param  [type] $id          [description]
	 * @param  [type] $subbidangId [description]
	 * @return [type]              [description]
	 */
	public function deleteSubbidang($id, $subbidangId)
	{
		$this->asesorRepository->deleteSubbidang($id, $subbidangId);
		Flash::success('Data sub bidang berhasil dihapus');
		return redirect()->route('dashboard.master.asesor.subbidang.index',['id' => $id]);
	}

	/**
	 * [apiByLsk description]
	 * 
	 * @param  [type] $lsk [description]
	 * @return [type]      [description]
	 */
	public function apiByLsk($lsk, $subbidangId = null)
	{
		return $this->asesorRepository->lists($lsk, $subbidangId);
	}

}
