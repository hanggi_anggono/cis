<?php namespace Certification\Http\Controllers\Administrator;

use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DashboardController extends Controller {

	public function index()
	{
		$data['title'] = 'Dashboard';
		$data['subtitle'] = 'Administrator';
		return view('backend.Administrator.dashboard', compact('data'));
	}
}
