<?php namespace Certification\Http\Controllers\SDM;

use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DashboardController extends Controller {

	public function index()
	{
		$data['title'] = 'Dashboard';
		$data['subtitle'] = 'Human Resource';
		return view('backend.SDM.dashboard', compact('data'));
	}
}
