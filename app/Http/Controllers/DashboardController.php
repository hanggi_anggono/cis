<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Dashboard\EloquentDbDashboard;

use Illuminate\Http\Request;

class DashboardController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;

	/**
	 * [$dashboardRepository description]
	 * 
	 * @var [type]
	 */
	private $dashboardRepository;

	/**
	 * [__construct description]
	 * 
	 * @param EloquentDbDashboard $dashboardRepository [description]
	 */
	public function __construct(EloquentDbDashboard $dashboardRepository)
	{
		$this->dashboardRepository = $dashboardRepository;
		$this->title               = 'Home';
	}

	/**
	 * [index description]
	 * 
	 * @return [type] [description]
	 */
	public function index()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Dashboard summary';
		$data['summary']  = $this->dashboardRepository->adminDashboardSummary();
		return view('backend.home',compact('data'));
	}

}
