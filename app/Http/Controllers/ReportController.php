<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests;
use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ReportController extends Controller {

	
	private $title;
	public function __construct()
	{
		$this->title = 'Laporan';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function pelaksanaan()
	{
		$data['title'] = $this->title;
		$data['subtitle'] = 'Laporan Pelaksanaan';
		return view('backend.reports.pelaksanaan', compact('data'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function evaluasi()
	{
		$data['title'] = $this->title;
		$data['subtitle'] = 'Laporan Evaluasi';
		$data['peserta'] = ['Didik Tri','Zaenal Abidin', 'Bekti Cahyo', 'Kentia Dea', 'Tyas Andriani', 'Taylor Otwell', 'Dayle rees', 'Jeffrey Way', 'Phill Sturgeon', 'Steve Wozniack'];
		return view('backend.reports.evaluasi', compact('data'));
	}

}
