<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterLembagasertifikasiRequest as RequestValidator;
use Certification\Repositories\LembagaSertifikasi\InterfaceLembagaSertifikasiRepository;
use Certification\Http\Controllers\Controller;
use Illuminate\Http\Request as Request;
use Config, Flash;

class MasterLembagasertifikasiController extends Controller {

	/**
	 * Lembaga sertifikasi repository
	 * 
	 * @var Interface
	 */
	private $lembagasertifikasiRepository;
	
	/**
	 * Modul title
	 * 
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param InterfaceLembagaSertifikasiRepository $lembagasertifikasiRepository
	 */
	public function __construct(InterfaceLembagaSertifikasiRepository $lembagasertifikasiRepository)
	{
		$this->lembagasertifikasiRepository = $lembagasertifikasiRepository;
		$this->title                        = 'Manajemen Master Lembaga Sertifikasi';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']              = $this->title;
		$data['subtitle']           = 'Daftar master lembaga sertifikasi';
		$data['lembagasertifikasi'] = $this->lembagasertifikasiRepository->all(true);
		$data['pagination_number']  = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		return view('backend.masterlembagasertifikasi.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah master lembaga sertifikasi';
		return view('backend.masterlembagasertifikasi.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->lembagasertifikasiRepository->store($request->except('_token'));
		if($affected) {
			Flash::success('Data lembaga sertifikasi berhasil disimpan');
			return redirect()->route('dashboard.master.lembagasertifikasi.index');
		}
		Flash::error('Data lembaga sertifikasi gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']              = $this->title;
		$data['subtitle']           = 'Edit master lembaga sertifikasi';
		$data['lembagasertifikasi'] = $this->lembagasertifikasiRepository->getById($id);
		return view('backend.masterlembagasertifikasi.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, RequestValidator $request)
	{
		$affected = $this->lembagasertifikasiRepository->update($id, $request->except('_token','_method'));
		if($affected) {
			Flash::success('Data lembaga sertifikasi berhasil dirubah');
			return redirect()->route('dashboard.master.lembagasertifikasi.index');
		}
		Flash::error('Data lembaga sertifikasi gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->lembagasertifikasiRepository->delete($id);
		Flash::success('Data lembaga sertifikasi berhasil dihapus');
		return redirect()->route('dashboard.master.lembagasertifikasi.index');
	}	
}
