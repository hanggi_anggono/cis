<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterPendidikanRequest as RequestValidator;
use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Certification\Repositories\Pendidikan\InterfacePendidikanRepository;
use \Config;

/**
 *  Resourceful controller for master pendidikan
 *  Handle CRUD of data pendidikan
 */
class MasterPendidikanController extends Controller {

	/**
	 * Pendidikan Repository Interface
	 * @var Interface
	 */
	private $pendidikanRepository;

	/**
	 * Global title name
	 * @var [type]
	 */
	private $title;
	
	/**
	 * Request provider
	 * @var [type]
	 */
	private $request;

	public function __construct(InterfacePendidikanRepository $pendidikanRepository, Request $request)
	{
		$this->pendidikanRepository = $pendidikanRepository;
		$this->request              = $request;
		$this->title                = 'Manajemen Pendidikan';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master pendidikan';
		$data['pendidikan']        = $this->pendidikanRepository->all(true);
		$data['pagination_number'] = numbering_pagination($this->request, Config::get('certification.default_pagination_count'));

		return view('backend.masterpendidikan.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Tambah data master pendidikan';

		return view('backend.masterpendidikan.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->pendidikanRepository->store($request->only('nama_pendidikan'));
		if ($affected) {
			return redirect()->route('dashboard.master.pendidikan.index')->with('success-notif', 'Data pendidikan berhasil disimpan');
		}
		return redirect()->route('dashboard.master.pendidikan.index')->with('fail-notif', 'Data pendidikan gagal disimpan');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['pendidikan'] = $this->pendidikanRepository->getById($id);
		$data['title']      = $this->title;
		$data['subtitle']   = 'Edit data master pendidikan';

		return view('backend.masterpendidikan.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->pendidikanRepository->update($id, $request->only('nama_pendidikan'));
		if ($affected) {
			return redirect()->route('dashboard.master.pendidikan.index')->with('success-notif', 'Data pendidikan berhasil dirubah');
		}
		return redirect()->route('dashboard.master.pendidikan.index')->with('fail-notif', 'Data pendidikan gagal dirubah');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->pendidikanRepository->delete($id);
		return redirect()->route('dashboard.master.pendidikan.index')->with('success-notif', 'Data pendidikan berhasil dihapus');
	}

}
