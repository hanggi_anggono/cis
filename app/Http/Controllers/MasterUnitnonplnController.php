<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterUnitnonplnRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Unit\EloquentDbUnonpln;

use Illuminate\Http\Request;
use Config, Flash;

class MasterUnitnonplnController extends Controller {

	/**
	 * Unit Repository
	 * 
	 * @var Object
	 */
	private $unitRepository;
	
	/**
	 * Module Title
	 * 
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param EloquentDbUnonpln $unitRepository Unit Non PLN Repository
	 */
	public function __construct(EloquentDbUnonpln $unitRepository)
	{
		$this->unitRepository = $unitRepository;
		$this->title          = 'Master Unit';
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master unit non PLN';
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		$data['unit']              = $this->unitRepository->all('paginate');

		return view('backend.masterunonpln.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah master unit non PLN';
		return view('backend.masterunonpln.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->unitRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data Unit Non PLN berhasil disimpan');
			return redirect()->route('dashboard.master.unit.nonpln.index');
		}
		Flash::error('Data Unit Non PLN gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit master unit non PLN';
		$data['unit']     = $this->unitRepository->getById($id);
		return view('backend.masterunonpln.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->unitRepository->update($id, $request->except('_token', '_method'));
		if ($affected) {
			Flash::success('Data Unit Non PLN berhasil dirubah');
			return redirect()->route('dashboard.master.unit.nonpln.index');
		}
		Flash::error('Data Unit Non PLN gagal dirubah');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$affected = $this->unitRepository->delete($id);
		Flash::success('Data Unit Non PLN berhasil dihapus');
		return redirect()->route('dashboard.master.unit.nonpln.index');
	}

}
