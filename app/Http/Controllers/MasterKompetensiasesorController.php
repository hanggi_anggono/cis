<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterKompetensiasesorRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\KompetensiAsesor\InterfaceKompetensiasesorRepository;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\Asesor\InterfaceAsesorRepository;

use Illuminate\Http\Request as Request;
use \Config, \Flash;

class MasterKompetensiasesorController extends Controller {

	/**
	 * Kompetensi Asesor & Asesor Repository
	 * @var interface
	 * 
	 */
	private $kompetensiasesorRepository;
	private $asesorRepository;
	
	/**
	 * Module title
	 * 
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param InterfaceKompetensiasesorRepository $kompetensiasesorRepository kompetensi asesor Repository
	 */
	public function __construct(InterfaceKompetensiasesorRepository $kompetensiasesorRepository, InterfaceAsesorRepository $asesorRepository)
	{
		$this->kompetensiasesorRepository = $kompetensiasesorRepository;
		$this->asesorRepository 		  = $asesorRepository;
		$this->title                = 'Manajemen Kompetensi Asesor';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request Http request
	 * @param Integer $asesor_id Asesor id
	 * @return Response
	 */
	public function showByAsesor(Request $request, $asesor_id)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar kompetensi asesor';
		$data['kompetensiasesor']  = $this->asesorRepository->getById($asesor_id);
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		// dd($data['kompetensiasesor']);
		$data['asesor_id']     = $asesor_id;

		return view('backend.masterkompetensiasesor.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param Interface $kompetensiasesorRepository Kompetensi Asesor repository
	 * @param Integer $kompetensi_id Kompetensi Id
	 * @param Integer $asesor_id Asesor Id
	 * @return Response
	 */
	public function create(InterfaceAsesorRepository $asesorRepository, InterfaceKompetensiRepository $kompetensiRepository, $asesor_id = null)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah data master kompetensi asesor';
		$data['kompetensi'] = $kompetensiRepository->lists();


		if (isset($asesor_id)) {
			$data['asesor_single'] = $asesorRepository->getById($asesor_id);
		} else {
			$data['asesor'] = $asesorRepository->lists();
		}		

		return view('backend.masterkompetensiasesor.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param RequestValidator $request Request Validator
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->kompetensiasesorRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data master kompetensi asesor berhasil disimpan');
			return redirect()->route('dashboard.master.kompetensiasesor.asesor.index',['asesor_id'=>$request->get('asesor_id')]);
		}
		Flash::error('Data master kompetensi asesor gagal disimpan');
		return redirect()->route('dashboard.master.asesor.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Interface $kompetensiasesorRepository Kompetensi asesor repository
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(InterfaceKompetensiRepository $kompetensiRepository, $asesor, $kompetensi)
	{
		$data['title']            = $this->title;
		$data['subtitle']         = 'Tambah data master kompetensi asesor';
		$data['kompetensi']       = $kompetensiRepository->lists();
		$data['asesor'] 		  = $this->asesorRepository->lists();
		$data['kompetensiasesor']  = $this->asesorRepository->getById($asesor);
		$data['kompetensipicked'] = $kompetensi;
		$data['asesorpicked'] 	  = $asesor;
		return view('backend.masterkompetensiasesor.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  RequestValidator $request Request Validator
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		// dd($request->all());
		$affected = $this->kompetensiasesorRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data master kompetensi asesor berhasil dirubah');
			return redirect()->route('dashboard.master.asesor.index');
		}

		Flash::error('Data master kompetensi asesor gagal dirubah');
		return redirect()->route('dashboard.master.kompetensiasesor.edit',['id' => $id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($asesor, $kompetensi)
	{
		$this->kompetensiasesorRepository->delete($asesor, $kompetensi);
		Flash::success('Data sub bidang berhasil dihapus');
		return redirect()->route('dashboard.master.kompetensiasesor.asesor.index', ['asesor_id' => $asesor]);
	}

}
