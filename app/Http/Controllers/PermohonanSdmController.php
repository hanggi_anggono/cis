<?php namespace Certification\Http\Controllers;

use Certification\Repositories\PermohonanSdm\EloquentDbPermohonansdm;
use Certification\Repositories\PermohonanSdm\EloquentDbKompetensipermohonansdm;
use Certification\Repositories\Peserta\InterfacePesertaRepository;
use Certification\Repositories\Unit\InterfaceUindukRepository;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\Bidang\InterfaceBidangRepository;
use Certification\Http\Requests\PermohonanSdm\StoreRequest as RequestValidator;
use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;
use \Config, \Flash, \Session;

class PermohonanSdmController extends Controller {
	/**
	 * [$permohonanRepository description]
	 * 
	 * @var [type]
	 */
	private $permohonanSdmRepository;
	
	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;

	public function __construct(EloquentDbPermohonansdm $permohonanSdmRepository)
	{
		$this->permohonanSdmRepository 	= $permohonanSdmRepository;
		$this->title                	= 'Manajemen Permohonan SDM';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(EloquentDbKompetensipermohonansdm $pivot, Request $request)
	{		
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar Permohonan SDM';
		$data['permohonansdm']     = $this->permohonanSdmRepository->all('paginate', Session::get('params'));
		$data['peserta']		   = $pivot->all();
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		return view('backend.permohonansdm.index', compact('data'));
		// return view('backend.permohonansdm.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(InterfaceUindukRepository $unitinduk)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah Permohonan SDM';
		$data['unitinduk'] = $unitinduk->lists();
		return view('backend.permohonansdm.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->permohonanSdmRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data Permohonan SDM berhasil disimpan');
			return redirect()->route('dashboard.permohonansdm.index');
		}
		Flash::error('Data Permohonan SDM gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(InterfaceUindukRepository $unitinduk, $id)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Edit Permohonan SDM';
		$data['unitinduk']  = $unitinduk->lists();
		$data['permohonansdm'] = $this->permohonanSdmRepository->getById($id);
		return view('backend.permohonansdm.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->permohonanSdmRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data Permohonan SDM berhasil dirubah');
			return redirect()->route('dashboard.permohonansdm.index');
		}
		Flash::error('Data Permohonan SDM gagal dirubah');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->permohonanSdmRepository->delete($id);
		Flash::success('Data Permohonan SDM berhasil dihapus');
		return redirect()->route('dashboard.permohonansdm.index');
	}

	/**
	 * [createKompetensi description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function indexKompetensi($id = '' )
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Daftar Kompetensi Permohonan SDM';
		$data['permohonansdm'] = $this->permohonanSdmRepository->getById($id);
		return view('backend.permohonansdm.indexKompetensi', compact('data'));
	}

	/**
	 * [createKompetensi description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function createKompetensi(InterfaceKompetensiRepository $kompetensi, InterfaceBidangRepository $bidang, $id)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Tambah Kompetensi Permohonan SDM';
		$data['permohonansdm'] = $this->permohonanSdmRepository->getById($id);
		$data['kompetensi'] = $kompetensi->lists();
		$data['bidang']     = $bidang->lists();
		return view('backend.permohonansdm.createKompetensi', compact('data'));
	}

	/**
	 * [storeKompetensi description]
	 * 
	 * @param  Request $request [description]
	 * @param  [type]  $id      [description]
	 * @return [type]           [description]
	 */
	public function storeKompetensi(Request $request, $id)
	{
		$response = $this->permohonanSdmRepository->storeKompetensi($id, $request->get('kompetensi_id'));
		if ($response['requested'] > 0) {
			if (count($response['failed']) == 0) {
				Flash::success('Data kompetensi berhasil dimasukkan');
			} else {
				$kompetensi = '';
				foreach ($response['failed'] as $value) {
					$kompetensi .= $value.', ';
				}
				Flash::warning('Data kompetensi '.$kompetensi.' sudah ada pada permohonan ini');
			}
			return redirect()->route('dashboard.permohonansdm.kompetensi.index', ['id' => $id]);
		}
		Flash::error('Pilihan kompetensi tidak boleh kosong. Minimal 1 kompetensi dipilih');
		return redirect()->back();
	}

	/**
	 * [deleteKompetensi description]
	 * 
	 * @param  [type] $id           [description]
	 * @param  [type] $kompetensiid [description]
	 * @return [type]               [description]
	 */
	public function deleteKompetensi($id, $kompetensiid)
	{
		$this->permohonanSdmRepository->deleteKompetensi($id, $kompetensiid);
		Flash::success('Data kompetensi berhasil dihapus');
		return redirect()->route('dashboard.permohonansdm.kompetensi.index', ['id' => $id]);
	}

	/**
	 * [indexKompetensiPeserta description]
	 * 
	 * @param  EloquentDbKompetensipermohonan $pivot        [description]
	 * @param  InterfaceKompetensiRepository  $kompetensi   [description]
	 * @param  [type]                         $id           [description]
	 * @param  [type]                         $kompetensiid [description]
	 * @return [type]                                       [description]
	 */
	public function indexKompetensiPeserta(EloquentDbKompetensipermohonansdm $pivot, InterfaceKompetensiRepository $kompetensi, $id, $kompetensiid)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Tambah Peserta Permohonan SDM';
		$data['permohonansdm'] = $this->permohonanSdmRepository->getById($id);
		$data['kompetensi'] = $kompetensi->getById($kompetensiid);
		foreach ($data['permohonansdm']->kompetensipermohonansdm as $key => $value) {
			if($value->kompetensi_id == $kompetensiid) {
				$data['peserta']    = $pivot->find($value->id);
			}
		}
		return view('backend.permohonansdm.indexkompetensipeserta', compact('data'));
	}

	/**
	 * [createKompetensiPeserta description]
	 * 
	 * @param  InterfacePesertaRepository    $peserta      [description]
	 * @param  InterfaceKompetensiRepository $kompetensi   [description]
	 * @param  [type]                        $id           [description]
	 * @param  [type]                        $kompetensiid [description]
	 * @return [type]                                      [description]
	 */
	public function createKompetensiPeserta(InterfacePesertaRepository $peserta, InterfaceKompetensiRepository $kompetensi, $id, $kompetensiid)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Tambah Peserta Permohonan SDM';
		$data['permohonansdm'] = $this->permohonanSdmRepository->getById($id);
		$data['kompetensi'] = $kompetensi->getById($kompetensiid);
		$data['peserta']    = $peserta->allNoRelationship();
		foreach ($data['permohonansdm']->kompetensipermohonansdm as $key => $value) {
			if($value->kompetensi_id == $kompetensiid) {
				$data['kompetensipermohonansdm_id'] = $value->id;
			}
		}
		return view('backend.permohonansdm.createkompetensipeserta', compact('data'));
	}

	/**
	 * [storeKompetensiPeserta description]
	 * 
	 * @param  Request $request      [description]
	 * @param  [type]  $id           [description]
	 * @param  [type]  $kompetensiid [description]
	 * @return [type]                [description]
	 */
	public function storeKompetensiPeserta(Request $request, $id, $kompetensiid)
	{
		$response = $this->permohonanSdmRepository->storeKompetensiPeserta($id, $request->get('kompetensipermohonansdm_id'), $request->get('peserta_id'));
		if ($response['requested'] > 0) {
			if (count($response['failed']) == 0) {
				Flash::success('Data peserta berhasil ditambahkan');
			} else {
				$pesertaNIP = '';
				foreach ($response['failed'] as $value) {
					$pesertaNIP .= $value .' ';
				}
				Flash::warning('Data peserta '.$pesertaNIP.'sudah ada pada permohonan kompetensi ini.');
			}
			return redirect()->route('dashboard.permohonansdm.kompetensi.peserta.index', ['id' => $id, 'kompetensiid' => $kompetensiid]);
		}
		Flash::error('Pilihan peserta tidak boleh kosong. Minimal 1 peserta dipilih');
		return redirect()->back();
		
	}

	/**
	 * [deleteKompetensi description]
	 * 
	 * @param  [type] $id           [description]
	 * @param  [type] $pesertaid [description]
	 * @return [type]               [description]
	 */
	public function deleteKompetensiPeserta($id, $kompetensiid, $pesertaid)
	{
		$this->permohonanSdmRepository->deleteKompetensiPeserta($id, $pesertaid);
		Flash::success('Data peserta berhasil dihapus');
		return redirect()->route('dashboard.permohonansdm.kompetensi.peserta.index', ['id' => $id, 'kompetensiid' => $kompetensiid]);
	}
///////////////////////////////////////////////////
	// /**
	//  * Display a listing of the resource.
	//  *
	//  * @return Response
	//  */
	// public function indexPermohonanSdm()
	// {
	// 	return view('welcome');
	// }

	// /**
	//  * Show the form for creating a new resource.
	//  *
	//  * @return Response
	//  */
	// public function createPermohonanSdm()
	// {
	// 	//
	// }

	// /**
	//  * Store a newly created resource in storage.
	//  *
	//  * @return Response
	//  */
	// public function storePermohonanSdm()
	// {
	// 	//
	// }

	// *
	//  * Display the specified resource.
	//  *
	//  * @param  int  $id
	//  * @return Response
	 
	// public function show($id)
	// {
	// 	//
	// }

	// /**
	//  * Show the form for editing the specified resource.
	//  *
	//  * @param  int  $id
	//  * @return Response
	//  */
	// public function edit($id)
	// {
	// 	//
	// }

	// /**
	//  * Update the specified resource in storage.
	//  *
	//  * @param  int  $id
	//  * @return Response
	//  */
	// public function update($id)
	// {
	// 	//
	// }

	// /**
	//  * Remove the specified resource from storage.
	//  *
	//  * @param  int  $id
	//  * @return Response
	//  */
	// public function destroy($id)
	// {
	// 	//
	// }

}
