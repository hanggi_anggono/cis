<?php namespace Certification\Http\Controllers;

use Certification\Repositories\Unit\InterfaceUindukRepository;
use Certification\Repositories\Unit\InterfaceUcabangRepository;
use Certification\Http\Requests\StoreMasterUcabangRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config, Flash;

class MasterUcabangController extends Controller {

	/**
	 * [$ucabangRepository description]
	 * 
	 * @var [type]
	 */
	private $ucabangRepository;
	
	/**
	 * [$uindukRepository description]
	 * 
	 * @var [type]
	 */
	private $uindukRepository;
	
	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;

	/**
	 * [__construct description]
	 * 
	 * @param InterfaceUcabangRepository $ucabangRepository [description]
	 * @param InterfaceUindukRepository  $uindukRepository  [description]
	 */
	public function __construct(InterfaceUcabangRepository $ucabangRepository, InterfaceUindukRepository $uindukRepository)
	{
		$this->ucabangRepository = $ucabangRepository;
		$this->uindukRepository  = $uindukRepository;
		$this->title             = 'Master Unit Cabang';
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master unit cabang';
		$data['unit']              = $this->ucabangRepository->all('paginate');
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		return view('backend.masterucabang.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah master unit cabang';
		$data['unitinduk']  = $this->uindukRepository->lists();
		return view('backend.masterucabang.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->ucabangRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data unit cabang berhasil disimpan');
			return redirect()->route('dashboard.master.unit.cabang.index');
		}
		Flash::error('Data unit cabang gagal disimpan');
		return redirect()->back()->withInput();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit master unit cabang';
		$data['unit']	  = $this->ucabangRepository->getById($id);
		$data['unitinduk']  = $this->uindukRepository->lists();
		return view('backend.masterucabang.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->ucabangRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data unit cabang berhasil dirubah');
			return redirect()->route('dashboard.master.unit.cabang.index');
		}
		Flash::error('Data unit cabang gagal dirubah');
		return redirect()->back()->withInput();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ucabangRepository->delete($id);
		Flash::success('Data unit cabang berhasil dihapus');
		return redirect()->route('dashboard.master.unit.cabang.index');
	}

	/**
	 * Get unit ranting data. Used for API purposes
	 * 
	 * @param  Integer $id unit cabang id
	 * @return Json
	 */
	public function getUnitRanting($id)
	{
		return $this->ucabangRepository->unitranting($id);
	}

}
