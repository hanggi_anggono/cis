<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterSubbidangRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Bidang\InterfaceSubbidangRepository;
use Certification\Repositories\Bidang\InterfaceBidangRepository;
use Illuminate\Http\Request;
use \Config, \Flash;

/**
 * Master Sub Bidang Controller
 */
class MasterSubbidangController extends Controller {

	/**
	 * [$subbidangRepository description]
	 * @var [type]
	 */
	private $subbidangRepository;
	
	/**
	 * [$title description]
	 * @var [type]
	 */
	private $title;
	
	/**
	 * [$request description]
	 * @var [type]
	 */
	private $request;

	/**
	 * Class Instance
	 * 
	 * @param InterfacesubbidangRepository $subbidangRepository Sub Bidang repository interface
	 * @param Request $request Request handler
	 */
	public function __construct(InterfaceSubbidangRepository $subbidangRepository, Request $request)
	{
		$this->subbidangRepository = $subbidangRepository;
		$this->request             = $request;
		$this->title               = 'Manajemen Sub Bidang';
	}

	/**
	 * Show all sub bidang data with given bidang id
	 * 
	 * @param  Integer $bidang_id bidang id
	 * @return Response
	 */
	public function showByBidang($bidang_id)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar Sub Bidang';
		$data['subbidang']         = $this->subbidangRepository->all($bidang_id,'paginate');
		$data['pagination_number'] = numbering_pagination($this->request, Config::get('certification.default_pagination_count'));
		$data['bidang_id']         = $bidang_id;

		return view('backend.mastersubbidang.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(InterfaceBidangRepository $bidangRepository, $bidang_id = null)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah data master sub bidang';
		if (isset($bidang_id)) 
		{
			$data['bidang_single'] = $bidangRepository->getById($bidang_id);
		}
		$data['bidang'] = $bidangRepository->lists();
		return view('backend.mastersubbidang.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param RequestValidator $request Request form validator
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->subbidangRepository->store($request->except('_token'));
		if ($affected) 
		{
			Flash::success('Data sub bidang berhasil disimpan');
			return redirect()->route('dashboard.master.bidang.index');
		}
		Flash::error('Data sub bidang gagal disimipan');
		return redirect()->route('dashboard.master.subbidang.create');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(InterfaceBidangRepository $bidangRepository, $id)
	{
		$data['subbidang'] = $this->subbidangRepository->getById($id);
		$data['bidang']    = $bidangRepository->lists();
		$data['title']     = $this->title;
		$data['subtitle']  = 'Edit data master sub bidang';
		return view('backend.mastersubbidang.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->subbidangRepository->update($id, $request->except('_token','_method'));
		if ($affected) 
		{
			Flash::success('Data sub bidang berhasil dirubah');
			return redirect()->route('dashboard.master.bidang.index');
		}
		Flash::error('Data sub bidang gagal dirubah');
		return redirect()->route('dashboard.master.subbidang.edit',['id'=>$id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->subbidangRepository->delete($id);
		Flash::success('Data sub bidang berhasil dihapus');
		return redirect()->route('dashboard.master.bidang.index');
	}

	/**
	 * API lists collection of subbidang from given bidang id
	 * 
	 * @param  Integer $id Bidang id
	 * @return JSON
	 */
	public function apiGetByBidang($id)
	{
		return $this->subbidangRepository->listsByBidang($id);
	}

}
