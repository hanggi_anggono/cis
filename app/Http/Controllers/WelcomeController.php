<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\Register\StoreFormRequest as RequestValidator;
use Certification\Repositories\Bidang\EloquentDbBidang;
use Certification\Repositories\Register\EloquentDbRegistration;

use Illuminate\Http\Request;

use Flash, Config;

class WelcomeController extends Controller {

	private $bidang;

	private $registerRepository;

	private $pagination;

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(EloquentDbBidang $bidang, EloquentDbRegistration $registerRepository)
	{
		$this->bidang = $bidang;
		$this->registerRepository = $registerRepository;
		$this->pagination = Config::get('certification.default_pagination_count');
		// $this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('front.index');
	}

	public function register()
	{
		$bidang = $this->bidang->lists();
		return view('front.reg', compact('bidang'));
	}

	public function storeRegister(RequestValidator $request)
	{
		if($this->registerRepository->store($request->except('_token'))) {
			Flash::success('Permintaan uji kompetensi anda berhasil disimpan, silahkan tunggu untuk pemberitahuan selanjutnya.');
			return redirect()->route('register.index');
		}
		Flash::error('Terjadi kesalahan. Permintaan gagal disimpan.');
		return redirect()->back();
	}

	public function registrationDashboardIndex(Request $request)
	{
		$data['title'] = 'Daftar Permintaan Uji Kompetensi';
		$data['subtitle'] = '';
		$data['registers'] = $this->registerRepository->all('paginate', $request);
		$data['pagination'] = numbering_pagination($request, $this->pagination);
		return view('backend.registration.index', compact('data'));
	}

	public function registrationDashboardShow($id)
	{
		$data['title'] = 'Detail Calon Peserta Permintaan Uji Kompetensi';
		$data['subtitle'] = '';
		$data['register'] = $this->registerRepository->find($id);
		return view('backend.registration.show', compact('data'));
	}

}
