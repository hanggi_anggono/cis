<?php namespace Certification\Http\Controllers;

use Certification\Repositories\Unit\InterfaceUcabangRepository;
use Certification\Repositories\Unit\InterfaceUrantingRepository;
use Certification\Http\Requests\StoreMasterUrantingRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config, Flash;

class MasterUrantingController extends Controller {

	/**
	 * [$urantingRepository description]
	 * 
	 * @var [type]
	 */
	private $urantingRepository;
	
	/**
	 * [$ucabangRepository description]
	 * 
	 * @var [type]
	 */
	private $ucabangRepository;
	
	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;

	/**
	 * [__construct description]
	 * 
	 * @param InterfaceUrantingRepository $urantingRepository [description]
	 * @param InterfaceUcabangRepository  $ucabangRepository  [description]
	 */
	public function __construct(InterfaceUrantingRepository $urantingRepository, InterfaceUcabangRepository $ucabangRepository)
	{
		$this->urantingRepository = $urantingRepository;
		$this->ucabangRepository  = $ucabangRepository;
		$this->title             = 'Master Unit Ranting';
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master unit ranting';
		$data['unit']              = $this->urantingRepository->all('paginate');
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		return view('backend.masteruranting.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah master unit ranting';
		$data['unitcabang']  = $this->ucabangRepository->lists();
		return view('backend.masteruranting.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->urantingRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data unit ranting berhasil disimpan');
			return redirect()->route('dashboard.master.unit.ranting.index');
		}
		Flash::error('Data unit ranting gagal disimpan');
		return redirect()->back()->withInput();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Edit master unit ranting';
		$data['unit']       = $this->urantingRepository->getById($id);
		$data['unitcabang'] = $this->ucabangRepository->lists();
		return view('backend.masteruranting.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->urantingRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data unit ranting berhasil dirubah');
			return redirect()->route('dashboard.master.unit.ranting.index');
		}
		Flash::error('Data unit ranting gagal dirubah');
		return redirect()->back()->withInput();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->urantingRepository->delete($id);
		Flash::success('Data unit ranting berhasil dihapus');
		return redirect()->route('dashboard.master.unit.ranting.index');
	}

}
