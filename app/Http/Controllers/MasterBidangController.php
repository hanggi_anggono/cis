<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterBidangRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Bidang\InterfaceBidangRepository;
use Illuminate\Http\Request as Request;
use \Config, \Flash;

class MasterBidangController extends Controller {

	/**
	 * Bidang Repository
	 * 
	 * @var $bidangRepository
	 */
	private $bidangRepository;
	
	/**
	 * Page title attribute
	 * 
	 * @var $title
	 */
	private $title;

	/**
	 * Class Instance
	 * 
	 * @param InterfaceBidangRepository $bidangRepository [description]
	 */
	public function __construct(InterfaceBidangRepository $bidangRepository)
	{
		$this->bidangRepository = $bidangRepository;
		$this->title            = 'Manajemen Bidang';
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master bidang';
		$data['bidang']            = $this->bidangRepository->all('paginate');
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		return view('backend.masterbidang.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah data master bidang';
		return view('backend.masterbidang.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param RequestValidator $request Request form validator
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->bidangRepository->store($request->except('_token'));
		if ($affected) 
		{
			Flash::success('Data berhasil disimpan');
			return redirect()->route('dashboard.master.bidang.index');
		}
		Flash::error('Data gagal disimipan');
		return redirect()->route('dashboard.master.bidang.create');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['bidang']   = $this->bidangRepository->getById($id);
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit data master bidang';

		return view('backend.masterbidang.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->bidangRepository->update($id, $request->except('_token', '_method'));
		if ($affected) 
		{
			Flash::success('Data berhasil dirubah');
			return redirect()->route('dashboard.master.bidang.index');
		}
		Flash::error('Data gagal dirubah');
		return redirect()->route('dashboard.master.bidang.edit',['id'=>$id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->bidangRepository->delete($id);
		Flash::success('Data berhasil dihapus');
		return redirect()->route('dashboard.master.bidang.index');
	}

}
