<?php namespace Certification\Http\Controllers;

use Certification\Repositories\Evaluasi\EloquentDbEvaluasi;
use Certification\Repositories\Peserta\InterfacePesertaRepository;
use Certification\Repositories\Permohonan\EloquentDbKompetensipermohonan;
use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;
use \Config, \Flash, \Session;
use DB;

class EvaluasiController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;
	
	/**
	 * [$request description]
	 * 
	 * @var [type]
	 */
	private $request;

	/**
	 * [$evaluasiRepository description]
	 * @var [type]
	 */
	private $evaluasiRepository;

	/**
	 * [__construct description]
	 * 
	 * @param Request            $request            [description]
	 * @param EloquentDbEvaluasi $evaluasiRepository [description]
	 */
	public function __construct(Request $request, EloquentDbEvaluasi $evaluasiRepository)
	{
		$this->evaluasiRepository = $evaluasiRepository;
		$this->request            = $request;
		$this->title              = 'Management Evaluasi';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar Evaluasi';
		$data['evaluasi']          = $this->evaluasiRepository->allPeserta('paginate', $this->request, Session::get('params'));
		$data['pagination_number'] = numbering_pagination($this->request, 20);
		$data['params']            = $this->request->except('page');

		$data['evaluasisdm']	   = $this->listpermohonanSdm();		

		return view('backend.evaluasi.index', compact('data'));
	}

	public function listpermohonanSdm()
	{
		$sdm = DB::table('kompetensipermohonansdmpeserta')
				->join('peserta', 'kompetensipermohonansdmpeserta.peserta_id', '=', 'peserta.id')				
				->join('kompetensipermohonansdm', 'kompetensipermohonansdmpeserta.kompetensipermohonansdm_id', '=', 'kompetensipermohonansdm.id')				
				->join('kompetensi', 'kompetensipermohonansdm.kompetensi_id', '=', 'kompetensi.id')
				->leftJoin('evaluasi', 'kompetensipermohonansdmpeserta.kompetensipermohonansdm_id', '=', 'evaluasi.kompetensipermohonansdm_id')
				->leftJoin('unitinduk', 'peserta.unitinduk_id', '=', 'unitinduk.id')				
				->join('permohonansdm', 'kompetensipermohonansdmpeserta.permohonansdm_id', '=', 'permohonansdm.id')
				->select('peserta.nip',
						'peserta.nama',
						'peserta.id as id_peserta',
						'unitinduk.nama_unitinduk',
						'kompetensi.nama_kompetensi',
						'permohonansdm.tanggal_mohon',
						'evaluasi.hasil',
						'evaluasi.tanggal_evaluasi',
						'evaluasi.status_penjadwalan',						
						'kompetensipermohonansdmpeserta.kompetensipermohonansdm_id')
				->get();		

				//dd($sdm);

		return $sdm;
	}

	/**
	 * [setHasil description]
	 * 
	 * @param InterfacePesertaRepository $peserta [description]
	 * @param [type]                     $id      [description]
	 */
	public function setHasil(InterfacePesertaRepository $peserta, EloquentDbKompetensipermohonan $kompetensipermohonan, $id, $kompetensipermohonanid)
	{
		$data['title']                = $this->title;
		$data['subtitle']             = 'Daftar Evaluasi';
		$data['peserta']              = $peserta->getById($id);
		$data['kompetensipermohonan'] = $kompetensipermohonan->find($kompetensipermohonanid);
		return view('backend.evaluasi.setevaluasi', compact('data'));
	}

	/**
	 * [storeHasil description]
	 * 
	 * @param  [type] $id                     [description]
	 * @param  [type] $kompetensipermohonanid [description]
	 * @return [type]                         [description]
	 */
	public function storeHasil()
	{
		$affected = $this->evaluasiRepository->store($this->request->except('_token'));
		if ($affected) {
			Flash::success('Data peserta berhasil dievaluasi');
			return redirect()->route('dashboard.evaluasi.index');
		}
		Flash::success('Error, Data peserta gagal dievaluasi');
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function detail(InterfacePesertaRepository $peserta, $id, $kompetensipermohonanid)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Daftar Evaluasi';
		$data['evaluasi'] = $this->evaluasiRepository->getDetail($id, $kompetensipermohonanid);
		$data['peserta']  = $peserta->getById($data['evaluasi']['id_peserta']);
		return view('backend.evaluasi.detail', compact('data'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->evaluasiRepository->delete($id);
		Flash::success('Hasil evaluasi telah dihapus');
		return redirect()->route('dashboard.evaluasi.index');
	}

}
