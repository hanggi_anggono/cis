<?php namespace Certification\Http\Controllers;

use Certification\Http\Controllers\Controller;
use Certification\Repositories\Sertifikat\EloquentDbSertifikat;
use Certification\Repositories\Penjadwalan\EloquentDbPenjadwalan;

use Illuminate\Http\Request;
use \Config, \Flash, \Session;

class SertifikatController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;
	
	/**
	 * [$sertifikatRepository description]
	 * 
	 * @var [type]
	 */
	private $sertifikatRepository;

	/**
	 * [$penjadwalanRepository description]
	 * 
	 * @var [type]
	 */
	private $penjadwalanRepository;

	/**
	 * [$paginationCount description]
	 * 
	 * @var [type]
	 */
	private $paginationCount;
	
	/**
	 * [$request description]
	 * 
	 * @var [type]
	 */
	private $request;

	/**
	 * [__construct description]
	 * 
	 * @param EloquentDbSertifikat  $sertifikatRepository  [description]
	 * @param EloquentDbPenjadwalan $penjadwalanRepository [description]
	 * @param Request               $request               [description]
	 */
	public function __construct(EloquentDbSertifikat $sertifikatRepository, EloquentDbPenjadwalan $penjadwalanRepository, Request $request)
	{
		$this->sertifikatRepository  = $sertifikatRepository;
		$this->request               = $request;
		$this->penjadwalanRepository = $penjadwalanRepository;
		$this->title                 = 'Manajemen Sertifikat';
		$this->paginationCount       = Config::get('certification.default_pagination_count');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function indexBaru()
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar Sertifikat Baru';
		$data['penjadwalan']       = $this->penjadwalanRepository->all('paginate', null, Session::get('params'));
		$data['pagination_number'] = numbering_pagination($this->request, $this->paginationCount);
		return view('backend.sertifikat.indexpenjadwalan', compact('data'));
	}

	/**
	 * [detailByPenjadwalan description]
	 * 
	 * @param  [type] $penjadwalanId [description]
	 * @return [type]                [description]
	 */
	public function detailByPenjadwalan($penjadwalanId)
	{
		$data['tipeSertifikat']    = 'baru';
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar Sertifikat Baru';
		$data['sertifikat']        = $this->sertifikatRepository->all('paginate', null, $this->request, $penjadwalanId);
		$data['penjadwalan']       = $this->penjadwalanRepository->getById($penjadwalanId);
		$data['pagination_number'] = numbering_pagination($this->request, $this->paginationCount);
		return view('backend.sertifikat.index', compact('data'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function indexPerpanjangan()
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar Sertifikat Perpanjangan';
		$data['tipeSertifikat']    = 'perpanjangan';
		$data['sertifikat']        = $this->sertifikatRepository->all('paginate', $data['tipeSertifikat'], $this->request, null, Session::get('params'));
		$data['pagination_number'] = numbering_pagination($this->request, 20);
		return view('backend.sertifikat.indexperpanjangan', compact('data'));
	}

	/**
	 * [prosesPerpanjangan description]
	 * 
	 * @param  [type] $sertifikat_id [description]
	 * @return [type]              [description]
	 */
	public function prosesPerpanjangan($sertifikat_id)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Proses Perpanjangan Sertifikat Peserta';
		$data['sertifikat'] = $this->sertifikatRepository->find($sertifikat_id);
		return view('backend.sertifikat.prosessertifikat', compact('data'));
	}

	/**
	 * [store description]
	 * 
	 * @return [type] [description]
	 */
	public function store()
	{
		$affected = $this->sertifikatRepository->store($this->request->except('_token'));
		if ($affected) {
			return [
				'status' => true,
				'data'   => $affected
			];
		}

		return [
				'status' => false,
				'data'   => null
			];
	}

	/**
	 * [generateMultiple description]
	 * 
	 * @param  [type] $penjadwalanId [description]
	 * @return [type]                [description]
	 */
	public function generateMultiple($penjadwalanId)
	{
		$affected = $this->sertifikatRepository->generateMultipleCertificate($penjadwalanId);
		if($affected['status']) {
			Flash::success('Sertifikat peserta berhasil di-generate');
			return redirect()->route('dashboard.pelaksanaan.index');
		}
		Flash::error('Sertifikat peserta gagal di-generate');
		return redirect()->route('dashboard.pelaksanaan.index');
	}

	/**
	 * [updateSertifikat description]
	 * 
	 * @param  [type] $sertifikat_id [description]
	 * @return [type]              [description]
	 */
	public function updateSertifikat($sertifikat_id)
	{
		$affected = $this->sertifikatRepository->update($sertifikat_id, $this->request->except('_token','_method'));
		if($affected) {
			Flash::success('Sertifikat peserta berhasil diperbarui');
			return redirect()->route('dashboard.sertifikat.perpanjangan.index');
		}
		Flash::error('Error, Sertifikat peserta gagal  diperbarui');
		return redirect()->back();
	}

	/**
	 * [downloadSingle description]
	 * 
	 * @param  [type] $sertifikat_id [description]
	 * @return [type]                [description]
	 */
	public function downloadSingle($sertifikat_id)
	{
		return $this->sertifikatRepository->downloadSingle($sertifikat_id);
	}

	/**
	 * [downloadAll description]
	 * 
	 * @param  [type] $penjadwalanId [description]
	 * @return [type]                [description]
	 */
	public function downloadAll($penjadwalanId)
	{
		return $this->sertifikatRepository->downloadMultiple($penjadwalanId);
	}

}
