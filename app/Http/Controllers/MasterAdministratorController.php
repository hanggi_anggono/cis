<?php namespace Certification\Http\Controllers;

use Certification\Repositories\Administrator\InterfaceAdministratorRepository;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\LembagaSertifikasi\InterfaceLembagaSertifikasiRepository;
use Certification\Http\Requests\StoreMasterAdministratorRequest as RequestValidator;
use Certification\Services\DataSupportMasterService as DataService;
use Certification\Services\StoreAdministratorService as StoreService;
use Certification\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config, Flash;

class MasterAdministratorController extends Controller {

	/**
	 * Administrator Repository
	 * 
	 * @var Interface
	 */
	private $administratorRepository;
	
	/**
	 * Module Title
	 * 
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param InterfaceAdministratorRepository $administratorRepository administrator repository
	 */
	public function __construct(InterfaceAdministratorRepository $administratorRepository)
	{
		$this->administratorRepository = $administratorRepository;
		$this->title            = 'Master Administrator';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master administrator';
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		$data['administrator']     = $this->administratorRepository->all('paginate', $request);
		return view('backend.masteradministrator.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(DataService $dataService)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Daftar master administrator';
		$data['support']  = $dataService->forMasterAdministrator();
		return view('backend.masteradministrator.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request, StoreService $store)
	{
		$affected = $store->save($request->except('_token'));
		$request->flashExcept('_token');
		if ($affected) {
			Flash::success('Data administrator berhasil disimpan');
			return redirect()->route('dashboard.master.administrator.index');
		}
		Flash::error('Data administrator gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['title']    		 = $this->title;
		$data['subtitle'] 		 = 'Detail master administrator';
		$data['administrator']   = $this->administratorRepository->getById($id);
		return view('backend.masteradministrator.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(DataService $dataService, $id)
	{
		$data['title']    		 = $this->title;
		$data['subtitle'] 		 = 'Edit master administrator';
		$data['administrator']   = $this->administratorRepository->getById($id);
		$data['support']  		 = $dataService->forMasterAdministrator();
		return view('backend.masteradministrator.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, StoreService $store, $id)
	{
		$affected = $store->save($request->except('_token','_method'),'update',$id);
		if ($affected) {
			Flash::success('Data administrator berhasil dirubah');
			return redirect()->route('dashboard.master.administrator.index');
		}
		Flash::error('Data administrator gagal dirubah');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->administratorRepository->delete($id);
		Flash::success('Data berhasil dihapus');
		return redirect()->route('dashboard.master.administrator.index');
	}

	/**
	 * Display administrator kompetensi resource
	 * 
	 * @param  Integer $id Administrator Kompetensi
	 * @return Response
	 */
	public function indexKompetensi($id)
	{
		$data['title']    		 = $this->title;
		$data['subtitle'] 		 = 'Kompetensi master administrator';
		$data['administrator']   = $this->administratorRepository->getById($id);
		return view('backend.masteradministrator.indexkompetensi', compact('data'));
	}

	/**
	 * Display kompetensi form
	 * 
	 * @param  InterfaceKompetensiRepository $kompetensi kompetensi repository
	 * @param  Integer                        $id       administrator id
	 * @return Response
	 */
	public function createKompetensi(InterfaceKompetensiRepository $kompetensi, $id)
	{
		$data['title']    		 = $this->title;
		$data['subtitle'] 		 = 'Tambah kompetensi master administrator';
		$data['administrator']   = $this->administratorRepository->getById($id);
		$data['kompetensi']		 = $kompetensi->lists();
		return view('backend.masteradministrator.createkompetensi', compact('data'));
	}

	/**
	 * Store administrator kompetensi
	 * 
	 * @param  Request $request Http request
	 * @param  Integer  $id      administrator id
	 * @return Response
	 */
	public function storeKompetensi(Request $request, $id)
	{
		$this->administratorRepository->storeKompetensi($id, $request->get('kompetensi_id'));
		Flash::success('Data kompetensi berhasil ditambahkan');
		return redirect()->route('dashboard.master.administrator.kompetensi.index', ['id' => $id]);
	}

	/**
	 * Delete kompetensi administrator
	 * 
	 * @param  Integer $id           Administrator id
	 * @param  Integer $kompetensi_id kompetensi Id
	 * @return Response
	 */
	public function deleteKompetensi($id, $kompetensi_id)
	{
		$this->administratorRepository->deleteKompetensi($id, $kompetensi_id);
		Flash::success('Data kompetensi berhasil dihapus');
		return redirect()->route('dashboard.master.administrator.kompetensi.index', ['id' => $id]);
	}

	/**
	 * Display administrator kelompok resource
	 * 
	 * @param  Integer $id Administrator Kompetensi
	 * @return Response
	 */
	public function indexKelompok($id)
	{
		$data['title']    		 = $this->title;
		$data['subtitle'] 		 = 'Kelompok master administrator';
		$data['administrator']   = $this->administratorRepository->getById($id);
		return view('backend.masteradministrator.indexKelompok', compact('data'));
	}

	/**
	 * Display kelompok form
	 * 
	 * @param  InterfaceLembagaSertifikasiRepository $kelompok Lembaga sertifikasi repository
	 * @param  Integer                        $id       administrator id
	 * @return Response
	 */
	public function createKelompok(InterfaceLembagaSertifikasiRepository $kelompok, $id)
	{
		$data['title']    		 = $this->title;
		$data['subtitle'] 		 = 'Tambah kelompok master administrator';
		$data['administrator']   = $this->administratorRepository->getById($id);
		$data['kelompok']		 = $kelompok->lists();
		return view('backend.masteradministrator.createKelompok', compact('data'));
	}

	/**
	 * Store administrator kelompok
	 * 
	 * @param  Request $request Http request
	 * @param  Integer  $id      administrator id
	 * @return Response
	 */
	public function storeKelompok(Request $request, $id)
	{
		$this->administratorRepository->storeKelompok($id, $request->except('_token'));
		Flash::success('Data kelompok berhasil ditambahkan');
		return redirect()->route('dashboard.master.administrator.kelompok.index', ['id' => $id]);
	}

	/**
	 * Delete kelompok administrator
	 * 
	 * @param  Integer $id           Administrator id
	 * @param  Integer $lembagasertifikasi_id kelompok Id
	 * @return Response
	 */
	public function deleteKelompok($id, $lembagasertifikasi_id)
	{
		$this->administratorRepository->deleteKelompok($id, $lembagasertifikasi_id);
		Flash::success('Data kelompok berhasil dihapus');
		return redirect()->route('dashboard.master.administrator.kelompok.index', ['id' => $id]);
	}

	/**
	 * [apiByLsk description]
	 * 
	 * @param  [type] $lsk [description]
	 * @return [type]      [description]
	 */
	public function apiByLsk($lsk)
	{
		return $this->administratorRepository->lists($lsk);
	}

}
