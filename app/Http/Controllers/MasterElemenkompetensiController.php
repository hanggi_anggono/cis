<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreElemenkompetensiRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Kompetensi\InterfaceElemenkompetensiRepository;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Illuminate\Http\Request;
use \Config, \Flash;

/**
 * Class controller handler CRUD elemen kompetensi
 */
class MasterElemenkompetensiController extends Controller {

	/**
	 * Elemen kompetensi repository
	 * @var Interface
	 */
	private $elemenkompetensiRepository;
	
	/**
	 * Module title
	 * @var String
	 */
	private $title;

	/**
	 * Class Instance
	 * 
	 * @param InterfaceElemenkompetensiRepository $elemenkompetensiRepository Sub Bidang repository interface
	 */
	public function __construct(InterfaceElemenkompetensiRepository $elemenkompetensiRepository)
	{
		$this->elemenkompetensiRepository = $elemenkompetensiRepository;
		$this->title                      = 'Manajemen Elemen Kompetensi';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request Http request
	 * @param Integer $kompetensi_id Kompetensi id
	 * @return Response
	 */
	public function showByKompetensi(Request $request, $kompetensi_id)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar elemen kompetensi';
		$data['elemenkompetensi']  = $this->elemenkompetensiRepository->all($kompetensi_id,'paginate');
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		$data['kompetensi_id']     = $kompetensi_id;

		return view('backend.masterelemenkompetensi.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param Interface $kompetensiRepository Kompetensi repository
	 * @param Integer $kompetensi_id Kompetensi Id
	 * @return Response
	 */
	public function create(InterfaceKompetensiRepository $kompetensiRepository, $kompetensi_id = null)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah data master elemen kompetensi';

		if (isset($kompetensi_id)) {
			$data['kompetensi_single'] = $kompetensiRepository->getById($kompetensi_id);
		} else {
			$data['kompetensi'] = $kompetensiRepository->lists();
		}

		return view('backend.masterelemenkompetensi.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param RequestValidator $request Request Validator
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->elemenkompetensiRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data master elemen kompetensi berhasil disimpan');
			return redirect()->route('dashboard.master.kompetensi.index');
		}
		Flash::error('Data master elemen kompetensi gagal disimpan');
		return redirect()->route('dashboard.master.kompetensi.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Interface $kompetensiRepository Kompetensi repository
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(InterfaceKompetensiRepository $kompetensiRepository, $id)
	{
		$data['title']            = $this->title;
		$data['subtitle']         = 'Tambah data master elemen kompetensi';
		$data['kompetensi']       = $kompetensiRepository->lists();
		$data['elemenkompetensi'] = $this->elemenkompetensiRepository->getById($id);
		return view('backend.masterelemenkompetensi.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  RequestValidator $request Request Validator
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->elemenkompetensiRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data master elemen kompetensi berhasil dirubah');
			return redirect()->route('dashboard.master.kompetensi.index');
		}
		Flash::error('Data master elemen kompetensi gagal dirubah');
		return redirect()->route('dashboard.master.kompetensi.edit',['id' => $id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->elemenkompetensiRepository->delete($id);
		Flash::success('Data elemen kompetensi berhasil dihapus');
		return redirect()->route('dashboard.master.kompetensi.index');
	}

}
