<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterGradeRequest as RequestValidator;
use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Certification\Repositories\Grade\InterfaceGradeRepository;
use \Config;

class MasterGradeController extends Controller {

	/**
	 * Grade repository interface
	 * @var Interface
	 */
	private $gradeRepository;
	
	/**
	 * Global title name
	 * @var [type]
	 */
	private $title;

	/**
	 * Request provider
	 * @var [type]
	 */
	private $request;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param InterfaceGradeRepository $gradeRepository grade repository interface
	 * @param Request          $request           Request provider
	 */
	public function __construct(InterfaceGradeRepository $gradeRepository, Request $request)
	{
		$this->gradeRepository = $gradeRepository;
		$this->request         = $request;
		$this->title           = 'Manajemen Grade';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master grade';
		$data['grade']             = $this->gradeRepository->all(true);
		$data['pagination_number'] = numbering_pagination($this->request, Config::get('certification.default_pagination_count'));

		return view('backend.mastergrade.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah data master grade';
		return view('backend.mastergrade.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->gradeRepository->store($request->only('nama_grade'));
		if ($affected) {
			return redirect()->route('dashboard.master.grade.index')->with('success-notif', 'Data grade berhasil disimpan');
		}
		return redirect()->route('dashboard.master.grade.index')->with('fail-notif', 'Data grade gagal disimpan');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['grade']    = $this->gradeRepository->getById($id);
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit data master grade';

		return view('backend.mastergrade.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->gradeRepository->update($id, $request->only('nama_grade'));
		if ($affected) {
			return redirect()->route('dashboard.master.grade.index')->with('success-notif', 'Data grade berhasil dirubah');
		}
		return redirect()->route('dashboard.master.grade.index')->with('fail-notif', 'Data grade gagal dirubah');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->gradeRepository->delete($id);
		return redirect()->route('dashboard.master.grade.index')->with('success-notif', 'Data grade berhasil dihapus');
	}

}
