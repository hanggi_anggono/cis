<?php namespace Certification\Http\Controllers;

use Certification\Services\UnitSummariesService as Service;
use Certification\Http\Controllers\Controller;

/**
 * Class all master units controller
 */
class MasterUnitsController extends Controller {

	/**
	 * Display all data unit summaries
	 * 
	 * @param  Service $service [description]
	 * @return [type]           [description]
	 */
	public function index(Service $service)
	{
		$data['title']     = 'Unit Summaries';
		$data['subtitle']  = 'Data seluruh unit';
		$data['summaries'] = $service->getDataSummaries();
		return view('backend.allunits', compact('data'));
	}

}
