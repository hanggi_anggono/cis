<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterSertifikatasesorRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\SertifikatAsesor\EloquentDbSertifikatAsesor;
use Certification\Repositories\Asesor\InterfaceAsesorRepository;
use Certification\Repositories\Lembaga\InterfaceLembaga;

use Illuminate\Http\Request as Request;
use \Config, \Flash;

class MasterSertifikatasesorController extends Controller {

	/**
	 * Sertifikat Asesor, Asesor, & Lembaga Repository
	 * @var eloquent & interface
	 * 
	 */
	private $sertifikatasesorRepository;
	private $asesorRepository;
	private $lembagaRepository;
	
	/**
	 * Module title
	 * 
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param EloquentDbSertifikatAsesor $sertifikatasesorRepository sertifikat asesor Repository
	 */
	public function __construct(EloquentDbSertifikatAsesor $sertifikatasesorRepository, InterfaceAsesorRepository $asesorRepository, InterfaceLembaga $lembagaRepository)
	{
		$this->sertifikatasesorRepository = $sertifikatasesorRepository;
		$this->asesorRepository 		  = $asesorRepository;
		$this->lembagaRepository 		  = $lembagaRepository;
		$this->title                = 'Manajemen Sertifikat Asesor';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request Http request
	 * @param Integer $asesor_id Asesor id
	 * @return Response
	 */
	public function showByAsesor(Request $request, $asesor_id)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar sertifikat asesor';
		$data['sertifikat']  	   = $this->sertifikatasesorRepository->all($asesor_id);
		$data['asesor']            = $this->asesorRepository->getById($asesor_id);
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		$data['asesor_id']     = $asesor_id;
		//dd($data['sertifikat']);

		return view('backend.mastersertifikatasesor.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param Interface $asesorRepository Asesor repository
	 * @param Interface $lembagaRepository Lembaga repository
	 * @param Integer $asesor_id Asesor Id
	 * @return Response
	 */
	public function create(InterfaceAsesorRepository $asesorRepository, InterfaceLembaga $lembagaRepository, $asesor_id)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Tambah data master sertifikat asesor';
		$data['lembaga']    = $lembagaRepository->lists();


		if (isset($asesor_id)) {
			$data['asesor_single'] = $asesorRepository->getById($asesor_id);
		} else {
			$data['asesor'] = $asesorRepository->lists();
		}		

		return view('backend.mastersertifikatasesor.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param RequestValidator $request Request Validator
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->sertifikatasesorRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data master sertifikat asesor berhasil disimpan');
			return redirect()->route('dashboard.master.sertifikatasesor.asesor.index',['asesor_id'=>$request->get('asesor_id')]);
		}
		Flash::error('Data master sertifikat asesor gagal disimpan');
		return redirect()->route('dashboard.master.asesor.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Interface $asesorRepository Asesor repository
	 * @param Interface $lembagaRepository Lembaga repository
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(InterfaceLembaga $lembagaRepository, $id)
	{
		$data['title']            = $this->title;
		$data['subtitle']         = 'Edit data master sertifikat asesor';
		$data['lembaga']          = $this->lembagaRepository->lists();
		$data['sertifikat']       = $this->sertifikatasesorRepository->getById($id);
		$data['asesor_single']    = $this->asesorRepository->getById($data['sertifikat']->asesor_id);
		return view('backend.mastersertifikatasesor.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  RequestValidator $request Request Validator
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id, $asesor)
	{
		// dd($request->all());
		$affected = $this->sertifikatasesorRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data master sertifikat asesor berhasil dirubah');
			return redirect()->route('dashboard.master.sertifikatasesor.asesor.index', ['asesor_id' => $asesor]);
		}

		Flash::error('Data master sertifikat asesor gagal dirubah');
		return redirect()->route('dashboard.master.sertifikatasesor.edit',['id' => $id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, $asesor)
	{
		$this->sertifikatasesorRepository->delete($id);
		Flash::success('Data sertifikat asesor berhasil dihapus');
		return redirect()->route('dashboard.master.sertifikatasesor.asesor.index', ['asesor_id' => $asesor]);
	}

}
