<?php namespace Certification\Http\Controllers;

use Certification\Repositories\Unit\InterfaceUindukRepository;
use Certification\Repositories\Unit\InterfaceUdiklatRepository;
use Certification\Http\Requests\StoreMasterUnitindukRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config, Flash;

class MasterUindukController extends Controller {

	/**
	 * [$uindukRepository description]
	 * 
	 * @var [type]
	 */
	private $uindukRepository;

	/**
	 * [$udiklatRepository description]
	 * 
	 * @var [type]
	 */
	private $udiklatRepository;
	
	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;

	/**
	 * [__construct description]
	 * 
	 * @param InterfaceUindukRepository  $uindukRepository  [description]
	 * @param InterfaceUdiklatRepository $udiklatRepository [description]
	 * @return Void
	 */
	public function __construct(InterfaceUindukRepository $uindukRepository, InterfaceUdiklatRepository $udiklatRepository)
	{
		$this->uindukRepository = $uindukRepository;
		$this->udiklatRepository = $udiklatRepository;
		$this->title = 'Master Unit Induk';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master unit induk';
		$data['unit']              = $this->uindukRepository->all('paginate');
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		return view('backend.masteruinduk.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah master unit induk';
		$data['udiklat']  = $this->udiklatRepository->lists();
		return view('backend.masteruinduk.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->uindukRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data unit induk berhasil disimpan');
			return redirect()->route('dashboard.master.unit.induk.index');
		}
		Flash::error('Data unit induk gagal disimpan');
		return redirect()->back()->withInput();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit master unit induk';
		$data['unit']	  = $this->uindukRepository->getById($id);
		$data['udiklat']  = $this->udiklatRepository->lists();
		return view('backend.masteruinduk.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->uindukRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data unit induk berhasil dirubah');
			return redirect()->route('dashboard.master.unit.induk.index');
		}
		Flash::error('Data unit induk gagal dirubah');
		return redirect()->back()->withInput();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->uindukRepository->delete($id);
		Flash::success('Data unit induk berhasil dihapus');
		return redirect()->route('dashboard.master.unit.induk.index');
	}

	/**
	 * Get unit cabang data. Used for API purposes
	 * 
	 * @param  Integer $id unit induk id
	 * @return Json
	 */
	public function getUnitCabang($id)
	{
		return $this->uindukRepository->unitcabang($id);
	}

}
