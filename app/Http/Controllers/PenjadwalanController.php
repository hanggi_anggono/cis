<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\Penjadwalan\StorePenjadwalanRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Penjadwalan\EloquentDbPenjadwalan;
use Certification\Repositories\Penjadwalan\EloquentDbPesertapenjadwalan;
use Certification\Repositories\Penjadwalan\EloquentDbKompetensipenjadwalan;
use Certification\Repositories\Evaluasi\EloquentDbEvaluasi;
use Certification\Services\DataSupportMasterService;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\Peserta\InterfacePesertaRepository;
use \Config, \Flash, \Session;
use Illuminate\Http\Request;

class PenjadwalanController extends Controller {

	private $title;
	private $penjadwalanRepository;
	private $pesertaPenjadwalan;
	private $pesertaRepository;

	public function __construct(EloquentDbPenjadwalan $penjadwalanRepository, InterfacePesertaRepository $pesertaRepository, EloquentDbPesertapenjadwalan $pesertaPenjadwalan)
	{
		$this->penjadwalanRepository = $penjadwalanRepository;
		$this->pesertaRepository     = $pesertaRepository;
		$this->pesertaPenjadwalan    = $pesertaPenjadwalan;
		$this->title                 = 'Manajemen Penjadwalan';
		// $this->
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request, EloquentDbKompetensipenjadwalan $kompetensiPenjadwalan)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar Penjadwalan';
		$data['penjadwalan']       = $this->penjadwalanRepository->all('paginate', null, Session::get('params'));
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		$data['kompetensipenjadwalan'] = $kompetensiPenjadwalan->all(null, ['params' => Session::get('params'), 'role' => Session::get('role')]);
		return view('backend.penjadwalan.index', compact('data'));
	}

	/**
	 * [indexKompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function indexKompetensi($id)
	{
		$data['title']       = $this->title;
		$data['subtitle']    = 'Daftar Kompetensi Penjadwalan';
		$data['penjadwalan'] = $this->penjadwalanRepository->allByKompetensi($id);
		return view('backend.penjadwalan.indexKompetensi', compact('data'));
	}

	/**
	 * [indexKompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function indexPeserta(InterfaceKompetensiRepository $kompetensi, $id, $kompetensiId, $kompetensipenjadwalanId)
	{
		$data['title']                   = $this->title;
		$data['subtitle']                = 'Daftar Peserta Penjadwalan';
		$data['penjadwalan']             = $this->penjadwalanRepository->getById($id);
		$data['kompetensi']              = $kompetensi->getById($kompetensiId);
		$data['kompetensipenjadwalanId'] = $kompetensipenjadwalanId;
		$pesertapenjadwalan              = $this->pesertaPenjadwalan->getPeserta($id, $kompetensipenjadwalanId, Session::get('params'));
		$data['peserta']                 = [];
		foreach ($pesertapenjadwalan as $peserta) {
			$data['peserta'][] = ['master' => $this->pesertaRepository->getById($peserta->peserta_id), 'evaluasi_id' => $peserta->evaluasi_id];
		}
		return view('backend.penjadwalan.indexPeserta', compact('data'));
	}

	/**
	 * Get all evaluasi data peserta
	 * 
	 * @param  EloquentDbEvaluasi $evaluasi Evaluasi Repository
	 * @param  Request            $request  HTTP Request
	 * @return Response
	 */
	public function evaluasiPeserta(EloquentDbEvaluasi $evaluasi, Request $request)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Daftar Hasil Evaluasi Peserta yang Belum Dijadwalkan';
		$data['peserta']  = $evaluasi->evaluatedPesertaLists(false, true, null, $request, Session::get('params'));
		return view('backend.penjadwalan.pesertaevaluasi', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(DataSupportMasterService $dataSupport)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah Daftar Penjadwalan';
		$data['support']  = $dataSupport->forPenjadwalan();
		return view('backend.penjadwalan.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		// dd($request->except('_token'));
		$affected = $this->penjadwalanRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data Penjadwalan berhasil disimpan');
			return redirect()->route('dashboard.penjadwalan.index');
		}
		Flash::error('Data Penjadwalan gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(EloquentDbKompetensipenjadwalan $kompetensiPenjadwalan, $id)
	{
		$data['title']                 = $this->title;
		$data['subtitle']              = 'Detail Daftar Penjadwalan';
		$data['penjadwalan']           = $this->penjadwalanRepository->getById($id);
		$data['kompetensipenjadwalan'] = $kompetensiPenjadwalan->all($id, ['params' => Session::get('params'), 'role' => Session::get('role')]);
		return view('backend.penjadwalan.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(DataSupportMasterService $dataSupport, $id)
	{
		$data['title']       = $this->title;
		$data['subtitle']    = 'Edit Daftar Penjadwalan';
		$data['penjadwalan'] = $this->penjadwalanRepository->getById($id);
		$data['support']     = $dataSupport->forPenjadwalan($data['penjadwalan']->lembagasertifikasi_id, $data['penjadwalan']->subbidang_id);
		// dd($data['penjadwalan']);
		return view('backend.penjadwalan.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->penjadwalanRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data Penjadwalan berhasil dirubah');
			return redirect()->route('dashboard.penjadwalan.index');
		}
		Flash::error('Data Penjadwalan gagal dirubah');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->penjadwalanRepository->delete($id);
		Flash::success('Data Penjadwalan berhasil dihapus');
		return redirect()->route('dashboard.penjadwalan.index');
	}

	/**
	 * [createKompetensi description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function createKompetensi(EloquentDbEvaluasi $evaluasi, $id)
	{
		$data['title']       = $this->title;
		$data['subtitle']    = 'Tambah Kompetensi Penjadwalan';
		$data['penjadwalan'] = $this->penjadwalanRepository->getById($id, false);
		$data['kompetensi']  = $evaluasi->evaluatedKompetensiLists();
		return view('backend.penjadwalan.createkompetensi', compact('data'));
	}

	/**
	 * [createPeserta description]
	 * 
	 * @param  [type] $id           [description]
	 * @param  [type] $kompetensiid [description]
	 * @return [type]               [description]
	 */
	public function createPeserta(EloquentDbEvaluasi $evaluasi, InterfaceKompetensiRepository $kompetensi, $id, $kompetensiId, $kompetensipenjadwalanId)
	{
		$data['title']                   = $this->title;
		$data['subtitle']                = 'Tambah Peserta Penjadwalan';
		$data['penjadwalan']             = $this->penjadwalanRepository->getById($id, false);
		$data['kompetensi']              = $kompetensi->getById($kompetensiId);
		$data['peserta']                 = $evaluasi->evaluatedPesertaLists(false, true, $kompetensiId);
		$data['kompetensipenjadwalanId'] = $kompetensipenjadwalanId;
		return view('backend.penjadwalan.createPeserta', compact('data'));
	}

	/**
	 * [storeKompetensi description]
	 * 
	 * @param  Request $request [description]
	 * @param  [type]  $id      [description]
	 * @return [type]           [description]
	 */
	public function storeKompetensi(Request $request, $id)
	{
		$response = $this->penjadwalanRepository->storeKompetensi($id, $request->get('kompetensi_id'));
		if ($response['requested'] > 0) {
			if (count($response['failed']) == 0) {
				Flash::success('Data kompetensi berhasil dimasukkan');
			} else {
				$kompetensi = '';
				foreach ($response['failed'] as $value) {
					$kompetensi .= $value.', ';
				}
				Flash::warning('Data kompetensi '.$kompetensi.' sudah ada pada penjadwalan ini');
				return redirect()->back();
			}
			
			return redirect()->route('dashboard.penjadwalan.kompetensi.index', ['id' => $id]);
		}
		Flash::error('Pilihan kompetensi tidak boleh kosong. Minimal ada 1 kompetensi dipilih');
		return redirect()->back();
	}

	/**
	 * [storeKompetensi description]
	 * 
	 * @param  Request $request [description]
	 * @param  [type]  $id      [description]
	 * @return [type]           [description]
	 */
	public function storePeserta(Request $request, $id, $kompetensiId)
	{
		$response = $this->penjadwalanRepository->storePeserta($id, $request->except('_token'));
		if($response['requested'] > 0) {
			if (count($response['failed']) == 0) {
				Flash::success('Data peserta berhasil dimasukkan');
			} else {
				$peserta = '';
				foreach ($response['failed'] as $value) {
					$peserta .= $value.', ';
				}
				Flash::warning('Data peserta '.$peserta.' sudah ada pada penjadwalan ini');
				return redirect()->back();
			}
			
			return redirect()->route('dashboard.penjadwalan.peserta.index', ['id' => $id, 'kompetensiId' => $kompetensiId, 'kompetensipenjadwalanId' => $request->get('kompetensipenjadwalan_id')]);
		}
		Flash::error('Pilihan peserta tidak boleh kosong. Minimal ada 1 peserta dipilih');
		return redirect()->back();
	}

	/**
	 * [destroyKompetensi description]
	 * 
	 * @param  [type] $id           [description]
	 * @param  [type] $kompetensiId [description]
	 * @return [type]               [description]
	 */
	public function destroyKompetensi($id, $kompetensiId)
	{
		$this->penjadwalanRepository->deleteKompetensi($id, $kompetensiId);
		Flash::success('Data kompetensi berhasil dihapus');
		return redirect()->route('dashboard.penjadwalan.kompetensi.index', ['id' => $id]);
	}

	/**
	 * [destroyPeserta description]
	 * 
	 * @param  [type] $id                      [description]
	 * @param  [type] $pesertaId               [description]
	 * @return [type]                          [description]
	 */
	public function destroyPeserta($id, $kompetensiId, $pesertaId, $evaluasiId, $kompetensipenjadwalanId)
	{
		$this->penjadwalanRepository->deletePeserta($id, $pesertaId, $evaluasiId);
		Flash::success('Data peserta berhasil dihapus');
		return redirect()->route('dashboard.penjadwalan.peserta.index', ['id' => $id, 'kompetensiId' => $kompetensiId, 'kompetensipenjadwalanId' => $kompetensipenjadwalanId]);
	}
}
