<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Penjadwalan\EloquentDbPenjadwalan;
use Certification\Repositories\Penjadwalan\EloquentDbPesertapenjadwalan;
use Certification\Repositories\Penjadwalan\EloquentDbKompetensipenjadwalan;
use Certification\Repositories\Evaluasi\EloquentDbEvaluasi;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\Peserta\InterfacePesertaRepository;
use Certification\Repositories\Pelaksanaan\EloquentDbPelaksanaan;
use Certification\Repositories\Biaya\EloquentDbBiayapelaksanaan;
use \Config, \Flash, \Session;
use Illuminate\Http\Request;

class PelaksanaanController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;
	
	/**
	 * [$penjadwalanRepository description]
	 * 
	 * @var [type]
	 */
	private $penjadwalanRepository;
	
	/**
	 * [$pesertaPenjadwalan description]
	 * 
	 * @var [type]
	 */
	private $pesertaPenjadwalan;
	
	/**
	 * [$pesertaRepository description]
	 * 
	 * @var [type]
	 */
	private $pesertaRepository;
	
	/**
	 * [$pelaksanaanRepository description]
	 * 
	 * @var [type]
	 */
	private $pelaksanaanRepository;
	
	/**
	 * [$biayaRepository description]
	 * 
	 * @var [type]
	 */
	private $biayaRepository;

	/**
	 * [__construct description]
	 * 
	 * @param EloquentDbPenjadwalan        $penjadwalanRepository [description]
	 * @param InterfacePesertaRepository   $pesertaRepository     [description]
	 * @param EloquentDbPesertapenjadwalan $pesertaPenjadwalan    [description]
	 * @param EloquentDbPelaksanaan        $pelaksanaanRepository [description]
	 * @param EloquentDbBiayapelaksanaan   $biayaRepository       [description]
	 */
	public function __construct(EloquentDbPenjadwalan $penjadwalanRepository, InterfacePesertaRepository $pesertaRepository, EloquentDbPesertapenjadwalan $pesertaPenjadwalan, EloquentDbPelaksanaan $pelaksanaanRepository, EloquentDbBiayapelaksanaan $biayaRepository)
	{
		$this->penjadwalanRepository = $penjadwalanRepository;
		$this->pesertaRepository     = $pesertaRepository;
		$this->pesertaPenjadwalan    = $pesertaPenjadwalan;
		$this->pelaksanaanRepository = $pelaksanaanRepository;
		$this->biayaRepository       = $biayaRepository;
		$this->title                 = 'Manajemen Pelaksanaan';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar Pelaksanaan';
		$data['pelaksanaan']       = $this->pelaksanaanRepository->all($this->penjadwalanRepository->all('paginate', null, Session::get('params')), Session::get('params'), Session::get('role'));
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		return view('backend.pelaksanaan.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function realisasi(InterfaceKompetensiRepository $kompetensi, $id, $kompetensiId, $kompetensipenjadwalanId)
	{
		$data['title']                   = $this->title;
		$data['subtitle']                = 'Daftar Peserta Penjadwalan';
		$data['kompetensipenjadwalanId'] = $kompetensipenjadwalanId;
		$data['kompetensi']              = $kompetensi->getById($kompetensiId);
		$data['penjadwalan']             = $this->penjadwalanRepository->getById($id);
		$pesertapenjadwalan              = $this->pesertaPenjadwalan->getPeserta($id, $kompetensipenjadwalanId, Session::get('params'));
		$data['peserta']                 = $this->pelaksanaanRepository->getRealisasiPeserta($pesertapenjadwalan);
		// dd($pesertapenjadwalan);
		return view('backend.pelaksanaan.setkehadiran', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeRealisasi(Request $request)
	{
		$affected = $this->pelaksanaanRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data realisasi berhasil disimpan');
			return redirect()->route('dashboard.pelaksanaan.index');
		}
		Flash::error('Data realisasi gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['title']       = $this->title;
		$data['subtitle']    = 'Detail Kehadiran Pelaksanaan';
		$data['biaya']       = $this->biayaRepository->getTotalBiaya($id);
		$data['penjadwalan'] = $this->pelaksanaanRepository->detail($this->penjadwalanRepository->getById($id), Session::get('params'), Session::get('role'));
		return view('backend.pelaksanaan.show', compact('data'));
	}

	public function tes()
	{
		dd($this->pelaksanaanRepository->all());
	}

}
