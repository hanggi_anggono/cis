<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterUdiklatRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Unit\InterfaceUdiklatRepository;
use Illuminate\Http\Request;
use Config, Flash;

/**
 * Class controller handle master unit diklat resource
 */
class MasterUdiklatController extends Controller {

	/**
	 * Udiklat repository
	 * 
	 * @var Interface
	 */
	private $udiklatRepository;
	
	/**
	 * Modul title
	 * 
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param InterfaceUdiklatRepository $udiklatRepository [description]
	 * @return void
	 */
	public function __construct(InterfaceUdiklatRepository $udiklatRepository)
	{
		$this->udiklatRepository = $udiklatRepository;
		$this->title             = 'Master Unit';
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master unit diklat';
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		$data['unit']              = $this->udiklatRepository->all('paginate');

		return view('backend.masterudiklat.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah master unit diklat';
		return view('backend.masterudiklat.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->udiklatRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data unit diklat berhasil disimpan');
			return redirect()->route('dashboard.master.unit.diklat.index');
		}
		Flash::errror('Data unit diklat gagal disimpan');
		return redirect()->route('dashboard.master.unit.diklat.create');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit master unit diklat';
		$data['unit'] = $this->udiklatRepository->getById($id);
		return view('backend.masterudiklat.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->udiklatRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data unit diklat berhasil dirubah');
			return redirect()->route('dashboard.master.unit.diklat.index');
		}
		Flash::errror('Data unit diklat gagal dirubah');
		return redirect()->route('dashboard.master.unit.diklat.edit',['id'=>$id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->udiklatRepository->delete($id);
		Flash::success('Data unit diklat berhasil dihapus');
		return redirect()->route('dashboard.master.unit.diklat.index');
	}

	/**
	 * [getUnitinduk description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getUnitinduk($id)
	{
		return $this->udiklatRepository->getUnitinduk($id);
	}

}
