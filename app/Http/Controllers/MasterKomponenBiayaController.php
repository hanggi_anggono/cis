<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterKomponenBiayaRequest;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Komponenbiaya\EloquentDbKomponenbiaya;

use \Flash, \Config;
use Illuminate\Http\Request;

class MasterKomponenBiayaController extends Controller {

	/**
	 * [$komponenbiayaRepository description]
	 * 
	 * @var [type]
	 */
	private $komponenbiayaRepository;

	/**
	 * [$pagination description]
	 * 
	 * @var [type]
	 */
	private $pagination;
	
	/**
	 * [$request description]
	 * 
	 * @var [type]
	 */
	private $request;
	
	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;

	public function __construct(EloquentDbKomponenbiaya $komponenbiayaRepository, Request $request)
	{
		$this->request                 = $request;
		$this->komponenbiayaRepository = $komponenbiayaRepository;
		$this->title 				   = 'Manajemen Komponen Biaya';
		$this->pagination              = Config::get('certification.default_pagination_count');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar komponen biaya';
		$data['komponenbiaya']     = $this->komponenbiayaRepository->all('paginate');
		$data['pagination_number'] = numbering_pagination($this->request, $this->pagination);
		return view('backend.masterkomponenbiaya.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar komponen biaya';
		return view('backend.masterkomponenbiaya.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(StoreMasterKomponenBiayaRequest $request)
	{
		$affected = $this->komponenbiayaRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data komponen biaya berhasil disimpan');
			return redirect()->route('dashboard.master.komponenbiaya.index');
		}
		Flash::error('Data komponen biaya berhasil disimpan');
		return redirect()->back();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar komponen biaya';
		$data['komponenbiaya']     = $this->komponenbiayaRepository->find($id);
		return view('backend.masterkomponenbiaya.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(StoreMasterKomponenBiayaRequest $request, $id)
	{
		$affected = $this->komponenbiayaRepository->update($id, $request->except('_token', '_method'));
		if ($affected) {
			Flash::success('Data komponen biaya berhasil diperbarui');
			return redirect()->route('dashboard.master.komponenbiaya.index');
		}
		Flash::error('Data komponen biaya berhasil diperbarui');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->komponenbiayaRepository->delete($id);
		Flash::success('Data komponen biaya berhasil dihapus');
		return redirect()->route('dashboard.master.komponenbiaya.index');
	}

}
