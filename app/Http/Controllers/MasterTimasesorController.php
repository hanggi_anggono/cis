<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterTimasesorRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Timasesor\InterfaceTimasesorRepository;
use Illuminate\Http\Request;
use Config, Flash;

/**
 * Class controller handle master Tim asesor resource
 */
class MasterTimasesorController extends Controller {

	/**
	 * timasesor repository
	 * 
	 * @var Interface
	 */
	private $timasesorRepository;
	
	/**
	 * Modul title
	 * 
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param InterfacetimasesorRepository $timasesorRepository [description]
	 * @return void
	 */
	public function __construct(InterfaceTimasesorRepository $timasesorRepository)
	{
		$this->timasesorRepository = $timasesorRepository;
		$this->title               = 'Master Tim';
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master tim';
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		$data['tim']               = $this->timasesorRepository->all('paginate');

		return view('backend.mastertim.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah master tim asesor';
		return view('backend.mastertim.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->timasesorRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data tim berhasil disimpan');
			return redirect()->route('dashboard.master.tim.index');
		}
		Flash::errror('Data tim gagal disimpan');
		return redirect()->route('dashboard.master.tim.create');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit master tim';
		$data['tim']      = $this->timasesorRepository->getById($id);
		return view('backend.mastertim.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->timasesorRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data tim berhasil dirubah');
			return redirect()->route('dashboard.master.tim.index');
		}
		Flash::errror('Data tim gagal dirubah');
		return redirect()->route('dashboard.master.tim.edit',['id'=>$id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->timasesorRepository->delete($id);
		Flash::success('Data tim berhasil dihapus');
		return redirect()->route('dashboard.master.tim.index');
	}

}
