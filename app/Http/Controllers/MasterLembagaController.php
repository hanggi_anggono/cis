<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterLembagaRequest as RequestValidator;
use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Certification\Repositories\Lembaga\InterfaceLembaga;
use \Config;

/**
 *  Resourceful controller for master lembaga
 *  Handle CRUD of data lembaga
 */
class MasterLembagaController extends Controller {

	/**
	 * Lembaga repository interface
	 * @var Interface
	 */
	private $lembagaRepository;
	
	/**
	 * Global title name
	 * @var [type]
	 */
	private $title;

	/**
	 * Request provider
	 * @var [type]
	 */
	private $request;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param InterfaceLembaga $lembagaRepository lembaga repository interface
	 * @param Request          $request           Request provider
	 */
	public function __construct(InterfaceLembaga $lembagaRepository, Request $request)
	{
		$this->lembagaRepository = $lembagaRepository;
		$this->request           = $request;
		$this->title             = 'Manajemen Akreditor';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master akreditor';
		$data['lembaga']           = $this->lembagaRepository->all(true);
		$data['pagination_number'] = numbering_pagination($this->request, Config::get('certification.default_pagination_count'));
		return view('backend.masterlembaga.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah data master akreditor';
		return view('backend.masterlembaga.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->lembagaRepository->store($request->only('nama_lembaga'));
		if ($affected) {
			return redirect()->route('dashboard.master.akreditor.index')->with('success-notif', 'Data akreditor berhasil disimpan');
		}
		return redirect()->route('dashboard.master.akreditor.index')->with('fail-notif', 'Data akreditor gagal disimpan');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['lembaga']  = $this->lembagaRepository->getById($id);
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit data master akreditor';

		return view('backend.masterlembaga.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->lembagaRepository->update($id, $request->only('nama_lembaga'));
		if ($affected) {
			return redirect()->route('dashboard.master.akreditor.index')->with('success-notif', 'Data akreditor berhasil dirubah');
		}
		return redirect()->route('dashboard.master.akreditor.index')->with('fail-notif', 'Data akreditor gagal dirubah');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->lembagaRepository->delete($id);
		return redirect()->route('dashboard.master.akreditor.index')->with('success-notif', 'Data akreditor berhasil dihapus');
	}

}
