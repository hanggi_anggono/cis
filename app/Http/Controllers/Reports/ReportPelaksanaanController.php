<?php namespace Certification\Http\Controllers\Reports;

use Certification\Http\Controllers\Controller;
use Certification\Repositories\Unit\InterfaceUdiklatRepository;
use Certification\Repositories\Penjadwalan\EloquentDbPenjadwalan;
use Certification\Repositories\Pelaksanaan\EloquentDbPelaksanaan;
use Certification\Repositories\LembagaSertifikasi\InterfaceLembagaSertifikasiRepository;

use \Config, \Flash, \Session;
use Illuminate\Http\Request;

class ReportPelaksanaanController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;

	/**
	 * [$udiklatRepository description]
	 * 
	 * @var [type]
	 */
	private $udiklatRepository;
	
	/**
	 * [$penjadwalanRepository description]
	 * 
	 * @var [type]
	 */
	private $penjadwalanRepository;

	/**
	 * [$pelaksanaanRepository description]
	 * 
	 * @var [type]
	 */
	private $pelaksanaanRepository;
	
	/**
	 * [$lembagasertifikasiRepository description]
	 * 
	 * @var [type]
	 */
	private $lembagasertifikasiRepository;

	/**
	 * [__construct description]
	 * 
	 * @param EloquentDbPenjadwalan                 $penjadwalanRepository        [description]
	 * @param EloquentDbPelaksanaan                 $pelaksanaanRepository        [description]
	 * @param InterfaceUdiklatRepository            $udiklatRepository            [description]
	 * @param InterfaceLembagaSertifikasiRepository $lembagasertifikasiRepository [description]
	 */
	public function __construct(EloquentDbPenjadwalan $penjadwalanRepository, EloquentDbPelaksanaan $pelaksanaanRepository, InterfaceUdiklatRepository $udiklatRepository, InterfaceLembagaSertifikasiRepository $lembagasertifikasiRepository)
	{
		$this->title                        = 'Laporan Pelaksanaan';
		$this->penjadwalanRepository        = $penjadwalanRepository;
		$this->pelaksanaanRepository        = $pelaksanaanRepository;
		$this->udiklatRepository            = $udiklatRepository;
		$this->lembagasertifikasiRepository = $lembagasertifikasiRepository;
	}

	/**
	 * [index description]
	 * 
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = '';
		$data['params']            = $request->except('page');
		$data['tuk']               = $this->udiklatRepository->lists();
		$data['lsk']               = $this->lembagasertifikasiRepository->lists();
		$data['pelaksanaan']       = $this->pelaksanaanRepository->all($this->penjadwalanRepository->all('paginate', null, Session::get('params')), Session::get('params'), Session::get('role'));
		$data['summaries'] = [
			'pelaksanaan' => $this->pelaksanaanRepository->countPelaksanaan(Session::get('params'), Session::get('role')),
			'attended'    => $this->pelaksanaanRepository->countAttended(Session::get('params'), Session::get('role')),
			'passed'      => $this->pelaksanaanRepository->countPassed(Session::get('params'), Session::get('role')),
			'notpassed'   => $this->pelaksanaanRepository->countNotPassed(Session::get('params'), Session::get('role'))
		];
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));

		if ($request->has('export')) {
			return $this->pelaksanaanRepository->export($this->pelaksanaanRepository->all($this->penjadwalanRepository->all(null, $request, Session::get('params'), null, Session::get('role'))), $request->get('export'));
		}
		return view('backend.reports.pelaksanaan', compact('data'));
	}

}
