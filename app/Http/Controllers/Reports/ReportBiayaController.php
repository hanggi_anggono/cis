<?php namespace Certification\Http\Controllers\Reports;

use Certification\Http\Controllers\Controller;
use Certification\Repositories\Biaya\EloquentDbBiayapelaksanaan;

use \Config, \Session;
use Illuminate\Http\Request;

class ReportBiayaController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;
	
	/**
	 * [$request description]
	 * 
	 * @var [type]
	 */
	private $request;
	
	/**
	 * [$pagination description]
	 * 
	 * @var [type]
	 */
	private $pagination;
	
	/**
	 * [$biayaRepository description]
	 * 
	 * @var [type]
	 */
	private $biayaRepository;

	/**
	 * [__construct description]
	 * 
	 * @param Request                    $request         [description]
	 * @param EloquentDbBiayapelaksanaan $biayaRepository [description]
	 */
	public function __construct(Request $request, EloquentDbBiayapelaksanaan $biayaRepository)
	{
		$this->title           = 'Laporan Biaya';
		$this->request         = $request;
		$this->pagination      = Config::get('certification.default_pagination_count');
		$this->biayaRepository = $biayaRepository;
	}

	/**
	 * [index description]
	 * 
	 * @return [type] [description]
	 */
	public function index()
	{
		$data['subtitle'] = '';
		$data['title']    = $this->title;
		$data['biaya']    = $this->biayaRepository->all('paginate', $this->request, Session::get('params'));
		$data['total']    = $this->biayaRepository->getTotalBiaya(null, $this->request, Session::get('params'));
		$data['number']   = numbering_pagination($this->request, $this->pagination);
		$data['params']   = $this->request->except('page', 'export');

		if ($this->request->has('export')) {
			return $this->biayaRepository->export($this->request->get('export'), $this->request, Session::get('params'));
		}
		return view('backend.reports.biaya', compact('data'));
	}

}
