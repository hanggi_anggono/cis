<?php namespace Certification\Http\Controllers\Reports;

use Certification\Http\Controllers\Controller;
use Certification\Repositories\Evaluasi\EloquentDbEvaluasi;

use \Config, \Session;
use Illuminate\Http\Request;

class ReportEvaluasiController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;
	
	/**
	 * [$evaluasiRepository description]
	 * 
	 * @var [type]
	 */
	private $evaluasiRepository;

	/**
	 * [__construct description]
	 * 
	 * @param EloquentDbEvaluasi $evaluasiRepository [description]
	 */
	public function __construct(EloquentDbEvaluasi $evaluasiRepository)
	{
		$this->title                = 'Laporan Evaluasi';
		$this->evaluasiRepository   = $evaluasiRepository;
	}

	/**
	 * [index description]
	 * 
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function index(Request $request)
	{
		$data['subtitle']          = '';
		$data['title']             = $this->title;
		$data['params']            = $request->except('page');
		$data['pagination_number'] = numbering_pagination($request, 20);
		$data['summaries']         = $this->evaluasiRepository->summaries($request, Session::get('params'), Session::get('role'));
		$data['evaluasi']          = $this->evaluasiRepository->allPeserta('paginate', $request, Session::get('params'), Session::get('role'));

		if($request->has('export')) {
			return $this->evaluasiRepository->export($request->get('export'), $request, Session::get('params'), Session::get('role'));
		}
		
		return view('backend.reports.evaluasi', compact('data'));
	}

}
