<?php namespace Certification\Http\Controllers\Reports;

use Certification\Http\Controllers\Controller;
use Certification\Repositories\Asesor\InterfaceAsesorRepository;

use \Config, \Session;
use Illuminate\Http\Request;

class ReportAsesorController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;
	
	/**
	 * [$request description]
	 * 
	 * @var [type]
	 */
	private $request;
	
	/**
	 * [$asesorRepository description]
	 * 
	 * @var [type]
	 */
	private $asesorRepository;

	/**
	 * [__construct description]
	 * 
	 * @param InterfaceAsesorRepository $asesorRepository [description]
	 * @param Request                   $request          [description]
	 */
	public function __construct(InterfaceAsesorRepository $asesorRepository, Request $request)
	{
		$this->request           = $request;
		$this->title             = 'Laporan Asesor';
		$this->asesorRepository  = $asesorRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['subtitle']          = '';
		$data['title']             = $this->title;
		$data['params']            = $this->request->except('page');
		$data['asesor']            = $this->asesorRepository->all('paginate', $this->request, Session::get('params'));
		$data['summaries']         = [
										'total'      => $this->asesorRepository->countAll(null, Session::get('params')),
										'pln'        => $this->asesorRepository->countPln(null, Session::get('params')),
										'nonpln'     => $this->asesorRepository->countNonPln(null, Session::get('params')),
										'sertifikat' => $this->asesorRepository->countSertifikat(null, Session::get('params'))
									];
		$data['pagination_number'] = numbering_pagination($this->request, Config::get('certification.default_pagination_count'));

		if ($this->request->has('export')) {
			return $this->asesorRepository->export($this->request->get('export'), $this->request, Session::get('params'));
		}
		return view('backend.reports.asesor', compact('data'));
	}

	/**
	 * [show description]
	 * 
	 * @return [type] [description]
	 */
	public function detail($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Detail asesor';
		$data['asesor']   = $this->asesorRepository->getById($id);
		return view('backend.reports.detailasesor', compact('data'));
	}

}
