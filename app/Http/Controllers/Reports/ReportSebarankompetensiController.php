<?php namespace Certification\Http\Controllers\Reports;

use Certification\Http\Controllers\Controller;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;

use Illuminate\Http\Request;

class ReportSebarankompetensiController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;
	
	/**
	 * [$request description]
	 * 
	 * @var [type]
	 */
	private $request;
	
	/**
	 * [$kompetensiRepository description]
	 * 
	 * @var [type]
	 */
	private $kompetensiRepository;

	/**
	 * [__construct description]
	 * 
	 * @param InterfaceKompetensiRepository $kompetensiRepository [description]
	 * @param Request                       $request              [description]
	 */
	public function __construct(InterfaceKompetensiRepository $kompetensiRepository, Request $request)
	{
		$this->request              = $request;
		$this->kompetensiRepository = $kompetensiRepository;
		$this->title                = 'Laporan Sebaran Kompetensi';
	}

	/**
	 * [index description]
	 * 
	 * @return [type] [description]
	 */
	public function index()
	{
		$data['subtitle']          = '';
		$data['title']             = $this->title;
		$data['params']            = $this->request->except('page');
		$data['kompetensi']        = $this->kompetensiRepository->lists();
		$data['pagination_number'] = numbering_pagination($this->request, 20);
		$data['sebaran']           = $this->kompetensiRepository->getSebaran('paginate', $this->request);

		if ($this->request->has('export')) {
			return $this->kompetensiRepository->export($this->request->get('export'), $this->request);
		}
		return view('backend.reports.sebarankompetensi', compact('data'));
	}

}
