<?php namespace Certification\Http\Controllers\Reports;

use Certification\Http\Controllers\Controller;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\Realisasipeserta\EloquentDbRealisasipeserta;

use \Config, \Session;
use Illuminate\Http\Request;

class ReportKelulusanController extends Controller {

	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;
	
	/**
	 * [$request description]
	 * 
	 * @var [type]
	 */
	private $request;
	
	/**
	 * [$realisasiRepository description]
	 * 
	 * @var [type]
	 */
	private $realisasiRepository;
	
	/**
	 * [$kompetensiRepository description]
	 * 
	 * @var [type]
	 */
	private $kompetensiRepository;

	/**
	 * [__construct description]
	 * 
	 * @param Request                       $request              [description]
	 * @param EloquentDbRealisasipeserta    $realisasiRepository  [description]
	 * @param InterfaceKompetensiRepository $kompetensiRepository [description]
	 */
	public function __construct(Request $request, EloquentDbRealisasipeserta $realisasiRepository, InterfaceKompetensiRepository $kompetensiRepository)
	{
		$this->request              = $request;
		$this->title                = 'Laporan Kelulusan';
		$this->realisasiRepository  = $realisasiRepository;
		$this->kompetensiRepository = $kompetensiRepository;
	}

	/**
	 * [index description]
	 * 
	 * @return [type] [description]
	 */
	public function index()
	{
		$data['title']     = $this->title;
		$data['params']    = $this->request->except('page');
		$data['subtitle']  = '';
		$data['summaries'] = [
			'total'     => $this->realisasiRepository->countAll(Session::get('params')),
			'attended'  => $this->realisasiRepository->countAttended(Session::get('params')),
			'passed'    => $this->realisasiRepository->countPassed(Session::get('params')),
			'notpassed' => $this->realisasiRepository->countNotPassed(Session::get('params'))
		];
		$data['kompetensi']        = $this->kompetensiRepository->lists();
		$data['realisasi']         = $this->realisasiRepository->all('paginate', $this->request, Session::get('role'), Session::get('params'));
		$data['pagination_number'] = numbering_pagination($this->request, 20);

		if ($this->request->has('export')) {
			return $this->realisasiRepository->export($this->request->get('export'), $this->request, Session::get('role'), Session::get('params'));
		}
		return view('backend.reports.kelulusan', compact('data'));
	}

}
