<?php namespace Certification\Http\Controllers;

use Certification\Http\Controllers\Controller;
use Certification\Http\Requests\Biayapelaksanaan\StoreRequest;
use Certification\Repositories\Biaya\EloquentDbBiayapelaksanaan;
use Certification\Repositories\Komponenbiaya\EloquentDbKomponenbiaya;

use \Flash;

class BiayaPelaksanaanController extends Controller {

	/**
	 * [$biayaRepository description]
	 * 
	 * @var [type]
	 */
	private $biayaRepository;
	
	/**
	 * [$komponenbiayaRepository description]
	 * 
	 * @var [type]
	 */
	private $komponenbiayaRepository;
	
	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;

	/**
	 * [__Construct description]
	 * 
	 * @param  EloquentDbBiayapelaksanaan $biayaRepository         [description]
	 * @param  EloquentDbKomponenbiaya    $komponenbiayaRepository [description]
	 * @return [type]                                              [description]
	 */
	public function __Construct(EloquentDbBiayapelaksanaan $biayaRepository, EloquentDbKomponenbiaya $komponenbiayaRepository)
	{
		$this->biayaRepository         = $biayaRepository;
		$this->komponenbiayaRepository = $komponenbiayaRepository;
		$this->title                   = 'Manajemen Biaya Pelaksanaan';
	}

	/**
	 * [index description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function index($id)
	{
		$data['title']         = $this->title;
		$data['subtitle']      = 'Daftar biaya pelaksanaan';
		$data['biaya']         = $this->biayaRepository->allByPenjadwalan($id);
		$data['penjadwalanId'] = $id;
		return view('backend.biayapelaksanaan.indexbiayapelaksanaan', compact('data'));
	}

	/**
	 * [create description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function create($id)
	{
		$data['title']         = $this->title;
		$data['subtitle']      = 'Tambah biaya pelaksanaan';
		$data['komponenbiaya'] = $this->komponenbiayaRepository->lists();
		$data['penjadwalanId'] = $id;
		return view('backend.biayapelaksanaan.createbiayapelaksanaan', compact('data'));
	}

	/**
	 * [store description]
	 * 
	 * @param  StoreRequest $request [description]
	 * @param  [type]       $id      [description]
	 * @return [type]                [description]
	 */
	public function store(StoreRequest $request, $id)
	{
		$affected = $this->biayaRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Biaya pelaksanaan berhasil disimpan');
			return redirect()->route('dashboard.pelaksanaan.{id}.biaya.index', ['id' => $id]);
		}
		Flash::error('Biaya pelaksanaan gagal disimpan');
		return redirect()->back();
	}

	/**
	 * [edit description]
	 * 
	 * @param  [type] $id    [description]
	 * @param  [type] $biaya [description]
	 * @return [type]        [description]
	 */
	public function edit($id, $biaya)
	{
		$data['title']         = $this->title;
		$data['subtitle']      = 'Tambah biaya pelaksanaan';
		$data['komponenbiaya'] = $this->komponenbiayaRepository->lists();
		$data['biaya']         = $this->biayaRepository->getById($biaya);
		$data['penjadwalanId'] = $id;
		return view('backend.biayapelaksanaan.editbiayapelaksanaan', compact('data'));
	}

	/**
	 * [update description]
	 * 
	 * @param  StoreRequest $request [description]
	 * @param  [type]       $id      [description]
	 * @param  [type]       $biaya   [description]
	 * @return [type]                [description]
	 */
	public function update(StoreRequest $request, $id, $biaya)
	{
		$affected = $this->biayaRepository->update($biaya, $request->except('_token', '_method'));
		if ($affected) {
			Flash::success('Biaya pelaksanaan berhasil diperbarui');
			return redirect()->route('dashboard.pelaksanaan.{id}.biaya.index', ['id' => $id]);
		}
		Flash::error('Biaya pelaksanaan gagal diperbarui');
		return redirect()->back();
	}

	/**
	 * [destroy description]
	 * 
	 * @param  [type] $id    [description]
	 * @param  [type] $biaya [description]
	 * @return [type]        [description]
	 */
	public function destroy($id, $biaya)
	{
		$this->biayaRepository->delete($biaya);
		Flash::success('Biaya pelaksanaan berhasil dihapus');
		return redirect()->route('dashboard.pelaksanaan.{id}.biaya.index', ['id' => $id]);
	}
}
