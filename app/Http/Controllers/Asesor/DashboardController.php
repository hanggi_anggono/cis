<?php namespace Certification\Http\Controllers\Asesor;

use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DashboardController extends Controller {

	public function index()
	{
		$data['title'] = 'Dashboard';
		$data['subtitle'] = 'Asesor';
		return view('backend.Asesor.dashboard', compact('data'));
	}
}
