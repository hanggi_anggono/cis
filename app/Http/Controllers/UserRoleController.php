<?php namespace Certification\Http\Controllers;

use Certification\Http\Controllers\Controller;
use Certification\Repositories\Userrole\EloquentDbUserrole;
use Certification\Repositories\Role\EloquentDbRole;
use Certification\Repositories\Permission\EloquentDbPermission;
use Certification\Http\Requests\StoreMasterUserroleRequest as RequestValidator;
use Certification\Http\Requests\UpdateMasterUserroleRequest as RequestUpdateValidator;
use Illuminate\Http\Request;
use \Config, \Flash;

class UserRoleController extends Controller {

	private $title;
	private $userrole;
	private $roleRepository;
	private $permissionRepository;

	/**
	 * [__construct description]
	 * 
	 * @param ELoquentDbUserrole $userrole [description]
	 */
	public function __construct(ELoquentDbUserrole $userrole, EloquentDbRole $roleRepository, EloquentDbPermission $permissionRepository)
	{
		$this->userrole             = $userrole;
		$this->roleRepository       = $roleRepository;
		$this->permissionRepository = $permissionRepository;
		$this->title                = 'Management User Role';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar User Role';
		$data['users']			   = $this->userrole->all('paginate');
		$data['roles']             = $this->roleRepository->all();
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		// dd($data['users']);
		return view('backend.userrole.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Daftar User Role';
		$data['roles']    = $this->roleRepository->lists();
		return view('backend.userrole.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->userrole->store($request->except('_token'));
		if ($affected) {
			Flash::success('User berhasil disimpan');
			return redirect()->route('dashboard.master.userrole.index');
		}
		Flash::error('Error, User gagal ditambahkan');
		return redirect()->back();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Daftar User Role';
		$data['user']     = $this->userrole->getById($id);
		$data['roles']    = $this->roleRepository->lists();
		return view('backend.userrole.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestUpdateValidator $request, $id)
	{
		$affected = $this->userrole->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('User berhasil diperbarui');
			return redirect()->route('dashboard.master.userrole.index');
		}
		Flash::error('Error, User gagal diperbarui');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->userrole->delete($id);
		Flash::success('User berhasil dihapus');
		return redirect()->route('dashboard.master.userrole.index');
	}

	/**
	 * [setPermission description]
	 * 
	 * @param [type] $role_id [description]
	 */
	public function setPermission($role_id)
	{
		$data['permission_role'] = [];
		$data['title']           = $this->title;
		$data['subtitle']        = 'Permission Role';
		$data['role']            = $this->roleRepository->getById($role_id);
		$data['permissions']     = $this->permissionRepository->all();
		foreach ($data['role']->permissions()->get() as $permission) {
			$data['permission_role'][] = $permission->id;
		}
		return view('backend.userrole.permission', compact('data'));
	}

	/**
	 * [updatePermission description]
	 * 
	 * @param  Request $request [description]
	 * @param  [type]  $role_id [description]
	 * @return [type]           [description]
	 */
	public function updatePermission(Request $request, $role_id)
	{
		$affected = $this->roleRepository->syncPermission($role_id, $request->get('permission_id'));
		if ($affected) {
			Flash::success('Permission berhasil diperbarui');
			return redirect()->route('dashboard.master.userrole.role.permission', ['role_id' => $role_id]);
		}
		Flash::error('Error, Permission gagal diperbarui');
		return redirect()->back();
	}

}
