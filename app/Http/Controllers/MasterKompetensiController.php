<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterKompetensiRequest;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\Bidang\InterfaceSubbidangRepository;

use Illuminate\Http\Request as Request;
use \Config, \Flash;

class MasterKompetensiController extends Controller {

	/**
	 * Kompetensi Repository
	 * @var interface
	 * 
	 */
	private $kompetensiRepository;
	
	/**
	 * Module title
	 * 
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param InterfaceKompetensiRepository $kompetensiRepository kompetensi Repository
	 */
	public function __construct(InterfaceKompetensiRepository $kompetensiRepository)
	{
		$this->kompetensiRepository = $kompetensiRepository;
		$this->title                = 'Manajemen Kompetensi';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request Http request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master kompetensi';
		$data['kompetensi']        = $this->kompetensiRepository->all('paginate');
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		
		return view('backend.masterkompetensi.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param InterfaceSubbidangRepository $subbidang Subbidang repository
	 * @return Response
	 */
	public function create(InterfaceSubbidangRepository $subbidang)
	{
		$data['title']     = $this->title;
		$data['subtitle']  = "Tambah data master kompetensi";
		$data['subbidang'] = $subbidang->lists();
		return view('backend.masterkompetensi.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 * 
	 * @param StoreMasterKompetensi $request Filter and request validator
	 * @return Response
	 */
	public function store(StoreMasterKompetensiRequest $request)
	{
		$affected = $this->kompetensiRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data kompetensi berhasil disimpan');
			return redirect()->route('dashboard.master.kompetensi.index');
		}
		Flash::error('Data kompetensi gagal disimpan');
		return redirect()->route('dashboard.master.kompetensi.create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param InterfaceSubbidangRepository $subbidang Subbidang repository
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(InterfaceSubbidangRepository $subbidang, $id)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = "Edit data master kompetensi";
		$data['subbidang']  = $subbidang->lists();
		$data['kompetensi'] = $this->kompetensiRepository->getById($id);
		return view('backend.masterkompetensi.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(StoreMasterKompetensiRequest $request, $id)
	{
		$affected = $this->kompetensiRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data kompetensi berhasil diperbarui');
			return redirect()->route('dashboard.master.kompetensi.index');
		}
		Flash::error('Data kompetensi gagal diperbarui');
		return redirect()->route('dashboard.master.kompetensi.edit',['id'=>$id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->kompetensiRepository->delete($id);
		Flash::success('Data kompetensi berhasil dihapus');
		return redirect()->route('dashboard.master.kompetensi.index');
	}

	/**
	 * Get array list of kompetensi resource by subbidang
	 * 
	 * @param  Integer $id Subbidang id
	 * @return JSON
	 */
	public function apiGetBySubbidang($id)
	{
		return $this->kompetensiRepository->getBySubbidang($id);
	}

	/**
	 * [apiGetBelongsToBidang description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function apiGetBelongsToBidang($id)
	{
		return $this->kompetensiRepository->getBidangs($id);
	}

}
