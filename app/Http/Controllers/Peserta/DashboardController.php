<?php namespace Certification\Http\Controllers\Peserta;

use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DashboardController extends Controller {

	public function index()
	{
		$data['title'] = 'Dashboard';
		$data['subtitle'] = 'Peserta';
		return view('backend.Peserta.dashboard', compact('data'));
	}
}
