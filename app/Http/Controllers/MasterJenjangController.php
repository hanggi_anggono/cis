<?php namespace Certification\Http\Controllers;

use Certification\Http\Requests\StoreMasterJenjangRequest as RequestValidator;
use Certification\Http\Controllers\Controller;
use Certification\Repositories\Jenjang\InterfaceJenjangRepository;
use \Config, \Flash;

use Illuminate\Http\Request;

/**
 * Class controller to handle Master jenjang data
 */
class MasterJenjangController extends Controller {

	/**
	 * Jenjang repository
	 * @var interface
	 */
	private $jenjangRepository;
	
	/**
	 * Module title
	 * @var String
	 */
	private $title;

	/**
	 * Class instance
	 * 
	 * @param InterfaceJenjangRepository $jenjangRepository Jenjang repository
	 * @return void
	 */
	public function __construct(InterfaceJenjangRepository $jenjangRepository)
	{
		$this->jenjangRepository = $jenjangRepository;
		$this->title             = 'Manajemen Jenjang Jabatan';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request Http request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master jenjang';
		$data['jenjang']           = $this->jenjangRepository->all('paginate');
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		return view('backend.masterjenjang.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah master jenjang';
		return view('backend.masterjenjang.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request Form request validator
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->jenjangRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data jenjang jabatan berhasil disimpan');
			return redirect()->route('dashboard.master.jenjang.index');
		}
		Flash::error('Data jenjang jabatan gagal disimpan');
		return redirect()->route('dashboard.master.jenjang.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit master jenjang';
		$data['jenjang']  = $this->jenjangRepository->getById($id);
		return view('backend.masterjenjang.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request Form request validator
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->jenjangRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data jenjang jabatan berhasil dirubah');
			return redirect()->route('dashboard.master.jenjang.index');
		}
		Flash::error('Data jenjang jabatan gagal dirubah');
		return redirect()->route('dashboard.master.jenjang.edit', ['id'=>$id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->jenjangRepository->delete($id);
		Flash::success('Data berhasil dihapus');
		return redirect()->route('dashboard.master.jenjang.index');
	}

}
