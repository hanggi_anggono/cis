<?php namespace Certification\Http\Controllers\Registration;

use Certification\Http\Controllers\Controller;
use Certification\Http\Requests\Register\StoreFormRequest as RequestValidator;
use Certification\Services\DataSupportMasterService as DataService;
use Certification\Services\StoreRegisterService;
use Certification\Repositories\Bidang\EloquentDbBidang;
use Certification\Repositories\Register\EloquentDbRegistration;

use Illuminate\Http\Request;

use Flash, Config;

class RegistrationController extends Controller {

	/**
	 * [$bidang description]
	 * 
	 * @var [type]
	 */
	private $bidang;

	/**
	 * [$registerRepository description]
	 * 
	 * @var [type]
	 */
	private $registerRepository;

	/**
	 * [$pagination description]
	 * 
	 * @var [type]
	 */
	private $pagination;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(EloquentDbBidang $bidang, EloquentDbRegistration $registerRepository)
	{
		$this->bidang             = $bidang;
		$this->registerRepository = $registerRepository;
		$this->pagination         = Config::get('certification.default_pagination_count');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('front.index');
	}

	/**
	 * [register description]
	 * 
	 * @return [type] [description]
	 */
	public function register(DataService $dataService)
	{
		$bidang = $this->bidang->lists();
		$data['support']  = $dataService->forMasterAsesor();
		return view('front.reg', compact('bidang', 'data'));
	}

	/**
	 * [storeRegister description]
	 * 
	 * @param  RequestValidator $request [description]
	 * @return [type]                    [description]
	 */
	public function storeRegister(RequestValidator $request, StoreRegisterService $storeService)
	{
		if($storeService->save($request->except('_token'))) {
			Flash::success('Permintaan uji kompetensi anda berhasil disimpan, silahkan tunggu untuk pemberitahuan selanjutnya.');
			return redirect()->route('register.index');
		}
		$request->flashExcept('_token');
		Flash::error('Terjadi kesalahan. Permintaan gagal disimpan.');
		return redirect()->back();
	}

	/**
	 * [registrationDashboardIndex description]
	 * 
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function registrationDashboardIndex(Request $request)
	{
		$data['title']      = 'Daftar Permintaan Uji Kompetensi';
		$data['subtitle']   = '';
		$data['registers']  = $this->registerRepository->all('paginate', $request);
		$data['pagination'] = numbering_pagination($request, $this->pagination);
		return view('backend.registration.index', compact('data'));
	}

	/**
	 * [registrationDashboardShow description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function registrationDashboardShow($id)
	{
		$data['title']    = 'Detail Calon Peserta Permintaan Uji Kompetensi';
		$data['subtitle'] = '';
		$data['register'] = $this->registerRepository->find($id);
		return view('backend.registration.show', compact('data'));
	}

}
