<?php namespace Certification\Http\Controllers;

use Certification\Repositories\Peserta\InterfacePesertaRepository;
use Certification\Http\Requests\StoreMasterPesertaRequest as RequestValidator;
use Certification\Services\DataSupportMasterService as DataService;
use Certification\Services\StorePesertaService as StoreService;
use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash, Config;

class MasterPesertaController extends Controller {

	private $pesertaRepository;
	private $title;

	public function __construct(InterfacePesertaRepository $pesertaRepository)
	{
		$this->pesertaRepository = $pesertaRepository;
		$this->title             = 'Manajemen Peserta';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar master peserta';
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		$data['peserta']            = $this->pesertaRepository->all('paginate', $request);
		// dd($data['peserta']);
		return view('backend.masterpeserta.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(DataService $dataService)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah master peserta';
		$data['support']  = $dataService->forMasterAsesor();
		return view('backend.masterpeserta.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request, StoreService $store)
	{
		$affected = $store->save($request->except('_token'));
		$request->flashExcept('_token');
		if ($affected) {
			Flash::success('Data peserta berhasil disimpan');
			return redirect()->route('dashboard.master.peserta.index');
		}
		Flash::error('Data peserta gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Detail master peserta';
		$data['peserta']   = $this->pesertaRepository->getById($id);
		return view('backend.masterpeserta.show', compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(DataService $dataService, $id)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Edit master peserta';
		$data['peserta']   = $this->pesertaRepository->getById($id);
		$data['support']  = $dataService->forMasterAsesor();
		return view('backend.masterpeserta.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, StoreService $store, $id)
	{
		$affected = $store->save($request->except('_token','_method'),'update',$id);
		if ($affected) {
			Flash::success('Data peserta berhasil dirubah');
			return redirect()->route('dashboard.master.peserta.index');
		}
		Flash::error('Data peserta gagal dirubah');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->pesertaRepository->delete($id);
		Flash::success('Data berhasil dihapus');
		return redirect()->route('dashboard.master.peserta.index');
	}

	/**
	 * [apiDetail description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function apiDetail($id)
	{
		return $this->pesertaRepository->getById($id);
	}

}
