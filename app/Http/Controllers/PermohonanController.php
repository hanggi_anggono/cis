<?php namespace Certification\Http\Controllers;

use Certification\Repositories\Permohonan\EloquentDbPermohonan;
use Certification\Repositories\Permohonan\EloquentDbKompetensipermohonan;
use Certification\Repositories\Peserta\InterfacePesertaRepository;
use Certification\Repositories\Unit\InterfaceUindukRepository;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\Bidang\InterfaceBidangRepository;
use Certification\Http\Requests\Permohonan\StoreRequest as RequestValidator;
use Certification\Http\Controllers\Controller;

use Illuminate\Http\Request;
use \Config, \Flash, \Session;

class PermohonanController extends Controller {

	/**
	 * [$permohonanRepository description]
	 * 
	 * @var [type]
	 */
	private $permohonanRepository;
	
	/**
	 * [$title description]
	 * 
	 * @var [type]
	 */
	private $title;

	public function __construct(EloquentDbPermohonan $permohonanRepository)
	{
		$this->permohonanRepository = $permohonanRepository;
		$this->title                = 'Manajemen Permohonan';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(EloquentDbKompetensipermohonan $pivot, Request $request)
	{
		$data['title']             = $this->title;
		$data['subtitle']          = 'Daftar Permohonan';
		$data['permohonan']        = $this->permohonanRepository->all('paginate', Session::get('params'));
		$data['peserta']		   = $pivot->all();
		$data['pagination_number'] = numbering_pagination($request, Config::get('certification.default_pagination_count'));
		return view('backend.permohonan.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(InterfaceUindukRepository $unitinduk)
	{
		$data['title']    = $this->title;
		$data['subtitle'] = 'Tambah Permohonan';
		$data['unitinduk'] = $unitinduk->lists();
		return view('backend.permohonan.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RequestValidator $request)
	{
		$affected = $this->permohonanRepository->store($request->except('_token'));
		if ($affected) {
			Flash::success('Data Permohonan berhasil disimpan');
			return redirect()->route('dashboard.permohonan.index');
		}
		Flash::error('Data Permohonan gagal disimpan');
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(InterfaceUindukRepository $unitinduk, $id)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Edit Permohonan';
		$data['unitinduk']  = $unitinduk->lists();
		$data['permohonan'] = $this->permohonanRepository->getById($id);
		return view('backend.permohonan.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RequestValidator $request, $id)
	{
		$affected = $this->permohonanRepository->update($id, $request->except('_token','_method'));
		if ($affected) {
			Flash::success('Data Permohonan berhasil dirubah');
			return redirect()->route('dashboard.permohonan.index');
		}
		Flash::error('Data Permohonan gagal dirubah');
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->permohonanRepository->delete($id);
		Flash::success('Data Permohonan berhasil dihapus');
		return redirect()->route('dashboard.permohonan.index');
	}

	/**
	 * [createKompetensi description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function indexKompetensi($id)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Daftar Kompetensi Permohonan';
		$data['permohonan'] = $this->permohonanRepository->getById($id);
		return view('backend.permohonan.indexKompetensi', compact('data'));
	}

	/**
	 * [createKompetensi description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function createKompetensi(InterfaceKompetensiRepository $kompetensi, InterfaceBidangRepository $bidang, $id)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Tambah Kompetensi Permohonan';
		$data['permohonan'] = $this->permohonanRepository->getById($id);
		$data['kompetensi'] = $kompetensi->lists();
		$data['bidang']     = $bidang->lists();
		return view('backend.permohonan.createKompetensi', compact('data'));
	}

	/**
	 * [storeKompetensi description]
	 * 
	 * @param  Request $request [description]
	 * @param  [type]  $id      [description]
	 * @return [type]           [description]
	 */
	public function storeKompetensi(Request $request, $id)
	{
		$response = $this->permohonanRepository->storeKompetensi($id, $request->get('kompetensi_id'));
		if ($response['requested'] > 0) {
			if (count($response['failed']) == 0) {
				Flash::success('Data kompetensi berhasil dimasukkan');
			} else {
				$kompetensi = '';
				foreach ($response['failed'] as $value) {
					$kompetensi .= $value.', ';
				}
				Flash::warning('Data kompetensi '.$kompetensi.' sudah ada pada permohonan ini');
			}
			return redirect()->route('dashboard.permohonan.kompetensi.index', ['id' => $id]);
		}
		Flash::error('Pilihan kompetensi tidak boleh kosong. Minimal 1 kompetensi dipilih');
		return redirect()->back();
	}

	/**
	 * [deleteKompetensi description]
	 * 
	 * @param  [type] $id           [description]
	 * @param  [type] $kompetensiid [description]
	 * @return [type]               [description]
	 */
	public function deleteKompetensi($id, $kompetensiid)
	{
		$this->permohonanRepository->deleteKompetensi($id, $kompetensiid);
		Flash::success('Data kompetensi berhasil dihapus');
		return redirect()->route('dashboard.permohonan.kompetensi.index', ['id' => $id]);
	}

	/**
	 * [indexKompetensiPeserta description]
	 * 
	 * @param  EloquentDbKompetensipermohonan $pivot        [description]
	 * @param  InterfaceKompetensiRepository  $kompetensi   [description]
	 * @param  [type]                         $id           [description]
	 * @param  [type]                         $kompetensiid [description]
	 * @return [type]                                       [description]
	 */
	public function indexKompetensiPeserta(EloquentDbKompetensipermohonan $pivot, InterfaceKompetensiRepository $kompetensi, $id, $kompetensiid)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Tambah Peserta Permohonan';
		$data['permohonan'] = $this->permohonanRepository->getById($id);
		$data['kompetensi'] = $kompetensi->getById($kompetensiid);
		foreach ($data['permohonan']->kompetensipermohonan as $key => $value) {
			if($value->kompetensi_id == $kompetensiid) {
				$data['peserta']    = $pivot->find($value->id);
			}
		}
		return view('backend.permohonan.indexkompetensipeserta', compact('data'));
	}

	/**
	 * [createKompetensiPeserta description]
	 * 
	 * @param  InterfacePesertaRepository    $peserta      [description]
	 * @param  InterfaceKompetensiRepository $kompetensi   [description]
	 * @param  [type]                        $id           [description]
	 * @param  [type]                        $kompetensiid [description]
	 * @return [type]                                      [description]
	 */
	public function createKompetensiPeserta(InterfacePesertaRepository $peserta, InterfaceKompetensiRepository $kompetensi, $id, $kompetensiid)
	{
		$data['title']      = $this->title;
		$data['subtitle']   = 'Tambah Peserta Permohonan';
		$data['permohonan'] = $this->permohonanRepository->getById($id);
		$data['kompetensi'] = $kompetensi->getById($kompetensiid);
		$data['peserta']    = $peserta->allNoRelationship();
		foreach ($data['permohonan']->kompetensipermohonan as $key => $value) {
			if($value->kompetensi_id == $kompetensiid) {
				$data['kompetensipermohonan_id'] = $value->id;
			}
		}
		return view('backend.permohonan.createkompetensipeserta', compact('data'));
	}

	/**
	 * [storeKompetensiPeserta description]
	 * 
	 * @param  Request $request      [description]
	 * @param  [type]  $id           [description]
	 * @param  [type]  $kompetensiid [description]
	 * @return [type]                [description]
	 */
	public function storeKompetensiPeserta(Request $request, $id, $kompetensiid)
	{
		$response = $this->permohonanRepository->storeKompetensiPeserta($id, $request->get('kompetensipermohonan_id'), $request->get('peserta_id'));
		if ($response['requested'] > 0) {
			if (count($response['failed']) == 0) {
				Flash::success('Data peserta berhasil ditambahkan');
			} else {
				$pesertaNIP = '';
				foreach ($response['failed'] as $value) {
					$pesertaNIP .= $value.', ';
				}
				Flash::warning('Data peserta '.$pesertaNIP.'sudah ada pada permohonan kompetensi ini.');
			}
			return redirect()->route('dashboard.permohonan.kompetensi.peserta.index', ['id' => $id, 'kompetensiid' => $kompetensiid]);
		}
		Flash::error('Pilihan peserta tidak boleh kosong. Minimal 1 peserta dipilih');
		return redirect()->back();
		
	}

	/**
	 * [deleteKompetensi description]
	 * 
	 * @param  [type] $id           [description]
	 * @param  [type] $pesertaid [description]
	 * @return [type]               [description]
	 */
	public function deleteKompetensiPeserta($id, $kompetensiid, $pesertaid)
	{
		$this->permohonanRepository->deleteKompetensiPeserta($id, $pesertaid);
		Flash::success('Data peserta berhasil dihapus');
		return redirect()->route('dashboard.permohonan.kompetensi.peserta.index', ['id' => $id, 'kompetensiid' => $kompetensiid]);
	}

}
