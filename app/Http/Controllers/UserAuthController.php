<?php namespace Certification\Http\Controllers;

use Certification\Http\Controllers\Controller;
use Certification\Repositories\Authentication\AuthenticationRepository;
use Certification\Http\Requests\UpdatePasswordRequest as RequestValidator;

use \Auth, \Flash;
use Illuminate\Http\Request;

class UserAuthController extends Controller {

	/**
	 * [$authRepository description]
	 * 
	 * @var [type]
	 */
	private $authRepository;
	
	/**
	 * [__construct description]
	 * 
	 * @param AuthenticationRepository $authRepository [description]
	 */
	public function __construct(AuthenticationRepository $authRepository)
	{
		$this->authRepository = $authRepository;
	}

	/**
	 * [index description]
	 * 
	 * @return [type] [description]
	 */
	public function index()
	{
		return view('backend.auth.signin');
	}

	/**
	 * [signingin description]
	 * 
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function signingin(Request $request)
	{
		if($this->authRepository->signin($request->except('_token'))) {
			return redirect()->intended($this->authRepository->getRedirect());
		}
		Flash::error('Username atau password salah');
		return redirect()->back();
	}

	/**
	 * [signingout description]
	 * 
	 * @return [type] [description]
	 */
	public function signingout()
	{
		$this->authRepository->signout();
		Flash::info('Session anda telah berakhir');
		return redirect()->route('user.signin.index');
	}

	/**
	 * [changePassword description]
	 * 
	 * @return [type] [description]
	 */
	public function changePassword()
	{
		$data['title']    = 'Manajemen Password';
		$data['subtitle'] = 'Ganti password';
		return view('backend.auth.changepassword', compact('data')); 
	}

	/**
	 * [storeChangePassword description]
	 * 
	 * @return [type] [description]
	 */
	public function updatePassword(RequestValidator $request)
	{
		$affected = $this->authRepository->updatePassword($request->get('user_id'), $request->except('_token', '_method'));
		if ($affected) {
			Flash::success('Password berhasil diperbarui');
			return redirect()->route('dashboard.password.change');
		}
		Flash::error($this->authRepository->getErrors());
		return redirect()->back();
	}

}
