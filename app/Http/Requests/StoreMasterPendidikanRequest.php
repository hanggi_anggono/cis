<?php namespace Certification\Http\Requests;

use Certification\Http\Requests\Request;

/**
 *  Class handler for validation service in master pendidikan store / update request
 */
class StoreMasterPendidikanRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nama_pendidikan' => 'required'
		];
	}

}
