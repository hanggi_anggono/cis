<?php namespace Certification\Http\Requests\Penjadwalan;

use Certification\Http\Requests\Request;

class StorePenjadwalanRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'tanggal_mulai'         => 'required',
			'tanggal_selesai'       => 'required',
			'udiklat_id'            => 'required',
			'unitinduk_id'          => 'required',
			'lembagasertifikasi_id' => 'required',
			'ketua'                 => 'required'
		];
	}

}
