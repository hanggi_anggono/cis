<?php namespace Certification\Http\Requests;

use Certification\Http\Requests\Request;

class StoreMasterUnitindukRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'udiklat_id'     => 'required|numeric',
			'nama_unitinduk' => 'required'
		];
	}

}
