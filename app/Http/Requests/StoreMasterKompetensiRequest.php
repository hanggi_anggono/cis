<?php namespace Certification\Http\Requests;

use Certification\Http\Requests\Request;

class StoreMasterKompetensiRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'subbidang_id'    => 'required|numeric',
			'nama_kompetensi' => 'required'
		];
	}

}
