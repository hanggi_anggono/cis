<?php namespace Certification\Http\Requests\Register;

use Certification\Http\Requests\Request;

class StoreFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'kompetensi_id'     => 'required|integer',
			'nip'               => 'required',
			'no_identitas'      => 'required',
			'nama'              => 'required',
			'jenjangjabatan_id' => 'required|integer',
			'grade_id'          => 'required|integer',
			'unitinduk_id'      => 'integer',
			'foto'              => 'image|max:200',
			'kondisi_khusus'    => 'required'
		];
	}

	/**
	 * [messages description]
	 * 
	 * @return [type] [description]
	 */
	public function messages()
	{
		return [
			'required' => 'Field :attribute tidak boleh kosong',
			'email'    => 'Field :attribute harus berupa format email',
			'integer'  => 'Field kompetensi belum dipilih'
		];
	}

}
