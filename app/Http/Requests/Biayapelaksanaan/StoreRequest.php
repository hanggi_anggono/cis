<?php namespace Certification\Http\Requests\Biayapelaksanaan;

use Certification\Http\Requests\Request;

class StoreRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'penjadwalan_id'   => 'required|integer',
			'komponenbiaya_id' => 'integer',
			'biaya'            => 'required|numeric'
		];
	}

	/**
	 * Get the validation messages that apply to the request.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'numeric' => 'Field :attribute must be numeric value'
		];
	}

}
