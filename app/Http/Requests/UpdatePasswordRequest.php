<?php namespace Certification\Http\Requests;

use Certification\Http\Requests\Request;

class UpdatePasswordRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'current_password'      => 'required',
			'password'              => 'required|min:6|confirmed',
			'password_confirmation' => 'required|min:6'
		];
	}

	/**
	 * [messages description]
	 * 
	 * @return [type] [description]
	 */
	public function messages()
	{
		return [
			'min'       => 'Field :attribute minimal 6 karakter',
			'confirmed' => 'Field konfirmasi password tidak sama dengan password baru yang dimasukkan'
		];
	}

}
