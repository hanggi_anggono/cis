<?php namespace Certification\Http\Requests;

use Certification\Http\Requests\Request;

class StoreMasterPesertaRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nip'               => ($this->request->has('_method')) ? 'required|unique:peserta,id,'.$this->segment(4) : 'required|unique:peserta',
			'no_identitas'      => 'required',
			'nama'              => 'required',
			'jenjangjabatan_id' => 'required|integer',
			'grade_id'          => 'required|integer',
			'unitinduk_id'      => 'integer',
			'unitcabang_id'     => 'integer',
			'unitranting_id'    => 'integer',
			'foto'              => 'image|max:200'
		];
	}

	public function messages()
	{
		return [
			'unitinduk_id.not_in'   => 'Unit induk belum dipilih',
			'unitcabang_id.not_in'  => 'Unit cabang belum dipilih',
			'unitranting_id.not_in' => 'Unit ranting belum dipilih',
		];
	}

}
