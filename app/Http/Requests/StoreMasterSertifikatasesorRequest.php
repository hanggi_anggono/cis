<?php namespace Certification\Http\Requests;

use Certification\Http\Requests\Request;

class StoreMasterSertifikatasesorRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'no_registrasi'     => 'required',
			'no_sertifikat'     => 'required',		
			'lembaga_id' 		=> 'required|numeric',
			'asesor_id' 		=> 'required|numeric'
		];
	}

}
