<?php namespace Certification\Services;

use Certification\Repositories\Unit\InterfaceUindukRepository;
use Certification\Repositories\Unit\InterfaceUcabangRepository;
use Certification\Repositories\Unit\InterfaceUrantingRepository;
use Certification\Repositories\Unit\InterfaceUdiklatRepository;
use Certification\Repositories\Unit\EloquentDbUnonpln;
use Certification\Repositories\Pendidikan\InterfacePendidikanRepository;
use Certification\Repositories\Grade\InterfaceGradeRepository;
use Certification\Repositories\Jenjang\InterfaceJenjangRepository;
use Certification\Repositories\Administrator\InterfaceAdministratorRepository;
use Certification\Repositories\Asesor\InterfaceAsesorRepository;
use Certification\Repositories\LembagaSertifikasi\InterfaceLembagaSertifikasiRepository;
use Certification\Repositories\Bidang\InterfaceSubbidangRepository;


class DataSupportMasterService {

	private $udiklatRepository;
	private $uindukrepository;
	private $ucabangRepository;
	private $urantingRepository;
	private $jenjangRepository;
	private $pendidikanRepository;
	private $gradeRepository;
	private $unonplnRepository;
	private $administratorRepository;
	private $asesorRepository;
	private $lembagaSertifikasiRepository;
	private $subbidangRepository;

	/**
	 * [__construct description]
	 * 
	 * @param InterfaceUindukRepository             $uindukrepository             [description]
	 * @param InterfaceUcabangRepository            $ucabangRepository            [description]
	 * @param InterfaceUrantingRepository           $urantingRepository           [description]
	 * @param InterfacePendidikanRepository         $pendidikanRepository         [description]
	 * @param InterfaceGradeRepository              $gradeRepository              [description]
	 * @param InterfaceJenjangRepository            $jenjangRepository            [description]
	 * @param EloquentDbUnonpln                     $unonplnRepository            [description]
	 * @param InterfaceAdministratorRepository      $administratorRepository      [description]
	 * @param InterfaceAsesorRepository             $asesorRepository             [description]
	 * @param InterfaceLembagaSertifikasiRepository $lembagaSertifikasiRepository [description]
	 * @param InterfaceUdiklatRepository            $udiklatRepository            [description]
	 * @param InterfaceSubbidangRepository          $subbidangRepository          [description]
	 */
	public function __construct(InterfaceUindukRepository $uindukrepository, InterfaceUcabangRepository $ucabangRepository, InterfaceUrantingRepository $urantingRepository, InterfacePendidikanRepository $pendidikanRepository, InterfaceGradeRepository $gradeRepository, InterfaceJenjangRepository $jenjangRepository, EloquentDbUnonpln $unonplnRepository, InterfaceAdministratorRepository $administratorRepository, InterfaceAsesorRepository $asesorRepository, InterfaceLembagaSertifikasiRepository $lembagaSertifikasiRepository, InterfaceUdiklatRepository $udiklatRepository, InterfaceSubbidangRepository $subbidangRepository)
	{
		$this->udiklatRepository       = $udiklatRepository;
		$this->uindukrepository        = $uindukrepository;
		$this->ucabangRepository       = $ucabangRepository;
		$this->urantingRepository      = $urantingRepository;
		$this->jenjangRepository       = $jenjangRepository;
		$this->pendidikanRepository    = $pendidikanRepository;
		$this->gradeRepository         = $gradeRepository;
		$this->unonplnRepository       = $unonplnRepository;
		$this->administratorRepository = $administratorRepository;
		$this->asesorRepository        = $asesorRepository;
		$this->lembagaSertifikasiRepository = $lembagaSertifikasiRepository;
		$this->subbidangRepository     = $subbidangRepository;
	}

	/**
	 * Data support for master asesor resource
	 * 
	 * @return Array
	 */
	public function forMasterAsesor()
	{
		$data['uinduk']     = $this->uindukrepository->lists();
		$data['ucabang']    = $this->ucabangRepository->lists();
		$data['uranting']   = $this->urantingRepository->lists();
		$data['jenjang']    = $this->jenjangRepository->lists();
		$data['pendidikan'] = $this->pendidikanRepository->lists();
		$data['grade']      = $this->gradeRepository->lists();
		$data['nonpln']     = $this->unonplnRepository->lists();
		
		return $data;
	}

	/**
	 * Data support for master administrator resource
	 * 
	 * @return Array
	 */
	public function forMasterAdministrator()
	{
		$data['uinduk']     = $this->uindukrepository->lists();
		$data['ucabang']    = $this->ucabangRepository->lists();
		$data['uranting']   = $this->urantingRepository->lists();
		$data['jenjang']    = $this->jenjangRepository->lists();
		$data['pendidikan'] = $this->pendidikanRepository->lists();
		$data['grade']      = $this->gradeRepository->lists();
		$data['nonpln']     = $this->unonplnRepository->lists();
		
		return $data;
	}	

	/**
	 * [forPenjadwalan description]
	 * 
	 * @return Array
	 */
	public function forPenjadwalan($lskId = null, $subbidangId = null)
	{
		$data['udiklat']            = $this->udiklatRepository->lists();
		$data['uinduk']             = $this->uindukrepository->lists();
		$data['ucabang']            = $this->ucabangRepository->lists();
		$data['uranting']           = $this->urantingRepository->lists();
		$data['subbidang']          = $this->subbidangRepository->lists();
		$data['administrator']      = $this->administratorRepository->lists($lskId);
		$data['asesor']             = $this->asesorRepository->lists($lskId, $subbidangId);
		$data['lembagasertifikasi'] = $this->lembagaSertifikasiRepository->lists();
		
		return $data;
	}

}
