<?php namespace Certification\Services;

use \PDF, \Excel;

class ExportService {

	/**
	 * [export description]
	 * 
	 * @param  [type] $collection [description]
	 * @param  string $type       [description]
	 * @param  array  $parameters [viewName, orientation, filename]
	 * @return [type]             [description]
	 */
	public function export($collection, $type = 'excel', $parameters = [])
	{
		$data['collection'] = $collection;
		$data['parameters'] = $parameters;
		$data['paper']      = 'a4';
		if (isset($data['parameters']['paper'])) {
			$data['paper'] = $data['parameters']['paper'];
		}
		
		if ($type == 'pdf') {

			$pdf = PDF::loadView('backend.exported.'.$data['parameters']['viewName'], compact('data'))
						->setPaper($data['paper'])
						->setOrientation($data['parameters']['orientation']);

			return $pdf->download($data['parameters']['filename'].'_'.date('Y-m-d').'.pdf');
		} else {
			
			Excel::create($data['parameters']['filename'].'_'.date('Y-m-d'), function($excel) use($data)
			{
				$excel->sheet($data['parameters']['filename'].'_'.date('Y-m-d'), function($sheet) use($data)
				{
					$sheet->loadView('backend.exported.'.$data['parameters']['viewName'], compact('data'));
				});
			})->export('xls');
		
		}
	}
}
