<?php namespace Certification\Services;

use Certification\Repositories\Peserta\InterfacePesertaRepository;

/**
 * class service for store peserta data
 */
class StorePesertaService {

	/**
	 * Peserta repository
	 * 
	 * @var Interface
	 */
	private $pesertaRepository;

	/**
	 * Class instance
	 * 
	 * @param InterfacePesertaRepository $pesertaRepository peserta repository
	 */
	public function __construct(InterfacePesertaRepository $pesertaRepository)
	{
		$this->pesertaRepository = $pesertaRepository;
	}

	/**
	 * process data, upload handler and save data to database
	 * 
	 * @param  Array $request array of request data
	 * @param  String $option save option
	 * @param  Integer $id asesor id
	 * @return Boolean
	 */
	public function save($request, $option = 'store', $id = null)
	{
		if (isset($request['foto'])) {
			$extension = $request['foto']->getClientOriginalExtension();
			$imagename = 'photo_'.$request['nip'].'.'.$extension;
			// save / move to directory
			$request['foto']->move('resources/pesertaphotos/',$imagename);
			// generate new array
			array_set($request, 'photo', $imagename);
			// forget old request
			array_forget($request, 'foto');
		}
		
		// arrange data before storing to database
		array_set($request, 'tanggal_lahir', reformatBirthDate($request));
		
		// remove partial date birth
		array_forget($request, 'dd');
		array_forget($request, 'mm');
		array_forget($request, 'yy');

		if (strtolower($option) == 'update' && isset($id)) {
			return $this->pesertaRepository->update($id, $request);
		}
		return $this->pesertaRepository->store($request);
	}

}
