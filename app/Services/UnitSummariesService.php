<?php namespace Certification\Services;

use Certification\Repositories\Unit\InterfaceUdiklatRepository;
use Certification\Repositories\Unit\InterfaceUindukRepository;
use Certification\Repositories\Unit\InterfaceUcabangRepository;
use Certification\Repositories\Unit\InterfaceUrantingRepository;
use Certification\Repositories\Unit\EloquentDbUnonpln;

/**
 * Class for master unit summaries service
 */
class UnitSummariesService {

	/**
	 * Interface udiklat repository
	 * 
	 * @var Interface
	 */
	private $udiklatRepository;
	
	/**
	 * Interface unit induk repository
	 * 
	 * @var Interface
	 */
	private $uindukRepository;
	
	/**
	 * Interface unit cabang repository
	 * 
	 * @var Interface
	 */
	private $ucabangRepository;
	
	/**
	 * Interface unit ranting repository
	 * 
	 * @var Interface
	 */
	private $urantingRepository;

	/**
	 * Unit non pln repository
	 * 
	 * @var [type]
	 */
	private $unonplnRepository;

	/**
	 * Class instance
	 * 
	 * @param InterfaceUdiklatRepository  $udiklat            
	 * @param InterfaceUindukRepository   $uindukRepository   
	 * @param InterfaceUcabangRepository  $ucabangRepository  
	 * @param InterfaceUrantingRepository $urantingRepository 
	 */
	public function __construct(InterfaceUdiklatRepository $udiklatRepository, InterfaceUindukRepository $uindukRepository, InterfaceUcabangRepository $ucabangRepository, InterfaceUrantingRepository $urantingRepository, EloquentDbUnonpln $unonplnRepository)
	{
		$this->udiklatRepository  = $udiklatRepository;
		$this->uindukRepository   = $uindukRepository;
		$this->ucabangRepository  = $ucabangRepository;
		$this->urantingRepository = $urantingRepository;
		$this->unonplnRepository  = $unonplnRepository;
	}

	/**
	 * Get all unit data summaries
	 * 
	 * @return Array
	 */
	public function getDataSummaries()
	{
		$data['udiklat']  = $this->udiklatRepository->generalSummary();
		$data['uinduk']   = $this->uindukRepository->generalSummary();
		$data['ucabang']  = $this->ucabangRepository->generalSummary();
		$data['uranting'] = $this->urantingRepository->generalSummary();
		$data['unonpln']  = $this->unonplnRepository->generalSummary();
		return $data;
	}
}
