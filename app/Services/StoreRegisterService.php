<?php namespace Certification\Services;

use Certification\Repositories\Register\EloquentDbRegistration;

/**
 * class service for store peserta data
 */
class StoreRegisterService {

	/**
	 * Register repository
	 * 
	 * @var Interface
	 */
	private $registerRepository;

	/**
	 * Class instance
	 * 
	 * @param InterfacePesertaRepository $pesertaRepository peserta repository
	 */
	public function __construct(EloquentDbRegistration $registerRepository)
	{
		$this->registerRepository = $registerRepository;
	}

	/**
	 * process data, upload handler and save data to database
	 * 
	 * @param  Array $request array of request data
	 * @param  String $option save option
	 * @param  Integer $id asesor id
	 * @return Boolean
	 */
	public function save($request, $option = 'store', $id = null)
	{
		if (isset($request['foto'])) {
			$extension = $request['foto']->getClientOriginalExtension();
			$imagename = 'photo_'.$request['nip'].'.'.$extension;
			// save / move to directory
			$request['foto']->move('resources/register/',$imagename);
			// generate new array
			array_set($request, 'photo', $imagename);
			// forget old request
			array_forget($request, 'foto');
		}
		
		// arrange data before storing to database
		array_set($request, 'tanggal_lahir', reformatBirthDate($request));
		
		// remove partial date birth
		array_forget($request, 'dd');
		array_forget($request, 'mm');
		array_forget($request, 'yy');

		if (strtolower($option) == 'update' && isset($id)) {
			return $this->registerRepository->update($id, $request);
		}
		return $this->registerRepository->store($request);
	}

}
