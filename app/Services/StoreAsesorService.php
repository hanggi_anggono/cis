<?php namespace Certification\Services;

use Certification\Repositories\Asesor\InterfaceAsesorRepository;

/**
 * class service for store asesor data
 */
class StoreAsesorService {

	/**
	 * Asesor repository
	 * 
	 * @var Interface
	 */
	private $asesorRepository;

	/**
	 * Class instance
	 * 
	 * @param InterfaceAsesorRepository $asesorRepository asesor repository
	 */
	public function __construct(InterfaceAsesorRepository $asesorRepository)
	{
		$this->asesorRepository = $asesorRepository;
	}

	/**
	 * process data, upload handler and save data to database
	 * 
	 * @param  Array $request array of request data
	 * @param  String $option save option
	 * @param  Integer $id asesor id
	 * @return Boolean
	 */
	public function save($request, $option = 'store', $id = null)
	{
		if (isset($request['foto'])) {
			$extension = $request['foto']->getClientOriginalExtension();
			$imagename = 'photo_'.$request['nip'].'.'.$extension;
			// save / move to directory
			$request['foto']->move('resources/asesorphotos/',$imagename);
			// generate new array
			array_set($request, 'photo', $imagename);
			// forget old request
			array_forget($request, 'foto');
		}
		
		// arrange data before storing to database
		array_set($request, 'tanggal_lahir', reformatBirthDate($request));
		
		// remove partial date birth
		array_forget($request, 'dd');
		array_forget($request, 'mm');
		array_forget($request, 'yy');

		if (strtolower($option) == 'update' && isset($id)) {
			return $this->asesorRepository->update($id, $request);
		}
		return $this->asesorRepository->store($request);
	}

}
