<?php namespace Certification\Repositories\Unit;

/**
 * Interface for unit ranting repositories
 */
interface InterfaceUrantingRepository {

	/**
	 * Get all unit ranting collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null);

	/**
	 * Get unit ranting by given Id
	 * 
	 * @param  Integer $id unit ranting id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data unit ranting to database
	 * 
	 * @param  Array $data unit ranting
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data unit ranting to database
	 *
	 * @param  Integer $id unit ranting id
	 * @param  Array $data unit ranting
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data unit ranting, with soft delete method or not
	 * 
	 * @param  Integer  $id          unit ranting id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
}
