<?php namespace Certification\Repositories\Unit;

/**
 * Interface for udiklat repositories
 */
interface InterfaceUdiklatRepository {

	/**
	 * Get all udiklat collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null);

	/**
	 * Get udiklat by given Id
	 * 
	 * @param  Integer $id udiklat id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data udiklat to database
	 * 
	 * @param  Array $data udiklat
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data udiklat to database
	 *
	 * @param  Integer $id udiklat id
	 * @param  Array $data udiklat
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data udiklat, with soft delete method or not
	 * 
	 * @param  Integer  $id          udiklat id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
}
