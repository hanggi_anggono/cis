<?php namespace Certification\Repositories\Unit;

use Certification\Repositories\Unit\InterfaceUcabangRepository;
use Certification\Models\Unitcabang;
use Config;

/**
 *  Class DB repository for Master uinduk using Eloquent
 *  implements InterfaceuindukRepository
 */
class EloquentDbUcabang implements InterfaceUcabangRepository {

	/**
	 * uinduk model
	 * 
	 * @var Model
	 */
	private $ucabangModel;

	/**
	 * Class instance
	 * 
	 * @param uinduk $ucabangModel uinduk model
	 * @return void
	 */
	public function __construct(Unitcabang $ucabangModel)
	{
		$this->ucabangModel = $ucabangModel;
	}

	/**
	 * Get all uinduk collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->ucabangModel->withRelationship()->orderBy('nama_unitcabang');
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get uinduk by given Id
	 * 
	 * @param  Integer $id uinduk id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->ucabangModel->withRelationship()->find($id);
	}

	/**
	 * store data uinduk to database
	 * 
	 * @param  Array $data uinduk
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->ucabangModel->create($data);
	}

	/**
	 * update single data uinduk to database
	 *
	 * @param  Integer $id uinduk id
	 * @param  Array $data uinduk
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->ucabangModel->whereId($id)->update($data);
	}

	/**
	 * delete single data uinduk, with soft delete method or not
	 * 
	 * @param  Integer  $id          uinduk id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->ucabangModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get array list of cabang resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->ucabangModel->lists('nama_unitcabang','id');
	}

	/**
	 * Get general summary data of unit cabang
	 * 
	 * @return Collection
	 */
	public function generalSummary()
	{
		return $this->ucabangModel->select('nama_unitcabang','id')->orderBy('created_at','desc')->get();
	}

	/**
	 * Get unit ranting lists from given unit cabang id
	 * 
	 * @param  Integer $id unitcabang id
	 * @return Array
	 */
	public function unitranting($id)
	{
		return $this->ucabangModel->find($id)->unitranting()->lists('nama_unitranting','id');
	}
}
