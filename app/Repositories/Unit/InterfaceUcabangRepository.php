<?php namespace Certification\Repositories\Unit;

/**
 * Interface for unit cabang repositories
 */
interface InterfaceUcabangRepository {

	/**
	 * Get all unit cabang collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null);

	/**
	 * Get unit cabang by given Id
	 * 
	 * @param  Integer $id unit cabang id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data unit cabang to database
	 * 
	 * @param  Array $data unit cabang
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data unit cabang to database
	 *
	 * @param  Integer $id unit cabang id
	 * @param  Array $data unit cabang
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data unit cabang, with soft delete method or not
	 * 
	 * @param  Integer  $id          unit cabang id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
}
