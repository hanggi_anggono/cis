<?php namespace Certification\Repositories\Unit;

use Certification\Repositories\Unit\InterfaceUrantingRepository;
use Certification\Models\Unitranting;
use Config;

/**
 *  Class DB repository for Master unit ranting using Eloquent
 *  implements InterfaceUrantingRepository
 */
class EloquentDbUranting implements InterfaceUrantingRepository {

	/**
	 * unit ranting model
	 * 
	 * @var Model
	 */
	private $urantingModel;

	/**
	 * Class instance
	 * 
	 * @param unit ranting $urantingModel unit ranting model
	 * @return void
	 */
	public function __construct(Unitranting $urantingModel)
	{
		$this->urantingModel = $urantingModel;
	}

	/**
	 * Get all unit ranting collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->urantingModel->withRelationship()->orderBy('nama_unitranting');
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get unit ranting by given Id
	 * 
	 * @param  Integer $id unit ranting id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->urantingModel->withRelationship()->find($id);
	}

	/**
	 * store data unit ranting to database
	 * 
	 * @param  Array $data unit ranting
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->urantingModel->create($data);
	}

	/**
	 * update single data unit ranting to database
	 *
	 * @param  Integer $id unit ranting id
	 * @param  Array $data unit ranting
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->urantingModel->whereId($id)->update($data);
	}

	/**
	 * delete single data unit ranting, with soft delete method or not
	 * 
	 * @param  Integer  $id          unit ranting id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->urantingModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get array list of kompetensi resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->urantingModel->lists('nama_unitranting','id');
	}

	/**
	 * Get general summary data of unit ranting
	 * 
	 * @return Collection
	 */
	public function generalSummary()
	{
		return $this->urantingModel->select('nama_unitranting','id')->orderBy('created_at','desc')->get();
	}
}
