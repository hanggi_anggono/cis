<?php namespace Certification\Repositories\Unit;

/**
 * Interface for unit induk repositories
 */
interface InterfaceUindukRepository {

	/**
	 * Get all unit induk collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null);

	/**
	 * Get unit induk by given Id
	 * 
	 * @param  Integer $id unit induk id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data unit induk to database
	 * 
	 * @param  Array $data unit induk
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data unit induk to database
	 *
	 * @param  Integer $id unit induk id
	 * @param  Array $data unit induk
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data unit induk, with soft delete method or not
	 * 
	 * @param  Integer  $id          unit induk id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
}
