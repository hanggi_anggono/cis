<?php namespace Certification\Repositories\Unit;

use Certification\Models\Unitnonpln;
use Config;

/**
 *  Class DB repository for Master Unitnonpln using Eloquent
 */
class EloquentDbUnonpln {

	/**
	 * Unitnonpln model
	 * 
	 * @var Model
	 */
	private $unitnonplnModel;

	/**
	 * Class instance
	 * 
	 * @param Unitnonpln $unitnonplnModel Unitnonpln model
	 * @return void
	 */
	public function __construct(Unitnonpln $unitnonplnModel)
	{
		$this->unitnonplnModel = $unitnonplnModel;
	}

	/**
	 * Get all Unitnonpln collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->unitnonplnModel->orderBy('created_at', 'desc');
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get Unitnonpln by given Id
	 * 
	 * @param  Integer $id Unitnonpln id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->unitnonplnModel->find($id);
	}

	/**
	 * store data Unitnonpln to database
	 * 
	 * @param  Array $data Unitnonpln
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->unitnonplnModel->create($data);
	}

	/**
	 * update single data Unitnonpln to database
	 *
	 * @param  Integer $id Unitnonpln id
	 * @param  Array $data Unitnonpln
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->unitnonplnModel->whereId($id)->update($data);
	}

	/**
	 * delete single data Unitnonpln, with soft delete method or not
	 * 
	 * @param  Integer  $id          Unitnonpln id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->unitnonplnModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get array list of kompetensi resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->unitnonplnModel->lists('nama_unitnonpln','id');
	}

	/**
	 * Get general summary data of Unitnonpln
	 * 
	 * @return Collection
	 */
	public function generalSummary()
	{
		return $this->unitnonplnModel->select('nama_unitnonpln','id')->orderBy('created_at','desc')->get();
	}
}
