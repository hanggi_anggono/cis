<?php namespace Certification\Repositories\Unit;

use Certification\Repositories\Unit\InterfaceUdiklatRepository;
use Certification\Models\Udiklat;
use Config;

/**
 *  Class DB repository for Master Udiklat using Eloquent
 *  implements InterfaceUdiklatRepository
 */
class EloquentDbUdiklat implements InterfaceUdiklatRepository {

	/**
	 * Udiklat model
	 * 
	 * @var Model
	 */
	private $udiklatModel;

	/**
	 * Class instance
	 * 
	 * @param Udiklat $udiklatModel udiklat model
	 * @return void
	 */
	public function __construct(Udiklat $udiklatModel)
	{
		$this->udiklatModel = $udiklatModel;
	}

	/**
	 * Get all udiklat collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->udiklatModel->withRelationship()->orderBy('nama_udiklat');
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get udiklat by given Id
	 * 
	 * @param  Integer $id udiklat id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->udiklatModel->withRelationship()->find($id);
	}

	/**
	 * store data udiklat to database
	 * 
	 * @param  Array $data udiklat
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->udiklatModel->create($data);
	}

	/**
	 * update single data udiklat to database
	 *
	 * @param  Integer $id udiklat id
	 * @param  Array $data udiklat
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->udiklatModel->whereId($id)->update($data);
	}

	/**
	 * delete single data udiklat, with soft delete method or not
	 * 
	 * @param  Integer  $id          udiklat id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->udiklatModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get array list of kompetensi resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->udiklatModel->lists('nama_udiklat','id');
	}

	/**
	 * Get general summary data of udiklat
	 * 
	 * @return Collection
	 */
	public function generalSummary()
	{
		return $this->udiklatModel->select('nama_udiklat','id')->orderBy('created_at','desc')->get();
	}

	/**
	 * [getUnitinduk description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getUnitinduk($id)
	{
		return $this->udiklatModel->find($id)->unitinduk()->lists('nama_unitinduk', 'id');
	}
}
