<?php namespace Certification\Repositories\Unit;

use Certification\Repositories\Unit\InterfaceUindukRepository;
use Certification\Models\Unitinduk;
use Config;

/**
 *  Class DB repository for Master uinduk using Eloquent
 *  implements InterfaceuindukRepository
 */
class EloquentDbUinduk implements InterfaceUindukRepository {

	/**
	 * uinduk model
	 * 
	 * @var Model
	 */
	private $uindukModel;

	/**
	 * Class instance
	 * 
	 * @param uinduk $uindukModel uinduk model
	 * @return void
	 */
	public function __construct(Unitinduk $uindukModel)
	{
		$this->uindukModel = $uindukModel;
	}

	/**
	 * Get all uinduk collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->uindukModel->withRelationship()->orderBy('nama_unitinduk');
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get uinduk by given Id
	 * 
	 * @param  Integer $id uinduk id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->uindukModel->withRelationship()->find($id);
	}

	/**
	 * store data uinduk to database
	 * 
	 * @param  Array $data uinduk
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->uindukModel->create($data);
	}

	/**
	 * update single data uinduk to database
	 *
	 * @param  Integer $id uinduk id
	 * @param  Array $data uinduk
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->uindukModel->whereId($id)->update($data);
	}

	/**
	 * delete single data uinduk, with soft delete method or not
	 * 
	 * @param  Integer  $id          uinduk id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->uindukModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get array list of kompetensi resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->uindukModel->lists('nama_unitinduk','id');
	}

	/**
	 * Get general summary data of unit induk
	 * 
	 * @return Collection
	 */
	public function generalSummary()
	{
		return $this->uindukModel->select('nama_unitinduk','id')->orderBy('created_at','desc')->get();
	}

	/**
	 * Get unit cabang lists from given unit induk id
	 * 
	 * @param  Integer $id unit induk id
	 * @return Array
	 */
	public function unitcabang($id)
	{
		return $this->uindukModel->find($id)->unitcabang()->lists('nama_unitcabang','id');
	}
}
