<?php namespace Certification\Repositories\Pelaksanaan;

use \Config, \DB;
use Carbon\Carbon;
use Certification\Services\ExportService;
use Certification\Models\Peserta;
use Illuminate\Support\Collection;
use Certification\Models\Penjadwalan;
use Certification\Models\Realisasipeserta;
use Certification\Models\Pesertakompetensipenjadwalan;

class EloquentDbPelaksanaan {

	/**
	 * [$realisasiPeserta description]
	 * 
	 * @var [type]
	 */
	private $realisasiPeserta;
	private $penjadwalan;
	private $peserta;
	private $pesertaPenjadwalan;
	private $exportService;

	/**
	 * [__construct description]
	 * 
	 * @param Realisasipeserta             $realisasiPeserta   [description]
	 * @param Penjadwalan                  $penjadwalan        [description]
	 * @param Pesertakompetensipenjadwalan $pesertaPenjadwalan [description]
	 * @param Peserta                      $peserta            [description]
	 */
	public function __construct(Realisasipeserta $realisasiPeserta, Penjadwalan $penjadwalan, Pesertakompetensipenjadwalan $pesertaPenjadwalan, Peserta $peserta, ExportService $exportService)
	{
		$this->realisasiPeserta   = $realisasiPeserta;
		$this->penjadwalan        = $penjadwalan;
		$this->pesertaPenjadwalan = $pesertaPenjadwalan;
		$this->peserta            = $peserta;
		$this->exportService      = $exportService;
	}

	/**
	 * [countPelaksanaan description]
	 * 
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function countPelaksanaan($params = null, $role = null)
	{
		$collection =  $this->penjadwalan->noFilter();
		if (isset($params)) {
			$decode = json_decode($params);
			// if (isset($decode->unitinduk_id)) {
			// 	$collection->where('unitinduk_id', '=', $decode->unitinduk_id);
			// }
			if (isset($decode->lembagasertifikasi_id)) {
				$collection->where('lembagasertifikasi_id', '=', $decode->lembagasertifikasi_id);
			}
			if (isset($role) && $role == 'employee') {
				if (isset($decode->id)) {
					$collection->join('pesertakompetensipenjadwalan', 'pesertakompetensipenjadwalan.penjadwalan_id', '=', 'penjadwalan.id')->where('pesertakompetensipenjadwalan.peserta_id', '=', $decode->id);
				}
			}
		}
		return $collection->count();
	}

	/**
	 * [countAttended description]
	 * 
	 * @param  [type] $params [description]
	 * @param  [type] $role   [description]
	 * @return [type]         [description]
	 */
	public function countAttended($params = null, $role = null)
	{
		return $this->customRealisasiPesertaWithPenjadwalan($params, $role)->Attended()->count();
	}

	/**
	 * [countPassed description]
	 * 
	 * @param  [type] $params [description]
	 * @param  [type] $role   [description]
	 * @return [type]         [description]
	 */
	public function countPassed($params = null, $role = null)
	{
		return $this->customRealisasiPesertaWithPenjadwalan($params, $role)->passes()->count();
	}

	/**
	 * [countNotPassed description]
	 * 
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function countNotPassed($params = null)
	{
		return $this->customRealisasiPesertaWithPenjadwalan($params)->notPasses()->count();
	}


	/**
	 * [all description]
	 * 
	 * @param  [type] $penjadwalans [description]
	 * @return [type]               [description]
	 */
	public function all($penjadwalans, $params = null, $role = null)
	{
		$collection    = [];
		$kompetensiArr = [];
		$pesertaArr    = [];
		foreach ($penjadwalans as $penjadwalan) {

			foreach ($penjadwalan->kompetensi as $kompetensi) {

				$pesertaPenjadwalan = DB::table('pesertakompetensipenjadwalan')
									  ->join('peserta', 'peserta.id', '=', 'pesertakompetensipenjadwalan.peserta_id')
									  ->where('kompetensipenjadwalan_id', '=', $kompetensi->pivot->id);
									 if (isset($params)) {
											$decode = json_decode($params);
											if (isset($decode->unitinduk_id) && (isset($role) && $role == 'sdm')) {
												$pesertaPenjadwalan->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
											}
											
										}
					$pesertaPenjadwalan =  $pesertaPenjadwalan->get();


				$kehadiran = DB::table('realisasipeserta')
							 ->where('kehadiran', '=', 'y')
							 ->where('kompetensipenjadwalan_id', '=', $kompetensi->pivot->id)
							 ->join('peserta', 'realisasipeserta.peserta_id', '=', 'peserta.id');
							 if (isset($params)) {
								$decode = json_decode($params);
								if (isset($decode->unitinduk_id) && (isset($role) && $role == 'sdm')) {
									$kehadiran->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
								}	
							}
				$kehadiran = $kehadiran->get();

				$kompeten = DB::table('realisasipeserta')
							->where('kehadiran', '=', 'y')
							->where('kelulusan', '=', 'y')
							->where('kompetensipenjadwalan_id', '=', $kompetensi->pivot->id)
							->join('peserta', 'realisasipeserta.peserta_id', '=', 'peserta.id');
							 if (isset($params)) {
								$decode = json_decode($params);
								if (isset($decode->unitinduk_id) && (isset($role) && $role == 'sdm')) {
									$kompeten->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
								}
							}
				$kompeten =	$kompeten->get();

				$nonkompeten = DB::table('realisasipeserta')
							   ->where('kehadiran', '=', 'y')
							   ->where('kelulusan', '=', 'n')
							   ->where('kompetensipenjadwalan_id', '=', $kompetensi->pivot->id)
							   ->join('peserta', 'realisasipeserta.peserta_id', '=', 'peserta.id');
							   if (isset($params)) {
									$decode = json_decode($params);
									if (isset($decode->unitinduk_id) && (isset($role) && $role == 'sdm')) {
										$nonkompeten->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
									}
								}
				$nonkompeten = $nonkompeten->get();

				$kompetensiArr[] = [
						'kompetensipenjadwalan_id' => $kompetensi->pivot->id,
						'penjadwalan_id'           => $penjadwalan->id,
						'kompetensi_id'            => $kompetensi->id,
						'nama_kompetensi'          => $kompetensi->nama_kompetensi,
						'total_peserta'            => count($pesertaPenjadwalan),
						'realisasi'                => [
											'kehadiran'   => count($kehadiran),
											'kompeten'    => count($kompeten),
											'nonkompeten' => count($nonkompeten)
										]
				];
			}
		}

		return [
			'penjadwalan' => $penjadwalans,
			'pelaksanaan' => $kompetensiArr
		];
	}

	/**
	 * [detail description]
	 * 
	 * @param  [type] $penjadwalan [description]
	 * @return [type]              [description]
	 */
	public function detail($penjadwalan, $params = null, $role = null)
	{
		$kompetensiArr = [];
		$pesertaArr    = [];

			foreach ($penjadwalan->kompetensi as $kompetensi) {
				
				$pesertaPenjadwalan = DB::table('pesertakompetensipenjadwalan')
									  ->join('peserta', 'peserta.id', '=', 'pesertakompetensipenjadwalan.peserta_id')
									  ->where('kompetensipenjadwalan_id', '=', $kompetensi->pivot->id);
									 if (isset($params)) {
											$decode = json_decode($params);
											if (isset($decode->unitinduk_id) && (isset($role) && $role == 'sdm')) {
												$pesertaPenjadwalan->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
											}
											
										}
					$pesertaPenjadwalan =  $pesertaPenjadwalan->count();

				$kehadiran = DB::table('realisasipeserta')
							 ->where('kehadiran', '=', 'y')
							 ->where('kompetensipenjadwalan_id', '=', $kompetensi->pivot->id)
							 ->join('peserta', 'realisasipeserta.peserta_id', '=', 'peserta.id');
							 if (isset($params)) {
								$decode = json_decode($params);
								if (isset($decode->unitinduk_id) && (isset($role) && $role == 'sdm')) {
									$kehadiran->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
								}	
							}
				$kehadiran = $kehadiran->count();

				$kompeten = DB::table('realisasipeserta')
							->where('kehadiran', '=', 'y')
							->where('kelulusan', '=', 'y')
							->where('kompetensipenjadwalan_id', '=', $kompetensi->pivot->id)
							->join('peserta', 'realisasipeserta.peserta_id', '=', 'peserta.id');
							 if (isset($params)) {
								$decode = json_decode($params);
								if (isset($decode->unitinduk_id) && (isset($role) && $role == 'sdm')) {
									$kompeten->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
								}
							}
				$kompeten =	$kompeten->count();

				$nonkompeten = DB::table('realisasipeserta')
							   ->where('kehadiran', '=', 'y')
							   ->where('kelulusan', '=', 'n')
							   ->where('kompetensipenjadwalan_id', '=', $kompetensi->pivot->id)
							   ->join('peserta', 'realisasipeserta.peserta_id', '=', 'peserta.id');
							   if (isset($params)) {
									$decode = json_decode($params);
									if (isset($decode->unitinduk_id) && (isset($role) && $role == 'sdm')) {
										$nonkompeten->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
									}
								}
				$nonkompeten = $nonkompeten->count();
				
				$kompetensiArr[] = [
						'kompetensipenjadwalan_id' => $kompetensi->pivot->id,
						'penjadwalan_id'           => $penjadwalan->id,
						'kompetensi_id'            => $kompetensi->id,
						'nama_kompetensi'          => $kompetensi->nama_kompetensi,
						'total_peserta'            => $pesertaPenjadwalan,
						'realisasi'                => [
											'kehadiran'   => $kehadiran,
											'kompeten'    => $kompeten,
											'nonkompeten' => $nonkompeten
										]
				];
			}

		$pesertaRealisasi = DB::table('realisasipeserta')
							->select('nama', 'nama_kompetensi', 'nip', 'kehadiran', 'kelulusan')
							->join('peserta', 'peserta_id', '=', 'peserta.id')
							->join('kompetensi', 'kompetensi_id', '=', 'kompetensi.id')
							->join('kompetensipenjadwalan', 'kompetensipenjadwalan_id', '=', 'kompetensipenjadwalan.id')
							->join('penjadwalan', 'kompetensipenjadwalan.penjadwalan_id', '=', 'penjadwalan.id')
							->where('kompetensipenjadwalan.penjadwalan_id', '=', $penjadwalan->id);
							if (isset($params)) {
									$decode = json_decode($params);
									if (isset($decode->unitinduk_id) && (isset($role) && $role == 'sdm')) {
										$pesertaRealisasi->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
									}
								}
		$pesertaRealisasi = $pesertaRealisasi->get();

		return Collection::make([
					'penjadwalan'      => $penjadwalan,
					'pelaksanaan'      => $kompetensiArr,
					'pesertaRealisasi' => $pesertaRealisasi
				]);
	}

	/**
	 * [getRealisasiPeserta description]
	 * 
	 * @param  [type] $pesertaPenjadwalan [description]
	 * @return [type]                     [description]
	 */
	public function getRealisasiPeserta($pesertaPenjadwalan)
	{
		$pesertaArr = [];
		foreach ($pesertaPenjadwalan as $peserta) {
			$pesertaArr[] = $this->peserta->select('peserta.id as id_peserta', 'kelulusan', 'kehadiran', 'information', 'sertifikat.no_sertifikat', 'tanggal_keluar', 'nip', 'nama', 'realisasipeserta.kompetensipenjadwalan_id as realisasikompetensipenjadwalanid')
			->leftJoin('realisasipeserta', function($join) use($peserta)
			{
				$join->on('realisasipeserta.peserta_id', '=', 'peserta.id')->where('realisasipeserta.kompetensipenjadwalan_id', '=', $peserta->kompetensipenjadwalan_id);
			})
			->leftJoin('sertifikat', function($join) use($peserta)
			{
				$join->on('sertifikat.peserta_id', '=', 'peserta.id')->where('sertifikat.penjadwalan_id', '=', $peserta->penjadwalan_id);
			})
			->where('peserta.id', '=', $peserta->peserta_id)
			->first();
		}
		// dd(DB::getQueryLog());
		return Collection::make($pesertaArr);
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		try {
			$exception = DB::transaction(function() use($data)
			{
				$dataCount = count($data['peserta_id']);
				for ($i=0; $i < $dataCount; $i++) { 
					$store = [
						'kompetensipenjadwalan_id' => $data['kompetensipenjadwalan_id'],
						'kompetensi_id'            => $data['kompetensi_id'],
						'peserta_id'               => $data['peserta_id'][$i],
						'kehadiran'                => $data['kehadiran'][$i],
						'kelulusan'                => $data['kelulusan'][$i],
						'information'              => $data['information'][$i],
						'status_sertifikat'        => 'baru',
					];

					if (isset($data['no_sertifikat'])) {
						$store = [
							'no_sertifikat'  => $data['no_sertifikat'][$i],
							'tanggal_keluar' => $data['tanggal_keluar'][$i],
						];
					}

					// check if realisasi is exist
					$checkExist = $this->realisasiPeserta->where('kompetensipenjadwalan_id', '=', $store['kompetensipenjadwalan_id'])->where('peserta_id', '=', $store['peserta_id'])->where('kompetensi_id', '=', $store['kompetensi_id'])->first();
					
					if (count($checkExist) > 0) {
						$checkExist->update($store);
					} else {
						$this->realisasiPeserta->create($store);
					}
					
				}
				DB::table('penjadwalan')->where('id', '=', $data['penjadwalan_id'])->update(['status' => 1]);
			});
		
			return is_null($exception) ? true : $exception;
		
		} catch (Exception $e) {
			return $false;
		}
	}

	/**
	 * [export description]
	 * 
	 * @param  [type] $collection [description]
	 * @param  string $type       [description]
	 * @return [type]             [description]
	 */
	public function export($collection, $type = 'excel')
	{
		$data['pelaksanaan'] = $collection;
		return $this->exportService->export($data['pelaksanaan'], $type, ['viewName' => 'pelaksanaanexcel', 'orientation' => 'landscape', 'filename' => 'report_pelaksanaan', 'paper' => 'a3']);
	}

	/**
	 * [customRealisasiPesertaWithPenjadwalan description]
	 * 
	 * @param  [type] $params [description]
	 * @param  [type] $role   [description]
	 * @return [type]         [description]
	 */
	private function customRealisasiPesertaWithPenjadwalan($params = null, $role = null)
	{
		$collection = $this->realisasiPeserta->joinPenjadwalan();
		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$collection->join('peserta', 'realisasipeserta.peserta_id', '=', 'peserta.id')->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
			}
			if (isset($decode->lembagasertifikasi_id)) {
				$collection->where('lembagasertifikasi_id', '=', $decode->lembagasertifikasi_id);
			}
			if (isset($role) && $role == 'employee') {
				if (isset($decode->id)) {
					$collection->where('realisasipeserta.peserta_id', '=', $decode->id);
				}
			}
		}
		return $collection;
	}
}
