<?php namespace Certification\Repositories\SertifikatAsesor;

use Certification\Models\Sertifikatasesor;
use Config;

/**
 *  Class DB repository for Master sertifikatasesor using Eloquent
 */
class EloquentDbSertifikatAsesor {

	/**
	 * [$sertifikatasesorModel description]
	 * 
	 * @var [type]
	 */
	private $sertifikatasesorModel;

	/**
	 * [__construct description]
	 * 
	 * @param SertifikatAsesor $sertifikatasesorModel [description]
	 */
	public function __construct(Sertifikatasesor $sertifikatasesorModel)
	{
		$this->sertifikatasesorModel = $sertifikatasesorModel;
	}

	/**
	 * Get all sertifikat asesor collection with paginate or not
	 *
	 * @param  Integer $asesor_id asesor id
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($asesor_id, $paginate = null)
	{
		if (empty($asesor_id)) {
			$collection = $this->sertifikatasesorModel;
		} else {
			$collection = $this->sertifikatasesorModel->byAsesor($asesor_id);
		}
		
		if (isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get sertifikat asesor by given Id
	 * 
	 * @param  Integer $id asesor id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->sertifikatasesorModel->withRelationship()->find($id);
	}

	/**
	 * store data sertifikat asesor to database
	 * 
	 * @param  Array $data sertifikat asesor
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->sertifikatasesorModel->create($data);
	}

	/**
	 * update single data asesor to database
	 *
	 * @param  Integer $id asesor id
	 * @param  Array $data sertifikat asesor
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->sertifikatasesorModel->whereId($id)->update($data);
	}

	/**
	 * delete single data sertifikat asesor, with soft delete method or not
	 * 
	 * @param  Integer  $id sertifikat asesor id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->sertifikatasesorModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get lists of asesor data
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->sertifikatasesorModel->lists('id');
	}
}
