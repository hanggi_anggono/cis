<?php namespace Certification\Repositories\Evaluasi;

use Certification\Models\Evaluasi;
use Certification\Models\Kompetensipermohonan;
use Certification\Models\Kompetensipermohonanpeserta;
use Certification\Models\Peserta;
use Certification\Services\ExportService;

use \Config, \DB;

class EloquentDbEvaluasi {

	/**
	 * [$evaluasi description]
	 * 
	 * @var [type]
	 */
	private $evaluasi;

	/**
	 * [$kompetensipermohonanpeserta description]
	 * 
	 * @var [type]
	 */
	private $kompetensipermohonanpeserta;

	/**
	 * [$exportService description]
	 * 
	 * @var [type]
	 */
	private $exportService;

	/**
	 * [__construct description]
	 * 
	 * @param Evaluasi                    $evaluasi                    [description]
	 * @param Kompetensipermohonanpeserta $kompetensipermohonanpeserta [description]
	 * @param ExportService               $exportService               [description]
	 */
	public function __construct(Evaluasi $evaluasi, Kompetensipermohonanpeserta $kompetensipermohonanpeserta, ExportService $exportService)
	{
		$this->evaluasi                    = $evaluasi;
		$this->exportService               = $exportService;
		$this->kompetensipermohonanpeserta = $kompetensipermohonanpeserta;
	}

	/**
	 * [countAll description]
	 * 
	 * @param  [type] $params [description]
	 * @param  [type] $role   [description]
	 * @return [type]         [description]
	 */
	public function countAll($params = null, $role = null)
	{
		return $this->evaluasiWithPermohonan($params, $role)->count();
	}

	/**
	 * [countNotEvaluated description]
	 * 
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function countNotEvaluated($params = null, $role = null)
	{
		return $this->customCollection($params, $role)->whereNull('evaluasi.id')->count();
	}

	/**
	 * [countAccepted description]
	 * 
	 * @param  [type] $params [description]
	 * @param  [type] $role   [description]
	 * @return [type]         [description]
	 */
	public function countAccepted($params = null, $role = null)
	{
		return $this->evaluasiWithPermohonan($params, $role)->accepted()->count();
	}

	/**
	 * [countRejected description]
	 * 
	 * @param  [type] $params [description]
	 * @param  [type] $role   [description]
	 * @return [type]         [description]
	 */
	public function countRejected($params = null, $role = null)
	{
		return $this->evaluasiWithPermohonan($params, $role)->rejected()->count();
	}

	/**
	 * [summaries description]
	 * 
	 * @param  [type] $request [description]
	 * @param  [type] $params  [description]
	 * @param  [type] $role    [description]
	 * @return [type]          [description]
	 */
	public function summaries($request = null, $params = null, $role = null)
	{
		return [
			'total'        => $this->countAll($params, $role),
			'notevaluated' => $this->countNotEvaluated($params, $role),
			'accepted'     => $this->countAccepted($params, $role),
			'rejected'     => $this->countRejected($params, $role)
		];
	}

	/**
	 * [all description]
	 * 
	 * @param  string $paginate [description]
	 * @param  [type] $hasil    [description]
	 * @return [type]           [description]
	 */
	public function all($paginate = 'paginate', $hasil = null)
	{
		$collection = $this->evaluasi->withRelationship();
		if(isset($hasil)) {
			($hasil == 1) ? $collection->accepted() : $collection->rejected();
		}

		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * [allPeserta description]
	 * 
	 * @param  string $paginate [description]
	 * @param  array  $filters  [description]
	 * @param  [type] $params   [description]
	 * @param  [type] $role     [description]
	 * @return [type]           [description]
	 */
	public function allPeserta($paginate = 'paginate', $filters = [], $params = null, $role = null)
	{
		$collection = $this->customCollection();
		
		/**
		 *  filters by param session
		 */
		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($role) && $role == 'employee') {
				if (isset($decode->id)) {
					$collection->where('evaluasi.peserta_id', '=', $decode->id);
				}
			} else {
				if (isset($decode->unitinduk_id)) {
					$collection->where('permohonan.unitinduk_id', '=', $decode->unitinduk_id);
				}
			}
		}
		/**
		 *  Filters by hasil no surat
		 */
		if($filters->has('nosurat')) {
			$collection->where('nosurat', 'LIKE', '%'.$filters->get('nosurat').'%');
		}

		/**
		 * Filters by NIP / Name peserta
		 */
		if ($filters->has('nip')) {
			$collection->where(function($query) use($filters)
						{
							$query->where('peserta.nip', 'LIKE', '%'.$filters->get('nip').'%')
								  ->orWhere('peserta.nama', 'LIKE', '%'.$filters->get('nip').'%');
						});
		}

		/**
		 * Filters by tanggal evaluasi
		 */
		if($filters->has('tanggalevaluasi')) {
			$collection->where('evaluasi.tanggal_evaluasi', '=', $filters->get('tanggalevaluasi'));
		}

		/**
		 *  Filters by hasil evaluasi (pending, diterima, ditolak)
		 */
		if ($filters->has('hasil')) {
			if ($filters->get('hasil') == 'pending') {
				$collection->whereNull('evaluasi.id');
			} else {
				$collection->where('hasil', '=', $filters->get('hasil'));
			}
		}

		/**
		 * Filters by kompetensi
		 */
		if ($filters->has('kompetensi')) {
			$collection->where(function($query) use($filters)
						{
							$query->where('kodedjk', 'LIKE', '%'.$filters->get('kompetensi').'%')
								  ->orWhere('kodeskkni', 'LIKE', '%'.$filters->get('kompetensi').'%')
								  ->orWhere('nama_kompetensi', 'LIKE', '%'.$filters->get('kompetensi').'%');
						});
		}

		/**
		 *  Filters by unit induk
		 */
		if ($filters->has('unitinduk')) {
			$collection->where('nama_unitinduk', 'LIKE', '%'.$filters->get('unitinduk').'%');
		}

		/**
		 *  Filters by status penjadwalan (belum, sudah)
		 */
		if ($filters->has('status_penjadwalan')) {
			$collection->where('status_penjadwalan', '=', $filters->get('status_penjadwalan'));
		}

		$collection->orderBy('status_penjadwalan', 'asc')->orderBy('id_evaluasi', 'asc')->orderBy('kompetensipermohonanpeserta.created_at', 'desc');

		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			 return $collection->paginate(20);
		}

		return $collection->get();		
	}

	/**
	 * [getDetail description]
	 * 
	 * @param  [type] $id                     [description]
	 * @param  [type] $kompetensipermohonanid [description]
	 * @return [type]                         [description]
	 */
	public function getDetail($id, $kompetensipermohonanid)
	{
		return $this->customCollection()->where('peserta.id', '=', $id)
										->where('kompetensipermohonan.id', '=', $kompetensipermohonanid)
										->first();
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		return $this->evaluasi->create($data);
	}

	/**
	 * [delete description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id)
	{
		$this->evaluasi->whereId($id)->delete();
	}

	/**
	 * [evaluatedKompetensiLists description]
	 * 
	 * @param  boolean $scheduled [description]
	 * @return [type]             [description]
	 */
	public function evaluatedKompetensiLists()
	{
		$collection = $this->evaluasi->join('kompetensipermohonan', function($join)
										{
											$join->on('kompetensipermohonan.id', '=', 'kompetensipermohonan_id');
										})
									->join('kompetensi', function($join)
										{
											$join->on('kompetensipermohonan.kompetensi_id', '=', 'kompetensi.id');
										});
		
		return $collection->select('nama_kompetensi', 'kompetensipermohonan.id as kompetensipermohonan_id', 'kompetensi_id')->groupBy('kompetensi_id')->distinct()->get();
	}

	/**
	 * [evaluatedPesertaLists description]
	 * 
	 * @param  boolean $scheduled     [description]
	 * @param  boolean $accepted      [description]
	 * @param  [type]  $kompetensi_id [description]
	 * @param  [type]  $filters       [description]
	 * @param  [type]  $params        [description]
	 * @return [type]                 [description]
	 */
	public function evaluatedPesertaLists($scheduled = false, $accepted = true, $kompetensi_id = null, $filters = null, $params = null)
	{
		$collection = $this->evaluasi->join('peserta', 'peserta_id', '=', 'peserta.id')
									->leftJoin('unitinduk', 'peserta.unitinduk_id', '=', 'unitinduk.id')
									->join('kompetensipermohonan', 'kompetensipermohonan.id', '=', 'kompetensipermohonan_id')
									->join('permohonan', 'kompetensipermohonan.permohonan_id', '=', 'permohonan.id')
									->join('kompetensi', 'kompetensipermohonan.kompetensi_id', '=', 'kompetensi.id');
		if ($accepted) {
			$collection->accepted();
		} else {
			$collection->rejected();
		}
		
		if ($scheduled) {
			$collection->isScheduled();
		} else {
			$collection->isNotScheduled();
		}

		if (isset($kompetensi_id)) {
			$collection->where('kompetensi_id', '=', $kompetensi_id);
		}
		
		// filters section
		if (isset($filters)) {
			if ($filters->has('nosurat')) {
				$collection->where('nosurat', 'LIKE', '%'.$filters->get('nosurat').'%');
			}
			if ($filters->has('nip')) {
				$collection->where(function($query) use($filters)
							{
								$query->where('peserta.nip', 'LIKE', '%'.$filters->get('nip').'%')
									  ->orWhere('peserta.nama', 'LIKE', '%'.$filters->get('nip').'%');
							});
			}
			if ($filters->has('kode')) {
				$collection->where(function($query) use($filters)
							{
								$query->where('kodedjk', 'LIKE', '%'.$filters->get('kode').'%')
									  ->orWhere('kodeskkni', 'LIKE', '%'.$filters->get('kode').'%')
									  ->orWhere('nama_kompetensi', 'LIKE', '%'.$filters->get('kode').'%');
							});
			}
			if ($filters->has('unitinduk')) {
				$collection->where('nama_unitinduk', 'LIKE', '%'.$filters->get('unitinduk').'%');
			}

		}

		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$collection->where('permohonan.unitinduk_id', '=', $decode->unitinduk_id);
			}
		}

		return $collection->select('peserta_id', 'nama', 'nip', 'evaluasi.id as evaluasi_id', 'nosurat', 'status_penjadwalan', 'hasil', 'kodedjk', 'kodeskkni', 'nama_kompetensi')->get();
	}

	/**
	 * [customCollection description]
	 * 
	 * @param  [type] $params [description]
	 * @param  [type] $role   [description]
	 * @return [type]         [description]
	 */
	private function customCollection($params = null, $role = null)
	{
		$collection =  $this->kompetensipermohonanpeserta->selectedField()
														 ->joinWithPeserta()
														 ->joinWithKompetensipermohonan()
														 ->joinWithKompetensi()
														 ->joinWithEvaluasi()
														 ->joinWithUnitinduk()
														 //->joinWithKompetensipermohonansdm()
														 ->joinWithPermohonan();
		
		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$collection->where('permohonan.unitinduk_id', '=', $decode->unitinduk_id);
			}
			if (isset($role) && $role == 'employee') {
				if (isset($decode->id)) {
					$collection->where('evaluasi.peserta_id', '=', $decode->id);
				}
			}
		}
		return $collection;
	}

	/**
	 * [evaluasiWithPermohonan description]
	 * 
	 * @param  [type] $params [description]
	 * @param  [type] $role   [description]
	 * @return [type]         [description]
	 */
	private function evaluasiWithPermohonan($params = null, $role = null)
	{
		$collection = $this->evaluasi->joinWithPermohonan();
		if (isset($params)) {
			$decode = json_decode($params);
			
			if (isset($role) && $role == 'employee') {
				if (isset($decode->id)) {
					$collection->where('evaluasi.peserta_id', '=', $decode->id);
				}
			} else {
				if (isset($decode->unitinduk_id)) {
					$collection->where('permohonan.unitinduk_id', '=', $decode->unitinduk_id);
				}
			}
		}
		return $collection;
	}

	/**
	 * [export description]
	 * 
	 * @param  string $type    [description]
	 * @param  array  $filters [description]
	 * @param  [type] $params  [description]
	 * @param  [type] $role    [description]
	 * @return [type]          [description]
	 */
	public function export($type = 'excel', $filters = [], $params = null, $role = null)
	{
		$data['evaluasi'] = $this->allPeserta(null, $filters, $params, $role);
		return $this->exportService->export($data['evaluasi'], $type, ['viewName' => 'evaluasiexcel', 'orientation' => 'landscape', 'filename' => 'report_evaluasi']);
	}
}
