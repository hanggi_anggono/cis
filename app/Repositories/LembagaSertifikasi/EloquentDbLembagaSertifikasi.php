<?php namespace Certification\Repositories\LembagaSertifikasi;

use Certification\Repositories\LembagaSertifikasi\InterfaceLembagaSertifikasiRepository;
use Certification\Models\Lembagasertifikasi;
use \Config;

/**
 *  Class DB repository for Master lembagasertifikasi using Eloquent
 *  implements InterfacelembagasertifikasiSertifikasi
 */
class EloquentDbLembagaSertifikasi implements InterfaceLembagaSertifikasiRepository {

	/**
	 * Model lembagasertifikasi attribute
	 * 
	 * @var Model
	 */
	private $lembagasertifikasiModel;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param lembagasertifikasi $lembagasertifikasiModel Model lembagasertifikasi
	 */
	public function __construct(Lembagasertifikasi $lembagasertifikasiModel)
	{
		$this->lembagasertifikasiModel = $lembagasertifikasiModel;
	}
	/**
	 * Get all lembagasertifikasi collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->lembagasertifikasiModel->withAsesor()->orderedByName();
		if ($paginate) {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get lembagasertifikasi by given Id
	 * 
	 * @param  Integer $id lembagasertifikasi id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->lembagasertifikasiModel->findOrFail($id);
	}

	/**
	 * store data lembagasertifikasi to database
	 * 
	 * @param  Array $data lembagasertifikasi
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->lembagasertifikasiModel->create($data);
	}

	/**
	 * update single data lembagasertifikasi to database
	 *
	 * @param  Integer $id lembagasertifikasi id
	 * @param  Array $data lembagasertifikasi
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->lembagasertifikasiModel->whereId($id)->update($data);
	}

	/**
	 * delete single data lembagasertifikasi, with soft delete method or not
	 * 
	 * @param  Integer  $id          lembagasertifikasi id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->lembagasertifikasiModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get lists of lembaga sertifikasi data
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->lembagasertifikasiModel->lists('nama_lembagasertifikasi','id');
	}
}
