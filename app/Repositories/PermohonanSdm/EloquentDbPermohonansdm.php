<?php namespace Certification\Repositories\PermohonanSdm;

use Certification\Models\Permohonansdm;
use Certification\Repositories\PermohonanSdm\EloquentDbKompetensipermohonansdm;
use Certification\Repositories\PermohonanSdm\EloquentDbKompetensipermohonansdmpeserta;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\Peserta\InterfacePesertaRepository;
use \Config;

class EloquentDbPermohonansdm {

	/**
	 * [$permohonanModel description]
	 * 
	 * @var [type]
	 */
	private $permohonanSdmModel;

	/**
	 * [$kompetensiPermohonanRepository description]
	 * 
	 * @var [type]
	 */
	private $kompetensiPermohonanSdmRepository;

	/**
	 * [$kompetensiPermohonanPesertaRepository description]
	 * 
	 * @var [type]
	 */
	private $kompetensiPermohonanSdmPesertaRepository;

	/**
	 * [$kompetensiRepository description]
	 * 
	 * @var [type]
	 */
	private $kompetensiRepository;

	/**
	 * [$peserta description]
	 * 
	 * @var [type]
	 */
	private $pesertaRepository;

	/**
	 * [__construct description]
	 * 
	 * @param Permohonan $permohonanModel [description]
	 */
	public function __construct(
					Permohonansdm $permohonanSdmModel, 
					EloquentDbKompetensipermohonansdm $kompetensiPermohonanSdmRepository, 
					EloquentDbKompetensipermohonansdmpeserta $kompetensiPermohonanSdmPesertaRepository,
					InterfaceKompetensiRepository $kompetensiRepository, 
					InterfacePesertaRepository $pesertaRepository
	)
	{
		$this->permohonanSdmModel                       = $permohonanSdmModel;
		$this->kompetensiPermohonanSdmRepository        = $kompetensiPermohonanSdmRepository;
		$this->kompetensiPermohonanSdmPesertaRepository = $kompetensiPermohonanSdmPesertaRepository;
		$this->kompetensiRepository                  	= $kompetensiRepository;
		$this->pesertaRepository                     	= $pesertaRepository;
	}

	/**
	 * [all description]
	 * 
	 * @param  [type] $paginate [description]
	 * @param  [type] $params   [description]
	 * @param  [type] $role     [description]
	 * @return [type]           [description]
	 */
	public function all($paginate = null, $params = null, $role = null)
	{
		$collection = $this->permohonanSdmModel->withRelationship();
		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$collection->where('unitinduk_id', '=', $decode->unitinduk_id);
			}
			if (isset($role) && $role == 'sdm') {
				if (isset($decode->id)) {
					$collection->join('kompetensipermohonansdmpeserta', 'kompetensipermohonansdmpeserta.permohonansdm_id', '=', 'permohonansdm.id')->where('kompetensipermohonansdmpeserta.peserta_id', '=', $decode->id);
				}
			}
		}
		$collection->orderByCreated();
		if (isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get permohonan by given Id
	 * 
	 * @param  Integer $id permohonan id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->permohonanSdmModel->withRelationship()->findOrFail($id);
	}

	/**
	 * store data permohonan to database
	 * 
	 * @param  Array $data permohonan
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->permohonanSdmModel->create($data);
	}

	/**
	 * update single data permohonan to database
	 *
	 * @param  Integer $id permohonan id
	 * @param  Array $data permohonan
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->permohonanSdmModel->whereId($id)->update($data);
	}

	/**
	 * delete single data permohonan, with soft delete method or not
	 * 
	 * @param  Integer  $id          permohonan id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->permohonanSdmModel->whereId($id)->delete();
	}

	/**
	 * [storeKompetensi description]
	 * 
	 * @return [type] [description]
	 */
	public function storeKompetensi($id, $data)
	{
		if(isset($data)) {
			$failed = [];
			$filter = array_filter($data); // prevent empty string to be inserted
			foreach ($filter as $key => $kompetensiId) {
				if ($this->kompetensiPermohonanSdmRepository->isNotEmpty($id, $kompetensiId)) {
					$failed[] = $this->kompetensiRepository->getById($kompetensiId)['nama_kompetensi'];
				} else {
					 $this->permohonanSdmModel->find($id)->kompetensi()->attach($kompetensiId);
				}
			}
			$requested = count($filter);
			$success   = $requested - count($failed);
			return [
				'status'    => true,
				'requested' => $requested,
				'failed'    => $failed
			];
		}
		return [
				'status'    => false,
				'requested' => 0
			];
		
	}

	/**
	 * [deleteKompetensi description]
	 * 
	 * @param  [type] $id           [description]
	 * @param  [type] $kompetensiid [description]
	 * @return [type]               [description]
	 */
	public function deleteKompetensi($id, $kompetensiid)
	{
		$this->permohonanSdmModel->find($id)->kompetensi()->detach($kompetensiid);
	}

	/**
	 * [storeKompetensiPeserta description]
	 * 
	 * @param  [type] $id                     [description]
	 * @param  [type] $kompetensipermohonanId [description]
	 * @param  [type] $data                   [description]
	 * @return [type]                         [description]
	 */
	public function storeKompetensiPeserta($id, $kompetensipermohonansdmId, $data)
	{
		$failed = [];
		if (is_array($data)) {
			$filter = array_filter($data); // remove empty data
		} else {
			$filter = [$data];
		}
		if (count($filter) > 0) {
			foreach ($filter as $key => $value) {
				if(! $this->kompetensiPermohonanSdmPesertaRepository->isNotEmpty($value, $kompetensipermohonansdmId)) {
					$store = [$value => ['kompetensipermohonansdm_id' => $kompetensipermohonansdmId]]; // arrange data
					$this->permohonanSdmModel->find($id)->peserta()->attach($store);
				} else {
					$failed[] = $this->pesertaRepository->getById($value)['nip'];
				}
			}
			return [
				'requested' => count($filter),
				'failed' => $failed
			];
		}

		return [
			'requested' => count($filter)
		];	
	}

	/**
	 * [deleteKompetensiPeserta description]
	 * 
	 * @param  [type] $id        [description]
	 * @param  [type] $pesertaId [description]
	 * @return [type]            [description]
	 */
	public function deleteKompetensiPeserta($id, $pesertaId)
	{
		$this->permohonanSdmModel->find($id)->peserta()->detach($pesertaId);
	}
}
