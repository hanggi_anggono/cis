<?php namespace Certification\Repositories\PermohonanSdm;

use Certification\Models\Kompetensipermohonansdmpeserta;

class EloquentDbKompetensipermohonansdmpeserta {

	/**
	 * [$kompetensipermohonanpeserta description]
	 * 
	 * @var [type]
	 */
	private $kompetensipermohonansdmpeserta;

	/**
	 * [__construct description]
	 * 
	 * @param Kompetensipermohonanpeserta $kompetensipermohonanpeserta [description]
	 */
	public function __construct(Kompetensipermohonansdmpeserta $kompetensipermohonansdmpeserta)
	{
		$this->kompetensipermohonansdmpeserta = $kompetensipermohonansdmpeserta;
	}

	/**
	 * [all description]
	 * 
	 * @return [type] [description]
	 */
	public function all()
	{
		return $this->kompetensipermohonansdmpeserta->withRelationship()->get();
	}

	/**
	 * [isNotEmpty description]
	 * 
	 * @param  [type]  $pesertaId              [description]
	 * @param  [type]  $kompetensipermohonanId [description]
	 * @return boolean                         [description]
	 */
	public function isNotEmpty($pesertaId, $kompetensipermohonansdmId)
	{
		$count = $this->kompetensipermohonansdmpeserta->isNotEmpty($pesertaId, $kompetensipermohonansdmId)->count();
		if($count != 0) {
			return true;
		}
		return false;
	}
}
