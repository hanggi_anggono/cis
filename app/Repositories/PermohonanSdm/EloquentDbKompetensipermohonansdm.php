<?php namespace Certification\Repositories\PermohonanSdm;

use Certification\Models\Kompetensipermohonansdm;

class EloquentDbKompetensipermohonansdm {

	/**
	 * [$kompetensipermohonan description]
	 * 
	 * @var [type]
	 */
	private $kompetensipermohonansdm;

	/**
	 * [__construct description]
	 * 
	 * @param Kompetensipermohonan $kompetensipermohonan [description]
	 */
	public function __construct(Kompetensipermohonansdm $kompetensipermohonansdm)
	{
		$this->kompetensipermohonansdm = $kompetensipermohonansdm;
	}

	/**
	 * [all description]
	 * 
	 * @return [type] [description]
	 */
	public function all()
	{
		return $this->kompetensipermohonansdm->withRelationship()->get();
	}

	/**
	 * [find description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function find($id)
	{
		return $this->kompetensipermohonansdm->withRelationship()->find($id);
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		return $this->kompetensipermohonansdm->create($data);
	}

	/**
	 * [isNotEmpty description]
	 * 
	 * @param  [type]  $permohonanId [description]
	 * @param  [type]  $kompetensiId [description]
	 * @return boolean               [description]
	 */
	public function isNotEmpty($permohonansdmId, $kompetensiId)
	{
		$count = $this->kompetensipermohonansdm->isKompetensiPermohonanSdmExist($permohonansdmId, $kompetensiId)->count();
		if ($count != 0) {
			return true;
		}
		return false;
	}
}
