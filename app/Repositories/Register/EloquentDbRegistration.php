<?php namespace Certification\Repositories\Register;

use Certification\Models\Register;
use DB, Config;

class EloquentDbRegistration {

	/**
	 * [$register description]
	 * 
	 * @var [type]
	 */
	private $register;
	
	/**
	 * [$pagination description]
	 * 
	 * @var [type]
	 */
	private $pagination;

	/**
	 * [__construct description]
	 * 
	 * @param Register $register [description]
	 */
	public function __construct(Register $register)
	{
		$this->register = $register;
		$this->pagination = Config::get('certification.default_pagination_count');
	}

	/**
	 * [collection description]
	 * 
	 * @param  [type] $filters [description]
	 * @return [type]          [description]
	 */
	public function collection($filters = null)
	{
		$collection = $this->register->withRelationship();
		if (isset($filters)) {
			if ($filters->has('name')) {
				$collection->where('nama', 'LIKE', '%'.$filters->get('name').'%')->orWhere('nip', 'LIKE', '%'.$filters->get('name').'%');
			}
		}
		return $collection;
	}

	/**
	 * [all description]
	 * 
	 * @param  string $paginate [description]
	 * @param  [type] $filters  [description]
	 * @return [type]           [description]
	 */
	public function all($paginate = 'paginate', $filters = null)
	{
		$collection = $this->collection($filters)->orderBy('registers.created_at');
		if (isset($paginate) && $paginate == 'paginate') {
			return $collection->paginate($this->pagination);
		}
		return $collection->get();
	}

	/**
	 * [find description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function find($id)
	{
		return $this->collection()->whereId($id)->first();
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		return $this->register->create($data);
	}

	/**
	 * [update description]
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function update($id, $data)
	{
		return $this->register->where('id', '=', $id)->update($data);
	}

	/**
	 * [delete description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id)
	{
		return $this->register->where('id', '=', $id)->delete();
	}
}
