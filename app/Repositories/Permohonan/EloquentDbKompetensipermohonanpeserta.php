<?php namespace Certification\Repositories\Permohonan;

use Certification\Models\Kompetensipermohonanpeserta;

class EloquentDbKompetensipermohonanpeserta {

	/**
	 * [$kompetensipermohonanpeserta description]
	 * 
	 * @var [type]
	 */
	private $kompetensipermohonanpeserta;

	/**
	 * [__construct description]
	 * 
	 * @param Kompetensipermohonanpeserta $kompetensipermohonanpeserta [description]
	 */
	public function __construct(Kompetensipermohonanpeserta $kompetensipermohonanpeserta)
	{
		$this->kompetensipermohonanpeserta = $kompetensipermohonanpeserta;
	}

	/**
	 * [all description]
	 * 
	 * @return [type] [description]
	 */
	public function all()
	{
		return $this->kompetensipermohonanpeserta->withRelationship()->get();
	}

	/**
	 * [isNotEmpty description]
	 * 
	 * @param  [type]  $pesertaId              [description]
	 * @param  [type]  $kompetensipermohonanId [description]
	 * @return boolean                         [description]
	 */
	public function isNotEmpty($pesertaId, $kompetensipermohonanId)
	{
		$count = $this->kompetensipermohonanpeserta->isNotEmpty($pesertaId, $kompetensipermohonanId)->count();
		if($count != 0) {
			return true;
		}
		return false;
	}
}
