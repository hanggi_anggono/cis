<?php namespace Certification\Repositories\Permohonan;

use Certification\Models\Kompetensipermohonan;

class EloquentDbKompetensipermohonan {

	/**
	 * [$kompetensipermohonan description]
	 * 
	 * @var [type]
	 */
	private $kompetensipermohonan;

	/**
	 * [__construct description]
	 * 
	 * @param Kompetensipermohonan $kompetensipermohonan [description]
	 */
	public function __construct(Kompetensipermohonan $kompetensipermohonan)
	{
		$this->kompetensipermohonan = $kompetensipermohonan;
	}

	/**
	 * [all description]
	 * 
	 * @return [type] [description]
	 */
	public function all()
	{
		return $this->kompetensipermohonan->withRelationship()->get();
	}

	/**
	 * [find description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function find($id)
	{
		return $this->kompetensipermohonan->withRelationship()->find($id);
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		return $this->kompetensipermohonan->create($data);
	}

	/**
	 * [isNotEmpty description]
	 * 
	 * @param  [type]  $permohonanId [description]
	 * @param  [type]  $kompetensiId [description]
	 * @return boolean               [description]
	 */
	public function isNotEmpty($permohonanId, $kompetensiId)
	{
		$count = $this->kompetensipermohonan->isKompetensiPermohonanExist($permohonanId, $kompetensiId)->count();
		if ($count != 0) {
			return true;
		}
		return false;
	}
}
