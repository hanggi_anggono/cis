<?php namespace Certification\Repositories\Jenjang;

use Certification\Repositories\Jenjang\InterfaceJenjangRepository;
use Certification\Models\Jenjang;
use \Config;

/**
 *  Class DB repository for Master Jenjang jabatan using Eloquent
 *  Implements Interface Jenjang Repository
 */
class EloquentDbJenjang implements InterfaceJenjangRepository
{
	private $jenjangModel;
	private $status;

	function __construct(Jenjang $jenjangModel)
	{
		$this->jenjangModel = $jenjangModel;
		$this->status       = false;
	}

	/**
	 * Get all jenjang jabatan collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null)
	{
		$collection = $this->jenjangModel->orderBy('nama_jenjangjabatan');
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get jenjang jabatan by given Id
	 * 
	 * @param  Integer $id jenjang jabatan id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->jenjangModel->findOrFail($id);
	}

	/**
	 * store data jenjang jabatan to database
	 * 
	 * @param  Array $data jenjang jabatan
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->jenjangModel->create($data);
	}

	/**
	 * update single data jenjang jabatan to database
	 *
	 * @param  Integer $id jenjang jabatan id
	 * @param  Array $data jenjang jabatan
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->jenjangModel->whereId($id)->update($data);
	}

	/**
	 * delete single data jenjang jabatan, with soft delete method or not
	 * 
	 * @param  Integer  $id          jenjang jabatan id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->jenjangModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get array list of jenjang resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->jenjangModel->lists('nama_jenjangjabatan','id');
	}

}
