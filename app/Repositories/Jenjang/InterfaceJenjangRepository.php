<?php namespace Certification\Repositories\Jenjang;

interface InterfaceJenjangRepository {

	/**
	 * Get all jenjang jabatan collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null);

	/**
	 * Get jenjang jabatan by given Id
	 * 
	 * @param  Integer $id jenjang jabatan id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data jenjang jabatan to database
	 * 
	 * @param  Array $data jenjang jabatan
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data jenjang jabatan to database
	 *
	 * @param  Integer $id jenjang jabatan id
	 * @param  Array $data jenjang jabatan
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data jenjang jabatan, with soft delete method or not
	 * 
	 * @param  Integer  $id          jenjang jabatan id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
}
