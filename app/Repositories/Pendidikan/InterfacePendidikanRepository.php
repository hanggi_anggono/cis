<?php namespace Certification\Repositories\Pendidikan;

/**
 * Interface for master pendidikan database repository
 */
interface InterfacePendidikanRepository {

	/**
	 * Get all pendidikan collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false);

	/**
	 * Get pendidikan by given Id
	 * 
	 * @param  Integer $id pendidikan id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data pendidikan to database
	 * 
	 * @param  Array $data pendidikan
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data pendidikan to database
	 *
	 * @param  Integer $id pendidikan id
	 * @param  Array $data pendidikan
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data pendidikan, with soft delete method or not
	 * 
	 * @param  Integer  $id          pendidikan id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
}
