<?php namespace Certification\Repositories\Pendidikan;

use Certification\Repositories\Pendidikan\InterfacePendidikanRepository;
use Certification\Models\Pendidikan;
use \Config;

/**
 *  Class DB repository for Master pendidikan using Eloquent
 *  implements InterfacePendidikanRepository
 */
class EloquentDbPendidikan implements InterfacePendidikanRepository {

	/**
	 * Model Pendidikan attribute
	 * @var Model
	 */
	private $pendidikanModel;
	
	/**
	 * Status of function attribute
	 * @var Boolean
	 */
	private $status;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param Pendidikan $pendidikanModel Model pendidikan
	 */
	public function __construct(Pendidikan $pendidikanModel)
	{
		$this->pendidikanModel = $pendidikanModel;
		$this->status       = false;
	}
	
	/**
	 * Get all pendidikan collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->pendidikanModel->orderBy('nama_pendidikan');
		if ($paginate) {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get Pendidikan by given Id
	 * 
	 * @param  Integer $id pendidikan id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->pendidikanModel->findOrFail($id);
	}

	/**
	 * Store data pendidikan to database
	 * 
	 * @param  Array $data pendidikan
	 * @return Boolean
	 */
	public function store($data)
	{
		$affected = $this->pendidikanModel->create($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * update single data pendidikan to database
	 *
	 * @param  Integer $id pendidikan id
	 * @param  Array $data pendidikan
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		$affected = $this->pendidikanModel->where('id', '=', $id)->update($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * delete single data pendidikan, with soft delete method or not
	 * 
	 * @param  Integer  $id          pendidikan id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->pendidikanModel->where('id','=',$id)->delete();
		return true;
	}

	/**
	 * Get array list of pendidikan resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->pendidikanModel->lists('nama_pendidikan','id');
	}
}
