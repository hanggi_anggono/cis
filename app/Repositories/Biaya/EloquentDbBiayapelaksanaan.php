<?php namespace Certification\Repositories\Biaya;

use Certification\Models\Biayapelaksanaan;
use Certification\Services\ExportService;
use \Config;

class EloquentDbBiayapelaksanaan {

	/**
	 * [$biayaModel description]
	 * 
	 * @var [type]
	 */
	private $biayaModel;

	/**
	 * [$pagination description]
	 * 
	 * @var [type]
	 */
	private $pagination;
	
	/**
	 * [$exportService description]
	 * 
	 * @var [type]
	 */
	private $exportService;

	/**
	 * [__construct description]
	 * 
	 * @param Biayapelaksanaan $biaya [description]
	 */
	public function __construct(Biayapelaksanaan $biaya, ExportService $exportService)
	{
		$this->biayaModel    = $biaya;
		$this->exportService = $exportService;
		$this->pagination    = Config::get('certification.default_pagination_count');
	}

	/**
	 * [all description]
	 * 
	 * @param  string $paginate [description]
	 * @param  [type] $filters  [description]
	 * @return [type]           [description]
	 */
	public function all($paginate = 'paginate', $filters = null, $params = null)
	{
		$collection = $this->biayaModel->with('komponenbiaya')
									   ->join('penjadwalan', 'penjadwalan_id', '=', 'penjadwalan.id');

		if (isset($filters)) {
			if ($filters->has('periode')) {
				$collection->where('tanggal_mulai', 'LIKE', $filters->get('periode'))
						   ->orWhere('tanggal_selesai', 'LIKE', $filters->get('periode'));
			}
			if ($filters->has('tanggal_start') && $filters->has('tanggal_end')) {
				$collection->where('tanggal_mulai', '=', $filters->get('tanggal_start'))
					   	   ->where('tanggal_selesai', '=', $filters->get('tanggal_end'));
			}
		} 

		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$unitinduk = $decode->unitinduk_id;
				$collection->where('penjadwalan.unitinduk_id', '=', $decode->unitinduk_id);
			}
			if (isset($decode->lembagasertifikasi_id)) {
				$collection->where('penjadwalan.lembagasertifikasi_id', '=', $decode->lembagasertifikasi_id);
			}
		}

		$collection->orderBy('biayapelaksanaan.created_at', 'desc');
		
		if (isset($paginate) && $paginate == 'paginate') {
			return $collection->paginate($this->pagination);
		}

		return $collection->get();
	}

	/**
	 * [allByPenjadwalan description]
	 * 
	 * @param  [type] $penjadwalan_id [description]
	 * @return [type]                 [description]
	 */
	public function allByPenjadwalan($penjadwalan_id)
	{
		$collection = $this->biayaModel->with('komponenbiaya', 'penjadwalan')->wherePenjadwalanId($penjadwalan_id)->orderBy('created_at', 'desc')->get();
		return $collection;
	}

	/**
	 * [getById description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getById($id)
	{
		return $this->biayaModel->find($id);
	}

	/**
	 * [getTotalBiaya description]
	 * 
	 * @param  [type] $penjadwalan_id [description]
	 * @param  [type] $filters        [description]
	 * @return [type]                 [description]
	 */
	public function getTotalBiaya($penjadwalan_id = null, $filters = null, $params = null)
	{
		$collection = $this->biayaModel->join('penjadwalan', 'penjadwalan_id', '=', 'penjadwalan.id');
		
		if (isset($penjadwalan_id)) {
			$collection->wherePenjadwalanId($penjadwalan_id);
		}

		if (isset($filters)) {
			if ($filters->has('periode')) {
				$collection->where('tanggal_mulai', 'LIKE', $filters->get('periode'))
						   ->orWhere('tanggal_selesai', 'LIKE', $filters->get('periode'));
			}
			if ($filters->has('tanggal_start') && $filters->has('tanggal_end')) {
				$collection->where('tanggal_mulai', '=', $filters->get('tanggal_start'))
					   	   ->where('tanggal_selesai', '=', $filters->get('tanggal_end'));
			}
		}

		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$unitinduk = $decode->unitinduk_id;
				$collection->where('penjadwalan.unitinduk_id', '=', $decode->unitinduk_id);
			}
			if (isset($decode->lembagasertifikasi_id)) {
				$collection->where('penjadwalan.lembagasertifikasi_id', '=', $decode->lembagasertifikasi_id);
			}
		}
		
		return $collection->sum('biaya');
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		return $this->biayaModel->create($data);
	}

	/**
	 * [update description]
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function update($id, $data)
	{
		return $this->biayaModel->whereId($id)->update($data);
	}

	/**
	 * [delete description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id)
	{
		return $this->biayaModel->whereId($id)->delete();
	}

	/**
	 * [export description]
	 * 
	 * @param  string $type    [description]
	 * @param  [type] $filters [description]
	 * @param  [type] $params  [description]
	 * @return [type]          [description]
	 */
	public function export($type = 'excel', $filters = null, $params = null)
	{
		$collection = $this->all(null, $filters, $params);
		return $this->exportService->export($collection, $type, ['viewName' => 'biaya', 'orientation' => 'portrait', 'filename' => 'report_biaya']);
	}
}
