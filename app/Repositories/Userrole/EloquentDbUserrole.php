<?php namespace Certification\Repositories\Userrole;

use Certification\Models\User;
use \Config, \Hash;

class EloquentDbUserrole {

	/**
	 * [$users description]
	 * 
	 * @var [type]
	 */
	private $users;

	/**
	 * [__construct description]
	 * 
	 * @param User $users [description]
	 */
	public function __construct(User $users)
	{
		$this->users = $users;
	}

	/**
	 * [all description]
	 * 
	 * @param  string $paginate [description]
	 * @param  [type] $role     [description]
	 * @return [type]           [description]
	 */
	public function all($paginate = 'paginate', $role = null)
	{
		$collection = $this->users->with('roles')->orderBy('created_at', 'desc');
		if (isset($paginate) && $paginate = 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * [getById description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getById($id)
	{
		return $this->users->with('roles')->find($id);
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		$data['password'] = Hash::make($data['password']);
		$user = $this->users->create(array_only($data, ['nip', 'password']));
		$user->roles()->attach($data['role_id']);
		return $user;
	}

	/**
	 * [update description]
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function update($id, $data)
	{
		$role_id = $data['role_id'];
		if (! empty($data['password'])) {
			$data['password'] = Hash::make($data['password']);
			$data = array_only($data, ['nip', 'password']);
		} else {
			$data = array_only($data, ['nip']);
		}

		$user = $this->users->where('id', '=', $id)->first();
		$user->update($data);
		$user->roles()->sync([$role_id]);
		return $user;
	}

	/**
	 * [delete description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id)
	{
		return $this->users->where('id', '=', $id)->delete();
	}
	
}
