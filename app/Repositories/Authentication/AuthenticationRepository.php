<?php namespace Certification\Repositories\Authentication;

use Certification\Models\User;
use Certification\Models\Administrator;
use Certification\Models\Asesor;
use Certification\Models\Peserta;
use \Auth, \Hash, \Session;

class AuthenticationRepository {

	/**
	 * [$userModel description]
	 * 
	 * @var [type]
	 */
	private $userModel;

	/**
	 * [$administratorModel description]
	 * 
	 * @var [type]
	 */
	private $administratorModel;
	
	/**
	 * [$asesorModel description]
	 * 
	 * @var [type]
	 */
	private $asesorModel;
	
	/**
	 * [$pesertaModel description]
	 * 
	 * @var [type]
	 */
	private $pesertaModel;
	
	/**
	 * [$errors description]
	 * 
	 * @var [type]
	 */
	private $errors;

	/**
	 * [$redirectAfterSignin description]
	 * 
	 * @var [type]
	 */
	private $redirectSigninSuccess;

	/**
	 * [__construct description]
	 * 
	 * @param User          $userModel          [description]
	 * @param Administrator $administratorModel [description]
	 * @param Asesor        $asesorModel        [description]
	 * @param Peserta       $pesertaModel       [description]
	 */
	public function __construct(User $userModel, Administrator $administratorModel, Asesor $asesorModel, Peserta $pesertaModel)
	{
		$this->userModel          = $userModel;
		$this->administratorModel = $administratorModel;
		$this->asesorModel        = $asesorModel;
		$this->pesertaModel       = $pesertaModel;
	}

	/**
	 * [signin description]
	 * 
	 * @return [type] [description]
	 */
	public function signin($credential)
	{
		$status = Auth::attempt($credential);
		if ($status) {
			$this->getUnit(Auth::user()->nip);
			$this->redirectSigninSuccess = 'dashboard';
		} else {
			$this->redirectSigninSuccess = '';
		}
		return $status;
	}

	/**
	 * [signout description]
	 * 
	 * @return [type] [description]
	 */
	public function signout()
	{
		Auth::logout();
		Session::flush();
	}

	/**
	 * [updatePassword description]
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function updatePassword($id, $data)
	{
		if ($this->isPasswordValid($id, $data['current_password'])) {
			$affected = $this->userModel->whereId($id)->update(['password' => Hash::make($data['password'])]);
			if ($affected) {
				return true;
			} else {
				$this->errors = 'Gagal menyimpan data password';
				return false;
			}
		}
		$this->errors = 'Password saat ini anda salah, periksa kembali isian password';
		return false;
	}

	/**
	 * [getErrors description]
	 * 
	 * @return [type] [description]
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * [getRedirect description]
	 * 
	 * @return [type] [description]
	 */
	public function getRedirect()
	{
		return $this->redirectSigninSuccess;
	}

	/**
	 * [isPasswordValid description]
	 * 
	 * @param  [type]  $id              [description]
	 * @param  [type]  $currentPassword [description]
	 * @return boolean                  [description]
	 */
	private function isPasswordValid($id, $currentPassword)
	{
		$user = $this->userModel->whereId($id)->select('password')->first();
		if (Hash::check($currentPassword, $user->password)) {
			return true;
		}
		return false;
	}

	/**
	 * [getUnit description]
	 * 
	 * @return [type] [description]
	 */
	private function getUnit()
	{

		$params = null;
		$user   = $this->userModel->where('id', '=', Auth::id())->with('roles')->first();
		$role   = $user->roles[0]->name;

		if ($role != 'superadmin') {
			if ($role == 'employee' || $role == 'sdm') {
				$peserta = $this->pesertaModel->select('id','unitinduk_id', 'unitnonpln_id')->where('nip', '=', $user->nip)->first();
				if (isset($peserta)) {
					$params = $peserta->toJson();
				}
			} 
			
		}
		
		Session::put('role', $role);
		if (isset($params)) {
			Session::put('params', $params);
		}
		

	}
}
