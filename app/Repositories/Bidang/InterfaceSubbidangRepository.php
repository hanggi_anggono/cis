<?php namespace Certification\Repositories\Bidang;

/**
 * Interface for master sub bidang database repository
 */
interface InterfaceSubbidangRepository {

	/**
	 * Get all sub bidang collection with paginate or not
	 * 
	 * @param  Integer $bidang_id bidang id
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($bidang_id, $paginate = null);

	/**
	 * Get sub bidang by given Id
	 * 
	 * @param  Integer $id sub bidang id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data sub bidang to database
	 * 
	 * @param  Array $data sub bidang
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data sub bidang to database
	 *
	 * @param  Integer $id sub bidang id
	 * @param  Array $data sub bidang
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data sub bidang, with soft delete method or not
	 * 
	 * @param  Integer  $id          sub bidang id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
}
