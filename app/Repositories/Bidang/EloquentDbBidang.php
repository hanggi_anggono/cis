<?php namespace Certification\Repositories\Bidang;

use Certification\Repositories\Bidang\InterfaceBidangRepository;
use Certification\Models\Bidang;
use \Config;

class EloquentDbBidang implements InterfaceBidangRepository {

	/**
	 * Model bidang attribute
	 * 
	 * @var Model
	 */
	private $bidangModel;
	
	/**
	 * Status of function attribute
	 * 
	 * @var Boolean
	 */
	private $status;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param bidang $bidangModel Model bidang
	 */
	public function __construct(Bidang $bidangModel)
	{
		$this->bidangModel = $bidangModel;
		$this->status      = false;
	}

	/**
	 * Get all bidang collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null)
	{
		$collection = $this->bidangModel->with('subbidang')->orderBy('nama_bidang');
		if (isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get list array of Bidang collection
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->bidangModel->lists('nama_bidang','id');
	}

	/**
	 * Get all sub bidang collection with paginate or not with given bidang id
	 *
	 * @param  Integer $bidangId Bidang Id
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function subbidang($bidangId, $paginate = null)
	{
		$collection = $this->bidangModel->find($bidangId)
										->subbidang()
										->orderBy('nama_subbidang');

		if (isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get bidang by given Id
	 * 
	 * @param  Integer $id bidang id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->bidangModel->findOrFail($id);
	}

	/**
	 * store data bidang to database
	 * 
	 * @param  Array $data bidang
	 * @return Boolean
	 */
	public function store($data)
	{
		$affected = $this->bidangModel->create($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * update single data bidang to database
	 *
	 * @param  Integer $id bidang id
	 * @param  Array $data bidang
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		$affected = $this->bidangModel->whereId($id)->update($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * delete single data bidang, with soft delete method or not
	 * 
	 * @param  Integer  $id          bidang id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->bidangModel->whereId($id)->delete();
		return true;
	}
}
