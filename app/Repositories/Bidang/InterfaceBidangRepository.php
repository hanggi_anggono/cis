<?php namespace Certification\Repositories\Bidang;

/**
 * Interface for master bidang database repository
 */
interface InterfaceBidangRepository {

	/**
	 * Get all bidang collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null);

	/**
	 * Get bidang by given Id
	 * 
	 * @param  Integer $id bidang id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data bidang to database
	 * 
	 * @param  Array $data bidang
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data bidang to database
	 *
	 * @param  Integer $id bidang id
	 * @param  Array $data bidang
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data bidang, with soft delete method or not
	 * 
	 * @param  Integer  $id          bidang id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
}
