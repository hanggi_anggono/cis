<?php namespace Certification\Repositories\Bidang;

use Certification\Repositories\Bidang\InterfaceSubbidangRepository;
use Certification\Models\Subbidang;
use \Config;

/**
 * Sub bidang database provider implementing Interface Sub Bidang Repository
 * Using Eloquent Driver
 */
class EloquentDbSubbidang implements InterfaceSubbidangRepository {

	/**
	 * Model Sub bidang attribute
	 * 
	 * @var Model
	 */
	private $subsubbidangModel;
	
	/**
	 * Status of function attribute
	 * 
	 * @var Boolean
	 */
	private $status;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param bidang $subbidangModel Model bidang
	 */
	public function __construct(Subbidang $subbidangModel)
	{
		$this->subbidangModel = $subbidangModel;
		$this->status         = false;
	}

	/**
	 * Get all bidang collection with paginate or not
	 *
	 * @param  Integer $bidang_id bidang id
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($bidang_id, $paginate = null)
	{
		if (empty($bidang_id)) {
			$collection = $this->subbidangModel->orderBy('nama_subbidang');
		} else {
			$collection = $this->subbidangModel->byBidang($bidang_id)->orderBy('nama_subbidang');
		}
		
		if (isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get bidang by given Id
	 * 
	 * @param  Integer $id bidang id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->subbidangModel->findOrFail($id);
	}

	/**
	 * store data bidang to database
	 * 
	 * @param  Array $data bidang
	 * @return Boolean
	 */
	public function store($data)
	{
		$affected = $this->subbidangModel->create($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * update single data bidang to database
	 *
	 * @param  Integer $id bidang id
	 * @param  Array $data bidang
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		$affected = $this->subbidangModel->whereId($id)->update($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * delete single data bidang, with soft delete method or not
	 * 
	 * @param  Integer  $id          bidang id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->subbidangModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get array lists collection of sub bidang
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->subbidangModel->lists('nama_subbidang','id');
	}

	/**
	 * Get array lists collection of subbidang from given bidang id
	 * 
	 * @param  Integer $id Bidang id
	 * @return Array
	 */
	public function listsByBidang($id)
	{
		return $this->subbidangModel->where('bidang_id', '=', $id)->lists('nama_subbidang', 'id');
	}
}
