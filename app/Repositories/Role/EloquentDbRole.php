<?php namespace Certification\Repositories\Role;

use Certification\Models\Role;

class EloquentDbRole {

	/**
	 * [$roleModel description]
	 * 
	 * @var [type]
	 */
	private $roleModel;

	/**
	 * [__construct description]
	 * 
	 * @param Role $roleModel [description]
	 */
	public function __construct(Role $roleModel)
	{
		$this->roleModel = $roleModel;
	}

	/**
	 * [all description]
	 * 
	 * @return [type] [description]
	 */
	public function all()
	{
		return $this->roleModel->all();
	}

	/**
	 * [getById description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getById($id)
	{
		return $this->roleModel->with('permissions')->find($id);
	}

	/**
	 * [getByName description]
	 * 
	 * @param  [type] $name [description]
	 * @return [type]       [description]
	 */
	public function getByName($name)
	{
		return $this->roleModel->where('name', '=', $name)->first();
	}

	/**
	 * [lists description]
	 * 
	 * @return [type] [description]
	 */
	public function lists()
	{
		return $this->roleModel->lists('display_name', 'id');
	}

	/**
	 * [syncPermission description]
	 * 
	 * @param  [type] $id          [description]
	 * @param  [type] $permissions [description]
	 * @return [type]              [description]
	 */
	public function syncPermission($id, $permissions)
	{
		return $this->roleModel->find($id)->permissions()->sync($permissions);
	}
	
}
