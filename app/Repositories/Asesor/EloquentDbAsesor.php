<?php namespace Certification\Repositories\Asesor;

use Certification\Models\Asesor;
use Certification\Models\Sertifikatasesor;
use Certification\Repositories\Asesor\InterfaceAsesorRepository;
use Certification\Models\Subbidangasesor;
use Certification\Models\Unitcabang;
use Certification\Services\ExportService;

use \Config, \DB;

/**
 *  Class DB repository for Master asesor using Eloquent
 *  implements InterfaceAsesorRepository
 */
class EloquentDbAsesor implements InterfaceAsesorRepository {

	/**
	 * [$asesorModel description]
	 * 
	 * @var [type]
	 */
	private $asesorModel;

	/**
	 * [$sertifikatAsesorModel description]
	 * 
	 * @var [type]
	 */
	private $sertifikatAsesorModel;

	/**
	 * [$subbidangasesorModel description]
	 * 
	 * @var [type]
	 */
	private $subbidangasesorModel;

	/**
	 * [$exportService description]
	 * 
	 * @var [type]
	 */
	private $exportService;

	/**
	 * [$unitcabangModel description]
	 * 
	 * @var [type]
	 */
	private $unitcabangModel;

	/**
	 * [__construct description]
	 * 
	 * @param Asesor $asesorModel [description]
	 */
	public function __construct(Asesor $asesorModel, Sertifikatasesor $sertifikatAsesorModel, Subbidangasesor $subbidangasesorModel, ExportService $exportService, Unitcabang $unitcabangModel)
	{
		$this->asesorModel           = $asesorModel;
		$this->exportService         = $exportService;
		$this->sertifikatAsesorModel = $sertifikatAsesorModel;
		$this->subbidangasesorModel  = $subbidangasesorModel;
		$this->unitcabangModel = $unitcabangModel;
	}

	/**
	 * [countAll description]
	 * 
	 * @param  [type] $filters [description]
	 * @param  [type] $params  [description]
	 * @return [type]          [description]
	 */
	public function countAll($filters = null, $params = null)
	{
		return $this->filters($filters, $params)->count();
	}

	/**
	 * [countPln description]
	 * 
	 * @param  [type] $filters [description]
	 * @param  [type] $params  [description]
	 * @return [type]          [description]
	 */
	public function countPln($filters = null, $params = null)
	{
		return $this->filters($filters, $params)->groupPln()->count();
	}

	/**
	 * [countNonPln description]
	 * 
	 * @param  [type] $filters [description]
	 * @param  [type] $params  [description]
	 * @return [type]          [description]
	 */
	public function countNonPln($filters = null, $params = null)
	{
		return $this->filters($filters, $params)->groupNonPln()->count();
	}

	/**
	 * [countSertifikat description]
	 * 
	 * @param  [type] $filters [description]
	 * @param  [type] $params  [description]
	 * @return [type]          [description]
	 */
	public function countSertifikat($filters = null, $params = null)
	{
		return $this->sertifikatAsesorModel->count();
	}

	/**
	 * Get all asesor collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @param  Http Request $filters Http Request parameter
	 * @return Collection
	 */
	public function all($paginate = null, $filters = null, $params = null)
	{
		$collection = $this->filters($filters, $params)->orderBy('asesor.created_at','desc');
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get asesor by given Id
	 * 
	 * @param  Integer $id asesor id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->asesorModel->withRelationship()->find($id);
	}

	/**
	 * store data asesor to database
	 * 
	 * @param  Array $data asesor
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->asesorModel->create($this->checkType($data));
	}

	/**
	 * Attach kelompok / lembaga sertifikasi to pivot table
	 * 
	 * @param  Integer $id          Asesor Id
	 * @param  Mixed $data Kelompok / lembaga data
	 * @return void
	 */
	public function storeKelompok($id, $data)
	{
		$this->asesorModel->find($id)->kelompok()->attach([$data['lembagasertifikasi_id'] => ['tanggalpenetapan'=>$data['tanggalpenetapan']]]);
	}

	/**
	 * [storeSubbidang description]
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function storeSubbidang($id, $data)
	{
		if (! $this->isSubbidangExist($id, $data['subbidang_id'])) {
			$this->asesorModel->find($id)->subbidang()->attach($data['subbidang_id']);
			return true;
		}
		return false;
	}

	/**
	 * update single data asesor to database
	 *
	 * @param  Integer $id asesor id
	 * @param  Array $data asesor
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->asesorModel->whereId($id)->update($this->checkType($data, 'update'));
	}

	/**
	 * delete single data asesor, with soft delete method or not
	 * 
	 * @param  Integer  $id          asesor id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->asesorModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Delete / detach lembaga sertifikasi in asesor
	 * 
	 * @param  Integer $id          Asesor Id
	 * @param  Integer $kelompok_id Kelompok / lembaga sertifikasi ID
	 * @return void
	 */
	public function deleteKelompok($id, $kelompok_id)
	{
		$this->asesorModel->find($id)->kelompok()->detach($kelompok_id);
	}

	/**
	 * [deleteSubbidang description]
	 * 
	 * @param  [type] $id          [description]
	 * @param  [type] $subbidangId [description]
	 * @return [type]              [description]
	 */
	public function deleteSubbidang($id, $subbidangId)
	{
		$this->asesorModel->find($id)->subbidang()->detach($subbidangId);
	}

	/**
	 * Get lists of asesor data
	 * 
	 * @return Array
	 */
	public function lists($lskId = null, $subbidangId = null)
	{
		$collection =  $this->asesorModel->select('nama', 'asesor.id');
		if (isset($lskId)) {
			$collection->join('kelompokasesor', 'kelompokasesor.asesor_id', '=', 'asesor.id')
					   ->where('kelompokasesor.lembagasertifikasi_id', '=', $lskId);
		}
		if (isset($lskId)) {
			$collection->join('subbidangasesor', 'subbidangasesor.asesor_id', '=', 'asesor.id')
					   ->orWhere('subbidangasesor.subbidang_id', '=', $subbidangId);
		}

		return $collection->lists('nama','id');
	}

	/**
	 * [testHistory description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function testHistory($id)
	{
		return $this->asesorModel->join('penjadwalanasesor', 'asesor_id', '=', 'asesor.id')
								 ->join('penjadwalan', 'penjadwalanasesor.penjadwalan_id', '=', 'penjadwalan.id')
								 ->join('kompetensipenjadwalan', 'kompetensipenjadwalan.penjadwalan_id', '=', 'penjadwalan.id')
								 ->join('kompetensi', 'kompetensi_id', '=', 'kompetensi.id')
								 ->join('unitinduk', 'penjadwalan.unitinduk_id', '=', 'unitinduk.id')
								 ->select('nama_kompetensi', 'asesor.nama as nama_asesor', 'tanggal_mulai', 'tanggal_selesai', 'posisi', 'nama_unitinduk')
								 ->where('asesor_id', '=', $id)
								 ->get();
	}

	/**
	 * [export description]
	 * 
	 * @param  string $type    [description]
	 * @param  array  $filters [description]
	 * @param  [type] $params  [description]
	 * @param  [type] $role    [description]
	 * @return [type]          [description]
	 */
	public function export($type = 'excel', $filters = [], $params = null)
	{
		$data['asesor'] = $this->all(null, $filters, $params);
		return $this->exportService->export($data['asesor'], $type, ['viewName' => 'asesor', 'orientation' => 'landscape', 'filename' => 'report_asesor']);
	}

	/**
	 * [filters description]
	 * 
	 * @param  [type] $filters [description]
	 * @param  [type] $params  [description]
	 * @return [type]          [description]
	 */
	private function filters($filters = null, $params = null)
	{
		$unitinduk  = null;
		$collection = $this->asesorModel->withRelationship();

		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$unitinduk = $decode->unitinduk_id;
				$collection->where('unitinduk_id', '=', $decode->unitinduk_id);
			}
			if (isset($decode->lembagasertifikasi_id)) {
				$collection->join('kelompokasesor','kelompokasesor.asesor_id', '=', 'asesor.id')->orWhere('kelompokasesor.lembagasertifikasi_id', '=', $decode->lembagasertifikasi_id);
			}
		}

		if (isset($filters)) {
			if ($filters->has('group')) {
				$collection->where('tipe_asesor', '=', $filters->get('group'));
			}	

			if ($filters->has('uinduk')) {
				$collection->where('unitinduk_id', '=', $filters->get('uinduk'));
			}

			if ($filters->has('unonpln')) {
				$collection->where('unitnonpln_id', '=', $filters->get('unonpln'));
			}

			if ($filters->has('units')) {
				$collection->leftJoin('unitinduk', 'unitinduk.id', '=', 'asesor.unitinduk_id')
						   ->leftJoin('unitnonpln', 'unitnonpln.id', '=', 'asesor.unitnonpln_id')
						   ->where(function($query) use($filters)
							{
								$query->where('nama_unitinduk', 'LIKE', '%'.$filters->get('units').'%')
									  ->orWhere('nama_unitnonpln', 'LIKE', '%'.$filters->get('units').'%');
							});
			}

			if($filters->has('nipname')) {
				$collection->where('nip', 'LIKE', '%'.$filters->get('nipname').'%')->orWhere('nama', 'LIKE', '%'.$filters->get('nipname').'%');
			}
		}
		return $collection;

	}

	/**
	 * [checkType description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	private function checkType($data, $requestType = null)
	{
		if($data['tipe_asesor'] == 'pln') {
			
			$data['unitnonpln_id'] = null;
			
			if ($this->unitcabangModel->where('unitinduk_id', '=', $data['unitinduk_id'])->count() == 0 && $requestType == 'update') {
				$data['unitcabang_id']  = null;
				$data['unitranting_id'] = null;
			}
			if ($data['unitcabang_id'] == 0) {
				$data['unitcabang_id'] = null;
			}

			if ($data['unitranting_id'] == 0) {
				$data['unitranting_id'] = null;
			}

		} else {
			$data['unitinduk_id']   = null;
			$data['unitcabang_id']  = null;
			$data['unitranting_id'] = null;
		}
		return $data;
	}

	/**
	 * [isSubbidangExist description]
	 * 
	 * @param  [type]  $id          [description]
	 * @param  [type]  $subbidangId [description]
	 * @return boolean              [description]
	 */
	private function isSubbidangExist($id, $subbidangId)
	{
		$count = $this->subbidangasesorModel->where('asesor_id', '=', $id)->where('subbidang_id', '=', $subbidangId)->count();
		if ($count == 1) {
			return true;
		}
		return false;
	}

}
