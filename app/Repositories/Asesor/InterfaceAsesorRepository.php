<?php namespace Certification\Repositories\Asesor;

/**
 * Interface for asesor repositories
 */
interface InterfaceAsesorRepository {

	/**
	 * Get all asesor collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null);

	/**
	 * Get asesor by given Id
	 * 
	 * @param  Integer $id asesor id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data asesor to database
	 * 
	 * @param  Array $data asesor
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data asesor to database
	 *
	 * @param  Integer $id asesor id
	 * @param  Array $data asesor
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data asesor, with soft delete method or not
	 * 
	 * @param  Integer  $id          asesor id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
	
}
