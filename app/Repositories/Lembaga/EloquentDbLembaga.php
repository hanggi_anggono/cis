<?php namespace Certification\Repositories\Lembaga;

use Certification\Repositories\Lembaga\InterfaceLembaga;
use Certification\Models\Lembaga;
use \Config;

/**
 *  Class DB repository for Master Lembaga using Eloquent
 *  implements InterfaceLembaga
 */
class EloquentDbLembaga implements InterfaceLembaga {

	/**
	 * Model lembaga attribute
	 * @var Model
	 */
	private $lembagaModel;
	
	/**
	 * Status of function attribute
	 * @var Boolean
	 */
	private $status;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param Lembaga $lembagaModel Model lembaga
	 */
	public function __construct(Lembaga $lembagaModel)
	{
		$this->lembagaModel = $lembagaModel;
		$this->status       = false;
	}
	/**
	 * Get all lembaga collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->lembagaModel->orderBy('nama_lembaga');
		if ($paginate) {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get lembaga by given Id
	 * 
	 * @param  Integer $id lembaga id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->lembagaModel->findOrFail($id);
	}

	/**
	 * store data lembaga to database
	 * 
	 * @param  Array $data lembaga
	 * @return Boolean
	 */
	public function store($data)
	{
		$affected = $this->lembagaModel->create($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * update single data lembaga to database
	 *
	 * @param  Integer $id lembaga id
	 * @param  Array $data lembaga
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		$affected = $this->lembagaModel->where('id', '=', $id)->update($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * delete single data lembaga, with soft delete method or not
	 * 
	 * @param  Integer  $id          lembaga id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->lembagaModel->where('id','=',$id)->delete();
		return true;
	}

	/**
	 * Get array list of lembaga resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->lembagaModel->lists('nama_lembaga','id');
	}	
}
