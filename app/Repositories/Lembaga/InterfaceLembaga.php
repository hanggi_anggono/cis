<?php namespace Certification\Repositories\Lembaga;

/**
 * Interface for master lembaga database repository
 */
interface InterfaceLembaga {

	/**
	 * Get all lembaga collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false);

	/**
	 * Get lembaga by given Id
	 * 
	 * @param  Integer $id lembaga id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data lembaga to database
	 * 
	 * @param  Array $data lembaga
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data lembaga to database
	 *
	 * @param  Integer $id lembaga id
	 * @param  Array $data lembaga
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data lembaga, with soft delete method or not
	 * 
	 * @param  Integer  $id          lembaga id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);

}
