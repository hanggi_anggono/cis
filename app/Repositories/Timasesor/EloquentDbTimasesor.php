<?php namespace Certification\Repositories\Timasesor;

use Certification\Repositories\Timasesor\InterfaceTimasesorRepository;
use Certification\Models\Timasesor;
use Config;

/**
 *  Class DB repository for Master Timasesor using Eloquent
 *  implements InterfaceTimasesorRepository
 */
class EloquentDbTimasesor implements InterfaceTimasesorRepository {

	/**
	 * Timasesor model
	 * 
	 * @var Model
	 */
	private $TimasesorModel;

	/**
	 * Class instance
	 * 
	 * @param Timasesor $TimasesorModel Timasesor model
	 * @return void
	 */
	public function __construct(Timasesor $TimasesorModel)
	{
		$this->TimasesorModel = $TimasesorModel;
	}

	/**
	 * Get all Timasesor collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->TimasesorModel->orderBy('jabatantim');
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get Timasesor by given Id
	 * 
	 * @param  Integer $id Timasesor id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->TimasesorModel->find($id);
	}

	/**
	 * store data Timasesor to database
	 * 
	 * @param  Array $data Timasesor
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->TimasesorModel->create($data);
	}

	/**
	 * update single data Timasesor to database
	 *
	 * @param  Integer $id Timasesor id
	 * @param  Array $data Timasesor
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->TimasesorModel->whereId($id)->update($data);
	}

	/**
	 * delete single data Timasesor, with soft delete method or not
	 * 
	 * @param  Integer  $id          Timasesor id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->TimasesorModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get array list of kompetensi resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->TimasesorModel->lists('jabatantim','id');
	}

}
