<?php namespace Certification\Repositories\Timasesor;

/**
 * Interface for Tim Asesor repositories
 */
interface InterfaceTimasesorRepository {

	/**
	 * Get all Tim Asesor collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null);

	/**
	 * Get Tim Asesor by given Id
	 * 
	 * @param  Integer $id Tim Asesor id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data Tim Asesor to database
	 * 
	 * @param  Array $data Tim Asesor
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data Tim Asesor to database
	 *
	 * @param  Integer $id Tim Asesor id
	 * @param  Array $data Tim Asesor
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data Tim Asesor, with soft delete method or not
	 * 
	 * @param  Integer  $id          Tim Asesor id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
}
