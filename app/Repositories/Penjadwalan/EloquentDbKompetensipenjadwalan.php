<?php namespace Certification\Repositories\Penjadwalan;

use Certification\Models\Kompetensipenjadwalan;

class EloquentDbKompetensipenjadwalan {

	/**
	 * [$kompetensipenjadwalan description]
	 * 
	 * @var [type]
	 */
	private $kompetensipenjadwalan;

	/**
	 * [__construct description]
	 * 
	 * @param Kompetensipenjadwalan $kompetensipenjadwalan [description]
	 */
	public function __construct(Kompetensipenjadwalan $kompetensipenjadwalan)
	{
		$this->kompetensipenjadwalan = $kompetensipenjadwalan;
	}

	/**
	 * [all description]
	 * 
	 * @param  [type] $penjadwalanId [description]
	 * @return [type]                [description]
	 */
	public function all($penjadwalanId = null, $params = null)
	{
		$collection = $this->kompetensipenjadwalan->with(['peserta' => function($query) use($params)
							{
								if (isset($params)) {
									$decode = json_decode($params['params']);
									if (isset($decode->unitinduk_id) && $params['role'] == 'sdm') {
										$query->where('unitinduk_id', '=', $decode->unitinduk_id);
									}
								} else {
									$query;
								}
							}]);
		if (isset($penjadwalanId)) {
			$collection->where('penjadwalan_id', '=', $penjadwalanId);
		}
		
		return $collection->get();
	}

	/**
	 * [isNotEmpty description]
	 * 
	 * @param  [type]  $penjadwalanId [description]
	 * @param  [type]  $kompetensiId  [description]
	 * @return boolean                [description]
	 */
	public function isNotEmpty($penjadwalanId, $kompetensiId)
	{
		$count = $this->kompetensipenjadwalan->isKompetensiPenjadwalanExist($penjadwalanId, $kompetensiId)->count();
		if ($count != 0) {
			return true;
		}
		return false;
	}
}
