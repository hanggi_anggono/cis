<?php namespace Certification\Repositories\Penjadwalan;

use Certification\Models\Evaluasi;
use Certification\Models\Penjadwalan;
use Certification\Models\Penjadwalanasesor;
use Certification\Repositories\Peserta\InterfacePesertaRepository;
use Certification\Repositories\Penjadwalan\EloquentDbPesertapenjadwalan;
use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Repositories\Penjadwalan\EloquentDbKompetensipenjadwalan;

use \Config, \DB;

class EloquentDbPenjadwalan {

	/**
	 * [$penjadwalanModel description]
	 * 
	 * @var [type]
	 */
	private $penjadwalanModel;
	
	/**
	 * [$penjadwalanAsesorModel description]
	 * 
	 * @var [type]
	 */
	private $penjadwalanAsesorModel;
	
	/**
	 * [$kompetensiPenjadwalan description]
	 * 
	 * @var [type]
	 */
	private $kompetensiPenjadwalan;
	
	/**
	 * [$pesertaPenjadwalan description]
	 * 
	 * @var [type]
	 */
	private $pesertaPenjadwalan;
	
	/**
	 * [$kompetensiRepository description]
	 * 
	 * @var [type]
	 */
	private $kompetensiRepository;
	
	/**
	 * [$pesertaRepository description]
	 * 
	 * @var [type]
	 */
	private $pesertaRepository;
	
	/**
	 * [$evaluasiModel description]
	 * 
	 * @var [type]
	 */
	private $evaluasiModel;

	/**
	 * [__construct description]
	 * 
	 * @param Penjadwalan                     $penjadwalanModel       [description]
	 * @param Penjadwalanasesor               $penjadwalanAsesorModel [description]
	 * @param InterfaceKompetensiRepository   $kompetensiRepository   [description]
	 * @param EloquentDbKompetensipenjadwalan $kompetensiPenjadwalan  [description]
	 * @param EloquentDbPesertapenjadwalan    $pesertaPenjadwalan     [description]
	 * @param InterfacePesertaRepository      $pesertaRepository      [description]
	 * @param Evaluasi                        $evaluasiModel          [description]
	 */
	public function __construct(
			Penjadwalan $penjadwalanModel, 
			Penjadwalanasesor $penjadwalanAsesorModel, 
			InterfaceKompetensiRepository $kompetensiRepository, 
			EloquentDbKompetensipenjadwalan $kompetensiPenjadwalan, 
			EloquentDbPesertapenjadwalan $pesertaPenjadwalan, 
			InterfacePesertaRepository $pesertaRepository, 
			Evaluasi $evaluasiModel
	){
		$this->penjadwalanModel       = $penjadwalanModel;
		$this->penjadwalanAsesorModel = $penjadwalanAsesorModel;
		$this->kompetensiRepository   = $kompetensiRepository;
		$this->kompetensiPenjadwalan  = $kompetensiPenjadwalan;
		$this->pesertaPenjadwalan     = $pesertaPenjadwalan;
		$this->pesertaRepository      = $pesertaRepository;
		$this->evaluasiModel          = $evaluasiModel;
	}

	/**
	 * [all description]
	 * 
	 * @param  [type] $pagination [description]
	 * @param  [type] $filters    [description]
	 * @param  [type] $params     [description]
	 * @param  [type] $take       [description]
	 * @param  [type] $role       [description]
	 * @return [type]             [description]
	 */
	public function all($pagination = null, $filters = null, $params = null, $take = null, $role = null)
	{
		$collection = $this->penjadwalanModel->withRelationships();
		if (isset($params)) {
			$decode = json_decode($params);
			$collection->with(['peserta' => function($query) use($decode) {
				if (isset($decode->unitinduk_id)) {
					$query->where('unitinduk_id', '=', $decode->unitinduk_id);
				}
			}]);
			if (isset($decode->lembagasertifikasi_id)) {
				$collection->where('lembagasertifikasi_id', '=', $decode->lembagasertifikasi_id);
			}
			if (isset($role) && $role == 'employee') {
				if (isset($decode->id)) {
					$collection->join('pesertakompetensipenjadwalan', 'pesertakompetensipenjadwalan.penjadwalan_id', '=', 'penjadwalan.id')->where('pesertakompetensipenjadwalan.peserta_id', '=', $decode->id);
				}
			}

		}

		if (isset($filters)) {
			if ($filters->has('tanggal')) {
				$collection->where('tanggal_mulai', '=', $filters->get('tanggal'));
			}
			if ($filters->has('tuk')) {
				$collection->where('udiklat_id', '=', $filters->get('tuk'));
			}
			if ($filters->has('lsk')) {
				$collection->where('lembagasertifikasi_id', '=', $filters->get('lsk'));
			}
		}

		$collection->orderBy('created_at', 'desc');
		if (isset($pagination) && $pagination == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		if (isset($take)) {
			$collection->take($take);
		}
		return $collection->get();
	}

	/**
	 * [allByKompetensi description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function allByKompetensi($id)
	{
		return $this->penjadwalanModel->with('kompetensi')->find($id);
	}

	/**
	 * [allByPeserta description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function allByPeserta($id)
	{
		return $this->penjadwalanModel->find($id)->peserta();
	}

	/**
	 * [getById description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getById($id, $withrelationship = true)
	{
		$collection = $this->penjadwalanModel;
		if ($withrelationship) {
			return $collection->withRelationships()->whereId($id)->first();
		}
		return $collection->find($id);
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		DB::transaction(function() use($data)
		{
			// store penjadwalan
			$penjadwalan = $this->penjadwalanModel->create([
								'tanggal_mulai'         => $data['tanggal_mulai'],
								'tanggal_selesai'       => $data['tanggal_selesai'],
								'udiklat_id'            => $data['udiklat_id'],
								'unitinduk_id'          => $data['unitinduk_id'],
								'unitcabang_id'         => (! empty($data['unitcabang_id'])) ? $data['unitcabang_id'] : null,
								'unitranting_id'        => (! empty($data['unitranting_id'])) ? $data['unitranting_id'] : null,
								'administrator_id'      => $data['administrator_id'],
								'lembagasertifikasi_id' => $data['lembagasertifikasi_id'],
								'subbidang_id'          => (! empty($data['subbidang_id'])) ? $data['subbidang_id'] : null
							]);

			// store asesor
			$asesors = [$data['ketua'] => ['posisi' => 'ketua']];
			$cleanup = array_filter($data['anggota']);
			
			foreach ($data['anggota'] as $anggota) {
				$asesors[$anggota] = ['posisi' => 'anggota'];
			}
			$this->penjadwalanModel->find($penjadwalan->id)->asesors()->attach($asesors);

		});

		return true;
	}

	/**
	 * [update description]
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function update($id, $data)
	{
		$temp = compact('id', 'data');
		DB::transaction(function() use($temp)
		{
			$penjadwalan = $this->penjadwalanModel
								->whereId($temp['id'])
								->update([
									'tanggal_mulai'         => $temp['data']['tanggal_mulai'],
									'tanggal_selesai'       => $temp['data']['tanggal_selesai'],
									'udiklat_id'            => $temp['data']['udiklat_id'],
									'unitinduk_id'          => $temp['data']['unitinduk_id'],
									'unitcabang_id'         => (! empty($temp['data']['unitcabang_id'])) ? $temp['data']['unitcabang_id'] : null,
								'unitranting_id'        => (! empty($temp['data']['unitranting_id'])) ? $temp['data']['unitranting_id'] : null,
									'administrator_id'      => $temp['data']['administrator_id'],
									'lembagasertifikasi_id' => $temp['data']['lembagasertifikasi_id'],
									'subbidang_id'          => (! empty($temp['data']['subbidang_id'])) ? $temp['data']['subbidang_id'] : null
								]);

			$asesors = [$temp['data']['ketua'] => ['posisi' => 'ketua']];

			// if anggota is exist, then continued process
			if (isset($temp['data']['anggota'])) {
				$cleanup = array_filter($temp['data']['anggota']);
			
				foreach ($temp['data']['anggota'] as $anggota) {
					$asesors[$anggota] = ['posisi' => 'anggota'];
				}
			}

			$this->penjadwalanModel->find($temp['id'])->asesors()->sync($asesors);
			
		});

		return true;
	}

	/**
	 * [storeKompetensi description]
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function storeKompetensi($id, $data)
	{
		$failed = [];
		$filter = array_filter($data); // prevent empty string to be inserted
		if (count($filter) > 0) {
			foreach ($filter as $key => $kompetensiId) {

				if ($this->kompetensiPenjadwalan->isNotEmpty($id, $kompetensiId)) {
					$failed[] = $this->kompetensiRepository->getById($kompetensiId)['nama_kompetensi'];
				} else {
					 $this->penjadwalanModel->find($id)->kompetensi()->attach($kompetensiId);
				}
			}
			$requested = count($filter);
			$success   = $requested - count($failed);
			return [
				'requested' => $requested,
				'failed'    => $failed
			];
		}
		return [
				'requested' => 0,
				'failed'    => 0
			];
		
	}

	/**
	 * [storePeserta description]
	 * 
	 * @param  [type] $id           [description]
	 * @param  [type] $kompetensiId [description]
	 * @param  [type] $data         [description]
	 * @return [type]               [description]
	 */
	public function storePeserta($id, $data)
	{
		$failed = [];
		$filter = array_filter($data['peserta_id']); // prevent empty string to be inserted
		if (count($filter) > 0) {
			foreach ($filter as $key => $peserta) {

				$parseId = explode('#', $peserta); // parse peserta id & evaluasi id
				
				if ($this->pesertaPenjadwalan->isNotEmpty($data['kompetensipenjadwalan_id'], $parseId[0])) {
					$failed[] = $this->pesertaRepository->getById($parseId[0])['nama'];
				} else {
					 $this->penjadwalanModel->find($id)->peserta()->attach([$parseId[0] => ['kompetensipenjadwalan_id' => $data['kompetensipenjadwalan_id'], 'evaluasi_id' => $parseId[1]]]);
					 $this->flagEvaluasiToScheduled($parseId[1]);
				}
			}
			$requested = count($filter);
			$success   = $requested - count($failed);
			return [
				'requested' => $requested,
				'failed'    => $failed
			];
		}
		return [
				'requested' => 0,
				'failed'    => 0
			];
		
	}

	/**
	 * [delete description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id)
	{
		$this->penjadwalanModel->whereId($id)->delete();
	}

	/**
	 * [detachKompetensi description]
	 * 
	 * @param  [type] $id           [description]
	 * @param  [type] $kompetensiId [description]
	 * @return [type]               [description]
	 */
	public function deleteKompetensi($id, $kompetensiId)
	{
		$this->penjadwalanModel->find($id)->kompetensi()->detach($kompetensiId);
	}

	/**
	 * [deletePeserta description]
	 * 
	 * @param  [type] $id        [description]
	 * @param  [type] $pesertaId [description]
	 * @return [type]            [description]
	 */
	public function deletePeserta($id, $pesertaId, $evaluasiId)
	{
		$this->penjadwalanModel->find($id)->peserta()->newPivotStatementForId($pesertaId)->where('evaluasi_id', '=', $evaluasiId)->delete();
		$this->flagEvaluasiToScheduled($evaluasiId, false);
	}

	/**
	 * [flagEvaluasiToScheduled description]
	 * 
	 * @param  [type]  $evaluasiId [description]
	 * @param  boolean $flag       [description]
	 * @return [type]              [description]
	 */
	public function flagEvaluasiToScheduled($evaluasiId, $flag = true)
	{
		$status = '1';
		if (! $flag) {
			$status = '0';
		}
		$this->evaluasiModel->whereId($evaluasiId)->update(['status_penjadwalan' => $status]);
	}

}
