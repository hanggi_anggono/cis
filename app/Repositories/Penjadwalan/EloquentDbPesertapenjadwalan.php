<?php namespace Certification\Repositories\Penjadwalan;

use Certification\Models\Pesertakompetensipenjadwalan;

class EloquentDbPesertapenjadwalan {

	/**
	 * [$pesertaPenjadwalan description]
	 * 
	 * @var [type]
	 */
	private $pesertaPenjadwalan;

	/**
	 * [__construct description]
	 * 
	 * @param Kompetensipenjadwalan $pesertaPenjadwalan [description]
	 */
	public function __construct(Pesertakompetensipenjadwalan $pesertaPenjadwalan)
	{
		$this->pesertaPenjadwalan = $pesertaPenjadwalan;
	}

	/**
	 * [getPeserta description]
	 * 
	 * @param  [type] $penjadwalanId           [description]
	 * @param  [type] $kompetensipenjadwalanId [description]
	 * @return [type]                          [description]
	 */
	public function getPeserta($penjadwalanId = null, $kompetensipenjadwalanId = null, $params = null)
	{
		$collection = $this->pesertaPenjadwalan->join('peserta', 'pesertakompetensipenjadwalan.peserta_id', '=', 'peserta.id');
		if(isset($penjadwalanId)) {
			$collection->where('penjadwalan_id', '=', $penjadwalanId);
		}
		if (isset($kompetensipenjadwalanId)) {
			$collection->where('kompetensipenjadwalan_id', '=', $kompetensipenjadwalanId);
		}
		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				// dd($decode->unitinduk_id);
				$collection->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
			}
		}
		// dd(DB::getQueryLog());
		return $collection->get();
	}

	/**
	 * [isNotEmpty description]
	 * 
	 * @param  [type]  $penjadwalanId [description]
	 * @param  [type]  $kompetensiId  [description]
	 * @return boolean                [description]
	 */
	public function isNotEmpty($kompetensipenjadwalanId, $pesertaId)
	{
		$count = $this->pesertaPenjadwalan->isPesertaPenjadwalanExist($kompetensipenjadwalanId, $pesertaId)->count();
		if ($count != 0) {
			return true;
		}
		return false;
	}
}
