<?php namespace Certification\Repositories\Komponenbiaya;

use Certification\Models\Komponenbiaya;
use \Config;

class EloquentDbKomponenbiaya {

	/**
	 * [$komponenbiayaModel description]
	 * 
	 * @var [type]
	 */
	private $komponenbiayaModel;

	/**
	 * [$pagination description]
	 * 
	 * @var [type]
	 */
	private $pagination;

	/**
	 * [__construct description]
	 * 
	 * @param Komponenbiaya $komponenbiayaModel [description]
	 */
	public function __construct(Komponenbiaya $komponenbiayaModel)
	{
		$this->komponenbiayaModel = $komponenbiayaModel;
		$this->pagination = Config::get('certification.default_pagination_count');
	}

	/**
	 * [all description]
	 * 
	 * @param  string $paginate [description]
	 * @return [type]           [description]
	 */
	public function all($paginate = 'paginate')
	{
		$collection = $this->komponenbiayaModel->noFilter();
		if (isset($paginate) && $paginate = 'paginate') {
			return $collection->paginate($this->pagination);
		}

		return $collection->get();
	}

	/**
	 * [find description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function find($id)
	{
		return $this->komponenbiayaModel->find($id);
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		return $this->komponenbiayaModel->create($data);
	}

	/**
	 * [update description]
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function update($id, $data)
	{
		return $this->komponenbiayaModel->whereId($id)->update($data);
	}

	/**
	 * [delete description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id)
	{
		return $this->komponenbiayaModel->whereId($id)->delete();
	}

	/**
	 * [lists description]
	 * 
	 * @return [type] [description]
	 */
	public function lists()
	{
		return $this->komponenbiayaModel->lists('nama_komponenbiaya', 'id');
	}
}
