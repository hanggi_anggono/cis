<?php namespace Certification\Repositories\Permission;

use Certification\Models\Permission;

class EloquentDbPermission {

	/**
	 * [$permissionModel description]
	 * 
	 * @var [type]
	 */
	private $permissionModel;

	/**
	 * [__construct description]
	 * 
	 * @param Permission $permissionModel [description]
	 */
	public function __construct(Permission $permissionModel)
	{
		$this->permissionModel = $permissionModel;
	}

	/**
	 * [all description]
	 * 
	 * @return [type] [description]
	 */
	public function all()
	{
		return $this->permissionModel->all();
	}

	/**
	 * [getById description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getById($id)
	{
		return $this->permissionModel->find($id);
	}

	/**
	 * [getByName description]
	 * 
	 * @param  [type] $name [description]
	 * @return [type]       [description]
	 */
	public function getByName($name)
	{
		return $this->permissionModel->where('name', '=', $name)->first();
	}

	/**
	 * [lists description]
	 * 
	 * @return [type] [description]
	 */
	public function lists()
	{
		return $this->permissionModel->lists('display_name', 'id');
	}
	
}
