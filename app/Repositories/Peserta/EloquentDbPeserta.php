<?php namespace Certification\Repositories\Peserta;

use Certification\Repositories\Peserta\InterfacePesertaRepository;
use Certification\Models\Peserta;
use Config;

/**
* 
*/
class EloquentDbPeserta implements InterfacePesertaRepository
{
	/**
	 * [$pesertaModel description]
	 * @var [type]
	 */
	private $pesertaModel;

	/**
	 * [__construct description]
	 * 
	 * @param Peserta $peserta [description]
	 */
	public function __construct(Peserta $peserta)
	{
		$this->pesertaModel = $peserta;	
	}

	/**
	 * Get all peserta collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null, $filters = null)
	{
		$collection = $this->pesertaModel->withRelationship();

		if(isset($filters)) {
			if ($filters->has('nipname')) {
				$collection->where('nip', 'LIKE', '%'.$filters->get('nipname').'%')->orWhere('nip', 'LIKE', '%'.$filters->get('nipname').'%');
			}
		}
		$collection->orderedByCreated();
		
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get all peserta collection with no relationship
	 * 
	 * @return Collection
	 */
	public function allNoRelationship()
	{
		return $this->pesertaModel->orderedByCreated()->get();
	}

	/**
	 * Get peserta by given Id
	 * 
	 * @param  Integer $id peserta id
	 * @return Collection
	 */
	public function getById($id, $params = null, $role = null)
	{
		$collection =  $this->pesertaModel->withRelationship()->whereId($id);
		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				// dd($decode->unitinduk_id);
				$collection->where('unitinduk_id', '=', $decode->unitinduk_id);
			}
		}
		return $collection->first();
	}

	/**
	 * store data peserta to database
	 * 
	 * @param  Array $data peserta
	 * @return Boolean
	 */
	public function store($data)
	{

		return $this->pesertaModel->create($data);
	}

	/**
	 * update single data peserta to database
	 *
	 * @param  Integer $id peserta id
	 * @param  Array $data peserta
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->pesertaModel->whereId($id)->update($this->checkType($data));
	}

	/**
	 * delete single data peserta, with soft delete method or not
	 * 
	 * @param  Integer  $id          peserta id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->pesertaModel->whereId($id)->delete();
		return true;
	}

	/**
	 * [lists description]
	 * 
	 * @return [type] [description]
	 */
	public function lists()
	{
		return $this->pesertaModel->lists('nip', 'id');
	}

	/**
	 * [checkType description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	private function checkType($data)
	{
		if($data['tipe_peserta'] == 'pln') {
			$data['unitnonpln_id'] = null;
		} else {
			$data['unitinduk_id']   = null;
			$data['unitcabang_id']  = null;
			$data['unitranting_id'] = null;
		}
		return $data;
	}

}
