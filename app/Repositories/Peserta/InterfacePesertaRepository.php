<?php namespace Certification\Repositories\Peserta;

/**
 * Interface for peserta repository
 */
interface InterfacePesertaRepository {

	/**
	 * Get all peserta collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null, $filters = null);

	/**
	 * Get peserta by given Id
	 * 
	 * @param  Integer $id peserta id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data peserta to database
	 * 
	 * @param  Array $data peserta
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data peserta to database
	 *
	 * @param  Integer $id peserta id
	 * @param  Array $data peserta
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data peserta, with soft delete method or not
	 * 
	 * @param  Integer  $id          peserta id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);

}
