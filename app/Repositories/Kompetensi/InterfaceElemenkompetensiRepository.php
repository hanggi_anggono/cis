<?php namespace Certification\Repositories\Kompetensi;

/**
 * Interface for master lembaga database repository
 */
interface InterfaceElemenkompetensiRepository {

	/**
	 * Get all elemen kompetensi collection with paginate or not
	 *
	 * @param  Integer $kompetensi_id Kompetensi Id
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($kompetensi_id, $paginate = null);

	/**
	 * Get elemen kompetensi by given Id
	 * 
	 * @param  Integer $id elemen kompetensi id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data elemen kompetensi to database
	 * 
	 * @param  Array $data elemen kompetensi
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data elemen kompetensi to database
	 *
	 * @param  Integer $id elemen kompetensi id
	 * @param  Array $data elemen kompetensi
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data elemen kompetensi, with soft delete method or not
	 * 
	 * @param  Integer  $id          elemen kompetensi id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);

}
