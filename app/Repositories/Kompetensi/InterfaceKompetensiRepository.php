<?php namespace Certification\Repositories\Kompetensi;

/**
 * Interface for master lembaga database repository
 */
interface InterfaceKompetensiRepository {

	/**
	 * Get all kompetensi collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null);

	/**
	 * Get kompetensi by given Id
	 * 
	 * @param  Integer $id kompetensi id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data kompetensi to database
	 * 
	 * @param  Array $data kompetensi
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data kompetensi to database
	 *
	 * @param  Integer $id kompetensi id
	 * @param  Array $data kompetensi
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data kompetensi, with soft delete method or not
	 * 
	 * @param  Integer  $id          kompetensi id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);

}
