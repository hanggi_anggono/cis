<?php namespace Certification\Repositories\Kompetensi;

use Certification\Repositories\Kompetensi\InterfaceKompetensiRepository;
use Certification\Services\ExportService;
use Certification\Models\Realisasipeserta;
use Certification\Models\Kompetensi;
use \Config, \Collection, \DB;

/**
 * Database Kompetensi provider implementing Interface Kompetensi Repository
 * Using Eloquent driver
 */
class EloquentDbKompetensi implements InterfaceKompetensiRepository {

	/**
	 * Kompetensi Model
	 * 
	 * @var Model
	 */
	private $kompetensiModel;

	/**
	 * Function status
	 * 
	 * @var Boolean
	 */
	private $status;

	/**
	 * [$realisasiPesertaModel description]
	 * 
	 * @var [type]
	 */
	private $realisasiPesertaModel;

	/**
	 * [$exportService description]
	 * 
	 * @var [type]
	 */
	private $exportService;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param Kompetensi $kompetensiModel Model Kompetensi
	 */
	public function __construct(Kompetensi $kompetensiModel, Realisasipeserta $realisasiPesertaModel, ExportService $exportService)
	{
		$this->kompetensiModel       = $kompetensiModel;
		$this->realisasiPesertaModel = $realisasiPesertaModel;
		$this->exportService         = $exportService;
		$this->status                = false;
	}

	/**
	 * [countAll description]
	 * 
	 * @return [type] [description]
	 */
	public function countAll()
	{
		return $this->kompetensiModel->count();
	}

	/**
	 * Get all kompetensi collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null)
	{
		$collection = $this->kompetensiModel->with('subbidang','elemenKompetensi','administrator','permohonan')->orderBy('nama_kompetensi');
		if (isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get kompetensi by given Id
	 * 
	 * @param  Integer $id kompetensi id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->kompetensiModel->findOrFail($id);
	}

	/**
	 * Store data kompetensi to database
	 * 
	 * @param  Array $data kompetensi
	 * @return Boolean
	 */
	public function store($data)
	{
		$affected = $this->kompetensiModel->create($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * update single data kompetensi to database
	 *
	 * @param  Integer $id kompetensi id
	 * @param  Array $data kompetensi
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		$affected = $this->kompetensiModel->whereId($id)->update($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * delete single data kompetensi, with soft delete method or not
	 * 
	 * @param  Integer  $id          kompetensi id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->kompetensiModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Get array list of kompetensi resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->kompetensiModel->lists('nama_kompetensi','id');
	}

	/**
	 * Get array list of kompetensi resource by subbidang
	 * 
	 * @param  Integer $id Subbidang id
	 * @return Array
	 */
	public function getBySubbidang($id)
	{
		return $this->kompetensiModel->where('subbidang_id', '=', $id)->lists('nama_kompetensi', 'id');
	}

	/**
	 * [getBidangs description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getBidangs($id)
	{
		return $this->kompetensiModel->select('nama_bidang', 'nama_subbidang')
									->join('subbidang', function($join)
									{
										$join->on('subbidang_id', '=', 'subbidang.id');
									})
									->join('bidang', function($join)
									{
										$join->on('bidang_id', '=', 'bidang.id');
									})
									->where('kompetensi.id', '=', $id)
									->first();
	}

	/**
	 * [getSebaran description]
	 * 
	 * @param  string $paginate      [description]
	 * @param  [type] $kompetensi_id [description]
	 * @param  [type] $periode       [description]
	 * @return [type]                [description]
	 */
	public function getSebaran($paginate = 'paginate', $filters = null)
	{
		$collection = $this->kompetensiModel
						   ->select(
								'kompetensi.id', 
								'kodedjk', 
								'kodeskkni', 
								'nama_bidang', 
								'nama_subbidang',
								'nama_kompetensi', 
								DB::raw('sum(case when kehadiran = "y" and kelulusan = "y" then 1 else 0 end) passesCount, sum(case when kehadiran = "y" and kelulusan = "n" then 1 else 0 end) notpassesCount')
							)
							->join('subbidang', 'subbidang_id', '=', 'subbidang.id')
							->join('bidang', 'subbidang.bidang_id', '=', 'bidang.id')
							->leftJoin('realisasipeserta', 'kompetensi.id', '=', 'realisasipeserta.kompetensi_id');

		if ($filters->has('kompetensi')) {
			$collection->where('kompetensi.id', '=', $filters->get('kompetensi'));
		}
		
		if ($filters->has('periode')) {
			$collection->where('realisasipeserta.created_at', 'LIKE', $filters->get('periode').' %');
		}

		$collection->groupBy('kompetensi.id')->orderBy('passesCount', 'desc');
		
		if (isset($paginate) && $paginate == 'paginate') {
			return $collection->paginate(20);
		}

		return $collection->get();
	}

	/**
	 * [export description]
	 * 
	 * @param  string $type    [description]
	 * @param  [type] $filters [description]
	 * @return [type]          [description]
	 */
	public function export($type = 'excel', $filters = null)
	{
		$data['sebarankompetensi'] = $this->getSebaran(null, $filters);
		return $this->exportService->export($data['sebarankompetensi'], $type, ['viewName' => 'sebarankompetensi', 'orientation' => 'landscape', 'filename' => 'report_kompetensi']);
	}

}
