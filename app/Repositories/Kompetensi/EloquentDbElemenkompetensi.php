<?php namespace Certification\Repositories\Kompetensi;

use Certification\Repositories\Kompetensi\InterfaceElemenkompetensiRepository;
use Certification\Models\Elemenkompetensi;
use \Config;

/**
 * Database Kompetensi provider implementing Interface Kompetensi Repository
 * Using Eloquent driver
 */
class EloquentDbElemenkompetensi implements InterfaceElemenkompetensiRepository {

	/**
	 * Elemen Kompetensi Model
	 * 
	 * @var Model
	 */
	private $elemenkompetensiModel;

	/**
	 * Function status
	 * 
	 * @var Boolean
	 */
	private $status;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param Kompetensi $elemenkompetensiModel Model Elemen Kompetensi
	 */
	public function __construct(Elemenkompetensi $elemenkompetensiModel)
	{
		$this->elemenkompetensiModel = $elemenkompetensiModel;
		$this->status                = false;
	}

	/**
	 * [countAll description]
	 * 
	 * @return [type] [description]
	 */
	public function countAll()
	{
		return $this->elemenkompetensiModel->count();
	}

	/**
	 * Get all elemen kompetensi collection with paginate or not
	 *
	 * @param  Integer $kompetensi_id kompetensi id
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($kompetensi_id, $paginate = null)
	{
		if (empty($kompetensi_id)) {
			$collection = $this->elemenkompetensiModel->orderBy('nama_elemen');
		} else {
			$collection = $this->elemenkompetensiModel->byKompetensi($kompetensi_id)->orderBy('nama_elemen');
		}
		
		if (isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get elemen kompetensi by given Id
	 * 
	 * @param  Integer $id elemen kompetensi id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->elemenkompetensiModel->findOrFail($id);
	}

	/**
	 * Store data kompetensi to database
	 * 
	 * @param  Array $data kompetensi
	 * @return Boolean
	 */
	public function store($data)
	{
		$affected = $this->elemenkompetensiModel->create($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * update single data elemen kompetensi to database
	 *
	 * @param  Integer $id elemen kompetensi id
	 * @param  Array $data elemen kompetensi
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		$affected = $this->elemenkompetensiModel->whereId($id)->update($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * delete single data elemen kompetensi, with soft delete method or not
	 * 
	 * @param  Integer  $id          elemen kompetensi id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->elemenkompetensiModel->whereId($id)->delete();
		return true;
	}

}
