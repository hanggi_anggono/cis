<?php namespace Certification\Repositories\Sertifikat;

use Certification\Models\Realisasipeserta;

class EloquentDbSertifikatpeserta {

	/**
	 * [$realisasiPeserta description]
	 * 
	 * @var [type]
	 */
	private $realisasiPeserta;

	/**
	 * [__construct description]
	 * 
	 * @param Realisasipeserta $realisasiPeserta [description]
	 */
	public function __construct(Realisasipeserta $realisasiPeserta)
	{
		$this->realisasiPeserta = $realisasiPeserta;
	}

	/**
	 * [all description]
	 * 
	 * @param  string $paginate         [description]
	 * @param  string $statusSertifikat [description]
	 * @return [type]                   [description]
	 */
	public function all($paginate = 'paginate', $statusSertifikat = 'baru')
	{
		$collection = $this->getSertifikatCollection()->where('status_sertifikat', '=', $statusSertifikat)->orderBy('realisasipeserta.created_at', 'desc');
		if (isset($paginate) && $paginate == 'paginate') {
			return $collection->paginate(10);
		}
		return $collection->get();
	}

	/**
	 * [getByRealisasi description]
	 * 
	 * @param  [type] $realisasiId [description]
	 * @return [type]              [description]
	 */
	public function getByRealisasi($realisasiId)
	{
		return $this->getSertifikatCollection()->where('realisasipeserta.id', '=', $realisasiId)->first();
	}

	/**
	 * [updateSertifikat description]
	 * 
	 * @param  [type] $realisasiId [description]
	 * @param  [type] $data        [description]
	 * @return [type]              [description]
	 */
	public function updateSertifikat($realisasiId, $data)
	{
		return $this->realisasiPeserta->whereId($realisasiId)->update($data);
	}

	/**
	 * [getSertifikatCollection description]
	 * 
	 * @return [type] [description]
	 */
	private function getSertifikatCollection()
	{
		return  $this->realisasiPeserta
				->select('nip', 'nama', 'nama_kompetensi' ,'tanggal_mulai', 'tanggal_selesai', 'no_sertifikat', 'tanggal_awal', 'tanggal_akhir', 'tanggal_keluar', 'realisasipeserta.id as realisasipeserta_id', 'status_sertifikat')
				->join('peserta', 'peserta_id', '=', 'peserta.id')
				->join('kompetensipenjadwalan', 'kompetensipenjadwalan_id', '=', 'kompetensipenjadwalan.id')
				->join('kompetensi', 'kompetensipenjadwalan.kompetensi_id', '=', 'kompetensi.id')
				->join('penjadwalan', 'penjadwalan.id', '=', 'kompetensipenjadwalan.penjadwalan_id')
				->attended()->passes();
	}
}