<?php namespace Certification\Repositories\Sertifikat;

use Certification\Models\Sertifikat;
use Certification\Models\Sertifikathistory;
use Certification\Models\Elemenkompetensi;
use Certification\Models\Bidang;
use Certification\Models\Kompetensi;
use Certification\Models\Realisasipeserta;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use \DB, \PDF, \Session;

class EloquentDbSertifikat {

	/**
	 * [$sertifikatModel description]
	 * 
	 * @var [type]
	 */
	private $sertifikatModel;

	/**
	 * [$sertifikathistoryModel description]
	 * 
	 * @var [type]
	 */
	private $sertifikathistoryModel;

	/**
	 * [$elemenkompetensiModel description]
	 * 
	 * @var [type]
	 */
	private $elemenkompetensiModel;

	/**
	 * [$subbidang description]
	 * 
	 * @var [type]
	 */
	private $bidangModel;

	/**
	 * [$kompetensiModel description]
	 * 
	 * @var [type]
	 */
	private $kompetensiModel;

	/**
	 * [$realisasipesertaModel description]
	 * 
	 * @var [type]
	 */
	private $realisasipesertaModel;

	/**
	 * [__construct description]
	 * 
	 * @param Sertifikat        $sertifikatModel        [description]
	 * @param Sertifikathistory $sertifikathistoryModel [description]
	 * @param Elemenkompetensi  $elemenkompetensiModel  [description]
	 */
	public function __construct(Sertifikat $sertifikatModel, Sertifikathistory $sertifikathistoryModel, Elemenkompetensi $elemenkompetensiModel, Bidang $bidangModel, Kompetensi $kompetensiModel, Realisasipeserta $realisasipesertaModel)
	{
		$this->sertifikatModel        = $sertifikatModel;
		$this->sertifikathistoryModel = $sertifikathistoryModel;
		$this->elemenkompetensiModel  = $elemenkompetensiModel;
		$this->bidangModel            = $bidangModel;
		$this->kompetensiModel        = $kompetensiModel;
		$this->realisasipesertaModel  = $realisasipesertaModel;
	}

	/**
	 * [all description]
	 * 
	 * @param  [type] $paginate          [description]
	 * @param  [type] $status_sertifikat [description]
	 * @param  [type] $filters           [description]
	 * @param  [type] $penjadwalanId     [description]
	 * @param  [type] $params            [description]
	 * @return [type]                    [description]
	 */
	public function all($paginate = null, $status_sertifikat = null, $filters = null, $penjadwalanId = null, $params = null)
	{
		$collection = $this->sertifikatModel->select(
												'peserta_id', 
												'peserta.nama',
												'peserta.nip',
												'sertifikat.id as sertifikat_id', 
												'penjadwalan.tanggal_mulai', 
												'penjadwalan.tanggal_selesai', 
												'nama_udiklat', 
												'nama_lembagasertifikasi', 
												'nama_kompetensi', 
												'sertifikat.id',
												'no_sertifikat', 
												'tanggal_mulai_berlaku', 
												'tanggal_akhir_berlaku', 
												'sertifikat.status as status_sertifikat', 
												'lembagasertifikasi.grup', 
												'lembagasertifikasi.jenis_sertifikat',
												'sertifikat.penjadwalan_id'
											);
		
		$collection = $this->customJoin($collection);

		if (isset($status_sertifikat)) {
			if ($status_sertifikat == 'perpanjangan') {
				$collection->where('tanggal_akhir_berlaku', '<', Carbon::now()->toDateString());
			} else {
				$collection->where('tanggal_akhir_berlaku', '>', Carbon::now()->toDateString());
			}
			
		}

		if (isset($filters)) {
			if ($filters->has('no_sertifikat')) {
				$collection->where('sertifikat.no_sertifikat', '=', $filters->get('no_sertifikat'));
			}
			if ($filters->has('nip')) {
				$collection->where('peserta.nip', '=', $filters->get('nip'))
						   ->orWhere('peserta.nama', 'LIKE', '%'.$filters->get('nip').'%');
			}

			if ($filters->has('tanggal_mulai') && $filters->has('tanggal_selesai')) {
				$collection->where('penjadwalan.tanggal_mulai', '=', $filters->get('tanggal_mulai'))
						   ->where('penjadwalan.tanggal_selesai', '=', $filters->get('tanggal_selesai'));
			}
			
			if ($filters->has('expired')) {
				switch ($filters->get('expired')) {
					case '1m':
						$collection->where('tanggal_akhir_berlaku', '<=', Carbon::now()->subMonth()->toDateString());
						break;
					case '3m':
						$collection->where('tanggal_akhir_berlaku', '<=', Carbon::now()->subMonths(3)->toDateString());
						break;
					case '6m':
						$collection->where('tanggal_akhir_berlaku', '<=', Carbon::now()->subMonths(6)->toDateString());
						break;
					default:
						$collection->where('tanggal_akhir_berlaku', '<=', Carbon::now()->subYear()->toDateString());
						break;
				}
			}
		}

		if(isset($penjadwalanId)) {
			$collection->where('penjadwalan_id', '=', $penjadwalanId);
		}

		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$collection->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
			}
		}

		$collection->orderBy('sertifikat.created_at', 'desc');

		if (isset($paginate) && $paginate == 'paginate') {
			return $collection->paginate(20);
		}

		return $collection->get();
	}

	/**
	 * [find description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function find($id)
	{
		$collection =  $this->sertifikatModel->select(
												'peserta_id', 
												'peserta.nama',
												'peserta.nip',
												'sertifikat.id as sertifikat_id', 
												'penjadwalan.tanggal_mulai', 
												'penjadwalan.tanggal_selesai', 
												'nama_udiklat', 
												'nama_lembagasertifikasi', 
												'nama_kompetensi', 
												'sertifikat.id',
												'no_sertifikat', 
												'tanggal_mulai_berlaku', 
												'tanggal_akhir_berlaku', 
												'sertifikat.status as status_sertifikat', 
												'lembagasertifikasi.grup', 
												'lembagasertifikasi.jenis_sertifikat'
											);
		return $this->customJoin($collection)->where('sertifikat.id', '=', $id)->first();
	}

	/**
	 * [detail description]
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function detail($id)
	{
		$collection = $this->sertifikatModel
						   ->with(['peserta', 'lembagasertifikasi', 'penjadwalan'])
						   ->whereId($id)
						   ->first();

		$kompetensi        = $this->kompetensiModel
						         ->with(['subbidang', 'elemenkompetensi'])
						         ->where('id', '=', $collection->kompetensi_id)
						         ->first();
		$bidang     = $this->bidangModel->find($kompetensi->subbidang->bidang_id);

		return [
			'sertifikat' => $collection,
			'kompetensi' => $kompetensi,
			'bidang'     => $bidang,
			'template'   => $collection->lembagasertifikasi->jenis_sertifikat
		];
	}

	/**
	 * [getExpired description]
	 * 
	 * @param  [type] $beforePeriode [description]
	 * @param  [type] $take          [description]
	 * @param  [type] $params        [description]
	 * @param  [type] $role          [description]
	 * @return [type]                [description]
	 */
	public function getExpired($beforePeriode = null, $take = null, $params = null, $role = null)
	{
		$collection = $this->sertifikatModel->join('penjadwalan', 'sertifikat.penjadwalan_id', '=', 'penjadwalan.id')
											->join('peserta', 'peserta.id', '=', 'sertifikat.peserta_id')
											->with(['peserta', 'kompetensi', 'lembagasertifikasi']);
		
		switch ($beforePeriode) {
			case '1month':
				$collection->where(DB::raw('MONTH(tanggal_akhir_berlaku)'), '=', Carbon::now()->addMonth()->month);
				break;
			case '3month':
				$collection->where(DB::raw('MONTH(tanggal_akhir_berlaku)'), '=', Carbon::now()->addMonths(3)->month);
				break;
			case '6month':
				$collection->where(DB::raw('MONTH(tanggal_akhir_berlaku)'), '=', Carbon::now()->addMonths(6)->month);
				break;
			default:
				$collection->where('tanggal_akhir_berlaku', '<', Carbon::now()->toDateString());
				break;
		}

		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$collection->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
			}
			if (isset($role) && $role == 'employee') {
				if (isset($decode->id)) {
					$collection->where('sertifikat.peserta_id', '=', $decode->id);
				}
			}
		}

		$collection->orderBy('tanggal_akhir_berlaku');

		if (isset($take)) {
		 	$collection->take($take);
		 }		

		return $collection->get();
	}

	/**
	 * [store description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function store($data)
	{
		if (isset($data['no_sertifikat'])) {
			$data = $data;
		} else {
			$currentDate                   = Carbon::now()->toDateString();
			$masaBerlaku                   = Carbon::now()->addYears(3)->toDateString();
			$data['tanggal_mulai_berlaku'] = $currentDate;
			$data['tanggal_akhir_berlaku'] = $masaBerlaku;
			$data['no_sertifikat']         = $this->setCertificateNumber($data['lembagasertifikasi_id'], $data['kompetensi_id']);
		}
		
		// check if sertifikat is exist
		$certificate = $this->sertifikatModel->where('peserta_id', '=', $data['peserta_id'])
											 ->where('kompetensi_id', '=', $data['kompetensi_id'])
											 ->where('penjadwalan_id', '=', $data['penjadwalan_id'])
											 ->count();
		if ($certificate == 0) {
			return $this->sertifikatModel->create($data);
		}
		return false;
	}

	/**
	 * [update description]
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function update($id, $data)
	{
		$collection           = $this->sertifikatModel->whereId($id)->first();
		$toUpdate             = $collection;
		$arr                  = $collection->toArray();
		$arr['sertifikat_id'] = $id;
		$arr['status']        = 'perpanjangan';
		array_forget($arr, 'id');
		
		$this->sertifikathistoryModel->create($arr);
		
		$affected = $toUpdate->update($data);
		
		return $affected;
	}

	/**
	 * [generateMultipleCertificate description]
	 * 
	 * @param  [type] $penjadwalanId [description]
	 * @return [type]                [description]
	 */
	public function generateMultipleCertificate($penjadwalanId)
	{
		$collection = $this->realisasipesertaModel
							->select('peserta_id', 'lembagasertifikasi_id', 'kompetensipenjadwalan.kompetensi_id', 'kompetensipenjadwalan.penjadwalan_id')
						   ->join('kompetensipenjadwalan', 'kompetensipenjadwalan.id', '=', 'realisasipeserta.kompetensipenjadwalan_id')
						   ->join('penjadwalan', 'kompetensipenjadwalan.penjadwalan_id', '=', 'penjadwalan.id')
						   ->join('peserta', 'realisasipeserta.peserta_id', '=', 'peserta.id')
						   ->where('kompetensipenjadwalan.penjadwalan_id', '=', $penjadwalanId)
						   ->attended()
						   ->passes()
						   ->get();
		
		if (count($collection) > 0) {
			$lsk = DB::table('lembagasertifikasi')->select('grup')->where('id', '=', $collection[0]['lembagasertifikasi_id'])->first();
			if ($lsk->grup == 'pln') {
				try {

					DB::transaction(function() use($collection)
					{
						foreach ($collection as $key => $value) {
							$this->store($value->toArray());
						}
					});
				
				} catch (Exception $e) {
					return [
						'status'  => false,
						'message' => 'Error, Generate gagal'
					];
				}
				
				return [
					'status'  => true,
					'message' => 'Generate berhasil'
				];
			} else {
				return [
					'status'  => false,
					'message' => 'fungsi ini hanya tersedia untuk LSK grup PLN'
				];
			}
		}
		return [
			'status'  => false,
			'message' => 'Tidak ada data peserta'
		];
	}

	/**
	 * [downloadSingle description]
	 * 
	 * @param  [type] $sertifikat_id [description]
	 * @return [type]                [description]
	 */
	public function downloadSingle($sertifikat_id)
	{
		$data        = $this->detail($sertifikat_id);
		$template    = 'sertifikat';
		$orientation = 'landscape';

		if($data['template'] == 'bnsp') {
			$template    = 'sertifikatbnsp';
			$orientation = 'portrait';
		}
		$arr['collection'] = [$data];
		$pdf = PDF::loadView('pdf.'.$template, $arr)->setPaper('a4')->setOrientation($orientation);
		return $pdf->download('sertifikat.pdf');
	}

	/**
	 * [downloadMultiple description]
	 * 
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function downloadMultiple($penjadwalanId)
	{
		set_time_limit(300);
		ini_set('memory_limit', '256M');

		$template                    = 'sertifikat';
		$orientation                 = 'landscape';
		$allSertifikat['collection'] = [];

		$collection = $this->sertifikatModel->select('sertifikat.id')
											->where('penjadwalan_id', '=', $penjadwalanId)
											->orderBy('sertifikat.created_at', 'desc')
											->get();
		
		foreach ($collection as $sertifikat) {
			
			$detailSertifikat              = $this->detail($sertifikat->id);
			$allSertifikat['collection'][] = $detailSertifikat;

			if($detailSertifikat['template'] == 'bnsp') {
				$template    = 'sertifikatbnsp';
				$orientation = 'portrait';
			}

		}
		
		$pdf = PDF::loadView('pdf.'.$template, $allSertifikat)->setPaper('a4')->setOrientation($orientation);
		return $pdf->download('sertifikat-all.pdf');
	}

	/**
	 * [setCertificateNumber description]
	 * Format: xxxxxxxx.yyyy.zzzzz
	 * xxxxxx: skkni / djk. (kode skkni untuk LSK PLN, djk untuk LSK grup non pln)
	 * yyyy: year
	 * zzzzz: sequence number. ganti tahun, restart number
	 * 
	 * @param [type] $lembagasertifikasi_id [description]
	 * @param [type] $kompetensi_id         [description]
	 */
	public function setCertificateNumber($lembagasertifikasi_id, $kompetensi_id)
	{
		$kompetensiCode = '';
		$year           = date('Y');
		$lsk            = DB::table('lembagasertifikasi')
							->select('grup')
							->where('id', '=', $lembagasertifikasi_id)
							->first();

		$kompetensi     = DB::table('kompetensi')
							->select('kodedjk', 'kodeskkni')
							->where('id', '=', $kompetensi_id)
							->first();

		if ($lsk->grup == 'pln') {
			$kompetensiCode = $kompetensi->kodeskkni;
		} else {
			$kompetensiCode = $kompetensi->kodedjk;
		}

		return $kompetensiCode.'.'.$year.'.'.$this->generateCounter();
	}

	/**
	 * [generateCounter description]
	 * 
	 * @return [type] [description]
	 */
	private function generateCounter()
	{
		$counter_path = __DIR__ . '/../../../storage/app/certificate.counter';
		$year_path    = __DIR__ . '/../../../storage/app/year.counter';

        if (file_exists($counter_path)) {
            $counter = file_get_contents($counter_path);
        } else {
            file_put_contents($counter_path, '1');
            $counter = 1;
        }

        // reset counter when year is changing
        if (date('Y') !== file_get_contents($year_path)) {
        	file_put_contents($counter_path, '1');
        	file_put_contents($year_path, date('Y'));
            $counter = 1;
        } 

        $certicateSequence = sprintf('%05d', $counter);

        file_put_contents($counter_path, $counter + 1);

        return $certicateSequence;
	}


	/**
	 * [customJoin description]
	 * 
	 * @param  [type] $collection [description]
	 * @return [type]             [description]
	 */
	private function customJoin($collection)
	{
		return $collection->join('peserta', 'sertifikat.peserta_id', '=', 'peserta.id')
				   		  ->join('penjadwalan', 'sertifikat.penjadwalan_id', '=', 'penjadwalan.id')
				   		  ->join('kompetensi', 'sertifikat.kompetensi_id', '=', 'kompetensi.id')
				   		  ->join('udiklat', 'penjadwalan.udiklat_id', '=', 'udiklat.id')
				   		  ->join('lembagasertifikasi', 'penjadwalan.lembagasertifikasi_id', '=', 'lembagasertifikasi.id');
	}

}
