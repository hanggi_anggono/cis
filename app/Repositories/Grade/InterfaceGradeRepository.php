<?php namespace Certification\Repositories\Grade;

/**
 * Interface for master grade database repository
 */
interface InterfaceGradeRepository {

	/**
	 * Get all grade collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false);

	/**
	 * Get grade by given Id
	 * 
	 * @param  Integer $id grade id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data grade to database
	 * 
	 * @param  Array $data grade
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data grade to database
	 *
	 * @param  Integer $id grade id
	 * @param  Array $data grade
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data grade, with soft delete method or not
	 * 
	 * @param  Integer  $id          grade id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);

}
