<?php namespace Certification\Repositories\Grade;

use Certification\Repositories\Grade\InterfaceGradeRepository;
use Certification\Models\Grade;
use \Config;

/**
 *  Class DB repository for Master Grade using Eloquent
 *  implements InterfaceGradeRepository
 */
class EloquentDbGrade implements InterfaceGradeRepository {

	/**
	 * Model Grade attribute
	 * @var Model
	 */
	private $gradeModel;
	
	/**
	 * Status of function attribute
	 * @var Boolean
	 */
	private $status;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param Grade $GradeModel Model Grade
	 */
	public function __construct(Grade $gradeModel)
	{
		$this->gradeModel = $gradeModel;
		$this->status       = false;
	}
	/**
	 * Get all Grade collection with paginate or not
	 * 
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = false)
	{
		$collection = $this->gradeModel->orderBy('nama_grade');
		if ($paginate) {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get Grade by given Id
	 * 
	 * @param  Integer $id Grade id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->gradeModel->findOrFail($id);
	}

	/**
	 * store data Grade to database
	 * 
	 * @param  Array $data Grade
	 * @return Boolean
	 */
	public function store($data)
	{
		$affected = $this->gradeModel->create($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * update single data Grade to database
	 *
	 * @param  Integer $id Grade id
	 * @param  Array $data Grade
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		$affected = $this->gradeModel->where('id', '=', $id)->update($data);
		if ($affected) {
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * delete single data Grade, with soft delete method or not
	 * 
	 * @param  Integer  $id          Grade id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->gradeModel->where('id','=',$id)->delete();
		return true;
	}

	/**
	 * Get array list of grade resource
	 * 
	 * @return Array
	 */
	public function lists()
	{
		return $this->gradeModel->lists('nama_grade','id');
	}
}
