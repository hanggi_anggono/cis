<?php namespace Certification\Repositories\Dashboard;

use Certification\Repositories\Permohonan\EloquentDbPermohonan;
use Certification\Repositories\Penjadwalan\EloquentDbPenjadwalan;
use Certification\Repositories\Pelaksanaan\EloquentDbPelaksanaan;
use Certification\Repositories\Sertifikat\EloquentDbSertifikat;
use Carbon\Carbon;
use \DB, \Session;

class EloquentDbDashboard {

	/**
	 * [$permohonanRepository description]
	 * 
	 * @var [type]
	 */
	private $permohonanRepository;
	
	/**
	 * [$penjadwalanRepository description]
	 * 
	 * @var [type]
	 */
	private $penjadwalanRepository;
	
	/**
	 * [$pelaksanaanRepository description]
	 * 
	 * @var [type]
	 */
	private $pelaksanaanRepository;

	/**
	 * [$sertifikatRepositories description]
	 * 
	 * @var [type]
	 */
	private $sertifikatRepositories;

	/**
	 * [__construct description]
	 * 
	 * @param EloquentDbPermohonan  $permohonanRepository   [description]
	 * @param EloquentDbPenjadwalan $penjadwalanRepository  [description]
	 * @param EloquentDbPelaksanaan $pelaksanaanRepository  [description]
	 * @param EloquentDbSertifikat  $sertifikatRepositories [description]
	 */
	public function __construct(EloquentDbPermohonan $permohonanRepository, EloquentDbPenjadwalan $penjadwalanRepository, EloquentDbPelaksanaan $pelaksanaanRepository, EloquentDbSertifikat $sertifikatRepositories)
	{
		$this->permohonanRepository   = $permohonanRepository;
		$this->penjadwalanRepository  = $penjadwalanRepository;
		$this->pelaksanaanRepository  = $pelaksanaanRepository;
		$this->sertifikatRepositories = $sertifikatRepositories;
	}

	/**
	 * [adminDashboardSummary description]
	 * 
	 * @return [type] [description]
	 */
	public function adminDashboardSummary()
	{
		$data['permohonan']  = $this->permohonanRepository->all(null, Session::get('params'), Session::get('role'))->count();
		$data['pelaksanaan'] = $this->pelaksanaanRepository->countPelaksanaan(Session::get('params'), Session::get('role'));
		$data['kompeten']    = $this->pelaksanaanRepository->countPassed(Session::get('params'), Session::get('role'));
		$data['kehadiran']   = $this->pelaksanaanRepository->countAttended(Session::get('params'), Session::get('role'));
		$data['penjadwalan'] = [
			'count'      => $this->penjadwalanRepository->all(null, null, Session::get('params'), Session::get('role'))->count(),
			'collection' => $this->penjadwalanRepository->all(null, null, Session::get('params'), 5)
		];
		$data['sertifikat'] = [
			'1month'  => $this->sertifikatRepositories->getExpired('1month', 10, Session::get('params'), Session::get('role')),
			'3month'  => $this->sertifikatRepositories->getExpired('3month', 10, Session::get('params'), Session::get('role')),
			'6month'  => $this->sertifikatRepositories->getExpired('6month', 10, Session::get('params'), Session::get('role')),
			'expired' => $this->sertifikatRepositories->getExpired('expired', 10, Session::get('params'), Session::get('role'))
		];
		return $data;
	}
}
