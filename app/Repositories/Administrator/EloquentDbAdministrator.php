<?php namespace Certification\Repositories\Administrator;

use Certification\Repositories\Administrator\InterfaceAdministratorRepository;
use Certification\Models\Administrator;
use Config;

/**
 *  Class DB repository for Master administrator using Eloquent
 *  implements InterfaceAdministratorRepository
 */
class EloquentDbAdministrator implements InterfaceAdministratorRepository {

	/**
	 * [$administratorModel description]
	 * 
	 * @var [type]
	 */
	private $administratorModel;

	/**
	 * [__construct description]
	 * 
	 * @param Administrator $administratorModel [description]
	 */
	public function __construct(Administrator $administratorModel)
	{
		$this->administratorModel = $administratorModel;
	}

	/**
	 * Get all administrator collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null, $filters = null)
	{
		$collection = $this->administratorModel->withRelationship();

		if (isset($filters)) {
			if ($filters->has('nipname')) {
				$collection->where('nip', 'LIKE', '%'.$filters->get('nipname').'%')->orWhere('nip', 'LIKE', '%'.$filters->get('nipname').'%');
			}
		}
		
		$collection->orderBy('created_at','desc');
		
		if(isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get administrator by given Id
	 * 
	 * @param  Integer $id administrator id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->administratorModel->withRelationship()->find($id);
	}

	/**
	 * store data administrator to database
	 * 
	 * @param  Array $data administrator
	 * @return Boolean
	 */
	public function store($data)
	{
		return $this->administratorModel->create($data);
	}

	/**
	 * Store new kompetensi
	 * 
	 * @param  Integer $id           administrator id
	 * @param  Integer $kompetensi_id kompetensi id
	 * @return void
	 */
	public function storeKompetensi($id, $kompetensi_id)
	{
		$this->administratorModel->find($id)->kompetensi()->attach($kompetensi_id);
	}

	/**
	 * Store new kelompok
	 * 
	 * @param  Integer $id administrator id
	 * @param  Mixed $data Kelompok data
	 * @return void
	 */
	public function storeKelompok($id, $data)
	{
		$this->administratorModel->find($id)->kelompok()->attach([$data['lembagasertifikasi_id'] => ['tanggalpenetapan'=>$data['tanggalpenetapan']]]);
	}

	/**
	 * update single data administrator to database
	 *
	 * @param  Integer $id administrator id
	 * @param  Array $data administrator
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->administratorModel->whereId($id)->update($data);
	}

	/**
	 * delete single data administrator, with soft delete method or not
	 * 
	 * @param  Integer  $id          administrator id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false)
	{
		$this->administratorModel->whereId($id)->delete();
		return true;
	}

	/**
	 * Delete Kompetensi
	 * 
	 * @param  Integer $id           administrator id
	 * @param  Integer $kompetensi_id kompetensi id
	 * @return void
	 */
	public function deleteKompetensi($id, $kompetensi_id)
	{
		$this->administratorModel->find($id)->kompetensi()->detach($kompetensi_id);
	}

	/**
	 * Delete Kelompok
	 * 
	 * @param  Integer $id           administrator id
	 * @param  Integer $kompetensi_id kompetensi id
	 * @return void
	 */
	public function deleteKelompok($id, $lembagasertifikasi_id)
	{
		$this->administratorModel->find($id)->kelompok()->detach($lembagasertifikasi_id);
	}

	/**
	 * Get lists of administrator data
	 * 
	 * @return Array
	 */
	public function lists($lskId = null)
	{
		$collection =  $this->administratorModel->select('nama', 'administrator.id');
		if (isset($lskId)) {
			$collection->join('kelompokadministrator', 'administrator_id', '=', 'administrator.id')
					   ->where('kelompokadministrator.lembagasertifikasi_id', '=', $lskId);
		}

		return $collection->lists('nama','id');
	}

}
