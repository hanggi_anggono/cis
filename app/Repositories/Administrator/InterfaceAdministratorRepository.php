<?php namespace Certification\Repositories\Administrator;

/**
 * Interface for administrator repositories
 */
interface InterfaceAdministratorRepository {

	/**
	 * Get all administrator collection with paginate or not
	 * 
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($paginate = null, $filters = null);

	/**
	 * Get administrator by given Id
	 * 
	 * @param  Integer $id administrator id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data administrator to database
	 * 
	 * @param  Array $data administrator
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data administrator to database
	 *
	 * @param  Integer $id administrator id
	 * @param  Array $data administrator
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data administrator, with soft delete method or not
	 * 
	 * @param  Integer  $id     administrator id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($id, $forceDelete = false);
	
}
