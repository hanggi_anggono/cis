<?php namespace Certification\Repositories\Realisasipeserta;

use Certification\Models\Realisasipeserta;
use Certification\Services\ExportService;
use Config, DB;

class EloquentDbRealisasipeserta {

	/**
	 * [$realisasipesertaModel description]
	 * 
	 * @var [type]
	 */
	private $realisasipesertaModel;

	/**
	 * [$exportService description]
	 * 
	 * @var [type]
	 */
	private $exportService;

	/**
	 * [__construct description]
	 * 
	 * @param Realisasipeserta $realisasipesertaModel [description]
	 * @param ExportService    $exportService         [description]
	 */
	public function __construct(Realisasipeserta $realisasipesertaModel, ExportService $exportService)
	{
		$this->realisasipesertaModel = $realisasipesertaModel;
		$this->exportService         =$exportService;
	}

	/**
	 * [countAll description]
	 * 
	 * @return [type] [description]
	 */
	public function countAll($params = null)
	{
		return $this->filtersData(null, null, $params)->count();
	}

	/**
	 * [countAttended description]
	 * 
	 * @return [type] [description]
	 */
	public function countAttended($params = null)
	{
		return $this->filtersData(null, null, $params, ['attended' => true])->count();
	}

	/**
	 * [countPassed description]
	 * 
	 * @return [type] [description]
	 */
	public function countPassed($params = null)
	{
		return $this->filtersData(null, null, $params, ['attended' => true, 'passes' => true])->count();
	}

	/**
	 * [countNotPassed description]
	 * 
	 * @return [type] [description]
	 */
	public function countNotPassed($params = null)
	{
		return $this->filtersData(null, null, $params, ['attended' => true, 'passes' => false])->count();
	}

	/**
	 * [all description]
	 * 
	 * @param  string $paginate [description]
	 * @param  [type] $filters  [description]
	 * @return [type]           [description]
	 */
	public function all($paginate = 'paginate', $filters = null, $role = null, $params = null)
	{
		$collection = $this->filtersData($filters, $role, $params, ['attended' => true], true);
		
		if (isset($paginate) && $paginate == 'paginate') {
			return $collection->paginate(20);
		}
		return $collection->get();
	}

	/**
	 * [filtersData description]
	 * 
	 * @param  [type]  $filters          [description]
	 * @param  [type]  $role             [description]
	 * @param  [type]  $params           [description]
	 * @param  array   $realization      [description]
	 * @param  boolean $withRelationship [description]
	 * @return [type]                    [description]
	 */
	public function filtersData($filters = null, $role = null, $params = null, $realization = [], $withRelationship = false)
	{
		$collection = $this->realisasipesertaModel->noFilter();

		if ($withRelationship) {
			$collection->withRelationships(['peserta', 'kompetensi']);
		}

		if(isset($filters)) {
			if ($filters->has('tanggalrealisasi')) {
				$collection->where('realisasipeserta.created_at', 'LIKE', $filters->get('tanggalrealisasi').'%' );
			}

			if ($filters->has('kelulusan')) {
				$collection->where('kelulusan', '=', $filters->get('kelulusan'));
			}

			if($filters->has('kompetensi')) {
				$collection->where('kompetensi_id', '=', $filters->get('kompetensi'));
			}
		}
		if (isset($params)) {
			$decode = json_decode($params);
			if (isset($decode->unitinduk_id)) {
				$collection->join('peserta', 'realisasipeserta.peserta_id', '=', 'peserta.id')->where('peserta.unitinduk_id', '=', $decode->unitinduk_id);
			}
			if (isset($decode->lembagasertifikasi_id)) {
				$collection->where('penjadwalan.lembagasertifikasi_id', '=', $decode->lembagasertifikasi_id);
			}
			if (isset($role) && $role == 'employee') {
				if (isset($decode->id)) {
					$collection->orWhere('realisasipeserta.peserta_id', '=', $decode->id);
				}
			}
		}

		if (count($realization) > 0) {
			if (isset($realization['attended'])) {
				if ($realization['attended'] == true) {
					$collection->attended();
				}
			}	
			if (isset($realization['passes'])) {
				if ($realization['passes'] == true) {
					$collection->passes();
				} else {
					$collection->notPasses();
				}
			}
		}

		return $collection->orderBy('realisasipeserta.created_at', 'desc');
	}

	/**
	 * [export description]
	 * 
	 * @param  [type] $type    [description]
	 * @param  [type] $filters [description]
	 * @param  [type] $role    [description]
	 * @param  [type] $params  [description]
	 * @return [type]          [description]
	 */
	public function export($type, $filters = null, $role = null, $params = null)
	{
		$data['realisasi'] = $this->all(null, $filters, $role, $params);
		return $this->exportService->export($data['realisasi'], $type, ['viewName' => 'kelulusan', 'orientation' => 'landscape', 'filename' => 'report_kelulusan']);
	}
}
