<?php namespace Certification\Repositories\KompetensiAsesor;

/**
 * Interface for master kompetensi asesor database repository
 */
interface InterfaceKompetensiasesorRepository {

	/**
	 * Get all kompetensi asesor collection with paginate or not
	 *
	 * @param  Integer $asesor_id Asesor Id
	 * @param  String $paginate pagination status
	 * @return Collection
	 */
	public function all($asesor_id, $paginate = null);

	/**
	 * Get kompetensi asesor by given Id
	 * 
	 * @param  Integer $id kompetensi asesor id
	 * @return Collection
	 */
	public function getById($id);

	/**
	 * store data kompetensi asesor to database
	 * 
	 * @param  Array $data kompetensi asesor
	 * @return Boolean
	 */
	public function store($data);

	/**
	 * update single data kompetensi asesor to database
	 *
	 * @param  Integer $id kompetensi asesor id
	 * @param  Array $data kompetensi asesor
	 * @return Boolean
	 */
	public function update($id, $data);

	/**
	 * delete single data kompetensi asesor, with soft delete method or not
	 * 
	 * @param  Integer  $asesorId   asesor id
	 * @param  Integer  $kompetensiId   kompetensi id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($asesorId, $kompetensiId, $forceDelete = false);

}
