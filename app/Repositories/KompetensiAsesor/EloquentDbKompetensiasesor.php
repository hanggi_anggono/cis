<?php namespace Certification\Repositories\KompetensiAsesor;

use Certification\Repositories\Kompetensiasesor\InterfaceKompetensiasesorRepository;
use Certification\Models\Kompetensiasesor;
use Certification\Models\Asesor;
use \Config;

/**
 * Database Kompetensiasesor provider implementing Interface Kompetensiasesor Repository
 * Using Eloquent driver
 */
class EloquentDbKompetensiasesor implements InterfaceKompetensiasesorRepository {

	/**
	 * Elemen Kompetensiasesor Model
	 * 
	 * @var Model
	 */
	private $kompetensiasesorModel;

	/**
	 * Elemen asesor Model
	 * 
	 * @var Model
	 */
	private $asesorModel;	

	/**
	 * Function status
	 * 
	 * @var Boolean
	 */
	private $status;

	/**
	 * Class constructor for inject dependencies
	 * 
	 * @param Kompetensiasesor $kompetensiasesorModel Model Kompetensi Asesor
	 * @param Asesor $asesorModel Model Asesor
	 */
	public function __construct(Kompetensiasesor $kompetensiasesorModel, Asesor $asesorModel)
	{
		$this->kompetensiasesorModel = $kompetensiasesorModel;
		$this->asesorModel			 = $asesorModel;
		$this->status                = false;
	}

	/**
	 * Get all kompetensi asesor collection with paginate or not
	 *
	 * @param  Integer $asesor_id asesor id
	 * @param  boolean $paginate pagination status
	 * @return Collection
	 */
	public function all($asesor_id, $paginate = null)
	{
		if (empty($asesor_id)) {
			$collection = $this->kompetensiasesorModel;
		} else {
			$collection = $this->kompetensiasesorModel->byAsesor($asesor_id);
		}
		
		if (isset($paginate) && strtolower($paginate) == 'paginate') {
			return $collection->paginate(Config::get('certification.default_pagination_count'));
		}
		return $collection->get();
	}

	/**
	 * Get kompetensi asesor by given Id
	 * 
	 * @param  Integer $id kompetensi asesor id
	 * @return Collection
	 */
	public function getById($id)
	{
		return $this->kompetensiasesorModel->findOrFail($id);
	}

	/**
	 * Store data kompetensi asesor to database
	 * 
	 * @param  Array $data kompetensi asesor
	 * @return Boolean
	 */
	public function store($data)
	{
		$asesor = $this->asesorModel->withRelationship()->find($data['asesor_id']);

		if (!empty($asesor)) {
			$asesor->kompetensi()->attach($data['kompetensi_id']);
			$this->status = true;
		}
		return $this->status;
	}

	/**
	 * update single data kompetensi asesor to database
	 *
	 * @param  Integer $id kompetensi asesor id
	 * @param  Array $data kompetensi asesor
	 * @return Boolean
	 */
	public function update($id, $data)
	{
		return $this->kompetensiasesorModel->where('kompetensi_id','=',$data['kompetensi_old'])
										   ->where('asesor_id','=',$data['asesor_id'])
										   ->update(['kompetensi_id'=>$data['kompetensi_id']]);
	}

	/**
	 * delete single data kompetensi asesor, with soft delete method or not
	 * 
	 * @param  Integer  $asesorId   asesor id
	 * @param  Integer  $kompetensiId   kompetensi id
	 * @param  boolean $forceDelete force delete status
	 * @return Boolean
	 */
	public function delete($asesorId, $kompetensiId, $forceDelete = false)
	{
		$this->kompetensiasesorModel->whereAttributes($asesorId, $kompetensiId)->delete();
		return true;
	}

}
