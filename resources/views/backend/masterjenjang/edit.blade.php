@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Edit Jenjang</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	<form class="form-horizontal" action="{{ route('dashboard.master.jenjang.update', ['id' => $data['jenjang']->id]) }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<input type="hidden" name="_method" value="put">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-2 control-label">Nama Jenjang</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama jenjang jabatan" name="nama_jenjangjabatan" value="{{ $data['jenjang']->nama_jenjangjabatan }}" required>
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Update</button>&nbsp;
		            <a href="{{ route('dashboard.master.jenjang.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    