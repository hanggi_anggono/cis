@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_masterjenjangjabatan'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.jenjang.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Jenjang Jabatan</a>
		    </div><!-- /.box-header -->
		@endif
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th class="col-md-1">No.</th>
				          	<th>Nama Jenjang</th>
				          	<th class="col-md-3 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @foreach($data['jenjang'] as $jenjang)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $jenjang->nama_jenjangjabatan }}</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterjenjangjabatan'))
				          		<a href="{{ route('dashboard.master.jenjang.edit', ['id' => $jenjang->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.jenjang.delete', ['id' => $jenjang->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>    
			          @endforeach
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['jenjang']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop