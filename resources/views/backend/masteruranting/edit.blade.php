@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Edit Unit Ranting</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	{!! Form::open(['route' => ['dashboard.master.unit.ranting.update', $data['unit']->id], 'class' => 'form-horizontal', 'method' => 'put']) !!}
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Unit Cabang</label>
                      <div class="col-sm-7">
                  		{!! Form::select('unitcabang_id', $data['unitcabang'], $data['unit']->unitcabang_id,['class'=>'selectpicker','data-live-search'=>'true'])!!}
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Nama Unit ranting</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama unit ranting" name="nama_unitranting" value="{{ $data['unit']->nama_unitranting }}" required>
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Update</button>&nbsp;
		            <a href="{{ route('dashboard.master.unit.ranting.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    {!! Form::close() !!}
		</div>
	</div>
</div>		    
@stop		    