@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Tambah Unit ranting</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	<form class="form-horizontal" action="{{ route('dashboard.master.unit.ranting.store') }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Unit Induk</label>
                      <div class="col-sm-7">
                      	@if(isset($data['unitcabang_single']))
                      		<input type="text" class="form-control" value="{{ $data['unitcabang_single']->nama_unitcabang }}" readonly="readonly">
                      		<input type="hidden" class="form-control" name="unitcabang_id" value="{{ $data['unitcabang_single']->id }}">
                      	@else
                      		{!! Form::select('unitcabang_id', $data['unitcabang'], null,['class'=>'selectpicker','data-live-search'=>'true'])!!}
                      	@endif          
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Nama Unit Ranting</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama unit ranting" name="nama_unitranting" required>
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		            <a href="{{ route('dashboard.master.unit.ranting.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop