@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
	  <div class="box">
	  @if(Auth::user()->can('can_write_permohonan'))
	    <div class="box-header">
	      <div class="input-group">
	        <a href="{{ route('dashboard.permohonansdm.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Permohonan SDM</a>
	      </div>
	    </div><!-- /.box-header -->
	    @endif
	    <div class="box-body">
	    @if (Session::has('flash_notification.message'))
		    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        {{ Session::get('flash_notification.message') }}
		    </div>
		@endif
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover" style="font-size:12px;">
	        <thead>
	            <tr class="bg-info">
	              <th class="text-center">No.</th>
	              <th class="text-center">Tanggal Permohonan</th>
	              <th class="text-center">Unit Induk</th>
	              <th class="text-center">Kompetensi</th>
	              <th class="text-center">Jumlah Peserta</th>
	              <th class="col-md-3 text-center">Keterangan</th>
	              <th class="col-md-1 text-center">Aksi</th>
	            </tr>
	        </thead>
	        <tbody>
	        	 @forelse($data['permohonansdm'] as $permohonansdm)
	          <tr>
	          	<td>{{ $data['pagination_number'] }}</td>
	          	<td><a href="{{ route('dashboard.permohonansdm.kompetensi.index', ['id' => $permohonansdm->id]) }}"> {{ $permohonansdm->tanggal_mohon }}</a></td>
	          	<td>{{ $permohonansdm->unitinduk->nama_unitinduk }}</td>
	          	<td style="white-space:nowrap;">
	          		@forelse($permohonansdm->kompetensi as $kompetensi)
		          		- {{$kompetensi->nama_kompetensi}}<br>
	          		@empty
	          		-
	          		@endforelse
	          	</td>
	          	<td class="text-center">
	          		@forelse($permohonansdm->kompetensi as $kompetensi)
	          			@forelse($data['peserta'] as $peserta)
	          				@if($kompetensi->id == $peserta->kompetensi_id && $permohonansdm->id == $peserta->permohonansdm_id)
			          			<a href="{{ route('dashboard.permohonansdm.kompetensi.peserta.index', ['id' => $permohonansdm->id, 'kompetensiid' => $kompetensi->id]) }}">
			          			{{ $peserta->peserta->count() }}</a><br>
	          				@endif
	          			@empty
	          			-
	          			@endforelse
	          		@empty
	          		-
	          		@endforelse
	          	</td>
	          	<td>{{ $permohonansdm->keterangan }}</td>
	          	<td class="text-center">
                  {{-- <a href="{{ route('dashboard.permohonansdm.show',['id' => $permohonansdm->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a> --}}
                @if(Auth::user()->can('can_write_permohonan'))
                  <a href="{{ route('dashboard.permohonansdm.edit',['id' => $permohonansdm->id]) }}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> </a>
                  <a href="{{ route('dashboard.permohonansdm.delete',['id' => $permohonansdm->id]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Hapus data?'); ">
                  	<i class="fa fa-trash"></i> </a>
                @else
                	-
                @endif
                </td>
	          </tr>
	          <?php $data['pagination_number']++; ?>
	          @empty
	          <tr>
	          	<td colspan="8" class="text-center"> Tidak ada data permohonan SDM</td>
	          </tr>
	          @endforelse
	        </tbody>
	      </table>	
		</div>
	      <br>
	      <div class="text-center">
	      </div>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
</div><!-- /.row -->
@stop