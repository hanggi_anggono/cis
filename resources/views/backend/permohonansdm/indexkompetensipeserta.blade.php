@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-lg-12">
	  <div class="box">
	    <div class="box-header">
	      <div class="input-group">
            <label>Tanggal Permohonan:</label> {{$data['permohonansdm']->tanggal_mohon}}
          </div>
          <div class="input-group">
            <label>Unit Induk:</label> {{$data['permohonansdm']->unitinduk->nama_unitinduk }}
          </div>
          <div class="input-group">
            <label>Kompetensi:</label> {{$data['kompetensi']->nama_kompetensi}}
          </div> <br>
	      <div class="input-group">
	    @if(Auth::user()->can('can_write_permohonan'))
	        <a href="{{ route('dashboard.permohonansdm.kompetensi.peserta.create', ['id' => $data['permohonansdm']->id, 'kompetensiid' => $data['kompetensi']->id ]) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Peserta</a> &nbsp; 
	    @endif
	        <a href="{{ route('dashboard.permohonansdm.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
	      </div>
	    </div><!-- /.box-header -->
	    <div class="box-body">
	    @if (Session::has('flash_notification.message'))
		    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        {{ Session::get('flash_notification.message') }}
		    </div>
		@endif
			<table class="table table-striped table-bordered table-hover table-condensed">
	            <thead>
	                <tr class="bg-info">
	                  <th class="col-sm-1 text-center">No.</th>
	                  <th class="text-center">Tipe</th>
	                  <th class="col-sm-2 text-center">NIP</th>
                      <th class="col-sm-2 text-center">Nama</th>
                      <th class="text-center">Grade</th>
                      <th class="text-center">Jabatan</th>
                      <th class="text-center">Unit Induk</th>
                      <th class="text-center">Unit Non PLN</th>
	                  <th class="col-sm-1 text-center">Aksi</th>
	                </tr>
	            </thead>
	            <tbody>
	            <?php $no=1; ?>
	            @forelse($data['peserta']->peserta as $peserta)
	            <tr>
	            	<td>{{ $no }}</td>
	            	<td>{{ ($peserta->tipe_peserta == 'pln') ? 'PLN' : 'NON PLN' }}</td>
	            	<td>{{ $peserta->nip }}</td>
	            	<td>{{ $peserta->nama }}</td>
	            	<td>{{ $peserta->grade->nama_grade }}</td>
	            	<td>{{ $peserta->jabatan }}</td>
	            	<td>{{ (! is_null($peserta->unitinduk_id)) ? $peserta->unitinduk->nama_unitinduk : '-' }}</td>
	            	<td>{{ (! is_null($peserta->unitnonpln_id)) ? $peserta->unitnonpln->nama_unitnonpln : '-' }}</td>
	            	<td class="text-center">
	            	@if(Auth::user()->can('can_write_permohonan'))
	            		<a href="{{ route('dashboard.permohonansdm.kompetensi.peserta.delete', ['id' => $data['permohonansdm']->id, 'kompetensiid' => $data['kompetensi']->id, 'pesertaid' => $peserta->id ]) }}" class="btn btn-xs btn-danger"  onclick="return confirm('Hapus data peserta?'); "><i class="fa fa-trash"></i> </a>
	            	@else
	            		-
	            	@endif
	            	</td>
	            </tr>
	            <?php $no++; ?>
	            @empty
	            <tr>
	            	<td colspan="9" class="text-center">Tidak ada data peserta untuk permohonan ini</td>
	            </tr>
	            @endforelse
	            </tbody>
	        </table>    
		</div>
	  </div>
	</div>
</div>	  
@stop
