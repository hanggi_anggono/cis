@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-lg-12">
	  <div class="box">
	    <div class="box-header">
	      <div class="input-group">
            <label>Tanggal Permohonan:</label> {{$data['permohonansdm']->nosurat}}
          </div>
          <div class="input-group">
            <label>Unit Induk:</label> {{$data['permohonansdm']->unitinduk->nama_unitinduk }}
          </div> <br>
	      <div class="input-group">
	    @if(Auth::user()->can('can_write_permohonan'))
	        <a href="{{ route('dashboard.permohonansdm.kompetensi.create', ['id' => $data['permohonansdm']->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Kompetensi</a> &nbsp; 
	    @endif
	        <a href="{{ route('dashboard.permohonansdm.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
	      </div>
	    </div><!-- /.box-header -->
	    <div class="box-body">
	    @if (Session::has('flash_notification.message'))
		    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        {{ Session::get('flash_notification.message') }}
		    </div>
		@endif
			<table class="table table-striped table-bordered table-hover table-condensed">
	            <thead>
	                <tr class="bg-info">
	                  <th class="text-center">No.</th>
	                  <th class="text-center">Bidang</th>
	                  <th class="text-center">Sub Bidang</th>
	                  <th class="text-center">Kompetensi</th>
	                  <th class="text-center">Kode DJK</th>
	                  <th class="text-center">Kode SKKNI</th>
	                  <th class="col-sm-1 text-center">Aksi</th>
	                </tr>
	            </thead>
	            <tbody>
	            <?php $no=1; ?>
	            @forelse($data['permohonansdm']->kompetensi as $kompetensi)
	            <tr>
	            	<td>{{ $no }}</td>
	            	<td>{{ $kompetensi->subbidang->bidang->nama_bidang }}</td>
	            	<td>{{ $kompetensi->subbidang->nama_subbidang }}</td>
	            	<td>{{ $kompetensi->nama_kompetensi }}</td>
	            	<td>{{ (! empty($kompetensi->kodedjk)) ? $kompetensi->kodedjk : '-' }}</td>
	            	<td>{{ (! empty($kompetensi->kodeskkni)) ? $kompetensi->kodeskkni : '-' }}</td>
	            	<td class="text-center">
	            	@if(Auth::user()->can('can_write_permohonan'))
	            		<a href="{{ route('dashboard.permohonansdm.kompetensi.delete', ['id' => $data['permohonansdm']->id, 'kompetensiid' => $kompetensi->id ]) }}" class="btn btn-xs btn-danger"  onclick="return confirm('Hapus data kompetensi?'); "><i class="fa fa-trash"></i> </a>
	            	@else
	            		-
	            	@endif
	            	</td>
	            </tr>
	            <?php $no++; ?>
	            @empty
	            <tr>
	            	<td colspan="7" class="text-center"> Tidak ada data kompetensi untuk permohonan SDM ini</td>
	            </tr>
	            @endforelse
	            </tbody>
	        </table>    
		</div>
	  </div>
	</div>
</div>	  
@stop
