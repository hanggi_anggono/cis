@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-aqua"><i class="fa fa-external-link"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">TUK</span>
	      <span class="info-box-number"><a href="{{ route('dashboard.master.unit.diklat.index') }}">{{ count($data['summaries']['udiklat']) }} unit</a></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-red"><i class="fa fa-external-link"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Unit Induk</span>
	      <span class="info-box-number"><a href="{{ route('dashboard.master.unit.induk.index') }}">{{ count($data['summaries']['uinduk']) }} unit</a></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-green"><i class="fa fa-external-link"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Unit Cabang</span>
	      <span class="info-box-number"><a href="{{ route('dashboard.master.unit.cabang.index') }}">{{ count($data['summaries']['ucabang']) }} unit</a></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-yellow"><i class="fa fa-external-link"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Unit Ranting</span>
	      <span class="info-box-number"><a href="{{ route('dashboard.master.unit.ranting.index') }}">{{ count($data['summaries']['uranting']) }} unit</a></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
	<div class="col-md-3">
		<div class="box box-primary">
			<div class="box-header with-border">
				<span>TUK Terbaru</span>
			</div>
		    <div class="box-body">
		    	<div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th>No.</th>
				          	<th>TUK</th>
				          </tr>
			          </thead>
			          <tbody>
			          <?php $noudiklat = 1; ?>
			          @foreach($data['summaries']['udiklat'] as $udiklat)
			          <tr>
			          	<td>{{ $noudiklat }}</td>
			          	<td>{{ $udiklat->nama_udiklat }}</td>
			          </tr>
			          <?php $noudiklat++; ?>
			          @if($noudiklat == 6)
			          <?php break; ?>
			          @endif
			          @endforeach
			          </tbody>
			        </table>
			    </div>      
			    <div class="box-footer pull-right">
		        	<a href="{{ route('dashboard.master.unit.diklat.index') }}" class="btn btn-default btn-xs">Lihat semua</a>
		        </div>
		    </div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="box box-danger">
			<div class="box-header with-border">
				<span>Unit Induk Terbaru</span>
			</div>
		    <div class="box-body">
		    	<div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-danger">
				          <tr>
				          	<th>No.</th>
				          	<th>Unit Induk</th>
				          </tr>
			          </thead>
			          <tbody>
			          <?php $nouinduk = 1; ?>
			          @foreach($data['summaries']['uinduk'] as $uinduk)
			          <tr>
			          	<td>{{ $nouinduk }}</td>
			          	<td>{{ $uinduk->nama_unitinduk }}</td>
			          </tr>
			          <?php $nouinduk++; ?>
			          @if($nouinduk == 6)
			          <?php break; ?>
			          @endif
			          @endforeach
			          </tbody>
			        </table>
			    </div>      
			    <div class="box-footer pull-right">
		        	<a href="{{ route('dashboard.master.unit.induk.index') }}" class="btn btn-default btn-xs">Lihat semua</a>
		        </div>
		    </div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="box box-success">
			<div class="box-header with-border">
				<span>Unit Cabang Terbaru</span>
			</div>
		    <div class="box-body">
		    	<div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-success">
				          <tr>
				          	<th>No.</th>
				          	<th>Unit Cabang</th>
				          </tr>
			          </thead>
			          <tbody>
			          <?php $noucabang = 1; ?>
			          @foreach($data['summaries']['ucabang'] as $ucabang)
			          <tr>
			          	<td>{{ $noucabang }}</td>
			          	<td>{{ $ucabang->nama_unitcabang }}</td>
			          </tr>
			          <?php $noucabang++; ?>
			          @if($noucabang == 6)
			          <?php break; ?>
			          @endif
			          @endforeach
			          </tbody>
			        </table>
			    </div>      
			    <div class="box-footer pull-right">
		        	<a href="{{ route('dashboard.master.unit.cabang.index') }}" class="btn btn-default btn-xs">Lihat semua</a>
		        </div>
		    </div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="box box-warning">
			<div class="box-header with-border">
				<span>Unit Ranting Terbaru</span>
			</div>
		    <div class="box-body">
		    	<div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-warning">
				          <tr>
				          	<th>No.</th>
				          	<th>Unit Ranting</th>
				          </tr>
			          </thead>
			          <tbody>
			          <?php $nounitranting = 1; ?>
			          @foreach($data['summaries']['uranting'] as $uranting)
			          <tr>
			          	<td>{{ $nounitranting }}</td>
			          	<td>{{ $uranting->nama_unitranting }}</td>
			          </tr>
			          <?php $nounitranting++; ?>
			          @if($nounitranting == 6)
			          <?php break; ?>
			          @endif
			          @endforeach
			          </tbody>
			        </table>
			    </div>      
			    <div class="box-footer pull-right">
		        	<a href="{{ route('dashboard.master.unit.ranting.index') }}" class="btn btn-default btn-xs">Lihat semua</a>
		        </div>
		    </div>
		</div>
	</div>	    
</div>
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-aqua"><i class="fa fa-external-link"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Unit Non PLN</span>
	      <span class="info-box-number"><a href="{{ route('dashboard.master.unit.nonpln.index') }}">{{ count($data['summaries']['unonpln']) }} unit</a></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
</div><!-- /.row -->
<div class="row">
	<div class="col-md-3">
		<div class="box box-info">
			<div class="box-header with-border">
				<span>Unit Non PLN Terbaru</span>
			</div>
		    <div class="box-body">
		    	<div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th>No.</th>
				          	<th>Unit Non PLN</th>
				          </tr>
			          </thead>
			          <tbody>
			          <?php $nounitnonpln = 1; ?>
			          @foreach($data['summaries']['unonpln'] as $nonpln)
			          <tr>
			          	<td>{{ $nounitnonpln }}</td>
			          	<td>{{ $nonpln->nama_unitnonpln }}</td>
			          </tr>
			          <?php $nounitnonpln++; ?>
			          @if($nounitnonpln == 6)
			          <?php break; ?>
			          @endif
			          @endforeach
			          </tbody>
			        </table>
			    </div>      
			    <div class="box-footer pull-right">
		        	<a href="{{ route('dashboard.master.unit.nonpln.index') }}" class="btn btn-default btn-xs">Lihat semua</a>
		        </div>
		    </div>
		</div>
	</div>
</div>
@stop