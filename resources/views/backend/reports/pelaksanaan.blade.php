@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $data['summaries']['pelaksanaan'] }}</h3>
          <p>Total Pelaksanaan</p>
        </div>
        <div class="icon">
          <i class="fa fa-edit"></i>
        </div>
      </div>
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $data['summaries']['attended'] }}</h3>
          <p>Total Kehadiran</p>
        </div>
        <div class="icon">
          <i class="fa fa-check"></i>
        </div>
      </div>
    </div><!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $data['summaries']['passed'] }}</h3>
          <p>Peserta Kompeten</p>
        </div>
        <div class="icon">
          <i class="fa fa-certificate"></i>
        </div>
      </div>
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $data['summaries']['notpassed'] }}</h3>
          <p>Peserta Belum Kompeten</p>
        </div>
        <div class="icon">
          <i class="fa fa-ban"></i>
        </div>
      </div>
    </div><!-- /.col -->
  </div><!-- /.row -->

   <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Filter</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-md-3">
              <form action="{{ route('report.pelaksanaan.index') }}" method="get" accept-charset="utf-8" class="form-inline">
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" name="tanggal" id="tanggal_pelaksanaan" placeholder="Tanggal pelaksanaan" value="{{ Request::get('tanggal') }}"/>
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Cari</button>
                    </span>
                  </div><!-- /.input group -->
                </div>
              </form>
            </div>
            <div class="col-md-3">
              <form action="{{ route('report.pelaksanaan.index') }}" method="get" class="form-inline">
                <div class="form-group">
                  <div class="input-group">
                  {!! Form::select('tuk', ['' => '--------- Pilih TUK ---------'] + $data['tuk'], null, ['class' => 'selectpicker', 'data-live-search' => 'true']) !!}
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Cari</button>
                    </span>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-3">
              <form action="{{ route('report.pelaksanaan.index') }}" method="get" class="form-inline">
                <div class="form-group">
                  <div class="input-group">
                  {!! Form::select('lsk', ['' => '--------- Pilih LSK ---------'] + $data['lsk'], null, ['class' => 'selectpicker', 'data-live-search' => 'true']) !!}
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Cari</button>
                    </span>
                  </div>
                </div>
              </form>
            </div>
        </div><br>
      </div>
    </div>
  </div>      

  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
      	<div class="box-header with-border">
      		<h3 class="box-title">Daftar Pelaksanaan Sertifikasi</h3>
          <a href="{{ route('report.pelaksanaan.index') }}" class="btn btn-info btn-sm pull-right"><i class="fa fa-reply"></i> All Data</a>
          <a href="{{ route('report.pelaksanaan.index', $data['params'] + ['export' => 'pdf']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-pdf-o"></i> Download PDF</a> &nbsp;
          <a href="{{ route('report.pelaksanaan.index', $data['params'] + ['export' => 'excel']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Download Excel</a>
      	</div>
        <div class="box-body">
          <table class="table table-condensed table-bordered table-hover" style="font-size:12px;">
            <thead>
                <tr class="bg-info">
                  <th class="text-center" rowspan="2">No.</th>
                  <th class="text-center" rowspan="2">Tanggal Pelaksanaan</th>
                  <th class="text-center" rowspan="2">TUK</th>
                  <th class="text-center" rowspan="2">LSK</th>
                  <th class="text-center" rowspan="2">Unit Induk</th>
                  <th class="text-center" rowspan="2">Kompetensi</th>
                  <th class="text-center" rowspan="2">Rencana</th>
                  <th class="col-md-1 text-center" colspan="3">Realisasi</th>
                </tr>
                <tr class="bg-info">
                  <th class="col-md-1 text-center">Kehadiran</th>
                  <th class="col-md-1 text-center">Kompeten</th>
                  <th class="col-md-1 text-center">Belum Kompeten</th>
                </tr>
            </thead>
            <tbody>
              @forelse($data['pelaksanaan']['penjadwalan'] as $pelaksanaan)
              <tr>
                <td>{{ $data['pagination_number']++ }}</td>
                <td>{{ $pelaksanaan->tanggal_mulai }} s/d {{ $pelaksanaan->tanggal_selesai }}</td>
                <td>{{ $pelaksanaan->udiklat->nama_udiklat }}</td>
                <td>{{ $pelaksanaan->lembagasertifikasi->nama_lembagasertifikasi }}</td>
                <td>{{ $pelaksanaan->unitinduk->nama_unitinduk }}</td>
                <td style="white-space:nowrap;">
                  @forelse($pelaksanaan->kompetensi as $kompetensi)
                    {{ $kompetensi->nama_kompetensi }}<br>
                  @empty
                    -
                  @endforelse
                </td>
                <td class="text-center" style="white-space:nowrap;">
                  @forelse($data['pelaksanaan']['pelaksanaan'] as $realisasi)
                  @if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
                    {{ $realisasi['total_peserta'] }}<br>
                  @endif    
                  @empty
                    -
                  @endforelse
                </td>
                <td class="text-center" style="white-space:nowrap;">
                  @forelse($data['pelaksanaan']['pelaksanaan'] as $realisasi)
                    @if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
                      {{ $realisasi['realisasi']['kehadiran'] }}<br>
                    @endif  
                  @empty
                  -
                  @endforelse
                </td>
                <td class="text-center" style="white-space:nowrap;">
                  @forelse($data['pelaksanaan']['pelaksanaan'] as $realisasi)
                    @if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
                      {{ $realisasi['realisasi']['kompeten'] }}<br>
                    @endif  
                  @empty
                  -
                  @endforelse
                </td>
                <td class="text-center" style="white-space:nowrap;">
                  @forelse($data['pelaksanaan']['pelaksanaan'] as $realisasi)
                    @if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
                      {{ $realisasi['realisasi']['nonkompeten'] }}<br>
                    @endif  
                  @empty
                  -
                  @endforelse
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="12" class="text-center">Tidak ada data pelaksanaan</td>
              </tr>
              @endforelse
            </tbody>
          </table><br>
              <div class="text-center">
                {!! paginationHelper($data['pelaksanaan']['penjadwalan']->appends($data['params'])->render()) !!}
              </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
  $('[id^=tanggal]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  });
 
</script>
@stop