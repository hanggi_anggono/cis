@extends('layouts.backend.master')
@section('content')
   <div class="row">
    <div class="col-lg-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Filter</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-xs-12">
              <form class="form-inline" method="get" action="{{ route('report.sebarankompetensi.index') }}">
              <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" id="periode" placeholder="Periode" name="periode" value="{{ Request::get('periode') }}" />
                  </div><!-- /.input group -->
                </div>
              <div class="form-group">
                {!! Form::select('kompetensi', ['' => '---- Pilih Kompetensi ----'] + $data['kompetensi'], Request::get('kompetensi'), ['class'=>'selectpicker form-control','data-live-search' => 'true'])!!}
              </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> </button>
            </form>
            </div>
        </div><br>
      </div>
    </div>
  </div>      

  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
      	<div class="box-header">
      		<h3 class="box-title">Data Sebaran Kompetensi ({{ (Request::has('periode')) ? Request::get('periode') : date('Y-m-d') }})</h3>
          <a href="{{ route('report.sebarankompetensi.index') }}" class="btn btn-info btn-sm pull-right">All Data</a>
          <a href="{{ route('report.sebarankompetensi.index', $data['params'] + ['export' => 'pdf']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-pdf-o"></i> Download PDF</a> &nbsp;
          <a href="{{ route('report.sebarankompetensi.index', $data['params'] + ['export' => 'excel']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Download Excel</a>
      	</div>
        <div class="box-body">
          <div class="table-responsive">
              <table class="table no-margin table-condensed table-bordered table-hover">
                      <thead>
                          <tr class="bg-info">
                            <th class="text-center">No.</th>
                            <th class="text-center">Kode DJK</th>
                            <th class="text-center">Kode SKKNI</th>
                            <th class="text-center">Nama Kompetensi</th>
                            <th class="text-center">Nama Bidang</th>
                            <th class="text-center">Nama Subbidang</th>
                            <th class="text-center">Kompeten</th>
                            <th class="text-center">Belum Kompeten</th>
                          </tr>
                      </thead>
                      <tbody>
                      @forelse($data['sebaran'] as $sebaran)
                        <tr>
                          <td>{{ $data['pagination_number']++ }}</td>
                          <td>{{ (! empty($sebaran->kodedjk)) ? $sebaran->kodedjk : '-' }}</td>
                          <td>{{ (! empty($sebaran->kodeskkni)) ? $sebaran->kodeskkni : '-' }}</td>
                          <td>{{ $sebaran->nama_kompetensi }}</td>
                          <td>{{ $sebaran->nama_bidang }}</td>
                          <td>{{ $sebaran->nama_subbidang }}</td>
                          <td>{{ $sebaran->passesCount }} Peserta</td>
                          <td>{{ $sebaran->notpassesCount }} Peserta</td>
                        </tr>
                      @empty
                        <tr>
                          <td colspan="8" class="text-center">Tidak ada data kompetensi</td>
                        </tr>
                      @endforelse
                      </tbody>
              </table><br>
              <div class="text-center">
               {!! paginationHelper($data['sebaran']->appends($data['params'])->render()) !!}
              </div>
           </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
  $('#periode').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  });
 
</script>
@stop