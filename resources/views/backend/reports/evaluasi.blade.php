@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $data['summaries']['total'] }}</h3>
          <p>Total Peserta Evaluasi</p>
        </div>
        <div class="icon">
          <i class="fa fa-group"></i>
        </div>
      </div>
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $data['summaries']['notevaluated'] }}</h3>
          <p>Belum Dievaluasi</p>
        </div>
        <div class="icon">
          <i class="fa fa-check"></i>
        </div>
      </div>
    </div><!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $data['summaries']['accepted'] }}</h3>
          <p>Diterima</p>
        </div>
        <div class="icon">
          <i class="fa fa-certificate"></i>
        </div>
      </div>
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $data['summaries']['rejected'] }}</h3>
          <p>Ditolak</p>
        </div>
        <div class="icon">
          <i class="fa fa-ban"></i>
        </div>
      </div>
    </div><!-- /.col -->
  </div><!-- /.row -->

   <div class="row">
    <div class="col-lg-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Filter</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-xs-3">
              <form action="{{ route('report.evaluasi.index') }}" method="get" accept-charset="utf-8" class="form-inline">
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" id="tanggal_evaluasi" placeholder="Tanggal evaluasi" name="tanggalevaluasi" value="{{ Request::get('tanggalevaluasi') }}" />
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                  </div><!-- /.input group -->
                </div>
              </form>
            </div>
            <div class="col-xs-9">
              <form class="form-inline" method="get" action="{{ route('report.evaluasi.index') }}">
              <div class="form-group">
                <input type="text" class="form-control" name="nosurat" value="{{ Request::get('nosurat') }}" placeholder="No. Surat">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="kompetensi" value="{{ Request::get('kompetensi') }}" placeholder="Nama / kode kompetensi">
              </div>
              <div class="form-group">
                {!! Form::select('hasil', ['' => 'Semua hasil', 'pending' => 'Belum Dievaluasi', '0' => 'Ditolak', '1' => 'Diterima'], Request::get('hasil'), ['class' => 'form-control'])!!}
              </div>
              <div class="form-group">
                {!! Form::select('status_penjadwalan', ['' => 'Semua status', '0' => 'Belum dijadwalkan', '1' => 'Sudah Dijadwalkan'], Request::get('status_penjadwalan'), ['class' => 'form-control'])!!}
              </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
              
            </form>
            </div>
        </div><br>
      </div>
    </div>
  </div>      

  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
      	<div class="box-header">
      		<h3 class="box-title">Daftar Evaluasi Peserta Pemohonan</h3>
          <a href="{{ route('report.evaluasi.index') }}" class="btn btn-info btn-sm pull-right">All Data</a>
          <a href="{{ route('report.evaluasi.index', $data['params'] + ['export' => 'pdf']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-pdf-o"></i> Download PDF</a> &nbsp;
          <a href="{{ route('report.evaluasi.index', $data['params'] + ['export' => 'excel']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Download Excel</a>
      	</div>
        <div class="box-body">
          <div class="table-responsive">
              <table class="table no-margin table-condensed table-bordered table-hover">
                      <thead>
                          <tr class="bg-info">
                            <th class="text-center">No.</th>
                            <th class="text-center">NIP</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Unit Induk</th>
                            <th class="text-center">Kompetensi</th>
                            <th class="text-center">No. Surat</th>
                            <th class="text-center">Hasil</th>
                            <th class="text-center">Tgl. Evaluasi</th>
                            <th class="text-center">Dijadwalkan?</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php $hasilArr = ['ditolak','diterima']; $classArr = ['bg-danger','bg-success']; ?>
                      @forelse($data['evaluasi'] as $evaluasi)
                        <tr class="{{ ($evaluasi->hasil != '') ? $classArr[$evaluasi->hasil] : '' }}">
                          <td>{{ $data['pagination_number'] }}</td>
                          <td>{{ $evaluasi->nip }}</td>
                          <td>{{ $evaluasi->nama }}</td>
                          <td>{{ (! is_null($evaluasi->nama_unitinduk)) ? $evaluasi->nama_unitinduk : '-'}}</td>
                          <td>{{ $evaluasi->nama_kompetensi }}</td>
                          <td>{{ $evaluasi->nosurat }}</td>
                          <td>
                            @if($evaluasi->hasil != '')
                              {{ $hasilArr[$evaluasi->hasil] }}
                            @else
                              @if(Auth::user()->can('can_write_evaluasi'))
                              <a href="{{ route('dashboard.evaluasi.set.hasil', ['id' => $evaluasi->id_peserta, 'kompetensipermohonanid' => $evaluasi->kompetensipermohonan_id ]) }}">Evaluasi</a>
                              @else
                                -
                              @endif
                            @endif
                          </td>
                          <td>{{ ($evaluasi->tanggal_evaluasi != '') ? $evaluasi->tanggal_evaluasi : '-' }}</td>
                          <td class="text-center">
                            @if($evaluasi->status_penjadwalan != '')
                              <span class="label {{ ($evaluasi->status_penjadwalan == '1') ? 'label-success' : 'label-warning' }}">{{ ($evaluasi->status_penjadwalan == '1') ? 'Sudah' : 'Belum' }}</span>
                            @else
                              -
                            @endif
                          </td>
                        </tr>
                        <?php $data['pagination_number']++; ?>
                      @empty
                        <tr>
                          <td colspan="9" class="text-center">Tidak ada data evaluasi</td>
                        </tr>
                      @endforelse
                      </tbody>
              </table><br>
              <div class="text-center">
                {!! paginationHelper($data['evaluasi']->appends($data['params'])->render()) !!}
              </div>
           </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
  $('[id^=tanggal]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  });
 
</script>
@stop
