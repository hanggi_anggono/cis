@extends('layouts.backend.master')
@section('content')
 <div class="row">
    <div class="col-lg-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Filter</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-xs-4">
              <form class="form-inline" method="get" action="{{ route('report.biaya.index') }}">
              <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" id="tanggal_periode" placeholder="Periode" name="periode" value="{{ Request::get('periode') }}" />
                  </div><!-- /.input group -->
                </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-magnifier"></i> Cari</button>
            </form>
            </div>
            <div class="col-xs-6">
              <form class="form-inline" method="get" action="{{ route('report.biaya.index') }}">
              <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" id="tanggal_start" placeholder="Tanggal awal" name="tanggal_start" value="{{ Request::get('tanggal_start') }}" />
                  </div><!-- /.input group -->
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" id="tanggal_end" placeholder="Tanggal akhir" name="tanggal_end" value="{{ Request::get('tanggal_end') }}" />
                  </div><!-- /.input group -->
                </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-magnifier"></i> Cari</button>
            </form>
            </div>
        </div><br>
      </div>
    </div>
  </div> 
  <div class="row">
    <div class="col-md-8">
      <div class="box box-info">
      	<div class="box-header">
      		<h3 class="box-title">Daftar Biaya</h3>
           <a href="{{ route('report.biaya.index') }}" class="btn btn-info btn-sm pull-right">All Data</a>
          <a href="{{ route('report.biaya.index', $data['params'] + ['export' => 'pdf']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-pdf-o"></i> Download PDF</a> &nbsp;
          <a href="{{ route('report.biaya.index', $data['params'] + ['export' => 'excel']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Download Excel</a>
      	</div>
        <div class="box-body">
          <div class="table-responsive">
          	<table class="table table-striped">
          		<thead>
          			<tr class="bg-info">
          				<th class="text-center">No.</th>
          				<th class="text-center">Periode Pelaksanaan</th>
          				<th class="text-center">Komponen</th>
                  <th class="text-center">No. Tagihan</th>
          				<th class="text-center">Total</th>
          			</tr>
          		</thead>
          		<tbody>
              @forelse($data['biaya'] as $biaya)
              <tr>
                <td class="text-center">{{ $data['number']++ }}</td>
                <td class="text-center">{{ $biaya->penjadwalan->tanggal_mulai }} s/d {{ $biaya->penjadwalan->tanggal_selesai }}</td>
                <td class="text-center">{{ $biaya->komponenbiaya->nama_komponenbiaya }}</td>
                <td class="text-center">{{ (! empty($biaya->nosurat_tagihan)) ? $biaya->nosurat_tagihan : '-' }}</td>
                <td>{{ IDRFormatter($biaya->biaya) }}</td>
              </tr>
              @empty
          		<tr>
          			<td colspan="5" class="text-center"> Tidak ada data biaya</td>
          		</tr>
              @endforelse
          		</tbody>
          	</table><br>
            <div class="text-center">
              {!! paginationHelper($data['biaya']->appends(Request::except('page'))->render()) !!}
            </div>
          </div>
        </div>
      </div>   
    </div>
    <div class="col-md-4">
       <!-- Info Boxes Style 2 -->
        <div class="info-box bg-aqua">
          <span class="info-box-icon"><i class="fa fa-money"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Total Biaya</span>
            <span class="info-box-number">{{ IDRFormatter($data['total']) }}</span>
            <div class="progress">
              <div class="progress-bar" style="width: 100%"></div>
            </div>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>
  </div>         
@stop

@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">

<script type="text/javascript">
  $('[id^=tanggal]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  });

</script>
@stop