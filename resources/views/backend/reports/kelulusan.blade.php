@extends('layouts.backend.master')
@section('content')
@if(Session::get('role') != 'employee')
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $data['summaries']['total'] }}</h3>
          <p>Total Peserta Sertifikasi</p>
        </div>
        <div class="icon">
          <i class="fa fa-group"></i>
        </div>
      </div>
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $data['summaries']['attended'] }}</h3>
          <p>Total Kehadiran</p>
        </div>
        <div class="icon">
          <i class="fa fa-check"></i>
        </div>
      </div>
    </div><!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $data['summaries']['passed'] }}</h3>
          <p>Total Lulus</p>
        </div>
        <div class="icon">
          <i class="fa fa-certificate"></i>
        </div>
      </div>
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $data['summaries']['notpassed'] }}</h3>
          <p>Total Tidak Lulus</p>
        </div>
        <div class="icon">
          <i class="fa fa-ban"></i>
        </div>
      </div>
    </div><!-- /.col -->
  </div><!-- /.row -->
@endif
   <div class="row">
    <div class="col-lg-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Filter</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-xs-3">
              <form action="{{ route('report.kelulusan.index') }}" method="get" accept-charset="utf-8" class="form-inline">
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" id="tanggal_realisasi" placeholder="Tanggal realisasi" name="tanggalrealisasi" value="{{ Request::get('tanggalrealisasi') }}" />
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> </button>
                    </span>
                  </div><!-- /.input group -->
                </div>
              </form>
            </div>
            <div class="col-xs-9">
              <form class="form-inline" method="get" action="{{ route('report.kelulusan.index') }}">
              <div class="form-group">
                {!! Form::select('kelulusan', ['' => 'Semua hasil', 'y' => 'Kompeten', 'n' => 'Belum Kompeten'], Request::get('kelulusan'), ['class' => 'form-control'])!!}
              </div>
              <div class="form-group">
                {!! Form::select('kompetensi', ['' => '-- Pilih Kompetensi--'] + $data['kompetensi'], Request::get('kompetensi'), ['class' => 'selectpicker form-control', 'data-search-live' => 'true'])!!}
              </div>
              <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
              
            </form>
            </div>
        </div><br>
      </div>
    </div>
  </div>      

  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
      	<div class="box-header">
      		<h3 class="box-title">Daftar Hasil Kelulusan Peserta</h3>
          <a href="{{ route('report.kelulusan.index') }}" class="btn btn-info btn-sm pull-right">All Data</a>
          <a href="{{ route('report.kelulusan.index', $data['params'] + ['export' => 'pdf']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-pdf-o"></i> Download PDF</a> &nbsp;
          <a href="{{ route('report.kelulusan.index', $data['params'] + ['export' => 'excel']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Download Excel</a>
      	</div>
        <div class="box-body">
          <div class="table-responsive">
              <table class="table no-margin table-condensed table-bordered table-hover">
                      <thead>
                          <tr class="bg-info">
                            <th class="text-center">No.</th>
                            <th class="text-center">NIP</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Tipe Peserta</th>
                            <th class="text-center">Kompetensi</th>
                            <th class="text-center">Kelulusan</th>
                            <th class="text-center">Tanggal Realisasi</th>
                          </tr>
                      </thead>
                      <tbody>
                      @forelse($data['realisasi'] as $realisasi)
                      <tr>
                        <td>{{ $data['pagination_number']++ }}</td>
                        <td>{{ $realisasi->peserta->nip }}</td>
                        <td>{{ $realisasi->peserta->nama }}</td>
                        <td>{{ ($realisasi->peserta->tipe_peserta == 'pln') ? 'PLN' : 'NON PLN' }}</td>
                        <td>{{ $realisasi->kompetensi->nama_kompetensi }}</td>
                        <td class="text-center">{{ ($realisasi->kelulusan == 'y') ? 'Kompeten' : 'Belum Kompeten' }}</td>
                        <td>{{ $realisasi->created_at }}</td>
                      </tr>
                      @empty
                      <tr>
                        <td colspan="7" class="text-center">Tidak ada data</td>
                      </tr>
                      @endforelse
                      </tbody>
              </table><br>
              <div class="text-center">
                {!! paginationHelper($data['realisasi']->appends($data['params'])->render()) !!}
              </div>
           </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
  $('[id^=tanggal]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  });
 
</script>
@stop