@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $data['summaries']['total'] }}</h3>
          <p>Total Asesor</p>
        </div>
        <div class="icon">
          <i class="fa fa-group"></i>
        </div>
      </div>
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $data['summaries']['pln'] }}</h3>
          <p>Total Asesor PLN</p>
        </div>
        <div class="icon">
          <i class="fa fa-check"></i>
        </div>
      </div>
    </div><!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $data['summaries']['nonpln'] }}</h3>
          <p>Total Asesor Non PLN</p>
        </div>
        <div class="icon">
          <i class="fa fa-external-link"></i>
        </div>
      </div>
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $data['summaries']['sertifikat'] }}</h3>
          <p>Total Sertifikat</p>
        </div>
        <div class="icon">
          <i class="fa fa-certificate"></i>
        </div>
      </div>
    </div><!-- /.col -->
  </div><!-- /.row -->

   <div class="row">
    <div class="col-lg-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">Filter</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="col-xs-6">
              <form class="form-inline" method="get" action="{{ route('report.asesor.index') }}">
              <div class="form-group">
                {!! Form::select('group', ['' => 'All Group', 'pln' => 'PLN', 'nonpln' => 'NON PLN'], Request::get('group'), ['class' => 'form-control'])!!}
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="units" placeholder="Unit induk / non PLN" value="{{ Request::get('units') }}">
              </div>
             
              <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> </button>
            </form>
            </div>
            <div class="col-xs-5">
              <form action="{{ route('report.asesor.index') }}" method="get" class="form-horizontal">
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-user"></i>
                    </div>
                    <input type="text" class="form-control" name="nipname" placeholder="NIP / Nama Asesor" value="{{ Request::get('nipname') }}" required/>
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> </button>
                    </span>
                  </div><!-- /.input group -->
                </div>
              </form>
            </div>
        </div><br>
      </div>
    </div>
  </div>      

  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
      	<div class="box-header">
      		<h3 class="box-title">Daftar Asesor</h3>
          <a href="{{ route('report.asesor.index') }}" class="btn btn-info btn-sm pull-right">All Data</a>
          <a href="{{ route('report.asesor.index', $data['params'] + ['export' => 'pdf']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-pdf-o"></i> Download PDF</a> &nbsp;
          <a href="{{ route('report.asesor.index', $data['params'] + ['export' => 'excel']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Download Excel</a>
      	</div>
        <div class="box-body">
          <div class="table-responsive">
              <table class="table no-margin table-condensed table-bordered table-hover table-striped">
                <thead>
                      <tr class="bg-info">
                        <th class="text-center">No.</th>
                        <th class="text-center">NIP</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Induk / Institusi</th>
                        <th class="text-center">Non PLN</th>
                        <th class="text-center">Lembaga Sertifikasi</th>
                        <th class="text-center">Subbidang Kompetensi</th>
                        <th class="text-center">Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                  @forelse($data['asesor'] as $asesor)
                    <tr>
                      <td>{{ $data['pagination_number']++ }}</td>
                      <td>{{ $asesor->nip }}</td>
                      <td>{{ $asesor->nama }}</td>
                      <td>{{ (! is_null($asesor->unitinduk)) ? $asesor->unitinduk->nama_unitinduk : '-' }}</td>
                      <td>{{ (! is_null($asesor->unitnonpln)) ? $asesor->unitnonpln->nama_unitnonpln : '-' }}</td>
                      <td>
                        @forelse($asesor->kelompok as $kelompok)
                          - {{ $kelompok->nama_lembagasertifikasi }}<br>
                        @empty
                          -
                        @endforelse
                      </td>
                      <td>
                        @forelse($asesor->subbidang as $subbidang)
                          - {{ $subbidang->nama_subbidang }}<br>
                        @empty
                          -
                        @endforelse
                      </td>
                      <td class="text-center">
                          <a href="{{ route('report.asesor.detail', ['id' => $asesor->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>
                        </td>
                    </tr>
                  @empty
                    <tr>
                      <td colspan="11">Tidak ada data</td>
                    </tr>
                  @endforelse
                  </tbody>
              </table><br>
              <div class="text-center">
                {!! paginationHelper($data['asesor']->appends($data['params'])->render()) !!}
              </div>
           </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
  $('[id^=tanggal]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  });
 
</script>
@stop