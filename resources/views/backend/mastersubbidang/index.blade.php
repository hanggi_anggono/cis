@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
			@if(Auth::user()->can('can_write_masterbidang'))
		      <a href="{{ route('dashboard.master.subbidang.bidang.create',['bidang_id'=>$data['bidang_id']]) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Sub Bidang</a>&nbsp;
		    @endif
		     <a href="{{ route('dashboard.master.bidang.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th class="col-md-1">No.</th>
				          	<th>Nama Sub Bidang</th>
				          	<th class="col-md-3 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @foreach($data['subbidang'] as $subbidang)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $subbidang->nama_subbidang }} {!! (! is_null($subbidang->nama_subbidang_english)) ? '(<em>'.$subbidang->nama_subbidang_english.'</em>)' : '' !!}</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterbidang'))
				          		<a href="{{ route('dashboard.master.subbidang.edit', ['id' => $subbidang->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.subbidang.delete', ['id' => $subbidang->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>    
			          @endforeach
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['subbidang']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop