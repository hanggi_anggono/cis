@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Edit Bidang</h3>
		    </div><!-- /.box-header -->
		    <form class="form-horizontal" action="{{ route('dashboard.master.bidang.update',['id'=>$data['bidang']->id]) }}" method="post">
			    <div class="box-body">
				   @if (Session::has('flash_notification.message'))
					    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
					        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

					        {{ Session::get('flash_notification.message') }}
					    </div>
					@endif
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<input type="hidden" name="_method" value="put" >
		    		<div class="form-group">
	                  <label for="nama" class="col-sm-3 control-label">Nama Bidang</label>
	                  <div class="col-sm-7">
	                      <input type="text" class="form-control" placeholder="Nama bidang" name="nama_bidang" value="{{ $data['bidang']->nama_bidang }}" required >
	                  </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Nama Bidang (English)</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama bidang (english)" name="nama_bidang_english" value="{{ $data['bidang']->nama_bidang_english }}">
                      </div>
		    		</div>
			    </div>
			    <div class="box-footer">
			    	<div class="col-sm-4 col-md-offset-3">
	    				<button type="submit" class="btn btn-primary">Update</button>&nbsp;
			            <a href="{{ route('dashboard.master.bidang.index') }}" class="btn btn-warning">Kembali</a>
	    			</div>
			    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    