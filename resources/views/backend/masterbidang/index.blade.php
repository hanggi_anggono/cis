@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-10">
		<div class="box box-primary">
			<div class="box-header with-border">
			@if(Auth::user()->can('can_write_masterbidang'))
		      <a href="{{ route('dashboard.master.bidang.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Bidang</a>&nbsp; 
		      <a href="{{ route('dashboard.master.subbidang.create') }}" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Tambah Sub Bidang</a>
		    @endif
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th>No.</th>
				          	<th>Nama Bidang</th>
				          	<th>Jumlah Sub Bidang</th>
				          	<th class="col-md-3 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @foreach($data['bidang'] as $bidang)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $bidang->nama_bidang }} {!! (! is_null($bidang->nama_bidang_english)) ? '(<em>'.$bidang->nama_bidang_english.'</em>)' : '' !!}</td>
				          	<td><a href="{{ route('dashboard.master.subbidang.bidang.index', ['bidang_id' => $bidang->id]) }}">{{ $bidang->subbidang()->count() }} Sub bidang</a></td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterbidang'))
				          		<a href="{{ route('dashboard.master.bidang.edit', ['id' => $bidang->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.bidang.delete', ['id' => $bidang->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>    
			          @endforeach
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['bidang']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop