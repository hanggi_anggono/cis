@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Edit Sertifikat Asesor</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	<form class="form-horizontal" action="{{ route('dashboard.master.sertifikatasesor.update', ['id' => $data['sertifikat']->id , 'asesor' => $data['sertifikat']->asesor_id]) }}" method="post" accept-charset="utf-8">
      				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			        <input type="hidden" name="_method" value="put">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Asesor</label>
                      <div class="col-sm-7">
                            <input type="text" class="form-control" value="{{ $data['asesor_single']->nama }}" readonly="readonly">
                      		<input type="hidden" class="form-control" name="asesor_id" value="{{ $data['asesor_single']->id }}">
                      </div>
		    		</div>
			        <div class="form-group">
			          <label for="no_registrasi" class="col-sm-3 control-label">No. Registrasi</label>
			          <div class="col-sm-7">
			              <input type="text" class="form-control" placeholder="No. registrasi sertifikat asesor" name="no_registrasi" value="{{ $data['sertifikat']->no_registrasi }}" required>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="no_sertifikat" class="col-sm-3 control-label">No. Sertifikat</label>
			          <div class="col-sm-7">
			              <input type="text" class="form-control" placeholder="No. sertifikat asesor" name="no_sertifikat" value="{{ $data['sertifikat']->no_sertifikat }}" required>
			          </div>
			        </div>			
			        <div class="form-group">
			          <label for="tanggal_terbit" class="col-sm-3 control-label">Tanggal Terbit</label>
			          <div class="col-sm-7">
			              <input type="text" class="form-control" name="tanggal_terbit" id="tanggal_terbit" placeholder="Tanggal terbit sertifikat" value="{{ $data['sertifikat']->tanggal_terbit }}">
			          </div>
			        </div>	  
			        <div class="form-group">
			          <label for="masa_berlaku" class="col-sm-3 control-label">Masa Berlaku</label>
			          <div class="col-sm-7">
			              <input type="text" class="form-control" name="masa_berlaku" id="masa_berlaku" placeholder="Masa berlaku sertifikat" value="{{ $data['sertifikat']->masa_berlaku }}">
			          </div>
			        </div>			              	        		    		
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Akreditor</label>
                      <div class="col-sm-7">
							{!! Form::select('lembaga_id', $data['lembaga'], $data['sertifikat']->lembaga_id, ['class' => 'selectpicker form-control']) !!}
                      </div>
		    		</div>		    		
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		            <a href="{{ route('dashboard.master.asesor.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
	$('#tanggal_terbit').datepicker({
		format: 'yyyy-mm-dd'
	});
	$('#masa_berlaku').datepicker({
		format: 'yyyy-mm-dd'
	});
</script>
@stop  		    