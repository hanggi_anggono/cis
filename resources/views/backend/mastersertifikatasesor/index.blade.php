@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.sertifikatasesor.asesor.create',['asesor_id'=>$data['asesor_id']]) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Sertifikat Asesor</a>
		      <a href="{{ route('dashboard.master.asesor.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
		      <span class="pull-right"><strong>Asesor: {{ $data['asesor']->nama }}</strong></span>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th>No.</th>
				          	<th class="col-md-2">No. Registrasi</th>
				          	<th class="col-md-2">No. Sertifikat</th>
				          	<th class="col-md-2">Tanggal Terbit</th>
				          	<th class="col-md-2">Masa Berlaku</th>
				          	<th class="col-md-2 text-center">Akreditor</th>
				          	<th class="col-md-4 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @forelse($data['sertifikat'] as $sertifikat)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $sertifikat->no_registrasi }}</td>
				          	<td>{{ $sertifikat->no_sertifikat }}</td>
				          	<td>{{ $sertifikat->tanggal_terbit }}</td>
				          	<td>{{ $sertifikat->masa_berlaku }}</td>
				          	<td class="col-md-2 text-center">{{ $sertifikat->lembaga->nama_lembaga }}</td>
				          	<td class="text-center">
				          		<a href="{{ route('dashboard.master.sertifikatasesor.edit', ['id' => $sertifikat->id]) }}" class="btn btn-sm btn-warning" ><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.sertifikatasesor.delete', ['id' => $sertifikat->id, 'asesor' => $data['asesor']->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	    </td>
				          </tr>
				      <?php $data['pagination_number']++; ?>   
				      @empty
				      <tr>
				      	<td colspan="7" class="text-center">Tidak ada data</td>
				      </tr> 
			          @endforelse
			          </tbody>
			        </table>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop