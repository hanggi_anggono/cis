@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Edit Grade</h3>
		    </div><!-- /.box-header -->
		    <form class="form-horizntal" action="{{ route('dashboard.master.grade.update',['id'=>$data['grade']->id]) }}" method="post">
			    <div class="box-body">
				    @if($errors->any())
			    		<div class="alert alert-danger alert-dismissable">
			    			<ul>
				    		<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
				    		@foreach($errors->all() as $error)
				    			<li>{{ $error }}</li>
				    		@endforeach
				    		</ul>
				    	</div>
			    	@endif
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<input type="hidden" name="_method" value="put" >
		    		<div class="form-group">
	                  <label for="nama" class="col-sm-3 control-label">Nama Grade</label>
	                  <div class="col-sm-7">
	                      <input type="text" class="form-control" placeholder="Nama grade" name="nama_grade" value="{{ $data['grade']->nama_grade }}" required >
	                  </div>
		    		</div>
			    </div>
			    <div class="box-footer">
			    	<div class="col-sm-4 col-md-offset-3">
	    				<button type="submit" class="btn btn-primary">Update</button>&nbsp;
			            <a href="{{ route('dashboard.master.grade.index') }}" class="btn btn-warning">Kembali</a>
	    			</div>
			    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    