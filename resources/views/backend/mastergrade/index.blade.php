@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_mastergrade'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.grade.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Grade</a>
		    </div><!-- /.box-header -->
		@endif
		    <div class="box-body">
		    @if(Session::has('success-notif'))
		    	<div class="alert alert-success alert-dismissable">
		    		<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
		    		<h4><i class="icon fa fa-check"></i>Sukses</h4>
		    		{{ Session::get('success-notif') }}
		    	</div>
		    @endif
		    @if(Session::has('fail-notif'))	
		    	<div class="alert alert-danger alert-dismissable">
		    		<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
		    		<h4><i class="icon fa fa-check"></i>Gagal</h4>
		    		{{ Session::get('fail-notif') }}
		    	</div>
		    @endif	
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th class="col-md-1">No.</th>
				          	<th>Nama Grade</th>
				          	<th class="col-md-3 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @foreach($data['grade'] as $grade)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $grade->nama_grade }}</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_mastergrade'))
				          		<a href="{{ route('dashboard.master.grade.edit', ['id' => $grade->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.grade.delete', ['id' => $grade->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>    
			          @endforeach
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['grade']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop