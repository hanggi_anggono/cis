@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Manajemen Password</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if($errors->any())
		    		<div class="alert alert-danger alert-dismissable">
		    			<ul>
			    		<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			    		@foreach($errors->all() as $error)
			    			<li>{{ $error }}</li>
			    		@endforeach
			    		</ul>
			    	</div>
		    	@endif
		    	@if (Session::has('flash_notification.message'))
		          <div class="alert alert-{{ Session::get('flash_notification.level') }}">
		              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		              {{ Session::get('flash_notification.message') }}
		          </div>
		        @endif
		    	<form class="form-horizontal" action="{{ route('dashboard.password.update') }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<input type="hidden" name="_method" value="put">
		    		<input type="hidden" name="user_id" value="{{ Auth::id() }}">
		    		<div class="form-group">
                      <label for="password" class="col-sm-3 control-label">Password saat ini</label>
                      <div class="col-sm-7">
                          <input type="password" class="form-control" placeholder="Password saat ini" name="current_password" required>
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="password" class="col-sm-3 control-label">Password Baru</label>
                      <div class="col-sm-7">
                          <input type="password" class="form-control" placeholder="Password baru" name="password" required>
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="password" class="col-sm-3 control-label">Konfirmasi Password</label>
                      <div class="col-sm-7">
                          <input type="password" class="form-control" placeholder="Konfirmasi password baru" name="password_confirmation" required>
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		            <a href="{{ route('dashboard.index') }}" class="btn btn-warning">Home</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    