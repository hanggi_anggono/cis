@extends('layouts.backend.master')
@section('content')
<div class="row">
<div class="col-md-8">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Sertifikat Peserta</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
    @if (Session::has('flash_notification.message'))
      <div class="alert alert-{{ Session::get('flash_notification.level') }}">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          {{ Session::get('flash_notification.message') }}
      </div>
    @endif
    <form class="form-horizontal" action="{{ route('dashboard.sertifikat.update', ['sertifikat_id' => $data['sertifikat']->id ]) }}" method="post" accept-charset="utf-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="tanggal_mulai_berlaku" value="{{ date('Y-m-d') }}">
      <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Nomor Sertifikat</label>
          <div class="col-sm-6">
              <input type="text" class="form-control" id="no_sertifikat" name="no_sertifikat" value="{{ (! empty($data['sertifikat']->no_sertifikat)) ? $data['sertifikat']->no_sertifikat : '' }}" placeholder="Nomor sertifikat peserta" required>
          </div>
        </div>
      <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">NIP</label>
          <div class="col-sm-6">
              <input type="text" class="form-control" id="nip" value="{{ $data['sertifikat']->nip }}" readonly="readonly">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Nama</label>
          <div class="col-sm-6">
              <input type="text" class="form-control" id="nip" value="{{ $data['sertifikat']->nama }}" readonly="readonly">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tanggal Uji</label>
          <div class="col-sm-6">
              <input type="text" class="form-control" id="nip" value="{{ $data['sertifikat']->tanggal_mulai }} s/d {{ $data['sertifikat']->tanggal_selesai }}" readonly="readonly">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">LSK</label>
          <div class="col-sm-6">
              <input type="text" class="form-control" id="nip" value="{{ $data['sertifikat']->nama_lembagasertifikasi }}" readonly="readonly">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">TUK</label>
          <div class="col-sm-6">
              <input type="text" class="form-control" id="nip" value="{{ $data['sertifikat']->nama_udiklat }}" readonly="readonly">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Kompetensi</label>
          <div class="col-sm-6">
              <input type="text" class="form-control" id="nip" value="{{ $data['sertifikat']->nama_kompetensi }}" readonly="readonly">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Masa Berlaku</label>
          <div class="col-sm-6">
              <input type="text" class="form-control" id="nip" value="{{ $data['sertifikat']->tanggal_mulai_berlaku }} s/d {{ $data['sertifikat']->tanggal_akhir_berlaku }}" readonly="readonly">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tanggal Akhir</label>
          <div class="col-sm-6">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="tanggal_akhir_berlaku" id="tanggal_akhir_berlaku" class="form-control pull-right" placeholder="Tanggal berakhir sertifikat" required/>
              </div><!-- /.input group -->
          </div>
        </div>
    </div>
    <div class="box-footer">
    	<div class="col-md-offset-2">
    		<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		    <button class="btn btn-warning" onclick="window.history.back();return false;">Kembali</button>&nbsp;
    	</div>
    </div> 
    </form> 
  </div><!-- /.box -->
</div>
</div><!-- /.row -->
@stop
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
	$('#tanggal_akhir_berlaku').datepicker({
		format: 'yyyy-mm-dd',
    autoclose: true
	});
</script>
@stop