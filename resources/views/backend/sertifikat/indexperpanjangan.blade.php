@extends('layouts.backend.master')
@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Pencarian Lanjut</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="col-sm-6">
          <form class="form-inline" method="get" action="{{ route('dashboard.sertifikat.perpanjangan.index') }}">
              <div class="form-group">
                <input type="text" class="form-control" name="nip" value="{{ Request::get('nip') }}" placeholder="NIP / Nama Peserta">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="no_sertifikat" value="{{ Request::get('no_sertifikat') }}" placeholder="No. Sertifikat">
              </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-magnifier"></i> Cari</button>
          </form>
        </div>
        <div class="col-sm-3">
          <form class="form-inline" method="get" action="{{ route('dashboard.sertifikat.perpanjangan.index') }}">
              <div class="form-group">
                {!! Form::select('expired', ['' => 'Expired in', '1m' => '< 1 Bulan', '3m' => '< 3 Bulan', '6m' => '< 6 Bulan', '1y' => '< 1 Tahun'], Request::get('expired'), ['class' => 'form-control']) !!}
              </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-magnifier"></i> Cari</button>
          </form>
        </div>
        {{-- <div class="col-sm-3">
          <form class="form-inline" method="get" action="{{ route('dashboard.sertifikat.perpanjangan.index') }}">
               <div class="form-group">
                  <input type="text" class="form-control" name="pelaksanaan" value="{{ Request::get('pelaksanaan') }}" placeholder="No. Pelaksanaan">
                </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-magnifier"></i> Cari</button>
          </form>
        </div> --}}
       
      </div>
    </div>
  </div>
</div>     

<div class="row">
    <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Sertifikat</h3>
          <a href="{{ route('dashboard.sertifikat.perpanjangan.index') }}" class="btn btn-sm btn-info pull-right">All Data</a>
        </div><!-- /.box-header -->
        <div class="box-body">
          @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
          @endif
          <table class="table table-condensed table-bordered table-hover">
            <thead>
                <tr class="bg-info">
                  <th class="text-center">No.</th>
                  <th class="text-center">NIP</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">TUK</th>
                  <th class="text-center">LSK</th>
                  <th class="text-center">Kompetensi</th>
                  <th class="text-center">No. Sertifikat</th>
                  <th class="text-center">Masa Berlaku</th>
                  <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
              @forelse($data['sertifikat'] as $sertifikat)
              <tr>
                <td>{{ $data['pagination_number']++ }}</td>
                <td>{{ $sertifikat->nip }}</td>
                <td>{{ $sertifikat->nama }}</td>
                <td>{{ $sertifikat->nama_udiklat }}</td>
                <td>{{ $sertifikat->nama_lembagasertifikasi }}</td>
                <td>{{ $sertifikat->nama_kompetensi }}</td>
                <td>{{ (! empty($sertifikat->no_sertifikat)) ? $sertifikat->no_sertifikat : '-' }}</td>
                <td>{{ (! empty($sertifikat->tanggal_mulai_berlaku)) ? $sertifikat->tanggal_mulai_berlaku.' s/d '.$sertifikat->tanggal_akhir_berlaku : '-' }}</td>
                <td class="text-center">
                  @if($data['tipeSertifikat'] == 'baru')
                    @if(Auth::user()->can('can_write_sertifikatbaru'))
                    
                    @endif
                  @else
                    @if(Auth::user()->can('can_write_sertifikatperpanjangan'))
                    <a href="{{ route('dashboard.sertifikat.perpanjangan.proses', ['sertifikat_id' => $sertifikat->id]) }}" class="btn btn-xs btn-primary">Perpanjangan</a>
                    @endif
                  @endif
                  <a href="{{ route('dashboard.sertifikat.download.single', ['sertifikat_id' => $sertifikat->id]) }}" class="btn btn-xs btn-success"><i class="fa fa-download"></i></a>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="10" class="text-center">Tidak ada data sertifikat</td>
              </tr>
              @endforelse
            </tbody>
          </table><br>
          <div class="text-center">
              {!! paginationHelper($data['sertifikat']->render()) !!}
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop