@extends('layouts.backend.master')
@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="box box-primary">
      <div class="box-body">
          <div class="table-responsive">
            <table class="table table-striped text-center">
              <tr>
                <td>Tanggal Pelaksanaan</td>
                <td>{{ $data['penjadwalan']->tanggal_mulai }} s/d {{ $data['penjadwalan']->tanggal_selesai }}</td>
              </tr>
              <tr>
                <td>TUK</td>
                <td>{{ $data['penjadwalan']->udiklat->nama_udiklat }}</td>
              </tr>
              <tr>
                <td>Unit Induk</td>
                <td>{{ $data['penjadwalan']->unitinduk->nama_unitinduk }}</td>
              </tr>
              <tr>
                <td>LSK</td>
                <td>{{ $data['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}</td>
              </tr>
              <tr>
                <td>Jumlah Sertifikat</td>
                <td>{{ $data['sertifikat']->count() }} Sertifikat</td>
              </tr>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>    
<div class="row">
  <div class="col-sm-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Pencarian Lanjut</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="col-sm-8">
          <form class="form-inline" method="get" action="{{ route('dashboard.sertifikat.baru.bypenjadwalan.index', ['penjadwalanId' => $data['penjadwalan']->id]) }}">
              <div class="form-group">
                <input type="text" class="form-control" name="nip" value="{{ Request::get('nip') }}" placeholder="NIP / Nama Peserta">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="no_sertifikat" value="{{ Request::get('no_sertifikat') }}" placeholder="No. Sertifikat">
              </div>
              <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> </button>
          </form>
        </div>
        <div class="col-md-4">
          <a href="{{ route('dashboard.sertifikat.download.all', ['penjadwalanId' => $data['penjadwalan']->id]) }}" class="btn btn-sm btn-success pull-right {{ ($data['sertifikat']->count() == 0) ? 'disabled' : '' }}"><i class="fa fa-download"></i>Download All</a>
        </div>
      </div>
    </div>
  </div>
</div>     

<div class="row">
    <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Sertifikat</h3>
          <a href="{{ route('dashboard.sertifikat.baru.bypenjadwalan.index', ['penjadwalanId' => $data['penjadwalan']->id]) }}" class="btn btn-sm btn-info pull-right">All Data</a>
        </div><!-- /.box-header -->
        <div class="box-body">
          @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
          @endif
          <table class="table table-condensed table-bordered table-striped table-hover">
            <thead>
                <tr class="bg-info">
                  <th class="text-center">No.</th>
                  <th class="text-center">No. Sertifikat</th>
                  <th class="text-center">NIP</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Kompetensi</th>
                  <th class="text-center">Masa Berlaku</th>
                  <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
              @forelse($data['sertifikat'] as $sertifikat)
              <tr>
                <td>{{ $data['pagination_number']++ }}</td>
                 <td>{{ (! empty($sertifikat->no_sertifikat)) ? $sertifikat->no_sertifikat : '-' }}</td>
                <td>{{ $sertifikat->nip }}</td>
                <td>{{ $sertifikat->nama }}</td>
                <td>{{ $sertifikat->nama_kompetensi }}</td>
                <td>{{ (! empty($sertifikat->tanggal_mulai_berlaku)) ? $sertifikat->tanggal_mulai_berlaku.' s/d '.$sertifikat->tanggal_akhir_berlaku : '-' }}</td>
                <td class="text-center">
                  <a href="{{ route('dashboard.sertifikat.download.single', ['sertifikat_id' => $sertifikat->id]) }}" class="btn btn-xs btn-success"><i class="fa fa-download"></i></a>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="10" class="text-center">Tidak ada data sertifikat</td>
              </tr>
              @endforelse
            </tbody>
          </table><br>
          <div class="text-center">
              {!! paginationHelper($data['sertifikat']->render()) !!}
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop
