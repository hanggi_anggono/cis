@extends('layouts.backend.master')
@section('content')
<div class="row">
<div class="col-md-12">
  <div class="box">
    <div class="box-header">
      
    </div><!-- /.box-header -->
    <div class="box-body">
      @if (Session::has('flash_notification.message'))
          <div class="alert alert-{{ Session::get('flash_notification.level') }}">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              {{ Session::get('flash_notification.message') }}
          </div>
      @endif
      <table class="table table-condensed table-bordered table-hover table-striped" style="font-size:12px;">
        <thead>
            <tr class="bg-info">
              <th class="text-center">No.</th>
              <th class="text-center">Tanggal Pelaksanaan</th>
              <th class="text-center">Unit Induk</th>
               <th class="text-center">LSK</th>
              <th class="text-center">Kompetensi</th>
              <th class="text-center">Sertifikat</th>
            </tr>
        </thead>
        <tbody>
        @forelse($data['penjadwalan'] as $penjadwalan)
          <tr>
            <td>{{ $data['pagination_number']++ }}</td>
            <td>{{ $penjadwalan->tanggal_mulai }} s/d {{ $penjadwalan->tanggal_selesai }}</td>
            <td>{{ $penjadwalan->unitinduk->nama_unitinduk }}</td>
            <td>{{ $penjadwalan->lembagasertifikasi->nama_lembagasertifikasi }}</td>
            <td style="white-space:nowrap;">
              @forelse($penjadwalan->kompetensi as $kompetensi)
                - {{ $kompetensi->nama_kompetensi }}<br>
              @empty
                -
              @endforelse
            </td>
            <td class="text-center">
              <a href="{{ route('dashboard.sertifikat.baru.bypenjadwalan.index', ['penjadwalanId' => $penjadwalan->id ]) }}" class="btn btn-xs btn-info"><i class="fa fa-certificate"></i> </a>
            </td>
          </tr>
        @empty
        <tr>
          <td colspan="11" class="text-center">Tidak ada data penjadwalan</td>
        </tr>
        @endforelse  
        </tbody>
      </table><br>
      <div class="text-center">
         {!! paginationHelper($data['penjadwalan']->appends(Request::except('_token'))->render()) !!}
      </div>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div>
</div><!-- /.row -->
@stop