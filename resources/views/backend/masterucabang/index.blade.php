@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-10">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_masterunit'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.unit.cabang.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Unit Cabang</a>&nbsp;
		    </div><!-- /.box-header -->
		@endif
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th>No.</th>
				          	<th class="col-md-4">Nama Unit Induk</th>
				          	<th class="col-md-4">Nama Unit Cabang</th>
				          	<th class="col-md-2">Jumlah Unit Ranting</th>
				          	<th class="col-md-2 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @forelse($data['unit'] as $ucabang)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $ucabang->unitinduk->nama_unitinduk }}</td>
				          	<td>{{ $ucabang->nama_unitcabang }}</td>
				          	<td class="text-center">{{ $ucabang->unitranting->count() }} Unit</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterunit'))
				          		<a href="{{ route('dashboard.master.unit.cabang.edit', ['id'=>$ucabang->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.unit.cabang.delete', ['id'=>$ucabang->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>    
			          @empty
			          	<tr>
			          		<td colspan="4" class="text-center">Tidak ada data</td>
			          	</tr>
			          @endforelse
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['unit']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop