@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_masterpeserta'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.peserta.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah peserta</a>
		      <form class="form-inline pull-right" method="get" action="{{ route('dashboard.master.peserta.index') }}">
		      	<div class="form-group">
		      		<input type="text" class="form-control" name="nipname" value="{{ Request::get('nipname') }}">
		      		<button type="input" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
		      	</div>
		      </form>
		    </div><!-- /.box-header -->
		@endif    
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover table-striped">
			        	<thead>
	                        <tr class="bg-info">
	                          <th class="text-center">No.</th>
	                          <th class="col-md-2 text-center">NIP</th>
	                          <th class="col-md-2 text-center">Nama</th>
	                          <th class="col-md-1 text-center">Tipe</th>
	                          <th class="col-md-2 text-center">Induk / Institusi</th>
	                          <th class="col-md-1 text-center">Cabang</th>
	                          <th class="col-md-1 text-center">Email</th>
	                          <th class="col-md-1 text-center">Telp</th>
	                          <th class="col-md-2 text-center">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    @forelse($data['peserta'] as $peserta)
	                    	<tr>
	                    		<td>{{ $data['pagination_number'] }}</td>
	                    		<td>{{ $peserta->nip }}</td>
	                    		<td>{{ $peserta->nama }}</td>
	                    		<td>{{ $peserta->tipe_peserta }}</td>
	                    		<td>{{ (! is_null($peserta->unitinduk)) ? $peserta->unitinduk->nama_unitinduk : '-'}}</td>
	                    		<td>{{ (! is_null($peserta->unitcabang)) ? $peserta->unitcabang->nama_unitcabang : '-' }}</td>
	                    		<td>{{ $peserta->email }}</td>
	                    		<td>{{ $peserta->no_hp }}</td>
	                    		<td class="text-center">
		                          <a href="{{ route('dashboard.master.peserta.show', ['id' => $peserta->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>
		                        @if(Auth::user()->can('can_write_masterpeserta'))  
		                          <a href="{{ route('dashboard.master.peserta.edit', ['id' => $peserta->id]) }}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> </a>
		                          <a href="{{ route('dashboard.master.peserta.delete', ['id' => $peserta->id]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> </a>
		                        @endif  
		                        </td>
	                    	</tr>
	                    	<?php $data['pagination_number']++; ?>
	                    @empty
	                    	<tr>
	                    		<td colspan="9">Tidak ada data</td>
	                    	</tr>
	                    @endforelse
	                    </tbody>
			        </table><br>
                  	<div class="text-center">
                  		{!! paginationHelper($data['peserta']->render()) !!}
                  	</div>
			     </div>
			</div>
		</div>
	</div>
</div>			        
@stop
