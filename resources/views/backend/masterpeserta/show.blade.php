@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
	<div class="col-md-7">
		 <div class="box box-primary">
		 	<form class="form-horizontal">
		      <div class="box-body">
		      	<div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Tipe Peserta</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ($data['peserta']->tipe_peserta == 'pln') ? 'PLN' : 'NON PLN' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">NIP</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ $data['peserta']->nip }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">No Identitas</label>
		          <div class="col-sm-7">
		             <p class="form-control-static">{{ $data['peserta']->no_identitas }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Nama</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ $data['peserta']->nama }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Jenis Kelamin</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">
		               	@if($data['peserta']->jenis_kelamin == 'm')
		               		Laki - laki
		               	@else
		               		Perempuan
		               	@endif
		               </p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Gol Darah</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['peserta']->golongan_darah) ? $data['peserta']->golongan_darah : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">TTL</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['peserta']->tempat_lahir) ? $data['peserta']->tempat_lahir : '-' }}, {{ parseDate($data['peserta']->tanggal_lahir) }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Alamat sesuai identitas</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['peserta']->alamat) ? $data['peserta']->alamat : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Kewarganegaraan</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['peserta']->kewarganegaraan) ? $data['peserta']->kewarganegaraan : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="email" class="col-sm-4 control-label">Email</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['peserta']->email) ? $data['peserta']->email : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">No. Handphone</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['peserta']->no_hp) ? $data['peserta']->no_hp : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Telepon Rumah</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['peserta']->no_telp) ? $data['peserta']->no_telp : '-' }}</p>
		          </div>
		        </div>
		     </div>   
		    </form>    
		 </div>
	</div>
	 <div class="col-md-5">
	 	<div class="box box-primary">
	 		<div class="box-body">
	 			<form class="form-horizontal">
	 				<div class="text-center">
	 					@if(! empty($data['peserta']->photo))
	 					<img src="{{ asset('resources/pesertaphotos/'.$data['peserta']->photo) }}" width="100" height="100">
	 					@else
	 						<img src="{{ asset('assets/dist/img/nophoto.jpg') }}" width="100" height="100">
	 					@endif
	 				</div><br>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Pendidikan</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['peserta']->pendidikan->nama_pendidikan }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Jurusan</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['peserta']->jurusan }}</p>
			          </div>
			        </div>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Jenjang Jabatan</label>
			          <div class="col-sm-5">
			              <p class="form-control-static">{{ $data['peserta']->jenjangjabatan->nama_jenjangjabatan }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Jabatan</label>
			          <div class="col-sm-7">
			               <p class="form-control-static">{{ ! empty($data['peserta']->jabatan) ? $data['peserta']->jabatan : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Grade</label>
			          <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['peserta']->grade->nama_grade }}</p>
			          </div>
			        </div>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Induk</label>
			           <div class="col-sm-5">
			              <p class="form-control-static">{{ (! is_null($data['peserta']->unitinduk)) ? $data['peserta']->unitinduk->nama_unitinduk : '-'}}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Cabang</label>
			          <div class="col-sm-5">
			              <p class="form-control-static">{{ (! is_null($data['peserta']->unitcabang)) ? $data['peserta']->unitcabang->nama_unitcabang : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Ranting</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ (! is_null($data['peserta']->unitranting)) ? $data['peserta']->unitranting->nama_unitranting : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Non PLN</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ (! is_null($data['peserta']->unitnonpln)) ? $data['peserta']->unitnonpln->nama_unitnonpln : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Dibuat pada</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ parseDate($data['peserta']->created_at, 'datetime') }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Dirubah pada</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ parseDate($data['peserta']->updated_at, 'datetime') }}</p>
			          </div>
			        </div>
			 	</form>
			 	<div class="box-footer text-center">
			 	@if(Auth::user()->can('can_write_masterpeserta'))
			 		<a href="{{ route('dashboard.master.peserta.edit',['id' => $data['peserta']->id ]) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"> </i> Edit</a>&nbsp;
			 		<a href="{{ route('dashboard.master.peserta.delete',['id' => $data['peserta']->id ]) }}" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"> </i> Delete</a>&nbsp;
			 	@endif
			 		<a href="{{ route('dashboard.master.peserta.index') }}" class="btn btn-info btn-sm"><i class="fa fa-reply"> </i> Kembali</a>&nbsp;
			 	</div>
	 		</div>
	 	</div>
	 </div>
</div>
@stop