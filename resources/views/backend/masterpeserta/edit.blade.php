@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
<div class="col-xs-8">
  <div class="box box-primary">
    @if($errors->any())
      <div class="alert alert-danger alert-dismissable">
        <ul>
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
        </ul>
      </div>
    @endif
    <form class="form-horizontal" action="{{ route('dashboard.master.peserta.update', ['id'=>$data['peserta']->id]) }}" method="post" accept-charset="utf-8" id="peserta" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="put">
      <div class="box-body">
         <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tipe</label>
          <div class="col-sm-3">
              {!! Form::select('tipe_peserta', ['pln'=>'PLN','nonpln'=>'NON PLN'], $data['peserta']->tipe_peserta, ['class'=>'form-control', 'id' => 'tipe_peserta', 'autocomplete' => 'off']) !!}
          </div>
        </div>
        <div class="form-group" id="info-nonpln" style="{{ ($data['peserta']->tipe_peserta == 'pln') ? 'display:none;' : ''}}">
          <label for="nip" class="col-sm-3 control-label">Keterangan Non PLN</label>
            <div class="col-sm-5">
              {!! Form::select('unitnonpln_id', $data['support']['nonpln'], $data['peserta']->unitnonpln_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitnonpln_id', 'autocomplete' => 'off'])!!}
            </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">NIP</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="NIP peserta" name="nip" value="{{ $data['peserta']->nip }}" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">No Identitas</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="No. KTP / SIM / passport" name="no_identitas" value="{{ $data['peserta']->no_identitas }}" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Nama</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Nama lengkap peserta" name="nama" value="{{ $data['peserta']->nama }}" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Gol Darah</label>
          <div class="col-sm-7">
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio1" value="O" @if($data['peserta']->golongan_darah == 'O') checked @endif> O
              </label>
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio2" value="A" @if($data['peserta']->golongan_darah == 'A') checked @endif> A
              </label>
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio1" value="B" @if($data['peserta']->golongan_darah == 'B') checked @endif> B
              </label>
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio2" value="AB" @if($data['peserta']->golongan_darah == 'AB') checked @endif> AB
              </label>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Kewarganegaraan</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Kewarganegaraan peserta" name="kewarganegaraan" value="{{ $data['peserta']->kewarganegaraan }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jenis Kelamin</label>
          <div class="col-sm-7">
              <label class="radio-inline">
                <input type="radio" name="jenis_kelamin" id="inlineRadio1" value="m" @if($data['peserta']->jenis_kelamin == 'm') checked @endif> Laki-laki
              </label>
              <label class="radio-inline">
                <input type="radio" name="jenis_kelamin" id="inlineRadio2" value="f"  @if($data['peserta']->jenis_kelamin == 'f') checked @endif> Perempuan
              </label>
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-sm-3 control-label">Email</label>
          <div class="col-sm-7">
              <input type="email" class="form-control" placeholder="Email peserta" name="email" value="{{ $data['peserta']->email }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">No. Handphone</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="No. Handphone peserta" name="no_hp" value="{{ $data['peserta']->no_hp }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Telepon Rumah</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Nama telepon rumah peserta" name="no_telp" value="{{ $data['peserta']->no_telp }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tempat Lahir</label>
           <div class="col-sm-7">
           	<input type="text" class="form-control" placeholder="Tempat lahir" name="tempat_lahir" value="{{ $data['peserta']->tempat_lahir }}">
           </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tanggal Lahir</label>
          <div class="col-sm-2">
              {!! Form::selectRange('dd', 1, 31, getPartialDate($data['peserta']->tanggal_lahir), ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off']) !!}
          </div>
          <div class="col-sm-3">
              {!! Form::selectMonth('mm', getPartialDate($data['peserta']->tanggal_lahir, 'month'),['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off']); !!}
          </div>
          <div class="col-sm-2">
              {!! Form::selectRange('yy', 1945, 2005, getPartialDate($data['peserta']->tanggal_lahir, 'year'), ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off']) !!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Alamat</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Alamat sesuai dengan kartu identitas peserta" name="alamat" value="{{ $data['peserta']->alamat_sekarang }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jenjang Jabatan</label>
          <div class="col-sm-5">
              {!! Form::select('jenjangjabatan_id', $data['support']['jenjang'], $data['peserta']->jenjangjabatan_id, ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jabatan</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Jabatan peserta" name="jabatan" value="{{ $data['peserta']->jabatan }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Grade</label>
          <div class="col-sm-5">
              {!! Form::select('grade_id', $data['support']['grade'], $data['peserta']->grade_id, ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Pendidikan</label>
           <div class="col-sm-5">
              {!! Form::select('pendidikan_id', $data['support']['pendidikan'], $data['peserta']->pendidikan_id, ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jurusan</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Jurusan peserta" value="{{ $data['peserta']->jurusan }}" name="jurusan">
          </div>
        </div>
        <div id="units" style="{{ ($data['peserta']->tipe_peserta == 'pln') ? '' : 'display:none;' }}">
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Induk</label>
           <div class="col-sm-5">
              {!! Form::select('unitinduk_id', $data['support']['uinduk'], (! is_null($data['peserta']->unitinduk_id)) ? $data['peserta']->unitinduk_id : '', ['class'=>'selectpicker form-control','data-live-search' => 'true', 'id'=>'unitinduk_id', 'autocomplete' => 'off'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Cabang</label>
          <div class="col-sm-5">
          @if(! is_null($data['peserta']->unitcabang_id))
            {!! Form::select('unitcabang_id', [$data['peserta']->unitcabang_id => $data['peserta']->unitcabang->nama_unitcabang], $data['peserta']->unitcabang_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitcabang', 'autocomplete' => 'off'])!!}
          @else
            <select name="unitcabang_id" class="selectpicker form-control" id="unitcabang" data-live-search="true" disabled="disabled" autocomplete="off">
              <option value="0">----------------------------------</option>
-           </select>
          @endif
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Ranting</label>
           <div class="col-sm-5">
           @if(! is_null($data['peserta']->unitranting_id))
              {!! Form::select('unitranting_id', [$data['peserta']->unitranting_id => $data['peserta']->unitranting->nama_unitranting], $data['peserta']->unitranting_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitranting', 'autocomplete' => 'off'])!!}
           @else
              <select name="unitranting_id" class="selectpicker form-control" id="unitranting" disabled="disabled" data-placeholder="-- Pilih unit ranting --" data-live-search="true" autocomplete="off">
                <option value="0">----------------------------------</option>
-             </select>
           @endif   
          </div>
        </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unggah Foto</label>
          <div class="col-sm-8">
              <input type="file" name="foto">
              <p class="help-block">Kosongkan field ini jika tidak mengganti foto. Berkas unggahan tidak boleh berkapasitas lebih dari 200kb.</p>
          </div>
        </div>
      </div><!-- /.box-body -->
      <div class="box-footer col-md-offset-3">
        <button type="submit" class="btn btn-primary">Update</button>&nbsp;
        <a href="{{ route('dashboard.master.peserta.index') }}" class="btn btn-warning">Kembali</a>
      </div>
    </form>
  </div><!-- /.box -->
</div>
</div><!-- /.row -->
@stop
@section('customjs')
<script type="text/javascript">
$(document).ready(function(){
    var form = $('form#peserta'),
    uinduk     = form.find('select#unitinduk_id'),
    ucabang    = form.find('select#unitcabang'),
    uranting   = form.find('select#unitranting'),
    tipepeserta = form.find('select#tipe_peserta'),
    url        = "{{ url('dashboard/master/unit/induk/api/cabang', 'id') }}",
    urlranting = "{{ url('dashboard/master/unit/cabang/api/ranting', 'id') }}";
    
    uinduk.change(function() {
      urlWithId = url.replace('id', uinduk.val());  
      var options = '';
      $.getJSON(urlWithId, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="0">-- Pilih Unit Cabang --</option>';
          $.each(data,function(index,value){
            options += '<option value="'+index+'">'+value+'</option>';
          });
          ucabang.removeAttr('disabled');
          uranting.removeAttr('disabled');
          ucabang.html(options).selectpicker('refresh');
        } else {
          ucabang.html('');
          uranting.html('');
          ucabang.attr('disabled',true).selectpicker('refresh');
          uranting.attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });
    ucabang.change(function() {
      urlrantingWithId = urlranting.replace('id', ucabang.val()); 
      var options = '';
      $.getJSON(urlrantingWithId, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="0">-- Pilih Unit Ranting --</option>';
            $.each(data,function(index,value){
                options += '<option value="'+index+'">'+value+'</option>';
            });
            uranting.removeAttr('disabled');
            uranting.html(options).selectpicker('refresh');
        } else {
          uranting.html('');
          uranting.attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });

    tipepeserta.change(function(event)
    {
      event.preventDefault();
      if($(this).val() == "pln") {
        $('div#info-nonpln').hide();
        $('div#units').show();
        $('#unitnonpln_id').attr('disabled', true).selectpicker('refresh');
      } else {
        $('div#info-nonpln').show();
        $('div#units').hide();
        uinduk.attr('disabled', true).selectpicker('refresh');
        ucabang.attr('disabled', true).selectpicker('refresh');
        uranting.attr('disabled', true).selectpicker('refresh');
      }
      
    });

});
</script>
@stop