@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <div class="input-group">
            <label>Tanggal Uji:</label> {{ $data['penjadwalan']->tanggal_mulai }} s/d {{ $data['penjadwalan']->tanggal_selesai }}
          </div>
          <div class="input-group">
            <label>TUK:</label> {{ $data['penjadwalan']->udiklat->nama_udiklat }}
          </div>
          <div class="input-group">
            <label>LSK:</label> {{ $data['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}
          </div>
          <div class="input-group">
            <label>Kompetensi:</label> {{ $data['kompetensi']->nama_kompetensi }}
          </div><br>
          <h3 class="box-title">Daftar Hadir & Hasil Uji</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <form method="post" action="{{ route('dashboard.pelaksanaan.realisasi.store') }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="penjadwalan_id" value="{{ $data['penjadwalan']->id }}">
          <input type="hidden" name="kompetensipenjadwalan_id" value="{{ $data['kompetensipenjadwalanId'] }}">
          <input type="hidden" name="kompetensi_id" value="{{ $data['kompetensi']->id }}">
          <table class="table table-condensed table-bordered table-hover">
            <thead>
                <tr class="bg-info">
                  <th class="text-center">No.</th>
                  <th class="text-center col-md-1">NIP</th>
                  <th class="text-center col-md-2">Nama</th>
                  <th class="text-center">Kehadiran</th>
                  <th class="text-center">Hasil Uji</th>
                  <th class="text-center col-md-3">Keterangan</th>
                  <th class="text-center col-md-2">Sertifikat</th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1; $countCertified=0;?>
             @forelse($data['peserta'] as $peserta)
             
             @if(is_null($peserta->no_sertifikat))
              <input type="hidden" name="peserta_id[]" value="{{ $peserta->id_peserta }}" >
             @else
              <?php $countCertified++; ?>
             @endif

             <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $peserta->nip }}</td>
                <td>{{ $peserta->nama }}</td>
                <td class="text-center">
                @if(Auth::user()->can('can_write_pelaksanaan'))
                  @if(! is_null($peserta->no_sertifikat))
                    @if($peserta->kehadiran == 'y')
                      Hadir
                    @else
                      Tidak Hadir
                    @endif
                  @else
                    {!! Form::select('kehadiran[]', ['n' => 'Tidak Hadir', 'y' => 'Hadir'], (! is_null($peserta->kehadiran)) ? $peserta->kehadiran : 'n', ['class' => 'form-control', 'autocomplete' => 'off'] ) !!}
                  @endif
                  
                @else
                  @if(! is_null($peserta->kehadiran))
                    @if($peserta->kehadiran == 'y')
                      Hadir
                    @else
                      Tidak Hadir
                    @endif
                  @else
                    Tidak Hadir
                  @endif
                @endif
                </td>
                <td class="text-center">
                @if(Auth::user()->can('can_write_pelaksanaan'))
                   @if(! is_null($peserta->no_sertifikat))
                    @if($peserta->kelulusan == 'y')
                      Kompeten
                    @else
                      Belum Kompeten
                    @endif
                  @else
                    {!! Form::select('kelulusan[]', ['n' => 'Belum Kompeten', 'y' => 'Kompeten'], (! is_null($peserta->kelulusan)) ? $peserta->kelulusan : 'n', ['class' => 'form-control', 'autocomplete' => 'off'] ) !!}
                  @endif
                  
                @else
                  @if(! is_null($peserta->kelulusan))
                    @if($peserta->kehadiran == 'y')
                      Kompeten
                    @else
                      Belum Kompeten
                    @endif
                  @else
                    Belum Kompeten
                  @endif
                @endif
                </td>
                <td>
                  @if(! is_null($peserta->no_sertifikat))
                    {{ (! empty($peserta->information)) ? $peserta->information : '-' }}
                  @else
                    <input type="text" name="information[]" placeholder="keterangan..." class="form-control" value="@if(! empty($peserta->information)) {{ $peserta->information }} @endif" {{(! Auth::user()->can('can_write_pelaksanaan')) ? 'disabled="disabled"' : ''}}>
                  @endif
                  
                </td>
                @if($data['penjadwalan']->lembagasertifikasi->grup == 'pln')
                <td class="text-center">
                  @if(Auth::user()->can('can_write_pelaksanaan'))
                    <span class="btn btn-sm {{ ($peserta->kelulusan == 'n' || $peserta->kehadiran == 'n') ? 'btn-danger disabled' : 'btn-success' }} {{ (! is_null($peserta->no_sertifikat) || is_null($peserta->kelulusan)) ? 'disabled' : ''}} sertifikat" data-toggle="modal" data-target="#myModal" id="{{ $peserta->id_peserta }}">Buat Sertifikat</span>
                  @else
                    -
                  @endif
                </td>
                @else
                <td class="text-center">
                  <span class="btn btn-sm {{ ($peserta->kelulusan == 'n' || $peserta->kehadiran == 'n') ? 'btn-danger disabled' : 'btn-success' }} {{ (! is_null($peserta->no_sertifikat)) ? 'disabled' : ''}} sertifikat" data-toggle="modal" data-target="#set-sertifikat" id="{{ $peserta->id_peserta }}">Buat Sertifikat</span>
                </td>
                @endif
              </tr>
             @empty
              <tr>
                <td colspan="7" class="text-center">Tidak ada peserta</td>
              </tr>
             @endforelse
            </tbody>
          </table>
        </div><!-- /.box-body -->
          <div class="box-footer">
          @if(Auth::user()->can('can_write_pelaksanaan'))
            @if($data['peserta']->count() != $countCertified)
        		<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
            @endif
          @endif
        		<a href="{{ route('dashboard.pelaksanaan.index') }}" class="btn btn-warning">Kembali</a>
          </div>
      </form>    
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop

@section('extension')
@if($data['penjadwalan']->lembagasertifikasi->grup == 'pln')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-green">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Konfirmasi Pembuatan Sertifikat Peserta Grup PLN</h4>
      </div>
      <form action="{{ route('dashboard.sertifikat.baru.store') }}" method="post" id="submit-sertifikat">
      <input type="hidden" name="peserta_id" id="input-peserta-id" value="">
      <input type="hidden" name="lembagasertifikasi_id" id="input-lsk-id" value="">
      <input type="hidden" name="kompetensi_id" id="input-kompetensi-id" value="">
      <input type="hidden" name="penjadwalan_id" id="input-penjadwalan-id" value="">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="modal-body">
        <p><strong>Data Sertifikat Peserta</strong></p>
        <table class="table table-striped">
           <tr>
            <td>Tanggal</td>
            <td>{{ date('Y-m-d') }}</td>
          </tr>
          <tr>
            <td>NIP</td>
            <td id="nip"></td>
          </tr>
          <tr>
            <td>Nama</td>
            <td id="nama"></td>
          </tr>
          <tr>
            <td>TUK</td>
            <td>{{ $data['penjadwalan']->udiklat->nama_udiklat }}</td>
          </tr>
          <tr>
            <td>LSK</td>
            <td>{{ $data['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}</td>
          </tr>
          <tr>
            <td>Unit Kompetensi</td>
            <td>{{ $data['kompetensi']->nama_kompetensi }}</td>
          </tr>
        </table>
      </div>
       </form>
      <div class="modal-footer">
        <a class="btn btn-warning pull-left" data-dismiss="modal">Close</a>
        <button type="submit" id="submit-sertifikat-button" class="btn btn-primary">Submit</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@else

<div class="modal fade" id="set-sertifikat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-green">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pembuatan Sertifikat Peserta Grup NON PLN</h4>
      </div>
      <form action="{{ route('dashboard.sertifikat.baru.store') }}" method="post" id="set-submit-sertifikat" class="form-horizontal">
      <input type="hidden" name="peserta_id" id="input-peserta-id" value="">
      <input type="hidden" name="lembagasertifikasi_id" id="input-lsk-id" value="">
      <input type="hidden" name="kompetensi_id" id="input-kompetensi-id" value="">
      <input type="hidden" name="penjadwalan_id" id="input-penjadwalan-id" value="">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="modal-body">
        <p><strong>Data Sertifikat Peserta</strong></p>
        <table class="table table-striped">
           <tr>
            <td>Tanggal</td>
            <td>{{ date('Y-m-d') }}</td>
          </tr>
          <tr>
            <td>NIP</td>
            <td id="nip"></td>
          </tr>
          <tr>
            <td>Nama</td>
            <td id="nama"></td>
          </tr>
          <tr>
            <td>TUK</td>
            <td>{{ $data['penjadwalan']->udiklat->nama_udiklat }}</td>
          </tr>
          <tr>
            <td>LSK</td>
            <td>{{ $data['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}</td>
          </tr>
          <tr>
            <td>Unit Kompetensi</td>
            <td>{{ $data['kompetensi']->nama_kompetensi }}</td>
          </tr>
        </table>
       <div class="form-group">
          <label for="nama" class="col-sm-3 control-label">No. Sertifikat</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Nomor Sertifikat" name="no_sertifikat" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nama" class="col-sm-3 control-label">Berlaku Sejak</label>
          <div class="col-sm-7">
              <input type="text" class="form-control clsDatePicker" placeholder="Tanggal mulai berlaku" name="tanggal_mulai_berlaku" id="tanggal_mulai_berlaku" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nama" class="col-sm-3 control-label">Berlaku Sampai</label>
          <div class="col-sm-7">
              <input type="text" class="form-control clsDatePicker" placeholder="Tanggal akhir berlaku" name="tanggal_akhir_berlaku" id="tanggal_akhir_berlaku" required>
          </div>
        </div>
      </div>
       </form>
      <div class="modal-footer">
        <a class="btn btn-warning pull-left" data-dismiss="modal">Close</a>
        <button type="submit" id="set-submit-sertifikat-button" class="btn btn-primary">Submit</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif
@stop

@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
  $('.clsDatePicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  });

  $('.sertifikat').click(function()
  {
    var endpoint   = "{{ route('api.get.detail.peserta', 'id') }}";
    var peserta_id = $(this).attr('id');
    var url        = endpoint.replace('id', peserta_id);
    $.getJSON(url, function(response)
    {
      $('#submit-sertifikat-button').removeClass('disabled');
      $('#set-submit-sertifikat-button').removeClass('disabled');

      $('.modal-body').find('td[id="nip"]').html(response.nip);
      $('.modal-body').find('td[id="nama"]').html(response.nama);
      $('#input-peserta-id').val(response.id);
      $('#input-penjadwalan-id').val("{{ $data['penjadwalan']->id }}");
      $('#input-kompetensi-id').val("{{ $data['kompetensi']->id }}");
      $('#input-lsk-id').val("{{ $data['penjadwalan']->lembagasertifikasi->id }}");
    });

    $('#submit-sertifikat-button').click(function(event)
    {
      event.preventDefault();
      $('#submit-sertifikat-button').addClass('disabled');

      var formData  = $("#submit-sertifikat").serializeArray();
      var url       = $("#submit-sertifikat").attr("action");
      $.post(url, formData, function(response)
      {
        if(response.status) {
          id = response.data.peserta_id;
          $('#' + id).addClass('disabled');
          $('#myModal').modal('hide');
          parent.window.location.reload();
        } else {
           $('#submit-sertifikat-button').removeClass('disabled');
          alert('Error, gagal menyimpan data');
        }
      });
    });

    $('#set-submit-sertifikat-button').click(function(event)
    {
      event.preventDefault();
      $('#set-submit-sertifikat-button').addClass('disabled');

      var formData  = $("#set-submit-sertifikat").serializeArray();
      var url       = $("#set-submit-sertifikat").attr("action");
      $.post(url, formData, function(response)
      {
        if(response.status) {
          id = response.data.peserta_id;
          $('#' + id).addClass('disabled');
          $('#set-sertifikat').modal('hide');
          parent.window.location.reload();
        } else {
          $('#set-submit-sertifikat-button').removeClass('disabled');
          alert('Error, gagal menyimpan data');
        }
      });
    });
  });
</script>
@stop  
