@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
	  <div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">Data Pelaksanaan</h3>
	    </div><!-- /.box-header -->
	    <div class="box-body">
	    @if (Session::has('flash_notification.message'))
		    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        {{ Session::get('flash_notification.message') }}
		    </div>
		@endif
		<div class="table-responsive">
			<table class="table table-condensed table-bordered table-hover" style="font-size:12px;">
	        <thead>
	            <tr class="bg-info">
	              <th class="text-center" rowspan="2">No.</th>
	              <th class="text-center" rowspan="2">Tanggal Pelaksanaan</th>
	              <th class="text-center" rowspan="2">TUK</th>
	              <th class="text-center" rowspan="2">Unit Induk</th>
	              <th class="text-center" rowspan="2">LSK</th>
	              <th class="text-center" rowspan="2">Biaya</th>
	              <th class="text-center" rowspan="2">Kompetensi</th>
	              <th class="text-center" rowspan="2">Rencana</th>
	              <th class="col-md-1 text-center" colspan="3">Realisasi</th>
	              <th class="text-center" rowspan="2" style="width:60px;">Aksi</th>
	            </tr>
	            <tr class="bg-info">
	              <th class="text-center">Kehadiran</th>
	              <th class="text-center">Kompeten</th>
	              <th class="text-center">Belum Kompeten</th>
	            </tr>
	        </thead>
	        <tbody>
	          @forelse($data['pelaksanaan']['penjadwalan'] as $pelaksanaan)
	          <tr>
	          	<td>{{ $data['pagination_number']++ }}</td>
	          	<td>{{ $pelaksanaan->tanggal_mulai }} s/d {{ $pelaksanaan->tanggal_selesai }}</td>
	          	<td>{{ $pelaksanaan->udiklat->nama_udiklat }}</td>
	          	<td>{{ $pelaksanaan->unitinduk->nama_unitinduk }}</td>
	          	<td>{{ $pelaksanaan->lembagasertifikasi->nama_lembagasertifikasi }}</td>
	          	<td><a href="{{ route('dashboard.pelaksanaan.{id}.biaya.index', ['id' => $pelaksanaan->id]) }}"><span class="label label-primary">Biaya</span></a></td>
	          	<td style="white-space:nowrap;">
	              @forelse($pelaksanaan->kompetensi as $kompetensi)
	               - <a href="{{ route('dashboard.pelaksanaan.realisasi', ['penjadwalanId' => $pelaksanaan->id, 'kompetensiId' => $kompetensi->id, 'kompetensipenjadwalanId' => $kompetensi->pivot->id]) }}">{{ $kompetensi->nama_kompetensi }}</a><br>
	              @empty
	                -
	              @endforelse
	            </td>
	            <td class="text-center" style="white-space:nowrap;">
	              @forelse($data['pelaksanaan']['pelaksanaan'] as $realisasi)
	            	@if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
	            		{{ $realisasi['total_peserta'] }}<br>
	            	@endif		
	              @empty
	                -
	              @endforelse
	            </td>
	            <td class="text-center" style="white-space:nowrap;">
	            	@forelse($data['pelaksanaan']['pelaksanaan'] as $realisasi)
	            		@if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
		            		{{ $realisasi['realisasi']['kehadiran'] }}<br>
		            	@endif	
	            	@empty
	            	-
	            	@endforelse
	            </td>
	            <td class="text-center" style="white-space:nowrap;">
	            	@forelse($data['pelaksanaan']['pelaksanaan'] as $realisasi)
	            		@if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
		            		{{ $realisasi['realisasi']['kompeten'] }}<br>
		            	@endif	
	            	@empty
	            	-
	            	@endforelse
	            </td>
	            <td class="text-center" style="white-space:nowrap;">
	            	@forelse($data['pelaksanaan']['pelaksanaan'] as $realisasi)
	            		@if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
		            		{{ $realisasi['realisasi']['nonkompeten'] }}<br>
		            	@endif	
	            	@empty
	            	-
	            	@endforelse
	            </td>
	            <td class="text-center">
	            	<a href="{{ route('dashboard.pelaksanaan.show', ['id' => $pelaksanaan->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>
	            	<a href="{{ route('dashboard.sertifikat.baru.multiple', ['penjadwalanId' => $pelaksanaan->id]) }}" class="btn btn-xs btn-success {{ ($pelaksanaan->lembagasertifikasi->grup != 'pln' || $pelaksanaan->peserta->count() == 0) ? 'disabled' : '' }}" onclick="return confirm('Generate semua sertifikat?'); "><i class="fa fa-certificate"></i></a>
	            </td>
	          </tr>
	          @empty
	          <tr>
	          	<td colspan="12" class="text-center">Tidak ada data pelaksanaan</td>
	          </tr>
	          @endforelse
	        </tbody>
	      </table><br>
		</div>
	      <div class="text-center">
	          {!! paginationHelper($data['pelaksanaan']['penjadwalan']->render()) !!}
	      </div>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	</div><!-- /.row -->
@stop