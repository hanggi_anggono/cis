@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
	  <div class="box box-primary">
	    <div class="box-header with-border">
	      <h3 class="box-title">Detail Pelaksanaan</h3> &nbsp;
	      <a href="{{ route('dashboard.pelaksanaan.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
	    </div><!-- /.box-header -->
	    <div class="box-body">
	      <table class="table table-condensed table-bordered">
	        <thead>
	            <tr class="bg-info">
	              <th class="text-center">Tanggal Pelaksanaan</th>
	              <th class="text-center">TUK</th>
	              <th class="text-center">Unit Induk</th>
	              <th class="text-center">LSK</th>
	              <th class="text-center">Total Peserta</th>
	              <th class="text-center">Total Kompetensi</th>
	              <th class="text-center">Total Kompeten</th>
	              <th class="text-center">Total Biaya</th>
	            </tr>
	        </thead>
	        <tbody>
	          <tr class="text-center">
	          	<td>{{ $data['penjadwalan']['penjadwalan']->tanggal_mulai }} s/d {{ $data['penjadwalan']['penjadwalan']->tanggal_selesai}}</td>
	          	<td>{{ $data['penjadwalan']['penjadwalan']->udiklat->nama_udiklat }}</td>
	          	<td>{{ $data['penjadwalan']['penjadwalan']->unitinduk->nama_unitinduk }}</td>
	          	<td>{{ $data['penjadwalan']['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}</td>
	          	<td>
	          		<?php $totalPeserta = 0; ?>
	          		@foreach($data['penjadwalan']['pelaksanaan'] as $pelaksanaan)
	          		<?php $totalPeserta += $pelaksanaan['total_peserta']; ?>
	          		@endforeach
	          		{{ $totalPeserta }}
	          	</td>
	          	</td>
	          	<td>{{ $data['penjadwalan']['penjadwalan']->kompetensi->count() }}</td>
	          	<td>
	          	<?php $totalKompeten = 0; ?>
	          		@foreach($data['penjadwalan']['pelaksanaan'] as $pelaksanaan)
	          		<?php $totalKompeten += $pelaksanaan['realisasi']['kompeten']; ?>
	          		@endforeach
	          		{{ $totalKompeten }}
	          	</td>
	          	<td>{{ IDRFormatter($data['biaya']) }}</td>
	          </tr>
	        </tbody>
	      </table>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
</div><!-- /.row -->

<div class="row">
	<div class="col-md-6">
	  <div class="box box-primary">
	    <div class="box-header with-border">
	      <h3 class="box-title">Hasil Realisasi Peserta</h3>
	    </div><!-- /.box-header -->
	    <div class="box-body">
	      <table class="table table-condensed table-bordered">
	        <thead>
	            <tr class="bg-info">
	              <th class="text-center">No.</th>
                  <th class="text-center">NIP</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Kompetensi</th>
                  <th class="text-center">Hadir</th>
                  <th class="text-center">kompeten</th>
	            </tr>
	        </thead>
	        <tbody>
	        <?php $no=1; ?>
	         @forelse($data['penjadwalan']['pesertaRealisasi'] as $peserta)
	         <tr>
	         	<td>{{ $no++ }}</td>
	         	<td>{{ $peserta->nip }}</td>
	         	<td>{{ $peserta->nama }}</td>
	         	<td>{{ $peserta->nama_kompetensi }}</td>
	         	<td class="text-center">{!! ($peserta->kehadiran == 'y') ? '<span class="label label-success">Ya</span>' : '<span class="label label-danger">tidak</span>' !!}</td>
	         	<td class="text-center">{!! ($peserta->kelulusan == 'y') ? '<span class="label label-success">Ya</span>' : '<span class="label label-danger">tidak</span>' !!}</td>
	         </tr>
	         @empty
	         <tr>
	         	<td colspan="6" class="text-center">Tidak ada data peserta</td>
	         </tr>
	         @endforelse
	        </tbody>
	      </table>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	<div class="col-md-6">
	  <div class="box box-primary">
	    <div class="box-header with-border">
	      <h3 class="box-title">Kompetensi</h3>
	    </div><!-- /.box-header -->
	    <div class="box-body">
	      <table class="table table-condensed table-bordered">
	        <thead>
	            <tr class="bg-info">
	              <th rowspan="2">No.</th>
	              <th class="text-center" rowspan="2">Kompetensi</th>
	              <th class="text-center" rowspan="2">Rencana</th>
	              <th class="col-md-1 text-center" colspan="3">Realisasi</th>
	            </tr>
	            <tr class="bg-info">
	              <th class="col-md-1 text-center">Kehadiran</th>
	              <th class="col-md-1 text-center">Kompeten</th>
	              <th class="col-md-1 text-center">Belum Kompeten</th>
	            </tr>
	        </thead>
	        <tbody>
	        <?php $no=1; ?>
	         @forelse($data['penjadwalan']['pelaksanaan'] as $realisasi)
	         <tr>
	         	<td>{{ $no++ }}</td>
	         	<td>{{ $realisasi['nama_kompetensi']}}</td>
	         	<td class="text-center">{{ $realisasi['total_peserta'] }}</td>
	         	<td class="text-center">{{ $realisasi['realisasi']['kehadiran'] }}</td>
	         	<td class="text-center">{{ $realisasi['realisasi']['kompeten'] }}</td>
	         	<td class="text-center">{{ $realisasi['realisasi']['nonkompeten'] }}</td>
	         </tr>
	         @empty
	         <tr>
	         	<td colspan="6" class="text-center">Tidak ada kompetensi</td>
	         </tr>
	         @endforelse
	        </tbody>
	      </table>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
</div><!-- /.row -->
@stop