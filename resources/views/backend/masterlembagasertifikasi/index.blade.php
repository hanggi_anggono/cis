@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_masterlembagasertifikasi'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.lembagasertifikasi.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Lembaga Sertifikasi</a>
		    </div><!-- /.box-header -->
		@endif
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th>No.</th>
				          	<th class="col-md-3">Nama Lembaga Sertifikasi</th>
				          	<th class="col-md-2">Kode Registrasi</th>
				          	<th>Grup</th>
				          	<th>Sertifikat</th>
				          	<th class="col-md-2">Jumlah Asesor</th>
				          	<th class="col-md-2">Jumlah Administrator</th>
				          	<th class="col-md-3 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @forelse($data['lembagasertifikasi'] as $lembaga)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $lembaga->nama_lembagasertifikasi }}</td>
				          	<td>{{ (! is_null($lembaga->koderegistrasilsp) ? $lembaga->koderegistrasilsp : '-')}}</td>
				          	<td>{{ $lembaga->grup }}</td>
				          	<td>{{ $lembaga->jenis_sertifikat }}</td>
				          	<td>{{ $lembaga->asesor->count() }} Asesor</td>
				          	<td>{{ $lembaga->administrator->count() }} Administrator</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterlembagasertifikasi'))
				          		<a href="{{ route('dashboard.master.lembagasertifikasi.edit', ['id' => $lembaga->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.lembagasertifikasi.delete', ['id' => $lembaga->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>
				      @empty
				      <tr>
				      	<td colspan="4" class="text-center">Tidak ada data</td>
				      </tr>    
			          @endforelse
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['lembagasertifikasi']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop