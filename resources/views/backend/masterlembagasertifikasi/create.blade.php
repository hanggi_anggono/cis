@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Tambah Lembaga Sertifikasi</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if($errors->any())
		    		<div class="alert alert-danger alert-dismissable">
		    			<ul>
			    		<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			    		@foreach($errors->all() as $error)
			    			<li>{{ $error }}</li>
			    		@endforeach
			    		</ul>
			    	</div>
		    	@endif
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	<form class="form-horizontal" action="{{ route('dashboard.master.lembagasertifikasi.store') }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Nama Lembaga</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama Lembaga Sertifikasi" name="nama_lembagasertifikasi" required>
                      </div>
		    		</div>
					<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Kode Registrasi</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Kode registrasi lsp" name="koderegistrasilsp">
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Grup</label>
                      <div class="col-sm-4">
                          {!! Form::select('grup', ['pln' => 'PLN', 'kerjasama' => 'Kerjasama'], 'null', ['class' => 'form-control']) !!}
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Sertifikat</label>
                      <div class="col-sm-4">
                          {!! Form::select('jenis_sertifikat', ['pln' => 'PLN', 'bnsp' => 'BNSP'], 'null', ['class' => 'form-control']) !!}
                      </div>
		    		</div>		    		
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		            <a href="{{ route('dashboard.master.lembagasertifikasi.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    