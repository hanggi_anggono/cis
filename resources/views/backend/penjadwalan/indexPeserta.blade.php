@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-sm-10">
      <div class="box">
        <div class="box-header with-border">
          <div class="input-group">
            <label>Tanggal Uji:</label> {{ $data['penjadwalan']->tanggal_mulai }} s/d {{ $data['penjadwalan']->tanggal_selesai}}
          </div>
          <div class="input-group">
            <label>TUK:</label> {{ $data['penjadwalan']->udiklat->nama_udiklat }}
          </div>
          <div class="input-group">
            <label>LSK:</label> {{ $data['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}
          </div> 
          <div class="input-group">
            <label>Kompetensi:</label> {{ $data['kompetensi']->nama_kompetensi }}
          </div>
          <h3>Daftar Peserta Penjadwalan</h3>
          <div class="input-group">
          @if(Auth::user()->can('can_write_penjadwalan'))
            <a href="{{ route('dashboard.penjadwalan.peserta.create', ['id' => $data['penjadwalan']->id, 'kompetensiid' => $data['kompetensi']->id, 'kompetensipenjadwalanId' => $data['kompetensipenjadwalanId']]) }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"> </i> Tambah</a> &nbsp;
          @endif
            <a href="{{ route('dashboard.penjadwalan.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"> </i> Kembali</a>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
           @if (Session::has('flash_notification.message'))
              <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('flash_notification.message') }}
              </div>
          @endif
          <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr class="bg-info">
                  <th class="text-center">No.</th>
                  <th class="text-center">NIP</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Grade</th>
                  <th class="text-center">Jabatan</th>
                  <th class="text-center">Unit Induk</th>
                  <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1; ?>
            @forelse($data['peserta'] as $peserta)
            <tr>
              <td>{{ $no++}}</td>
              <td>{{ $peserta['master']->nip }}</td>
              <td>{{ $peserta['master']->nama }}</td>
              <td>{{ $peserta['master']->grade->nama_grade }}</td>
              <td>{{ $peserta['master']->jenjangjabatan->nama_jenjangjabatan }}</td>
              <td>{{ (! is_null($peserta['master']->unitinduk)) ? $peserta['master']->unitinduk->nama_unitinduk : '-' }}</td>
              <td class="text-center">
              @if(Auth::user()->can('can_write_penjadwalan'))
                <a href="{{ route('dashboard.penjadwalan.peserta.delete', ['id' => $data['penjadwalan']->id,  'pesertaId' => $peserta['master']->id, 'evaluasiId' => $peserta['evaluasi_id'], 'kompetensiId' => $data['kompetensi']->id, 'kompetensipenjadwalanId' => $data['kompetensipenjadwalanId'] ]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> </a>
              @else
                -
              @endif
              </td>
            </tr>
            @empty
            <tr>
              <td colspan="7" class="text-center">Tidak ada peserta untuk penjadwalan ini</td>
            </tr>
            @endforelse
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop