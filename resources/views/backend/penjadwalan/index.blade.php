@extends('layouts.backend.master')
@section('content')
<div class="row">
<div class="col-md-12">
  <div class="box">
    <div class="box-header">
      <div class="input-group">
      @if(Auth::user()->can('can_write_penjadwalan'))
        <a href="{{ route('dashboard.penjadwalan.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Penjadwalan</a> &nbsp;
      @endif
        <a href="{{ route('dashboard.penjadwalan.peserta.evaluasipeserta') }}" class="btn btn-sm btn-info"><i class="fa fa-user"> </i> Hasil Evaluasi Peserta</a>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body">
      @if (Session::has('flash_notification.message'))
          <div class="alert alert-{{ Session::get('flash_notification.level') }}">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              {{ Session::get('flash_notification.message') }}
          </div>
      @endif
      <table class="table table-condensed table-bordered table-hover table-striped" style="font-size:12px;">
        <thead>
            <tr class="bg-info">
              <th class="text-center">No.</th>
              <th class="text-center">Tanggal Mulai</th>
              <th class="text-center">Tanggal Selesai</th>
              <th class="text-center">TUK</th>
              <th class="text-center">Unit Induk</th>
               <th class="text-center">LSK</th>
              <th class="text-center">Kompetensi</th>
              <th class="text-center">Peserta</th>
              <th class="col-md-1 text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
        @forelse($data['penjadwalan'] as $penjadwalan)
          <tr>
            <td>{{ $data['pagination_number']++ }}</td>
            <td><a href="{{ route('dashboard.penjadwalan.kompetensi.index', ['id' => $penjadwalan->id]) }}">{{ $penjadwalan->tanggal_mulai }}</a></td>
            <td>{{ $penjadwalan->tanggal_selesai }}</td>
            <td>{{ $penjadwalan->udiklat->nama_udiklat }}</td>
            <td>{{ $penjadwalan->unitinduk->nama_unitinduk }}</td>
            <td>{{ $penjadwalan->lembagasertifikasi->nama_lembagasertifikasi }}</td>
            <td style="white-space:nowrap;">
              @forelse($penjadwalan->kompetensi as $kompetensi)
                - <a href="{{ route('dashboard.penjadwalan.peserta.index', ['id' => $penjadwalan->id, 'kompetensiId' => $kompetensi->id, 'kompetensipenjadwalanId' => $kompetensi->pivot->id]) }}">{{ $kompetensi->nama_kompetensi }}</a><br>
              @empty
                -
              @endforelse
            </td>
            <td class="text-center"  style="white-space:nowrap;">
              @forelse($data['kompetensipenjadwalan'] as $kompetensi)
                @if($kompetensi->penjadwalan_id == $penjadwalan->id)
                  {{ $kompetensi->peserta->count() }}<br>
                @endif
              @empty
                -
              @endforelse
            </td>
            <td class="text-center">
              <a href="{{ route('dashboard.penjadwalan.show', ['id' => $penjadwalan->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>
            @if(Auth::user()->can('can_write_penjadwalan'))
              <a href="{{ route('dashboard.penjadwalan.edit', ['id' => $penjadwalan->id]) }}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> </a>
              <a href="{{ route('dashboard.penjadwalan.delete', ['id' => $penjadwalan->id]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> </a>
            @endif
            </td>
          </tr>
        @empty
        <tr>
          <td colspan="11" class="text-center">Tidak ada data penjadwalan</td>
        </tr>
        @endforelse  
        </tbody>
      </table><br>
      <div class="text-center">
         {!! paginationHelper($data['penjadwalan']->render()) !!}
      </div>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div>
</div><!-- /.row -->
@stop