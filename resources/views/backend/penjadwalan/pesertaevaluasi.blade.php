@extends('layouts.backend.master')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
          <h4>Pencarian Lanjut</h4>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form class="form-inline" method="get" action="{{ route('dashboard.penjadwalan.peserta.evaluasipeserta') }}">
            <div class="form-group">
              <input type="text" class="form-control" name="nosurat" placeholder="No. Surat" value="{{ Request::get('nosurat') }}">
            </div>
            <div class="form-group">
               <input type="text" class="form-control" name="nip" placeholder="NIP / Nama Peserta" value="{{ Request::get('nip') }}">
            </div>
            <div class="form-group">
               <input type="text" class="form-control" name="kode" placeholder="Kode / Nama Kompetensi" value="{{ Request::get('kode') }}">
            </div>
            <div class="form-group">
               <input type="text" class="form-control" name="unitinduk" placeholder="Nama Unit Induk" value="{{ Request::get('unitinduk') }}">
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
            <div class="pull-right">
              <a href="{{ route('dashboard.penjadwalan.index') }}" class="btn btn-warning "><i class="fa fa-reply"></i> Kembali ke Penjadwalan</a>
              <a href="{{ route('dashboard.penjadwalan.peserta.evaluasipeserta') }}" class="btn btn-info"><i class="fa fa-database"></i> All Data</a>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
<div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h4>Daftar Hasil Evaluasi Peserta yang Belum Dijadwalkan</h4>
        </div><!-- /.box-header -->
        <div class="box-body">
           @if (Session::has('flash_notification.message'))
              <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('flash_notification.message') }}
              </div>
          @endif
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-condensed" style="font-size:12px;">
              <thead>
                  <tr class="bg-info">
                    <th class="text-center">No.</th>
                    <th class="text-center">No. Surat</th>
                    <th class="text-center">NIP</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Kode DJK</th>
                    <th class="text-center">Kode SKKNI</th>
                    <th class="text-center">Kompetensi</th>
                    <th class="text-center">Dijadwalkan?</th>
                  </tr>
              </thead>
              <tbody>
              <?php $no=1; ?>
              @forelse($data['peserta'] as $peserta)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $peserta->nosurat }}</td>
                <td>{{ $peserta->nip }}</td>
                <td>{{ $peserta->nama }}</td>
                <td>{{ $peserta->kodedjk }}</td>
                <td>{{ $peserta->kodeskkni }}</td>
                <td>{{ $peserta->nama_kompetensi }}</td>
                <td>{{ ($peserta->status_penjadwalan == 1) ? 'Sudah' : 'Belum' }}</td>
              </tr>
              @empty
              <tr>
                <td colspan="9" class="text-center">Tidak ada data peserta</td>
              </tr>
              @endforelse
              </tbody>
            </table>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop