@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-sm-10">
      <div class="box">
        <div class="box-header with-border">
          <div class="input-group">
            <label>Tanggal Uji:</label> {{ $data['penjadwalan']->tanggal_mulai }} s/d {{ $data['penjadwalan']->tanggal_selesai}}
          </div>
          <div class="input-group">
            <label>TUK:</label> {{ $data['penjadwalan']->udiklat->nama_udiklat }}
          </div>
          <div class="input-group">
            <label>LSK:</label> {{ $data['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}
          </div> 
          <div class="input-group">
            <label>Kompetensi:</label> {{ $data['kompetensi']->nama_kompetensi }}
          </div>
          <h3>Tambah Peserta</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr class="bg-info">
                  <th class="text-center">No.</th>
                  <th class="col-md-5 text-center">Nama</th>
                  <th class="col-md-3 text-center">Jabatan</th>
                  <th class="col-md-4 text-center">Unit Induk</th>
                </tr>
            </thead>
            <tbody>
               @if (Session::has('flash_notification.message'))
                  <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{ Session::get('flash_notification.message') }}
                  </div>
              @endif
              <form action="{{ route('dashboard.penjadwalan.peserta.store', ['id' => $data['penjadwalan']->id, 'kompetensiId' => $data['kompetensi']->id]) }}" method="post" id='pesertapenjadwalanform'>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="kompetensipenjadwalan_id" value="{{$data['kompetensipenjadwalanId']}}">
              @for($i = 1; $i < 11; $i++)
              	<tr>
	                <td>{{ $i}}</td>
	                <td>
	                  <div class="form-group">
                      <select class="selectpicker form-control peserta" title="-- Pilih Peserta --" data-live-search="true" name="peserta_id[]" id="{{ $i }}" autocomplete="off">
                        <option data-hidden="true"></option>
                        @foreach($data['peserta'] as $peserta)
                        <option value="{{ $peserta->peserta_id }}#{{$peserta->evaluasi_id}}" id="peserta_{{$i}}">({{ $peserta->nip }}) {{ $peserta->nama }}</option>
                        @endforeach
                      </select>
	                  </div>
	                </td>
	                <td id="jabatan_{{$i}}"></td>
	                <td id="unitinduk_{{$i}}"></td>
              </tr>
              @endfor
            </tbody>
          </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
	        <div class="col-md-offset-1">
	        	<button type="submit" class="btn btn-primary">Simpan</button>
	        	<a href="{{ route('dashboard.penjadwalan.peserta.index', ['id' => $data['penjadwalan']->id, 'kompetensiId' => $data['kompetensi']->id, 'kompetensipenjadwalanId' => $data['kompetensipenjadwalanId']]) }}" class="btn btn-warning">Kembali</a>
	        </div>
        </div>
        </form>
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop
@section('customjs')
<script type="text/javascript">
  var url = "{{ url('dashboard/api/peserta/detail', 'id') }}";
  $('select.peserta').change(function()
  {
    valId  = $(this).val();
    attrId = $(this).attr('id');
    if(valId) {
      urlWithId = url.replace('id', valId); 
      $.getJSON(urlWithId, function(response)
      {
        $('#jabatan_' + attrId).html(response.jabatan);
        $('#unitinduk_' + attrId).html(response.unitinduk.nama_unitinduk);
      });
      $('select.peserta').not(this).children('option[value=' + $(this).val() + ']').remove();
    } else {
      $('#jabatan_' + attrId).html('');
      $('#unitinduk' + attrId).html('');
    }
    $('select.peserta').selectpicker('refresh');
  });

</script>
@stop