@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="input-group">
            <label>Tanggal Uji:</label> {{ $data['penjadwalan']->tanggal_mulai }} s/d {{ $data['penjadwalan']->tanggal_selesai }}
          </div>
          <div class="input-group">
            <label>TUK:</label> {{ $data['penjadwalan']->udiklat->nama_udiklat }}
          </div>
          <div class="input-group">
            <label>LSK:</label> {{ $data['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}
          </div>
          <h3>Daftar Kompetensi Penjadwalan</h3>
          <div class="input-group">
          @if(Auth::user()->can('can_write_penjadwalan'))
            <a href="{{ route('dashboard.penjadwalan.kompetensi.create', ['id' => $data['penjadwalan']->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"> </i> Tambah</a> &nbsp;
          @endif
            <a href="{{ route('dashboard.penjadwalan.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"> </i> Kembali</a>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          @if (Session::has('flash_notification.message'))
              <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('flash_notification.message') }}
              </div>
          @endif
          <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr class="bg-info">
                  <th class="text-center">No.</th>
                  <th class="text-center">Bidang</th>
                  <th class="text-center">Sub Bidang</th>
                  <th class="text-center">Kompetensi</th>
                  <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1; ?>
            @forelse($data['penjadwalan']->kompetensi as $kompetensi)
            <tr>
              <td>{{ $no++ }}</td>
              <td>{{ $kompetensi->subbidang->bidang->nama_bidang}}</td>
              <td>{{ $kompetensi->subbidang->nama_subbidang}}</td>
              <td>{{ $kompetensi->nama_kompetensi }}</td>
              <td class="text-center">
              @if(Auth::user()->can('can_write_penjadwalan'))
                <a href="{{ route('dashboard.penjadwalan.kompetensi.delete', ['id' => $data['penjadwalan']->id, 'kompetensiId' => $kompetensi->id]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> </a>
              @else
                -
              @endif
              </td>
            </tr>
            @empty
            <tr>
              <td colspan="5" class="text-center">Tidak ada data kompetensi</td>
            </tr>
            @endforelse
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop