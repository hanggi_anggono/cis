@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-sm-8">
	  <div class="box box-primary">
	  		<div class="box-header with-border">
	  			<h3>Edit Penjadwalan</h3>
	  		</div>
	    <form class="form-horizontal" action="{{ route('dashboard.penjadwalan.update', ['id' => $data['penjadwalan']->id]) }}" method="post" accept-charset="utf-8" id="penjadwalan">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	    <input type="hidden" name="_method" value="put">
	      <div class="box-body">
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Tgl. Mulai</label>
	          <div class="col-sm-6">
	              <div class="input-group">
	                <div class="input-group-addon">
	                  <i class="fa fa-calendar"></i>
	                </div>
	                <input type="text" class="form-control pull-right" id="tanggalmulai" name="tanggal_mulai" value="{{ $data['penjadwalan']->tanggal_mulai }}" placeholder="Tanggal mulai pelaksanaan" required/>
	              </div><!-- /.input group -->
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Tgl. Selesai</label>
	          <div class="col-sm-6">
	              <div class="input-group">
	                <div class="input-group-addon">
	                  <i class="fa fa-calendar"></i>
	                </div>
	                <input type="text" class="form-control pull-right" id="tanggalselesai" name="tanggal_selesai" value="{{ $data['penjadwalan']->tanggal_selesai }}" placeholder="Tanggal selesai pelaksanaan" required/>
	              </div><!-- /.input group -->
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">TUK</label>
	           <div class="col-sm-6">
	              {!! Form::select('udiklat_id', ['' => '-- Pilih LSK --'] + $data['support']['udiklat'], $data['penjadwalan']->udiklat_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'udiklat', 'autocomplete' => 'off'])!!}
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Unit Induk</label>
	           <div class="col-sm-6">
	           
	              {!! Form::select('unitinduk_id', ['' => '-- Pilih Unit Induk --'] + $data['support']['uinduk'], $data['penjadwalan']->unitinduk_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitinduk_id', 'autocomplete' => 'off'])!!}
	          </div>
	        </div>
	         <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Unit Cabang</label>
	           <div class="col-sm-6">
	              {!! Form::select('unitcabang_id', ['' => '-- Pilih Unit Cabang --'] + $data['support']['ucabang'], $data['penjadwalan']->unitcabang_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitcabang', 'autocomplete' => 'off'])!!}
	          </div>
	        </div>
	         <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Unit Ranting</label>
	           <div class="col-sm-6">
	              {!! Form::select('unitranting_id', ['' => '-- Pilih Unit Ranting --'] + $data['support']['uranting'], $data['penjadwalan']->unitranting_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitranting', 'autocomplete' => 'off'])!!}
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Subbidang Kompetensi</label>
	          <div class="col-sm-6">
	              {!! Form::select('subbidang_id', ['' => '-- Pilih Subbidang Kompetensi --'] + $data['support']['subbidang'], $data['penjadwalan']->subbidang_id, ['class' => 'selectpicker form-control', 'data-live-search' => 'true', 'required' => 'required', 'autocomplete' => 'off', 'id' => 'subbidang_id'])!!}
	          </div>
	        </div>
	         <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">LSK</label>
	          <div class="col-sm-6">
	              {!! Form::select('lembagasertifikasi_id', $data['support']['lembagasertifikasi'], $data['penjadwalan']->lembagasertifikasi_id , ['class' => 'selectpicker form-control', 'data-live-search' => 'true', 'autocomplete' => 'off', 'id' => 'lsk_id'])!!}
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Administrator</label>
	          <div class="col-sm-6">
	             {!! Form::select('administrator_id', $data['support']['administrator'], $data['penjadwalan']->administrator_id, ['class' => 'selectpicker form-control administrator', 'data-live-search' => 'true', 'autocomplete' => 'off'])!!}
	          </div>
	        </div>
	        <?php $index=0; ?>
	        @forelse($data['penjadwalan']->asesors as $penjadwalanasesor)
	        <?php $index++; ?>
	         <div class="form-group">
	         	<label for="nip" class="col-sm-3 control-label">Asesor {{ $penjadwalanasesor->pivot->posisi}}</label>
	         	<div class="col-sm-6">
		           {!! Form::select(($penjadwalanasesor->pivot->posisi == 'ketua') ? 'ketua' : 'anggota[]', $data['support']['asesor'], $penjadwalanasesor->id, ['class' => 'selectpicker form-control asesor', 'data-live-search' => 'true', 'autocomplete' => 'off'])!!}
	         	</div>
	        </div>
	        @if($data['penjadwalan']->asesors->count() == 1)
				<div class="form-group">
		          <label for="nip" class="col-sm-3 control-label">Asesor Anggota</label>
		          <div class="col-sm-6">
		              {!! Form::select('anggota[]', ['' => ' -- Pilih Anggota Asesor  (Opsional) --'] + $data['support']['asesor'], null, ['class' => 'selectpicker form-control asesor', 'data-live-search' => 'true', 'autocomplete' => 'off'])!!}
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-3 control-label">Asesor Anggota</label>
		          <div class="col-sm-6">
		              {!! Form::select('anggota[]', ['' => ' -- Pilih Anggota Asesor  (Opsional) --'] + $data['support']['asesor'], null, ['class' => 'selectpicker form-control asesor', 'data-live-search' => 'true',  'autocomplete' => 'off'])!!}
		          </div>
		        </div>
		    @elseif($data['penjadwalan']->asesors->count() == 2 && $index == 2)
		    	<div class="form-group">
		          <label for="nip" class="col-sm-3 control-label">Asesor Anggota</label>
		          <div class="col-sm-6">
		              {!! Form::select('anggota[]', ['' => ' -- Pilih Anggota Asesor  (Opsional) --'] + $data['support']['asesor'], null, ['class' => 'selectpicker form-control asesor', 'data-live-search' => 'true',  'autocomplete' => 'off'])!!}
		          </div>
		        </div>
	        @endif
	        @empty
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Asesor Ketua</label>
	          <div class="col-sm-6">
	             {!! Form::select('ketua', ['' => ' -- Pilih Ketua Asesor --'] + $data['support']['asesor'], null, ['class' => 'selectpicker form-control asesor', 'data-live-search' => 'true', 'required' => 'required', 'autocomplete' => 'off'])!!}
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Anggota Asesor 1</label>
	          <div class="col-sm-6">
	              {!! Form::select('anggota[]', ['' => ' -- Pilih Anggota Asesor --'] + $data['support']['asesor'], null, ['class' => 'selectpicker form-control asesor', 'data-live-search' => 'true', 'autocomplete' => 'off'])!!}
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Anggota Asesor 2</label>
	          <div class="col-sm-6">
	              {!! Form::select('anggota[]', ['' => ' -- Pilih Anggota Asesor --'] + $data['support']['asesor'], null, ['class' => 'selectpicker form-control asesor', 'data-live-search' => 'true',  'autocomplete' => 'off'])!!}
	          </div>
	        </div>
	        @endforelse
	      </div><!-- /.box-body -->
	      <div class="box-footer">
	      	<div class="col-md-offset-3">
	      		<button type="submit" class="btn btn-primary">Update</button>&nbsp;
		        <a href="{{ route('dashboard.penjadwalan.index') }}" class="btn btn-warning">Kembali</a>
	      	</div>
	      </div>
	    </form>
	  </div><!-- /.box -->
	</div>
	</div><!-- /.row -->
@stop
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
$(document).ready(function()
{
	$('#tanggalmulai').datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true
	});
	$('#tanggalselesai').datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true
	});

	var form = $('form#penjadwalan'),
	tuk        = form.find('select#udiklat'),
    uinduk     = form.find('select#unitinduk_id'),
    ucabang    = form.find('select#unitcabang'),
    uranting   = form.find('select#unitranting'),
    subbidang  = form.find('select#subbidang_id'),
    lsk        = form.find('select#lsk_id'),
    asesors    = form.find('select.asesor'),
    administrators = form.find('select.administrator'),
    urldiklat  = "{{ url('dashboard/master/unit/diklat/api/induk', 'id') }}",
    url        = "{{ url('dashboard/master/unit/induk/api/cabang', 'id') }}",
    urlranting = "{{ url('dashboard/master/unit/cabang/api/ranting', 'id') }}",
    urllsk     = "{{ url('dashboard/api/asesor/kelompok', ['lskid', 'subbidangId']) }}";
    urllsk2    = "{{ url('dashboard/api/administrator/kelompok', 'lskid') }}";
    
    tuk.change(function() {
      urlWithId = urldiklat.replace('id', tuk.val());  
      var options = '';
      $.getJSON(urlWithId, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="">-- Pilih Unit Induk --</option>';
          $.each(data,function(index,value){
            options += '<option value="'+index+'">'+value+'</option>';
          });
          uinduk.removeAttr('disabled');
          ucabang.removeAttr('disabled');
          uranting.removeAttr('disabled');
          uinduk.html(options).selectpicker('refresh');
        } else {
          uinduk.html('');
          ucabang.html('');
          uranting.html('');
          uinduk.attr('disabled', true).selectpicker('refresh');
          ucabang.attr('disabled',true).selectpicker('refresh');
          uranting.attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });

    uinduk.change(function() {
      urlWithId = url.replace('id', uinduk.val());  
      var options = '';
      $.getJSON(urlWithId, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="">-- Pilih Unit Cabang --</option>';
          $.each(data,function(index,value){
            options += '<option value="'+index+'">'+value+'</option>';
          });
          ucabang.removeAttr('disabled');
          uranting.removeAttr('disabled');
          ucabang.html(options).selectpicker('refresh');
        } else {
          ucabang.html('');
          uranting.html();
          ucabang.attr('disabled',true).selectpicker('refresh');
          uranting.attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });
    ucabang.change(function() {
      urlrantingWithId = urlranting.replace('id', ucabang.val()); 
      var options = '';
      $.getJSON(urlrantingWithId, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="">-- Pilih Unit Ranting --</option>';
            $.each(data,function(index,value){
                options += '<option value="'+index+'">'+value+'</option>';
            });
            uranting.removeAttr('disabled');
            uranting.html(options).selectpicker('refresh');
        } else {
          uranting.html('');
          uranting.attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });

    lsk.change(function() {
      urlkelompok = urllsk.replace('lskid', lsk.val()).replace('subbidangId', subbidang.val());
      var options = '';
      $.getJSON(urlkelompok, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="">-- Pilih Asesor --</option>';
            $.each(data,function(index,value){
                options += '<option value="'+index+'">'+value+'</option>';
            });
            asesors.removeAttr('disabled').html(options).selectpicker('refresh');
        } else {
          asesors.html('').attr('disabled',true).selectpicker('refresh');
        }
        
      });

      urladministrator = urllsk2.replace('lskid', lsk.val()); 
      var options2 = '';
      $.getJSON(urladministrator, function(data){
        if(! $.isEmptyObject(data)) {
          options2 += '<option value="">-- Pilih Administrator --</option>';
            $.each(data,function(index,value){
                options2 += '<option value="'+index+'">'+value+'</option>';
            });
            administrators.removeAttr('disabled').html(options2).selectpicker('refresh');
        } else {
          administrators.html('').attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });

    // remove another item in select asesor
    // $('select.asesor').change(function()
    // {
    // 	$('select.asesor').not(this).children('option[value=' + $(this).val() + ']').remove();
    // 	$('select.asesor').selectpicker('refresh');
    // })
});
</script>
@stop