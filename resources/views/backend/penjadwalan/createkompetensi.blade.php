@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="input-group">
            <label>Tanggal Uji:</label> {{ $data['penjadwalan']->tanggal_mulai }} s/d {{ $data['penjadwalan']->tanggal_selesai }}
          </div>
          <div class="input-group">
            <label>TUK:</label> {{ $data['penjadwalan']->udiklat->nama_udiklat }}
          </div>
          <div class="input-group">
            <label>LSK:</label> {{ $data['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}
          </div>
          <h3>Tambah Kompetensi</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
          <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr class="bg-info">
                  <th class="text-center">No.</th>
                  <th class="col-sm-6 text-center">Kompetensi</th>
                  <th class="col-sm-3 text-center">Bidang</th>
                  <th class="col-sm-3 text-center">Sub Bidang</th>
                </tr>
            </thead>
            <tbody>
              <form action="{{ route('dashboard.penjadwalan.kompetensi.store', ['id' => $data['penjadwalan']->id]) }}" method="post" id='kompetensipenjadwalanform'>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              @for($i = 1; $i < 11; $i++)
              	<tr>
	                <td>{{ $i }}</td>
	                <td>
	                  <div class="form-group">
	                     <select class="selectpicker form-control kompetensi" title="-- Pilih Kompetensi --" data-live-search="true" name="kompetensi_id[]" id="{{$i}}" autocomplete="off">
                        <option data-hidden="true"></option>
                        @foreach($data['kompetensi'] as $kompetensi)
                        <option value="{{ $kompetensi->kompetensi_id }}" id="kompetensi_{{$i}}">{{ $kompetensi->nama_kompetensi }}</option> 
                        @endforeach
                       </select>
	                  </div>
	                </td>
	                <td id="bidang_{{$i}}">
	                </td>
	                <td id="subbidang_{{$i}}">
	                </td>
	              </tr> 
              @endfor
            </tbody>
          </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
        	<div class="col-md-offset-1">
        		<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
        		<a href="{{ route('dashboard.penjadwalan.kompetensi.index', ['id' => $data['penjadwalan']->id]) }}" class="btn btn-warning">Kembali</a>
        	</div>
        </div>
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop
@section('customjs')
<script type="text/javascript">
$(document).ready(function()
{
  $('select.kompetensi').selectpicker('refresh');
  var url = "{{ url('dashboard/api/kompetensi/kompetensi_id/belongstobidang') }}"
  $('select.kompetensi').change(function()
  {
    valId = $(this).val();
    if(valId) {
      urlWithId = url.replace('kompetensi_id', valId); 
      attrId    = $(this).attr('id');
      $.getJSON(urlWithId, function(response)
      {
        $('#bidang_' + attrId).html(response.nama_bidang);
        $('#subbidang_' + attrId).html(response.nama_subbidang);
      });
      $('select.kompetensi').not(this).children('option[value=' + $(this).val() + ']').remove();
    } else {
      $('#bidang' + attrId).html('');
      $('#subbidang' + attrId).html('');
    }
    $('select.kompetensi').selectpicker('refresh');
  });
});  
</script>
@stop