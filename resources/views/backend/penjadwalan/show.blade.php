@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-sm-6">
	  <div class="box box-primary">
	  		<div class="box-header with-border">
	  			<h3>Detail Penjadwalan</h3>
	  		</div>
	    <form class="form-horizontal" action="#" method="post" accept-charset="utf-8">
	      <div class="box-body">
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Tgl. Mulai</label>
	          <div class="col-sm-6">
	              <div class="input-group">
	                <div class="input-group-addon">
	                  <i class="fa fa-calendar"></i>
	                </div>
	                <input type="text" class="form-control pull-right" id="tanggalmulai" value="{{ $data['penjadwalan']->tanggal_mulai }}" placeholder="Tanggal mulai pelaksanaan" readonly="readonly" />
	              </div><!-- /.input group -->
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Tgl. Selesai</label>
	          <div class="col-sm-6">
	              <div class="input-group">
	                <div class="input-group-addon">
	                  <i class="fa fa-calendar"></i>
	                </div>
	                <input type="text" class="form-control pull-right" id="tanggalselesai" value="{{ $data['penjadwalan']->tanggal_selesai }}" placeholder="Tanggal selesai pelaksanaan" readonly="readonly" />
	              </div><!-- /.input group -->
	          </div>
	        </div>
	         <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">TUK</label>
	          <div class="col-sm-6">
	              <input type="text" class="form-control" placeholder="TUK NIP / Nama ..." value="{{ $data['penjadwalan']->udiklat->nama_udiklat }}" readonly="readonly" >
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Unit Induk</label>
	          <div class="col-sm-6">
	              <input type="text" class="form-control" placeholder="TUK NIP / Nama ..." value="{{ $data['penjadwalan']->unitinduk->nama_unitinduk }}" readonly="readonly" >
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Unit Cabang</label>
	          <div class="col-sm-6">
	              <input type="text" class="form-control" placeholder="TUK NIP / Nama ..." value="{{ (! empty($data['penjadwalan']->unitcabang)) ? $data['penjadwalan']->unitcabang->nama_unitcabang : '-' }}" readonly="readonly" >
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Unit Ranting</label>
	          <div class="col-sm-6">
	              <input type="text" class="form-control" placeholder="TUK NIP / Nama ..." value="{{ (! empty($data['penjadwalan']->unitranting)) ? $data['penjadwalan']->unitranting->nama_unitranting : '-' }}" readonly="readonly" >
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Subbidang Kompetensi</label>
	          <div class="col-sm-6">
	              <input type="text" class="form-control" value="{{ (! empty($data['penjadwalan']->subbidang->nama_subbidang)) ? $data['penjadwalan']->subbidang->nama_subbidang : '-' }}" readonly="readonly" >
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">LSK</label>
	          <div class="col-sm-6">
	              <input type="text" class="form-control" placeholder="TUK NIP / Nama ..." value="{{ $data['penjadwalan']->lembagasertifikasi->nama_lembagasertifikasi }}" readonly="readonly" >
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Administrator</label>
	          <div class="col-sm-6">
	              <input type="text" class="form-control" placeholder="Administrator NIP / Nama ..." value="{{ $data['penjadwalan']->administrator->nama }}" readonly="readonly" >
	          </div>
	        </div>
	        @forelse($data['penjadwalan']->asesors as $penjadwalanasesor)
	        <div class="form-group">
	          <label for="nip" class="col-sm-3 control-label">Asesor {{ $penjadwalanasesor->pivot->posisi }}</label>
	          <div class="col-sm-6">
	              <input type="text" class="form-control" placeholder="Administrator NIP / Nama ..." value="{{ $penjadwalanasesor->nama }}" readonly="readonly" >
	          </div>
	        </div>
	        @empty
	        <!-- let it empty -->
	        @endforelse
	      </div><!-- /.box-body -->
	    </form>
	  </div><!-- /.box -->
	</div>
	<div class="col-md-6">
		<div class="box box-primary">
	  		<div class="box-header with-border">
	  			<h3>Kompetensi & Peserta</h3>
	  		</div>
	        <div class="box-body">
	        	<table class="table table-condensed table-striped">
	        		<thead>
	        			<tr class="bg-info">
	        				<th>No.</th>
	        				<th>Kompetensi</th>
	        				<th>Jumlah Peserta</th>
	        			</tr>
	        		</thead>
	        		<tbody>
	        		<?php $no=1; ?>
	        		@forelse($data['kompetensipenjadwalan'] as $kompetensipenjadwalan)
	        		<tr>
	        			<td>{{ $no++ }}</td>
	        			<td>{{ $kompetensipenjadwalan->kompetensi->nama_kompetensi }}</td>
	        			<td>{{ $kompetensipenjadwalan->peserta->count() }} Peserta</td>
	        		</tr>
	        		@empty
	        		<tr>
	        			<td colspan="3" class="text-center">Tidak ada data kompetensi</td>
	        		</tr>
	        		@endforelse
	        		</tbody>
	        	</table>
	        </div>
	        <div class="box-footer text-center">
	        	<a href="{{ route('dashboard.penjadwalan.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
	        </div>
	    </div>    
	</div>
</div><!-- /.row -->
@stop