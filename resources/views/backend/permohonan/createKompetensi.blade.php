@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-lg-12">
	  <div class="box">
	    <div class="box-header">
	     <div class="input-group">
            <label>No. Surat:</label> {{$data['permohonan']->nosurat}}
          </div>
          <div class="input-group">
            <label>Unit Induk:</label> {{$data['permohonan']->unitinduk->nama_unitinduk }}
          </div>
          <h3>Tambah Kompetensi Permohonan</h3>
	    </div><!-- /.box-header -->
	    <div class="box-body">
	    @if (Session::has('flash_notification.message'))
		    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        {{ Session::get('flash_notification.message') }}
		    </div>
		@endif
	      <table class="table table-striped table-bordered table-hover">
	        <thead>
	            <tr class="bg-info">
	              <th class="text-center">No.</th>
	              <th class="col-md-3 text-center">Bidang</th>
	              <th class="col-md-3 text-center">Sub Bidang</th>
	              <th class="col-md-6 text-center">Kompetensi</th>
	            </tr>
	        </thead>
	        <form class="form-control" action="{{ route('dashboard.permohonan.kompetensi.store', ['id' => $data['permohonan']->id]) }}" method="post">
	        <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <tbody>
	        @for($i=1; $i < 11; $i++)
	        <tr>
	        	<td>{{ $i }}</td>
	        	<td>
	        		<div class="form-group">
	        			<div class="col-sm-10">
	        				{!! Form::select(null, ['0' => '-- Pilih Bidang --'] + $data['bidang'], '', ['class' => 'selectpicker form-control bidang', 'data-live-search' => 'true', 'id' => $i, 'autocomplete' => 'off']) !!}
	        			</div>
	        		</div>
	        	</td>
	        	<td>
	        		<div class="form-group">
	        			<div class="col-sm-10">
	        				{!! Form::select(null, [], '', ['class' => 'selectpicker form-control subbidang', 'data-live-search' => 'true', 'disabled' => 'true', 'id' => 'subbidang_'.$i, 'seq' => $i, 'autocomplete' => 'off']) !!}
	        			</div>
	        		</div>
	        	</td>
	        	<td>
	        		<div class="form-group">
	        			<div class="col-sm-12">
	        				<?php $data['kompetensi'] = array_add($data['kompetensi'], '',''); ?>
	        				{!! Form::select('kompetensi_id[]', $data['kompetensi'], '', ['class' => 'selectpicker form-control', 'data-live-search' => 'true', 'disabled' => 'true', 'id' => 'kompetensi_'.$i, 'autocomplete' => 'off']) !!}
	        			</div>
	        		</div>
	        	</td>
	        </tr>
	        @endfor
	        </tbody>
	      </table>
	    </div><!-- /.box-body -->
	    <div class="box-footer">
	    	<div class="col-sm-4">
	    	   <button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		       <a href="{{ route('dashboard.permohonan.kompetensi.index', ['id' => $data['permohonan']->id]) }}" class="btn btn-warning">Kembali</a>
	    	</div>
	    </div>
	    </form>
	  </div><!-- /.box -->
	</div>
	</div><!-- /.row -->
@stop
@section('customjs')
<script type="text/javascript">
$(document).ready(function()
{
	urlSubbidang  = "{{ route('api.get.subbidang.byBidang', 'bidang_id') }}";
	urlKompetensi = "{{ route('api.get.kompetensi.getBySubbidang', 'subbidang_id') }}";
	
	$('select.bidang').change(function(event)
	{
		event.preventDefault();

		idAttr      = $(this).attr('id');
		urlWithId   = urlSubbidang.replace('bidang_id', $(this).val());  
		var options = '';

		$.getJSON(urlWithId, function(data){
	        if(! $.isEmptyObject(data)) {
	          options += '<option value="0">-- Pilih Sub Bidang --</option>';
	          $.each(data,function(index,value){
	            options += '<option value="'+index+'">'+value+'</option>';
	          });

	          $('#subbidang_' + idAttr).html(options).removeAttr('disabled').selectpicker('refresh');
	        } else {

	          $('#subbidang_' + idAttr).html('').attr('disabled', true).selectpicker('refresh');
	          $('#kompetensi_' + idAttr).html('').attr('disabled', true).selectpicker('refresh');
	        }
        
      });
	});

	$('select.subbidang').change(function(event)
	{
		event.preventDefault();

		idAttr2      = $(this).attr('seq');
		subbidang_id = $(this).val();
		urlWithId    = urlKompetensi.replace('subbidang_id', subbidang_id);  
		var options  = '';

		$.getJSON(urlWithId, function(data){
	        if(! $.isEmptyObject(data)) {
	          options += '<option value="0">-- Pilih Kompetensi --</option>';
	          $.each(data,function(index,value){
	            options += '<option value="'+index+'">'+value+'</option>';
	          });
	          
	          $('#kompetensi_' + idAttr2).html(options).removeAttr('disabled').selectpicker('refresh');
	        } else {
	          $('#kompetensi_' + idAttr2).html('').attr('disabled',true).selectpicker('refresh');
	        }
      });
		
	});
});
</script>
@stop
