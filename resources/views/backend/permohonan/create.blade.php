@extends('layouts.backend.master')
@section('content')
<div class="row">
<div class="col-md-8">
  <div class="box box-primary">
  	<div class="box-header with-border">
     <h3>Form Tambah Permohonan</h3>
    </div><!-- /.box-header -->
      <div class="box-body">
      	<form class="form-horizontal" action="{{ route('dashboard.permohonan.store') }}" method="post" accept-charset="utf-8">
      	<input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">No. Surat</label>
          <div class="col-sm-8">
              <input type="text" class="form-control" name="nosurat" placeholder="Nomor Surat Permohonan" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tanggal surat</label>
          <div class="col-sm-8">
              <input type="text" class="form-control" name="tanggal_surat" id="tanggal_surat" placeholder="Tanggal Surat Permohonan">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Induk</label>
          <div class="col-sm-8">
              {!! Form::select('unitinduk_id', $data['unitinduk'], '', ['class' => 'selectpicker form-control', 'data-live-search' => 'true', 'autocomplete' => 'off']) !!}
          </div>
        </div>
        <div class="form-group">
          <label for="keterangan" class="col-sm-3 control-label">Keterangan</label>
          <div class="col-sm-8">
              <textarea class="form-control" name="keterangan"></textarea>
          </div>
        </div>
      </div><!-- /.box-body -->
      <div class="box-footer">
      	<div class="col-sm-4 col-md-offset-3">
	        <button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
	        <a href="{{ route('dashboard.permohonan.index') }}" class="btn btn-warning">Kembali</a>
      	</div>
      </div>
    </form>
  </div><!-- /.box -->
</div>
</div><!-- /.row -->
@stop
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
	$('#tanggal_surat').datepicker({
		format: 'yyyy-mm-dd',
    autoclose: true
	});
</script>
@stop  