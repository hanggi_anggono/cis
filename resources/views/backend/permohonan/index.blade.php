@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
	  <div class="box">
	  @if(Auth::user()->can('can_write_permohonan'))
	    <div class="box-header">
	      <div class="input-group">
	        <a href="{{ route('dashboard.permohonan.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Permohonan</a>
	      </div>
	    </div><!-- /.box-header -->
	    @endif
	    <div class="box-body">
	    @if (Session::has('flash_notification.message'))
		    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        {{ Session::get('flash_notification.message') }}
		    </div>
		@endif
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover" style="font-size:12px;">
	        <thead>
	            <tr class="bg-info">
	              <th class="text-center">No.</th>
	              <th class="text-center">No. Surat</th>
	              <th class="text-center">Tgl. Surat</th>
	              <th class="text-center">Unit Induk</th>
	              <th class="text-center">Kompetensi</th>
	              <th class="text-center">Jumlah Peserta</th>
	              <th class="col-md-3 text-center">Keterangan</th>
	              <th class="col-md-1 text-center">Aksi</th>
	            </tr>
	        </thead>
	        <tbody>
	          @forelse($data['permohonan'] as $permohonan)
	          <tr>
	          	<td>{{ $data['pagination_number'] }}</td>
	          	<td><a href="{{ route('dashboard.permohonan.kompetensi.index', ['id' => $permohonan->id]) }}"> {{ $permohonan->nosurat }}</a></td>
	          	<td>{{ $permohonan->tanggal_surat }}</td>
	          	<td>{{ $permohonan->unitinduk->nama_unitinduk }}</td>
	          	<td style="white-space:nowrap;">
	          		@forelse($permohonan->kompetensi as $kompetensi)
		          		- {{$kompetensi->nama_kompetensi}}<br>
	          		@empty
	          		-
	          		@endforelse
	          	</td>
	          	<td class="text-center">
	          		@forelse($permohonan->kompetensi as $kompetensi)
	          			@forelse($data['peserta'] as $peserta)
	          				@if($kompetensi->id == $peserta->kompetensi_id && $permohonan->id == $peserta->permohonan_id)
			          			<a href="{{ route('dashboard.permohonan.kompetensi.peserta.index', ['id' => $permohonan->id, 'kompetensiid' => $kompetensi->id]) }}">{{ $peserta->peserta->count() }}</a><br>
	          				@endif
	          			@empty
	          			-
	          			@endforelse
	          		@empty
	          		-
	          		@endforelse
	          	</td>
	          	<td>{{ $permohonan->keterangan }}</td>
	          	<td class="text-center">
                  {{-- <a href="{{ route('dashboard.permohonan.show',['id' => $permohonan->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a> --}}
                @if(Auth::user()->can('can_write_permohonan'))
                  <a href="{{ route('dashboard.permohonan.edit',['id' => $permohonan->id]) }}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> </a>
                  <a href="{{ route('dashboard.permohonan.delete',['id' => $permohonan->id]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> </a>
                @else
                	-
                @endif
                </td>
	          </tr>
	          <?php $data['pagination_number']++; ?>
	          @empty
	          <tr>
	          	<td colspan="8" class="text-center"> Tidak ada data permohonan</td>
	          </tr>
	          @endforelse
	        </tbody>
	      </table>	
		</div>
	      <br>
	      <div class="text-center">
	          {!! paginationHelper($data['permohonan']->render()) !!}
	      </div>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
</div><!-- /.row -->
@stop