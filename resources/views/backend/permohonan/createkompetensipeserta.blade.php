@extends('layouts.backend.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
      <div class="box">
        <div class="box-header">
          <div class="input-group">
            <label>No. Surat:</label> {{ $data['permohonan']->nosurat }}
          </div>
          <div class="input-group">
            <label>Unit Induk:</label> {{ $data['permohonan']->unitinduk->nama_unitinduk }}
          </div>
          <div class="input-group">
            <label>Kompetensi:</label> {{ $data['kompetensi']->nama_kompetensi }}
          </div> <br>
          <h3>Form Tambah Peserta</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          @if (Session::has('flash_notification.message'))
              <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('flash_notification.message') }}
              </div>
          @endif
          <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr class="bg-info">
                  <th class="col-md-1 text-center">No.</th>
                  <th class="text-center">NIP</th>
                  <th class="text-center">Jabatan</th>
                  <th class="text-center">Unit Induk</th>
                </tr>
            </thead>
            <tbody>
            <form class="form-horizontal" method="post" id="pesertapermohonan" action="{{ route('dashboard.permohonan.kompetensi.peserta.store', ['id' => $data['permohonan']->id, 'kompetensiid' => $data['kompetensi']->id]) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="kompetensipermohonan_id" value="{{ $data['kompetensipermohonan_id'] }}">
              @for($i = 1; $i < 11; $i++)
              <tr>
              	<td>{{ $i }}</td>
              	<td>
              		<div class="form-group">
              			<div class="col-sm-10">
                      <select class="selectpicker form-control peserta" title="-- Pilih Peserta --" data-live-search="true" name="peserta_id[]" id="{{ $i }}" autocomplete="off">
                        <option data-hidden="true"></option>
                        @foreach($data['peserta'] as $peserta)
                        <option value="{{ $peserta->id }}" id="peserta_{{$i}}">({{ $peserta->nip }}) {{ $peserta->nama }}</option>
                        @endforeach
                      </select>
              			</div>
              		</div>
              	</td>
              	<td id="jabatan_peserta_{{$i}}"></td>
              	<td id="unitinduk_{{$i}}"></td>
              </tr>
              @endfor
            </tbody>
          </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
        	<div class="col-sm-4 col-md-offset-1">
        		<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
	        	<a href="{{ route('dashboard.permohonan.index') }}" class="btn btn-warning">Kembali</a>
        	</div>
        </div>
        </form>
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
@stop
@section('customjs')
<script type="text/javascript">
	var url = "{{ url('dashboard/api/peserta/detail', 'id') }}"
	$('select.peserta').change(function()
	{
		valId = $(this).val();
		if(valId) {
      urlWithId = url.replace('id', $(this).val());	
      attrId    = $(this).attr('id');
			$.getJSON(urlWithId, function(response)
			{
				$('#jabatan_peserta_' + attrId).html(response.jabatan);
				$('#unitinduk_' + attrId).html(response.unitinduk.nama_unitinduk);
			});
		} else {
			$('#jabatan_peserta_' + attrId).html('');
			$('#unitinduk' + attrId).html('');
		}
		
	});
</script>
@stop