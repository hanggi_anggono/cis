@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
	<div class="col-md-7">
		 <div class="box box-primary">
		 	<form class="form-horizontal">
		      <div class="box-body">
		      	<div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Unit Kompetensi</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ $data['register']->kompetensi->nama_kompetensi  }}</p>
		          </div>
		        </div>
		      	<div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Tipe register</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ($data['register']->tipe_peserta == 'pln') ? 'PLN' : 'NON PLN' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">NIP</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ $data['register']->nip }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">No Identitas</label>
		          <div class="col-sm-7">
		             <p class="form-control-static">{{ $data['register']->no_identitas }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Nama</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ $data['register']->nama }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Jenis Kelamin</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">
		               	@if($data['register']->jenis_kelamin == 'm')
		               		Laki - laki
		               	@else
		               		Perempuan
		               	@endif
		               </p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Gol Darah</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['register']->golongan_darah) ? $data['register']->golongan_darah : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">TTL</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['register']->tempat_lahir) ? $data['register']->tempat_lahir : '-' }}, {{ parseDate($data['register']->tanggal_lahir) }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Alamat sesuai identitas</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['register']->alamat) ? $data['register']->alamat : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Kewarganegaraan</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['register']->kewarganegaraan) ? $data['register']->kewarganegaraan : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="email" class="col-sm-4 control-label">Email</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['register']->email) ? $data['register']->email : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">No. Handphone</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['register']->no_hp) ? $data['register']->no_hp : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Telepon Rumah</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['register']->no_telp) ? $data['register']->no_telp : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Kondisi Khusus?</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ($data['register']->kondisi_khusus == 'y') ? 'Ya' : 'Tidak' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Keterangan Kondisi Khusus</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ (! empty($data['register']->keterangan_kondisi_khusus)) ? $data['register']->keterangan_kondisi_khusus : '-' }}</p>
		          </div>
		        </div>
		     </div>   
		    </form>    
		 </div>
	</div>
	 <div class="col-md-5">
	 	<div class="box box-primary">
	 		<div class="box-body">
	 			<form class="form-horizontal">
	 				<div class="text-center">
	 					@if(! empty($data['register']->photo))
	 					<img src="{{ asset('resources/register/'.$data['register']->photo) }}" width="100" height="100">
	 					@else
	 						<img src="{{ asset('assets/dist/img/nophoto.jpg') }}" width="100" height="100">
	 					@endif
	 				</div><br>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Pendidikan</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['register']->pendidikan->nama_pendidikan }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Jurusan</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['register']->jurusan }}</p>
			          </div>
			        </div>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Jenjang Jabatan</label>
			          <div class="col-sm-5">
			              <p class="form-control-static">{{ $data['register']->jenjangjabatan->nama_jenjangjabatan }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Jabatan</label>
			          <div class="col-sm-7">
			               <p class="form-control-static">{{ ! empty($data['register']->jabatan) ? $data['register']->jabatan : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Grade</label>
			          <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['register']->grade->nama_grade }}</p>
			          </div>
			        </div>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Induk</label>
			           <div class="col-sm-5">
			              <p class="form-control-static">{{ (! is_null($data['register']->unitinduk)) ? $data['register']->unitinduk->nama_unitinduk : '-'}}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Cabang</label>
			          <div class="col-sm-5">
			              <p class="form-control-static">{{ (! is_null($data['register']->unitcabang)) ? $data['register']->unitcabang->nama_unitcabang : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Ranting</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ (! is_null($data['register']->unitranting)) ? $data['register']->unitranting->nama_unitranting : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Non PLN</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ (! is_null($data['register']->unitnonpln)) ? $data['register']->unitnonpln->nama_unitnonpln : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Dibuat pada</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ parseDate($data['register']->created_at, 'datetime') }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Dirubah pada</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ parseDate($data['register']->updated_at, 'datetime') }}</p>
			          </div>
			        </div>
			 	</form>
			 	<div class="box-footer text-center">
			 		<a href="{{ route('dashboard.registration.index') }}" class="btn btn-info btn-sm"><i class="fa fa-reply"> </i> Kembali</a>&nbsp;
			 	</div>
	 		</div>
	 	</div>
	 </div>
</div>
@stop