@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
	  <div class="box box-primary">
	  	<div class="box-header with-border">
	  		<form class="form-inline pull-right" method="get" action="{{ route('dashboard.registration.index') }}">
		      	<div class="form-group">
		      		<input type="text" class="form-control" name="name" value="{{ Request::get('name') }}" placeholder="cari nip / nama">
		      		<button type="input" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
		      	</div>
		      </form>
	  	</div>
	    <div class="box-body">
	    @if (Session::has('flash_notification.message'))
		    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        {{ Session::get('flash_notification.message') }}
		    </div>
		@endif
			<div class="table-responsive">
				<table class="table table-striped table-condensed table-bordered">
					<thead class="bg-info">
						<tr>
							<th>No.</th>
							<th>NIP</th>
							<th>Nama Lengkap</th>
							<th>Unit Induk</th>
							<th>Email</th>
							<th>Telepon</th>
							<th>Kompetensi</th>
							<th>Tanggal</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						@forelse($data['registers'] as $register)
						<tr>
							<td>{{ $data['pagination']++ }}</td>
							<td>{{ $register->nip }}</td>
							<td>{{ $register->nama }}</td>
							<td>{{ $register->unitinduk->nama_unitinduk }}</td>
							<td>{{ $register->email }}</td>
							<td>{{ $register->no_hp }}</td>
							<td>{{ $register->kompetensi->nama_kompetensi }}</td>
							<td>{{ $register->created_at }}</td>
							<td class="text-center">
								<a href="{{ route('dashboard.registration.show', ['id' => $register->id ]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> Detail</a>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="7" class="text-center">Tidak ada data registrasi</td>	
						</tr>
						@endforelse
					</tbody>
				</table><br>
				<div class="text-center">
		        	{!! paginationHelper($data['registers']->render()) !!}
		        </div>
			</div>
		  </div>
	  </div>
	</div>
</div>	    	
@stop