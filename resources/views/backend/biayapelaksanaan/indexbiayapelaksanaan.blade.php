@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-10">
		<div class="box box-primary">
			<div class="box-header with-border">
			@if(Auth::user()->can('can_write_pelaksanaan'))
		      <a href="{{ route('dashboard.pelaksanaan.{id}.biaya.create', ['id' => $data['penjadwalanId']]) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Biaya</a>&nbsp;
		    @endif
		       <a href="{{ route('dashboard.pelaksanaan.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th>No.</th>
				          	<th>Komponen Biaya</th>
				          	<th>No. Tagihan</th>
				          	<th>Biaya</th>
				          	<th class="text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          <?php $no=1; $biayaTotal = 0; ?>
			         @forelse($data['biaya'] as $biaya)
			         <tr>
			         	<td>{{ $no++ }}</td>
			         	<td>{{ $biaya->komponenbiaya->nama_komponenbiaya }}</td>
			         	<td>{{ (! empty($biaya->nosurat_tagihan)) ? $biaya->nosurat_tagihan : '-' }}</td>
			         	<td>{{ IDRFormatter($biaya->biaya) }}</td>
			         	<td class="text-center">
			         	@if(Auth::user()->can('can_write_pelaksanaan'))
			          		<a href="{{ route('dashboard.pelaksanaan.{id}.biaya.edit', ['id' => $biaya->penjadwalan_id, 'biaya' => $biaya->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
			          		<a href="{{ route('dashboard.pelaksanaan.{id}.biaya.destroy', ['id' => $biaya->penjadwalan_id, 'biaya' => $biaya->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
			          	@else
			          		-
			          	@endif
			          	</td>
			         </tr>
			         <?php $biayaTotal += $biaya->biaya; ?>
			         @empty
			         <tr>
			         	<td colspan="5" class="text-center">Tidak ada biaya pada pelaksanaan ini</td>
			         </tr>
			         @endforelse
			          </tbody>
			          <tfoot>
			          	<tr>
			          		<td colspan="3" class="text-center"><strong>Total Biaya</strong></td>
			          		<td colspan="2" class="text-center"><strong>{{ IDRFormatter($biayaTotal) }}</strong></td>
			          	</tr>
			          </tfoot>
			        </table>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop