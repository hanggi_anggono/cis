@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Edit Biaya Kompetensi</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if($errors->any())
			      <div class="alert alert-danger alert-dismissable">
			        <ul>
			        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			        @foreach($errors->all() as $error)
			          <li>{{ $error }}</li>
			        @endforeach
			        </ul>
			      </div>
			    @endif
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	<form class="form-horizontal" action="{{ route('dashboard.pelaksanaan.{id}.biaya.update', ['id' => $data['penjadwalanId'], 'biaya' => $data['biaya']->id]) }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<input type="hidden" name="_method" value="put">
		    		<input type="hidden" name="penjadwalan_id" value="{{ $data['penjadwalanId'] }}">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Komponen</label>
                      <div class="col-sm-7">
                      	{!! Form::select('komponenbiaya_id', $data['komponenbiaya'], $data['biaya']->komponenbiaya_id,['class'=>'selectpicker form-control', 'data-live-search' => 'true'])!!}  
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">No. Surat Tagihan</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nomor surat tagihan (kalau ada)" name="nosurat_tagihan" value="{{ $data['biaya']->nosurat_tagihan }}">
                      </div>
		    		</div>
		    		<div class="form-group {{ ($errors->any() && $errors->has('biaya')) ? 'has-error' : '' }}">
                      <label for="nama" class="col-sm-3 control-label">Jumlah Biaya</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Jumlah biaya" name="biaya" value="{{ $data['biaya']->biaya }}" required>
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		            <a href="{{ route('dashboard.pelaksanaan.{id}.biaya.index', ['id' => $data['penjadwalanId']]) }}" onclick="window.history.back();return false;" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    