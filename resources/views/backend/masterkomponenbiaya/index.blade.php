@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_masterkomponenbiaya'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.komponenbiaya.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Komponen Biaya</a>
		    </div><!-- /.box-header -->
		@endif
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif	
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th class="col-md-1">No.</th>
				          	<th>Nama Komponen Biaya</th>
				          	<th class="col-md-3 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @forelse($data['komponenbiaya'] as $komponenbiaya)
				          <tr>
				          	<td>{{ $data['pagination_number']++ }}</td>
				          	<td>{{ $komponenbiaya->nama_komponenbiaya }}</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterkomponenbiaya'))
				          		<a href="{{ route('dashboard.master.komponenbiaya.edit', ['id' => $komponenbiaya->id ]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.komponenbiaya.delete', ['id' => $komponenbiaya->id ]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      @empty
				      	<tr>
				      		<td colspan="3" class="text-center">Tidak ada data komponen biaya</td>
				      	</tr>
			          @endforelse
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['komponenbiaya']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop