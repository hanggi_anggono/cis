@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-10">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Daftar Riwayat Menguji</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	<div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			        	<thead class="bg-info">
			        		<tr>
			        			<th>No. </th>
			        			<th>Kompetensi</th>
			        			<th>Jadwal Menguji</th>
			        			<th>Unit Induk</th>
			        			<th>Posisi</th>
			        		</tr>
			        	</thead>
			        	<tbody>
			        	<?php $no=1; ?>
			        	@forelse($data['history'] as $history)
			        	<tr>
			        		<td>{{ $no++ }}</td>
			        		<td>{{ $history->nama_kompetensi }}</td>
			        		<td>{{ $history->tanggal_mulai }} s/d {{ $history->tanggal_selesai }}</td>
			        		<td>{{ $history->nama_unitinduk }}</td>
			        		<td>{{ $history->posisi }} Asesor</td>
			        	</tr>
			        	@empty
			        	<tr>
			        		<td colspan="5" class="text-center">Tidak ada data riwayat menguji</td>
			        	</tr>
			        	@endforelse
			        	</tbody>
			        </table>
			    </div>
			    <div class="box-footer">
			        <a href="{{ route('dashboard.master.asesor.index') }}" class="btn btn-warning">Kembali</a>
			    </div>    
		    </div>
		</div>
	</div>
</div>		    
@stop
