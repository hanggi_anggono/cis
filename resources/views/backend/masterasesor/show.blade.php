@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
	<div class="col-md-7">
		 <div class="box box-primary">
		 	<form class="form-horizontal">
		      <div class="box-body">
		      <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Tipe</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ($data['asesor']->tipe_asesor == 'pln') ? 'PLN' : 'NON PLN' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">NIP</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ $data['asesor']->nip }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">No Identitas</label>
		          <div class="col-sm-7">
		             <p class="form-control-static">{{ $data['asesor']->no_identitas }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Nama</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ $data['asesor']->nama }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Jenis Kelamin</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">
		               	@if($data['asesor']->jenis_kelamin == 'm')
		               		Laki - laki
		               	@else
		               		Perempuan
		               	@endif
		               </p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Gol Darah</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['asesor']->golongan_darah) ? $data['asesor']->golongan_darah : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">TTL</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['asesor']->tempat_lahir) ? $data['asesor']->tempat_lahir : '-' }}, {{ parseDate($data['asesor']->tanggal_lahir) }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Alamat sesuai identitas</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['asesor']->alamat_identitas) ? $data['asesor']->alamat_identitas : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Alamat saat ini</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['asesor']->alamat_sekarang) ? $data['asesor']->alamat_sekarang : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Kewarganegaraan</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['asesor']->kewarganegaraan) ? $data['asesor']->kewarganegaraan : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="email" class="col-sm-4 control-label">Email</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['asesor']->email) ? $data['asesor']->email : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">No. Handphone</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['asesor']->no_hp) ? $data['asesor']->no_hp : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Telepon Rumah</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['asesor']->no_telp) ? $data['asesor']->no_telp : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Telepon Perusahaan</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['asesor']->no_telp_perusahaan) ? $data['asesor']->no_telp_perusahaan : '-' }}</p>
		          </div>
		        </div>
		        
		     </div>   
		    </form>    
		 </div>
	</div>
	 <div class="col-md-5">
	 	<div class="box box-primary">
	 		<div class="box-body">
	 			<form class="form-horizontal">
	 				<div class="text-center">
	 					@if(! empty($data['asesor']->photo))
	 					<img src="{{ asset('resources/asesorphotos/'.$data['asesor']->photo) }}" width="100" height="100">
	 					@else
	 						<img src="{{ asset('assets/dist/img/nophoto.jpg') }}" width="100" height="100">
	 					@endif
	 				</div><br>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Pendidikan</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['asesor']->pendidikan->nama_pendidikan }}</p>
			          </div>
			        </div>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Jenjang Jabatan</label>
			          <div class="col-sm-5">
			              <p class="form-control-static">{{ $data['asesor']->jenjangjabatan->nama_jenjangjabatan }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Jabatan</label>
			          <div class="col-sm-7">
			               <p class="form-control-static">{{ ! empty($data['asesor']->jabatan) ? $data['asesor']->jabatan : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Grade</label>
			          <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['asesor']->grade->nama_grade }}</p>
			          </div>
			        </div>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Induk</label>
			           <div class="col-sm-5">
			              <p class="form-control-static">{{ (! is_null($data['asesor']->unitinduk_id)) ? $data['asesor']->unitinduk->nama_unitinduk : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Cabang</label>
			          <div class="col-sm-5">
			              <p class="form-control-static">{{ (! is_null($data['asesor']->unitcabang_id)) ? $data['asesor']->unitcabang->nama_unitcabang : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Ranting</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ (! is_null($data['asesor']->unitranting_id)) ? $data['asesor']->unitranting->nama_unitranting : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Non PLN</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ (! is_null($data['asesor']->unitnonpln_id)) ? $data['asesor']->unitnonpln->nama_unitnonpln : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Dibuat pada</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ parseDate($data['asesor']->created_at, 'datetime') }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Dirubah pada</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ parseDate($data['asesor']->updated_at, 'datetime') }}</p>
			          </div>
			        </div>
			 	</form>
			 	<div class="box-footer text-center">
			 	@if(Auth::user()->can('can_write_masterasesor'))
			 		<a href="{{ route('dashboard.master.asesor.edit',['id' => $data['asesor']->id ]) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"> </i> Edit</a>&nbsp;
			 		<a href="{{ route('dashboard.master.asesor.delete',['id' => $data['asesor']->id ]) }}" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"> </i> Delete</a>&nbsp;
			 	@endif
			 		<a href="{{ route('dashboard.master.asesor.index') }}" class="btn btn-info btn-sm"><i class="fa fa-reply"> </i> Kembali</a>&nbsp;
			 	</div>
	 		</div>
	 	</div>
	 </div>
</div>
@stop