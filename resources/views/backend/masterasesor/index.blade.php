@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_masterasesor'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.asesor.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Asesor</a>
		      <form class="form-inline pull-right" method="get" action="{{ route('dashboard.master.asesor.index') }}">
		      	<div class="form-group">
		      		<input type="text" class="form-control" name="nipname" value="{{ Request::get('nipname') }}">
		      		<button type="input" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
		      	</div>
		      </form>
		    </div><!-- /.box-header -->
		@endif
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover table-striped">
			        	<thead>
	                        <tr class="bg-info">
	                          <th class="text-center">No.</th>
	                          <th class="text-center">Tipe</th>
	                          <th class="text-center">NIP</th>
	                          <th class="col-md-1 text-center">Nama</th>
	                          <th class="text-center">Induk / Institusi</th>
	                          <th class="col-md-1 text-center">Cabang</th>
	                          <th class="col-md-1 text-center">Non PLN</th>
	                          <th class="col-md-1 text-center">Lembaga Sertifikasi</th>
	                          <th class="text-center">Sub Bidang</th>
	                          <th class="col-md-1 text-center">Riwayat Menguji</th>
	                          <th class="text-center">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    @forelse($data['asesor'] as $asesor)
	                    	<tr>
	                    		<td>{{ $data['pagination_number'] }}</td>
	                    		<td>{{ $asesor->tipe_asesor }}</td>
	                    		<td>{{ $asesor->nip }}</td>
	                    		<td>{{ $asesor->nama }}</td>
	                    		<td>{{ (! is_null($asesor->unitinduk)) ? $asesor->unitinduk->nama_unitinduk : '-' }}</td>
	                    		<td>{{ (! is_null($asesor->unitcabang)) ? $asesor->unitcabang->nama_unitcabang : '-' }}</td>
	                    		<td>{{ (! is_null($asesor->unitnonpln)) ? $asesor->unitnonpln->nama_unitnonpln : '-' }}</td>
	                    		<td><a href="{{ route('dashboard.master.asesor.kelompok.index', ['id'=>$asesor->id]) }}">{{ $asesor->kelompok->count() }} Lembaga</a></td>
	                    		<td><a href="{{ route('dashboard.master.asesor.subbidang.index',['asesor_id' => $asesor->id]) }}">{{ $asesor->subbidang->count() }} Sub Bidang</a></td>
	                    		<td class="text-center"><a href="{{ route('dashboard.master.asesor.testhistory.index', ['id' => $asesor->id]) }}">Lihat riwayat</a></td>
	                    		<td class="text-center">
		                          <a href="{{ route('dashboard.master.asesor.show', ['id' => $asesor->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>

		                        @if(Auth::user()->can('can_write_masterasesor'))  
		                          
		                          <a href="{{ route('dashboard.master.asesor.edit', ['id' => $asesor->id]) }}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> </a>
		                          <a href="{{ route('dashboard.master.asesor.delete', ['id' => $asesor->id]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> </a>
		                          <a href="{{ route('dashboard.master.sertifikatasesor.asesor.index', ['asesor_id' => $asesor->id]) }}" class="btn btn-xs btn-success"><i class="fa fa-certificate"></i> </a>

		                        @endif  
		                        </td>
	                    	</tr>
	                    	<?php $data['pagination_number']++; ?>
	                    @empty
	                    	<tr>
	                    		<td colspan="11">Tidak ada data</td>
	                    	</tr>
	                    @endforelse
	                    </tbody>
			        </table><br>
                  	<div class="text-center">
                  		{!! paginationHelper($data['asesor']->render()) !!}
                  	</div>
			     </div>
			</div>
		</div>
	</div>
</div>			        
@stop
