@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
<div class="col-xs-8">
  <div class="box box-primary">
    @if($errors->any())
      <div class="alert alert-danger alert-dismissable">
        <ul>
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
        </ul>
      </div>
    @endif
    <form class="form-horizontal" action="{{ route('dashboard.master.asesor.update', ['id'=>$data['asesor']->id]) }}" method="post" accept-charset="utf-8" id="asesor" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="put">
      <div class="box-body">
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tipe</label>
          <div class="col-sm-3">
              {!! Form::select('tipe_asesor', ['pln'=>'PLN','nonpln'=>'NON PLN'], $data['asesor']->tipe_asesor, ['class'=>'form-control', 'id' => 'tipe_asesor']) !!}
          </div>
        </div>
        <div class="form-group" id="info-nonpln" style="{{ ($data['asesor']->tipe_asesor== 'pln') ? 'display:none;' : ''}}">
          <label for="nip" class="col-sm-3 control-label">Keterangan Non PLN</label>
            <div class="col-sm-5">
              {!! Form::select('unitnonpln_id', $data['support']['nonpln'], $data['asesor']->unitnonpln_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitnonpln_id', 'autocomplete' => 'off'])!!}
            </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">NIP</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="NIP asesor" name="nip" value="{{ $data['asesor']->nip }}" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">No Identitas</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="No. KTP / SIM / passport" name="no_identitas" value="{{ $data['asesor']->no_identitas }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Nama</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Nama lengkap asesor" name="nama" value="{{ $data['asesor']->nama }}" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Gol Darah</label>
          <div class="col-sm-7">
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio1" value="O" @if($data['asesor']->golongan_darah == 'O') checked @endif> O
              </label>
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio2" value="A" @if($data['asesor']->golongan_darah == 'A') checked @endif> A
              </label>
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio1" value="B" @if($data['asesor']->golongan_darah == 'B') checked @endif> B
              </label>
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio2" value="AB" @if($data['asesor']->golongan_darah == 'AB') checked @endif> AB
              </label>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Kewarganegaraan</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Kewarganegaraan asesor" name="kewarganegaraan" value="{{ $data['asesor']->kewarganegaraan }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jenis Kelamin</label>
          <div class="col-sm-7">
              <label class="radio-inline">
                <input type="radio" name="jenis_kelamin" id="inlineRadio1" value="m" @if($data['asesor']->jenis_kelamin == 'm') checked @endif> Laki-laki
              </label>
              <label class="radio-inline">
                <input type="radio" name="jenis_kelamin" id="inlineRadio2" value="f"  @if($data['asesor']->jenis_kelamin == 'f') checked @endif> Perempuan
              </label>
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-sm-3 control-label">Email</label>
          <div class="col-sm-7">
              <input type="email" class="form-control" placeholder="Email asesor" name="email" value="{{ $data['asesor']->email }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">No. Handphone</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="No. Handphone asesor" name="no_hp" value="{{ $data['asesor']->no_hp }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Telepon Rumah</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Nama telepon rumah asesor" name="no_telp" value="{{ $data['asesor']->no_telp }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Telepon Perusahaan</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Nomor telepon perusahaan asesor" name="no_telp_perusahaan" value="{{ $data['asesor']->no_telp_perusahaan }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tempat Lahir</label>
           <div class="col-sm-7">
           	<input type="text" class="form-control" placeholder="Tempat lahir" name="tempat_lahir" value="{{ $data['asesor']->tempat_lahir }}">
           </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tanggal Lahir</label>
          <div class="col-sm-2">
              {!! Form::selectRange('dd', 1, 31, getPartialDate($data['asesor']->tanggal_lahir), ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
          </div>
          <div class="col-sm-3">
              {!! Form::selectMonth('mm', getPartialDate($data['asesor']->tanggal_lahir, 'month'),['class' => 'form-control selectpicker', 'data-live-search' => 'true']); !!}
          </div>
          <div class="col-sm-2">
              {!! Form::selectRange('yy', 1945, 2005, getPartialDate($data['asesor']->tanggal_lahir, 'year'), ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Alamat sesuai identitas</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Alamat sesuai dengan kartu identitas asesor" name="alamat_identitas" value="{{ $data['asesor']->alamat_identitas }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Alamat saat ini</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Alamat asesor saat ini" name="alamat_sekarang" value="{{ $data['asesor']->alamat_sekarang }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jenjang Jabatan</label>
          <div class="col-sm-5">
              {!! Form::select('jenjangjabatan_id', $data['support']['jenjang'], $data['asesor']->jenjangjabatan_id, ['class' => 'form-control selectpicker', 'data-live-search' => 'true'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jabatan</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Jabatan asesor" name="jabatan" value="{{ $data['asesor']->jabatan }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Grade</label>
          <div class="col-sm-5">
              {!! Form::select('grade_id', $data['support']['grade'], $data['asesor']->grade_id, ['class' => 'form-control selectpicker', 'data-live-search' => 'true'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Pendidikan</label>
           <div class="col-sm-5">
              {!! Form::select('pendidikan_id', $data['support']['pendidikan'], $data['asesor']->pendidikan_id, ['class' => 'form-control selectpicker', 'data-live-search' => 'true'])!!}
          </div>
        </div>
        <div id="units" style="{{ ($data['asesor']->tipe_asesor == 'pln') ? '' : 'display:none;' }}">
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Induk</label>
           <div class="col-sm-5">
              {!! Form::select('unitinduk_id', $data['support']['uinduk'], (! is_null($data['asesor']->unitinduk_id)) ? $data['asesor']->unitinduk_id : '', ['class'=>'selectpicker form-control','data-live-search' => 'true', 'id'=>'unitinduk_id', 'autocomplete' => 'off'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Cabang</label>
          <div class="col-sm-5">
          @if(! is_null($data['asesor']->unitcabang_id))
            {!! Form::select('unitcabang_id', [$data['asesor']->unitcabang_id => $data['asesor']->unitcabang->nama_unitcabang], $data['asesor']->unitcabang_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitcabang'])!!}
          @else
            <select name="unitcabang_id" class="selectpicker form-control" id="unitcabang" data-live-search="true" disabled="disabled">
              <option value="0">----------------------------------</option>
-           </select>
          @endif
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Ranting</label>
           <div class="col-sm-5">
           @if(! is_null($data['asesor']->unitranting_id))
              {!! Form::select('unitranting_id', [$data['asesor']->unitranting_id => $data['asesor']->unitranting->nama_unitranting], $data['asesor']->unitranting_id, ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitranting'])!!}
           @else
              <select name="unitranting_id" class="selectpicker form-control" id="unitranting" disabled="disabled" data-placeholder="-- Pilih unit ranting --" data-live-search="true">
                <option value="0">----------------------------------</option>
-             </select>
           @endif   
          </div>
        </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unggah Foto</label>
          <div class="col-sm-8">
              <input type="file" name="foto">
              <p class="help-block">Kosongkan field ini jika tidak mengganti foto. Berkas unggahan tidak boleh berkapasitas lebih dari 200kb.</p>
          </div>
        </div>
      </div><!-- /.box-body -->
      <div class="box-footer col-md-offset-3">
        <button type="submit" class="btn btn-primary">Update</button>&nbsp;
        <a href="{{ route('dashboard.master.asesor.index') }}" class="btn btn-warning">Kembali</a>
      </div>
    </form>
  </div><!-- /.box -->
</div>
</div><!-- /.row -->
@stop
@section('customjs')
<script type="text/javascript">
$(document).ready(function(){
     var form = $('form#asesor'),
    uinduk     = form.find('select#unitinduk_id'),
    ucabang    = form.find('select#unitcabang'),
    uranting   = form.find('select#unitranting'),
    tipeasesor = form.find('select#tipe_asesor'),
    url        = "{{ url('dashboard/master/unit/induk/api/cabang', 'id') }}",
    urlranting = "{{ url('dashboard/master/unit/cabang/api/ranting', 'id') }}";
    
    uinduk.change(function(event) {
      event.preventDefault();
      urlWithId = url.replace('id', uinduk.val());  
      var options = '';
      $.getJSON(urlWithId, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="0">-- Pilih Unit Cabang --</option>';
          $.each(data,function(index,value){
            options += '<option value="'+index+'">'+value+'</option>';
          });
          ucabang.removeAttr('disabled');
          uranting.removeAttr('disabled');
          ucabang.html(options).selectpicker('refresh');
        } else {
          ucabang.html('');
          uranting.html('');
          ucabang.attr('disabled',true).selectpicker('refresh');
          uranting.attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });
    ucabang.change(function(event) {
      event.preventDefault();
      urlrantingWithId = urlranting.replace('id', ucabang.val()); 
      var options = '';
      $.getJSON(urlrantingWithId, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="0">-- Pilih Unit Ranting --</option>';
            $.each(data,function(index,value){
                options += '<option value="'+index+'">'+value+'</option>';
            });
            uranting.removeAttr('disabled');
            uranting.html(options).selectpicker('refresh');
        } else {
          uranting.html('');
          uranting.attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });

    tipeasesor.change(function(event)
    {
      event.preventDefault();
      if($(this).val() == "pln") {
        $('div#info-nonpln').hide();
        $('div#units').show();
        $('#unitnonpln_id').attr('disabled', true).selectpicker('refresh');
      } else {
        $('div#info-nonpln').show();
        $('div#units').hide();
        uinduk.attr('disabled', true).selectpicker('refresh');
        ucabang.attr('disabled', true).selectpicker('refresh');
        uranting.attr('disabled', true).selectpicker('refresh');
      }
      
    });
});
</script>
@stop