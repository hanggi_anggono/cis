@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header with-border">
		      <h4>Daftar Role</h4>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th class="col-md-1">No.</th>
				          	<th class="col-md-3">Nama Role</th>
				          	<th>Deskripsi</th>
				          	<th class="text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          <?php $no=1; ?>
			          @forelse($data['roles'] as $role)
				          <tr>
				          	<td>{{ $no++ }}</td>
				          	<td>{{ $role->display_name }}</td>
				          	<td>{{ (! is_null($role->description)) ? $role->description : '' }}</td>
				          	<td class="text-center">
				          		<a href="{{ route('dashboard.master.userrole.role.permission', ['role_id' => $role->id ]) }}" class="btn btn-sm btn-warning">Permission</a>
				          	</td>
			          @empty
			          	<tr>
			          		<td colspan="4" class="text-center">Tidak ada role</td>
			          	</tr>
			          @endforelse
			          </tbody>
			        </table>
			    </div>      
		    </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_masterrole'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.userrole.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah User</a>&nbsp;
		    </div><!-- /.box-header -->
		@endif
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th class="col-md-1">No.</th>
				          	<th class="col-md-3">NIP</th>
				          	<th class="col-md-3">Role</th>
				          	<th class="col-md-3 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @forelse($data['users'] as $user)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $user->nip }}</td>
				          	<td>{{ ($user->roles->count() > 0) ? $user->roles[0]->display_name : '-' }}</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterrole'))
				          		<a href="{{ route('dashboard.master.userrole.edit', ['id' => $user->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.userrole.delete', ['id' => $user->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>    
			          @empty
			          	<tr>
			          		<td colspan="4" class="text-center">Tidak ada data</td>
			          	</tr>
			          @endforelse
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['users']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop