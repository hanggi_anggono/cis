@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
			@if(Auth::user()->can('can_write_masterrole'))
		     <h3>Form Edit Permission Role</h3>
		    @else
		    <h3>Daftar Permission Role</h3>
		    @endif
		    </div><!-- /.box-header -->
		    <div class="box-body">
	    	@if($errors->any())
	    		<div class="alert alert-danger alert-dismissable">
	    			<ul>
		    		<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
		    		@foreach($errors->all() as $error)
		    			<li>{{ $error }}</li>
		    		@endforeach
		    		</ul>
		    	</div>
	    	@endif
	    	@if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
	    	<form class="form-horizontal" action="{{ route('dashboard.master.userrole.role.permission.update', ['role_id' => $data['role']->id ]) }}" method="post">
	    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	    		<div class="form-group">
                  <label for="nama" class="col-sm-3 control-label">Role</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="User NIP" value="{{ $data['role']->display_name }}" disabled>
                  </div>
	    		</div>
	    		<div class="form-group">
                  <label for="nama" class="col-sm-3 control-label">Permissions</label>
                  <div class="col-sm-offset-3 col-sm-10">
                  	@foreach($data['permissions'] as $permission)
					<div class="checkbox">
					  <label>
					    <input type="checkbox" name="permission_id[]" value="{{ $permission->id }}" @if(in_array($permission->id, $data['permission_role'])) {{ 'checked="checked"' }} @endif @if(! Auth::user()->can('can_write_masterrole')) {{ 'disabled' }}@endif>
					    {{ $permission->display_name }}
					  </label>
					</div>
		    		@endforeach
                  </div>
                </div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
		    	@if(Auth::user()->can('can_write_masterrole'))
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
    			@endif
		            <a href="{{ route('dashboard.master.userrole.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    