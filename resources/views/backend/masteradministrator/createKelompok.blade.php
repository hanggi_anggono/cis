@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h4>Form Tambah Kelompok Administrator</h4>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	<form class="form-horizontal" action="{{ route('dashboard.master.administrator.kelompok.store',['id'=>$data['administrator']->id]) }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Administrator</label>
                      <div class="col-sm-7">
                  		<input type="text" class="form-control" value="{{ $data['administrator']->nama }}" readonly="readonly">         
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Lembaga Sertifikasi</label>
                      <div class="col-sm-7">
                  		{!! Form::select('lembagasertifikasi_id', $data['kelompok'], null,['class'=>'selectpicker form-control','data-live-search' => 'true']) !!}
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Tanggal Penetapan</label>
                      <div class="col-sm-7">
                  		<input type="text" class="form-control" name="tanggalpenetapan" id="tanggalpenetapan" placeholder="Tanggal penetapan. (boleh kosong).">  
                      </div>
		    		</div>			    		
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		            <a href="{{ route('dashboard.master.administrator.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		  
@section('customjs')
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
<script type="text/javascript">
	$('#tanggalpenetapan').datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true
	});
</script>
@stop  