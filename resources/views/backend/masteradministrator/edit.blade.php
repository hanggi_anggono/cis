@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
<div class="col-xs-8">
  <div class="box box-primary">
    @if($errors->any())
      <div class="alert alert-danger alert-dismissable">
        <ul>
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
        </ul>
      </div>
    @endif
    <form class="form-horizontal" action="{{ route('dashboard.master.administrator.update', ['id'=>$data['administrator']->id]) }}" method="post" accept-charset="utf-8" id="administrator" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="put">
      <div class="box-body">
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">NIP</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="NIP administrator" name="nip" value="{{ $data['administrator']->nip }}" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">No Identitas</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="No. KTP / SIM / passport" name="no_identitas" value="{{ $data['administrator']->no_identitas }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Nama</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Nama lengkap administrator" name="nama" value="{{ $data['administrator']->nama }}" required>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Gol Darah</label>
          <div class="col-sm-7">
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio1" value="O" @if($data['administrator']->golongan_darah == 'O') checked @endif> O
              </label>
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio2" value="A" @if($data['administrator']->golongan_darah == 'A') checked @endif> A
              </label>
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio1" value="B" @if($data['administrator']->golongan_darah == 'B') checked @endif> B
              </label>
              <label class="radio-inline">
                <input type="radio" name="golongan_darah" id="inlineRadio2" value="AB" @if($data['administrator']->golongan_darah == 'AB') checked @endif> AB
              </label>
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Kewarganegaraan</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Kewarganegaraan administrator" name="kewarganegaraan" value="{{ $data['administrator']->kewarganegaraan }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jenis Kelamin</label>
          <div class="col-sm-7">
              <label class="radio-inline">
                <input type="radio" name="jenis_kelamin" id="inlineRadio1" value="m" @if($data['administrator']->jenis_kelamin == 'm') checked @endif> Laki-laki
              </label>
              <label class="radio-inline">
                <input type="radio" name="jenis_kelamin" id="inlineRadio2" value="f"  @if($data['administrator']->jenis_kelamin == 'f') checked @endif> Perempuan
              </label>
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-sm-3 control-label">Email</label>
          <div class="col-sm-7">
              <input type="email" class="form-control" placeholder="Email administrator" name="email" value="{{ $data['administrator']->email }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">No. Handphone</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="No. Handphone administrator" name="no_hp" value="{{ $data['administrator']->no_hp }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Telepon Rumah</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Nama telepon rumah administrator" name="no_telp" value="{{ $data['administrator']->no_telp }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tempat Lahir</label>
           <div class="col-sm-7">
           	<input type="text" class="form-control" placeholder="Tempat lahir" name="tempat_lahir" value="{{ $data['administrator']->tempat_lahir }}">
           </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Tanggal Lahir</label>
          <div class="col-sm-2">
              {!! Form::selectRange('dd', 1, 31, getPartialDate($data['administrator']->tanggal_lahir), ['class' => 'form-control selectpicker']) !!}
          </div>
          <div class="col-sm-3">
              {!! Form::selectMonth('mm', getPartialDate($data['administrator']->tanggal_lahir, 'month'),['class' =>'form-control selectpicker']); !!}
          </div>
          <div class="col-sm-2">
              {!! Form::selectRange('yy', 1945, 2005, getPartialDate($data['administrator']->tanggal_lahir, 'year'), ['class' => 'form-control selectpicker']) !!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jenjang Jabatan</label>
          <div class="col-sm-5">
              {!! Form::select('jenjangjabatan_id', $data['support']['jenjang'], $data['administrator']->jenjangjabatan_id, ['class'=>'selectpicker form-control', 'data-live-search' => 'true'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Jabatan</label>
          <div class="col-sm-7">
              <input type="text" class="form-control" placeholder="Jabatan administrator" name="jabatan" value="{{ $data['administrator']->jabatan }}">
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Grade</label>
          <div class="col-sm-5">
              {!! Form::select('grade_id', $data['support']['grade'], $data['administrator']->grade_id, ['class'=>'selectpicker form-control', 'data-live-search' => 'true'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Pendidikan</label>
           <div class="col-sm-5">
              {!! Form::select('pendidikan_id', $data['support']['pendidikan'], $data['administrator']->pendidikan_id, ['class'=>'selectpicker form-control', 'data-live-search' => 'true'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Induk</label>
           <div class="col-sm-5">
              {!! Form::select('unitinduk_id', $data['support']['uinduk'], $data['administrator']->unitinduk_id, ['class'=>'selectpicker form-control','id'=>'unitinduk_id', 'data-live-search' => 'true', 'id' => 'unitinduk_id'])!!}
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Cabang</label>
          <div class="col-sm-5">
            @if(! is_null($data['administrator']->unitcabang_id))
              {!! Form::select('unitcabang_id', [$data['administrator']->unitcabang_id => $data['administrator']->unitcabang->nama_unitcabang], $data['administrator']->unitcabang_id, ['class'=>'selectpicker form-control','id'=>'unitcabang', 'data-live-search' => 'true', 'id' => 'unitcabang'])!!}
            @else
               <select name="unitcabang_id" class="selectpicker form-control" id="unitcabang" data-live-search="true" disabled="disabled">
                <option value="0">----------------------------------</option>
  -           </select>
            @endif
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unit Ranting</label>
           <div class="col-sm-5">
           @if(! is_null($data['administrator']->unitranting_id))
              {!! Form::select('unitranting_id', [$data['administrator']->unitranting_id => $data['administrator']->unitranting->nama_unitranting], $data['administrator']->unitranting_id, ['class'=>'selectpicker form-control','id'=>'unitranting', 'data-live-search' => 'true', 'id' => 'unitranting'])!!}
            @else
              <select name="unitranting_id" class="selectpicker form-control" id="unitranting" disabled="disabled" data-placeholder="-- Pilih unit ranting --" data-live-search="true" autocomplete="off">
                <option value="0">----------------------------------</option>
-             </select>
            @endif
          </div>
        </div>
        <div class="form-group">
          <label for="nip" class="col-sm-3 control-label">Unggah Foto</label>
          <div class="col-sm-8">
              <input type="file" name="foto">
              <p class="help-block">Kosongkan field ini jika tidak mengganti foto. Berkas unggahan tidak boleh berkapasitas lebih dari 200kb.</p>
          </div>
        </div>
      </div><!-- /.box-body -->
      <div class="box-footer col-md-offset-3">
        <button type="submit" class="btn btn-primary">Update</button>&nbsp;
        <a href="{{ route('dashboard.master.administrator.index') }}" class="btn btn-warning">Kembali</a>
      </div>
    </form>
  </div><!-- /.box -->
</div>
</div><!-- /.row -->
@stop
@section('customjs')
<script type="text/javascript">
$(document).ready(function(){
    var form = $('form#administrator'),
    uinduk     = form.find('select#unitinduk_id'),
    ucabang    = form.find('select#unitcabang'),
    uranting   = form.find('select#unitranting'),
    url        = "{{ url('dashboard/master/unit/induk/api/cabang', 'id') }}",
    urlranting = "{{ url('dashboard/master/unit/cabang/api/ranting', 'id') }}";
    
    uinduk.change(function() {
      urlWithId = url.replace('id', uinduk.val());  
      var options = '';
      $.getJSON(urlWithId, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="0">-- Pilih Unit Cabang --</option>';
          $.each(data,function(index,value){
            options += '<option value="'+index+'">'+value+'</option>';
          });
          ucabang.removeAttr('disabled');
          uranting.removeAttr('disabled');
          ucabang.html(options).selectpicker('refresh');
        } else {
          ucabang.html('');
          uranting.html('');
          ucabang.attr('disabled',true).selectpicker('refresh');
          uranting.attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });
    ucabang.change(function() {
      urlrantingWithId = urlranting.replace('id', ucabang.val()); 
      var options = '';
      $.getJSON(urlrantingWithId, function(data){
        if(! $.isEmptyObject(data)) {
          options += '<option value="0">-- Pilih Unit Ranting --</option>';
            $.each(data,function(index,value){
                options += '<option value="'+index+'">'+value+'</option>';
            });
            uranting.removeAttr('disabled');
            uranting.html(options).selectpicker('refresh');
        } else {
          uranting.html();
          uranting.attr('disabled',true).selectpicker('refresh');
        }
        
      });
    });
});

</script>
@stop