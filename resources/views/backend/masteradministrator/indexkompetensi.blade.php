@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
			@if(Auth::user()->can('can_write_masteradministrator'))
		      <a href="{{ route('dashboard.master.administrator.kompetensi.create',['id'=>$data['administrator']->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Kompetensi Administrator</a>
		    @endif  
		      <span class="pull-right"><strong>Administrator: {{ $data['administrator']->nama }}</strong></span>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th class="col-md-1">No.</th>
				          	<th>Kompetensi</th>
				          	<th class="col-md-3 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          <?php $no = 1; ?>
			          @forelse($data['administrator']->kompetensi as $kompetensi)
			          	<tr>
			          		<td>{{ $no }}</td>
			          		<td>{{ $kompetensi->nama_kompetensi }}</td>
			          		<td class="text-center">
			          		@if(Auth::user()->can('can_write_masteradministrator'))
				          		<a href="{{ route('dashboard.master.administrator.kompetensi.delete',['id' => $data['administrator']->id,'kompetensi_id' => $kompetensi->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif	
			          		</td>
			          	</tr>
			          <?php $no++; ?>	
			          @empty
			          <tr>
			          	<td colspan="3" class="text-center">Tidak ada data</td>
			          </tr>
			          @endforelse
			          </tbody>
			        </table>
			    </div><br>      
			     <div class="text-center">
		        	<a href="{{ route('dashboard.master.administrator.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
		        </div>
		    </div>
		</div>
	</div>
</div>
@stop