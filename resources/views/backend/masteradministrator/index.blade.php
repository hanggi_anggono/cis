@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			@if(Auth::user()->can('can_write_masteradministrator'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.administrator.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Administrator</a>
		      <form class="form-inline pull-right" method="get" action="{{ route('dashboard.master.administrator.index') }}">
		      	<div class="form-group">
		      		<input type="text" class="form-control" name="nipname" value="{{ Request::get('nipname') }}">
		      		<button type="input" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
		      	</div>
		      </form>
		    </div><!-- /.box-header -->
		    @endif
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover table-striped">
	                    <thead>
	                        <tr class="bg-info">
	                          <th class="text-center">No.</th>
	                          <th class="text-center">NIP</th>
	                          <th class="col-md-1 text-center">Nama</th>
	                          <th class="text-center">Induk / Institusi</th>
	                          <th class="col-md-1 text-center">Cabang</th>
	                          <th class="text-center">Email</th>
	                          <th class="text-center">Telp</th>
	                          <th class="col-md-1 text-center">Lembaga Sertifikasi</th>
	                          <th class="text-center">Kompetensi</th>
	                          <th class="col-md-1 text-center">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    @forelse($data['administrator'] as $administrator)
	                    	<tr>
	                    		<td>{{ $data['pagination_number'] }}</td>
	                    		<td>{{ $administrator->nip }}</td>
	                    		<td>{{ $administrator->nama }}</td>
	                    		<td>{{ $administrator->unitinduk->nama_unitinduk }}</td>
	                    		<td>{{ (isset($administrator->unitcabang->nama_unitcabang)) ? $administrator->unitcabang->nama_unitcabang : '-' }}</td>
	                    		<td>{{ $administrator->email }}</td>
	                    		<td>{{ $administrator->no_hp }}</td>
	                    		<td><a href="{{ route('dashboard.master.administrator.kelompok.index',['id'=>$administrator->id]) }}">{{ $administrator->kelompok->count()}} LSK</a></td>
	                    		<td><a href="{{ route('dashboard.master.administrator.kompetensi.index',['id'=>$administrator->id]) }}">{{ $administrator->kompetensi->count()}} Kompetensi</a></td>
	                    		<td class="text-center">
		                          <a href="{{ route('dashboard.master.administrator.show', ['id' => $administrator->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>
		                          
		                          @if(Auth::user()->can('can_write_masteradministrator'))

		                          <a href="{{ route('dashboard.master.administrator.edit', ['id' => $administrator->id]) }}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> </a>
		                          <a href="{{ route('dashboard.master.administrator.delete', ['id' => $administrator->id]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> </a>
		                          
		                          @endif
		                        </td>
	                    	</tr>
	                    	<?php $data['pagination_number']++; ?>
	                    @empty
	                    	<tr>
	                    		<td colspan="11">Tidak ada data</td>
	                    	</tr>
	                    @endforelse
	                    </tbody>
			        </table><br>
                  	<div class="text-center">
                  		{!! paginationHelper($data['administrator']->render()) !!}
                  	</div>
			     </div>
			</div>
		</div>
	</div>
</div>			        
@stop
