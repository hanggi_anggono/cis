@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
	<div class="col-md-7">
		 <div class="box box-primary">
		 	<form class="form-horizontal">
		      <div class="box-body">
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">NIP</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ $data['administrator']->nip }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">No Identitas</label>
		          <div class="col-sm-7">
		             <p class="form-control-static">{{ $data['administrator']->no_identitas }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Nama</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ $data['administrator']->nama }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Jenis Kelamin</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">
		               	@if($data['administrator']->jenis_kelamin == 'm')
		               		Laki - laki
		               	@else
		               		Perempuan
		               	@endif
		               </p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Gol Darah</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['administrator']->golongan_darah) ? $data['administrator']->golongan_darah : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">TTL</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['administrator']->tempat_lahir) ? $data['administrator']->tempat_lahir : '-' }}, {{ $data['administrator']->tanggal_lahir }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Kewarganegaraan</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['administrator']->kewarganegaraan) ? $data['administrator']->kewarganegaraan : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="email" class="col-sm-4 control-label">Email</label>
		          <div class="col-sm-7">
		              <p class="form-control-static">{{ ! empty($data['administrator']->email) ? $data['administrator']->email : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">No. Handphone</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['administrator']->no_hp) ? $data['administrator']->no_hp : '-' }}</p>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="nip" class="col-sm-4 control-label">Telepon Rumah</label>
		          <div class="col-sm-7">
		               <p class="form-control-static">{{ ! empty($data['administrator']->no_telp) ? $data['administrator']->no_telp : '-' }}</p>
		          </div>
		        </div>
			 	<div class="form-group">
			        <label for="nip" class="col-sm-4 control-label">Pendidikan</label>
			    	<div class="col-sm-5">
			            <p class="form-control-static">{{ $data['administrator']->pendidikan->nama_pendidikan }}</p>
			        </div>
			    </div>	        
		        
		     </div>   
		    </form>    
		 </div>
	</div>
	 <div class="col-md-5">
	 	<div class="box box-primary">
	 		<div class="box-body">
	 			<form class="form-horizontal">
	 				<div class="text-center">
	 					@if(! empty($data['administrator']->photo))
	 					<img src="{{ asset('resources/adminphotos/'.$data['administrator']->photo) }}" width="100" height="100">
	 					@else
	 						<img src="{{ asset('assets/dist/img/nophoto.jpg') }}" width="100" height="100">
	 					@endif
	 				</div><br>
			 		<div class="form-group">
			        	<label for="nip" class="col-sm-4 control-label">Jenjang Jabatan</label>
			        	<div class="col-sm-5">
			            	<p class="form-control-static">{{ $data['administrator']->jenjangjabatan->nama_jenjangjabatan }}</p>
			        	</div>
			    	</div>		 				
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Jabatan</label>
			          <div class="col-sm-7">
			               <p class="form-control-static">{{ ! empty($data['administrator']->jabatan) ? $data['administrator']->jabatan : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Grade</label>
			          <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['administrator']->grade->nama_grade }}</p>
			          </div>
			        </div>
			 		<div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Induk</label>
			           <div class="col-sm-5">
			              <p class="form-control-static">{{ $data['administrator']->unitinduk->nama_unitinduk }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Cabang</label>
			          <div class="col-sm-5">
			              <p class="form-control-static">{{ (isset($data['administrator']->unitcabang->nama_unitcabang)) ? $data['administrator']->unitcabang->nama_unitcabang : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Unit Ranting</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ (isset($data['administrator']->unitranting->nama_unitranting)) ? $data['administrator']->unitranting->nama_unitranting : '-' }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Dibuat pada</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['administrator']->created_at }}</p>
			          </div>
			        </div>
			        <div class="form-group">
			          <label for="nip" class="col-sm-4 control-label">Dirubah pada</label>
			           <div class="col-sm-5">
			               <p class="form-control-static">{{ $data['administrator']->updated_at }}</p>
			          </div>
			        </div>
			 	</form>
			 	<div class="box-footer text-center">
		 		@if(Auth::user()->can('can_write_masteradministrator'))
			 		<a href="{{ route('dashboard.master.administrator.edit',['id' => $data['administrator']->id ]) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"> </i> Edit</a>&nbsp;
			 		<a href="{{ route('dashboard.master.administrator.delete',['id' => $data['administrator']->id ]) }}" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"> </i> Delete</a>&nbsp;
			 	@endif
			 		<a href="{{ route('dashboard.master.administrator.index') }}" class="btn btn-info btn-sm"><i class="fa fa-reply"> </i> Kembali</a>&nbsp;
			 	</div>
	 		</div>
	 	</div>
	 </div>
</div>
@stop