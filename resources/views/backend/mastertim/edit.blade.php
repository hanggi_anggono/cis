@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Edit Tim Asesor</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if($errors->any())
		    		<div class="alert alert-danger alert-dismissable">
		    			<ul>
			    		<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			    		@foreach($errors->all() as $error)
			    			<li>{{ $error }}</li>
			    		@endforeach
			    		</ul>
			    	</div>
		    	@endif
		    	<form class="form-horizntal" action="{{ route('dashboard.master.tim.update',['id' => $data['tim']->id]) }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<input type="hidden" name="_method" value="put">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Jabatan tim</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama jabatan tim" name="jabatantim" value="{{ $data['tim']->jabatantim }}" required>
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Update</button>&nbsp;
		            <a href="{{ route('dashboard.master.tim.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    