@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.tim.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Tim</a>&nbsp;
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th>No.</th>
				          	<th>Jabatan tim</th>
				          	<th class="text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @forelse($data['tim'] as $tim)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $tim->jabatantim }}</td>
				          	<td class="text-center">
				          		<a href="{{ route('dashboard.master.tim.edit', ['id'=>$tim->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.tim.delete', ['id'=>$tim->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>    
			          @empty
			          	<tr>
			          		<td colspan="4" class="text-center">Tidak ada data</td>
			          	</tr>
			          @endforelse
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['tim']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop