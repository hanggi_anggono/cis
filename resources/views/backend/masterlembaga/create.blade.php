@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Tambah Akreditor</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if($errors->any())
		    		<div class="alert alert-danger alert-dismissable">
		    			<ul>
			    		<button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
			    		@foreach($errors->all() as $error)
			    			<li>{{ $error }}</li>
			    		@endforeach
			    		</ul>
			    	</div>
		    	@endif
		    	<form class="form-horizontal" action="{{ route('dashboard.master.akreditor.store') }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Nama Akreditor</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama akreditor" name="nama_lembaga" required>
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		            <a href="{{ route('dashboard.master.akreditor.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    