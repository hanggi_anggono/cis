@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
		      <h4>Pencarian Lanjut</h4>
		    </div><!-- /.box-header -->
		    <div class="box-body">
			    <form class="form-inline" method="get" action="{{ route('dashboard.evaluasi.index') }}">
			    	<div class="form-group">
			    		<input type="text" class="form-control" name="nosurat" value="{{ Request::get('nosurat') }}" placeholder="No. Surat">
			    	</div>
			    	<div class="form-group">
		               <input type="text" class="form-control" name="nip" placeholder="NIP / nama peserta" value="{{ Request::get('nip') }}">
		            </div>
		            <div class="form-group">
		               <input type="text" class="form-control" name="unitinduk" placeholder="Nama Unit Induk" value="{{ Request::get('unitinduk') }}">
		            </div>
		            <div class="form-group">
		               <input type="text" class="form-control" name="kompetensi" placeholder="Kode / Nama kompetensi" value="{{ Request::get('kompetensi') }}">
		            </div>
			    	<div class="form-group">
			    		{!! Form::select('hasil', ['' => 'Semua Hasil', 'pending' => 'Belum Dievaluasi', '0' => 'Ditolak', '1' => 'Diterima'], Request::get('hasil'), ['class' => 'form-control'])!!}
			    	</div>
			    	<div class="form-group">
			    		{!! Form::select('status_penjadwalan', ['' => 'Semua status', '0' => 'Belum dijadwalkan', '1' => 'Sudah Dijadwalkan'], Request::get('status_penjadwalan'), ['class' => 'form-control'])!!}
			    	</div>
			    	<button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-search"></i></button>
			    </form>
		    </div>
		</div>
	</div>
</div>		    
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
		      <h4>Daftar Evaluasi Peserta<a href="{{ route('dashboard.evaluasi.index') }}" class="btn btn-info pull-right">All Data</a></h4>

		    </div><!-- /.box-header -->
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover" style="font-size:12px;">
	                    <thead>
	                        <tr class="bg-info">
	                          <th class="text-center">No.</th>
	                          <th class="text-center">NIP</th>
	                          <th class="text-center">Nama</th>
	                          <th class="text-center">Unit Induk</th>
	                          <th class="text-center">Kompetensi</th>
	                          <th class="text-center">No. Surat</th>
	                          <th class="text-center">Hasil</th>
	                          <th class="text-center">Tgl. Evaluasi</th>
	                          <th class="text-center">Dijadwalkan?</th>
	                          <th class="col-md-1 text-center">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php $hasilArr = ['ditolak','diterima']; $classArr = ['bg-danger','bg-success']; ?>
	                    @forelse($data['evaluasi'] as $evaluasi)
                    		<tr class="{{ ($evaluasi->hasil != '') ? $classArr[$evaluasi->hasil] : '' }}">
	                        <td>{{ $data['pagination_number'] }}</td>
	                        <td>{{ $evaluasi->nip }}</td>
	                        <td style="white-space:nowrap;">{{ $evaluasi->nama }}</td>
	                        <td>{{ (! is_null($evaluasi->nama_unitinduk)) ? $evaluasi->nama_unitinduk : '-'}}</td>
	                        <td>{{ $evaluasi->nama_kompetensi }}</td>
	                        <td>{{ $evaluasi->nosurat }}</td>
	                        <td>
	                        	@if($evaluasi->hasil != '')
	                        		{{ $hasilArr[$evaluasi->hasil] }}
	                        	@else
	                        		@if(Auth::user()->can('can_write_evaluasi'))
	                        		<a href="{{ route('dashboard.evaluasi.set.hasil', ['id' => $evaluasi->id_peserta, 'kompetensipermohonanid' => $evaluasi->kompetensipermohonan_id ]) }}">Evaluasi</a>
	                        		@else
	                        			-
	                        		@endif
	                        	@endif
	                        </td>
	                        <td>{{ ($evaluasi->tanggal_evaluasi != '') ? $evaluasi->tanggal_evaluasi : '-' }}</td>
	                        <td class="text-center">
	                        	@if($evaluasi->status_penjadwalan != '')
	                        		<span class="label {{ ($evaluasi->status_penjadwalan == '1') ? 'label-success' : 'label-warning' }}">{{ ($evaluasi->status_penjadwalan == '1') ? 'Sudah' : 'Belum' }}</span>
	                        	@else
	                        		-
	                        	@endif
	                        </td>
	                        <td class="text-center">
	                          <a href="{{ route('dashboard.evaluasi.detail',  ['id' => $evaluasi->id_peserta, 'kompetensipermohonanid' => $evaluasi->kompetensipermohonan_id ]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>
                          @if(Auth::user()->can('can_write_evaluasi'))
	                          <a href="{{ route('dashboard.evaluasi.delete', ['id' => $evaluasi->id_evaluasi ]) }}" class="btn btn-xs btn-danger {{ ($evaluasi->hasil == '') ? 'disabled' : ''}}"  onclick="return confirm('Hapus data evaluasi?');"><i class="fa fa-trash"></i> </a>
	                       @endif   
	                        </td>
	                      </tr>
	                      <?php $data['pagination_number']++; ?>
	                    @empty
	                    	<tr>
	                    		<td colspan="10" class="text-center">Tidak ada data evaluasi</td>
	                    	</tr>
	                    @endforelse
	                    </tbody>
			        </table><br>
                  	<div class="text-center">
                  		{!! paginationHelper($data['evaluasi']->appends($data['params'])->render()) !!}
                  	</div>
			     </div>
			</div>
		</div>
	</div>
</div>			        

<!-- Start of Evaluasi SDM -->
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
		      <h4>Daftar Evaluasi SDM</h4>

		    </div><!-- /.box-header -->
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover" style="font-size:12px;">
	                    <thead>
	                        <tr class="bg-info">
	                          <th class="text-center">No.</th>
	                          <th class="text-center">NIP</th>
	                          <th class="text-center">Nama</th>
	                          <th class="text-center">Unit Induk</th>
	                          <th class="text-center">Kompetensi</th>
	                          <th class="text-center">Tgl. Permohonan</th>
	                          <th class="text-center">Hasil</th>
	                          <th class="text-center">Tgl. Evaluasi</th>
	                          <th class="text-center">Dijadwalkan?</th>
	                          <th class="col-md-1 text-center">Aksi</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php $hasilArr = ['ditolak','diterima']; $classArr = ['bg-danger','bg-success']; ?>
	                    @forelse($data['evaluasisdm'] as $evaluasisdm)
                    		<tr class="{{ ($evaluasisdm->hasil != '') ? $classArr[$evaluasisdm->hasil] : '' }}">
	                        <td>{{ $data['pagination_number'] }}</td>
	                        <td>{{ $evaluasisdm->nip }}</td>
	                        <td style="white-space:nowrap;">{{ $evaluasisdm->nama }}</td>
	                        <td>{{ (! is_null($evaluasisdm->nama_unitinduk)) ? $evaluasisdm->nama_unitinduk : '-'}}</td>
	                        <td>{{ $evaluasisdm->nama_kompetensi }}</td>
	                        <td>{{ $evaluasisdm->tanggal_mohon }}</td>
	                        <td>
	                        	@if($evaluasisdm->hasil != '')
	                        		{{ $hasilArr[$evaluasisdm->hasil] }}
	                        	@else
	                        		@if(Auth::user()->can('can_write_evaluasi'))
	                        		<a href="{{ route('dashboard.evaluasi.set.hasil', ['id' => $evaluasisdm->id_peserta, 'kompetensipermohonanid' => $evaluasisdm->kompetensipermohonansdm_id ]) }}">Evaluasi</a>
	                        		@else
	                        			-
	                        		@endif
	                        	@endif
	                        </td>
	                        <td>{{ ($evaluasisdm->tanggal_evaluasi != '') ? $evaluasisdm->tanggal_evaluasi : '-' }}</td>
	                        <td class="text-center">
	                        	@if($evaluasisdm->status_penjadwalan != '')
	                        		<span class="label {{ ($evaluasisdm->status_penjadwalan == '1') ? 'label-success' : 'label-warning' }}">{{ ($evaluasisdm->status_penjadwalan == '1') ? 'Sudah' : 'Belum' }}</span>
	                        	@else
	                        		-
	                        	@endif
	                        </td>
	                        <td class="text-center">
	                          <a href="{{ route('dashboard.evaluasi.detail',  ['id' => $evaluasi->id_peserta, 'kompetensipermohonanid' => $evaluasi->kompetensipermohonansdm_id ]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> </a>
                          @if(Auth::user()->can('can_write_evaluasi'))
	                          <a href="{{ route('dashboard.evaluasi.delete', ['id' => $evaluasi->id_evaluasi ]) }}" class="btn btn-xs btn-danger {{ ($evaluasi->hasil == '') ? 'disabled' : ''}}"  onclick="return confirm('Hapus data evaluasi?');"><i class="fa fa-trash"></i> </a>
	                       @endif   
	                        </td>
	                      </tr>
	                      <?php $data['pagination_number']++; ?>
	                    @empty
	                    	<tr>
	                    		<td colspan="10" class="text-center">Tidak ada data evaluasi</td>
	                    	</tr>
	                    @endforelse
	                    </tbody>
			        </table><br>
                  	<div class="text-center">
                  		{!! paginationHelper($data['evaluasi']->appends($data['params'])->render()) !!}
                  	</div>
			     </div>
			</div>
		</div>
	</div>
</div>	

@stop
