@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Edit Elemen Kompetensi</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	{!! Form::open(['route' => ['dashboard.master.kompetensi.elemenkompetensi.update', $data['elemenkompetensi']->id], 'class' => 'form-horizontal', 'method' => 'put']) !!}
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Bidang</label>
                      <div class="col-sm-7">
                  		{!! Form::select('kompetensi_id', $data['kompetensi'], $data['elemenkompetensi']->kompetensi_id, ['class'=>'form-control']) !!}
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Nama Elemen</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" value="{{ $data['elemenkompetensi']->nama_elemen }}" name="nama_elemen" required>
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Nama Elemen (English)</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama elemen kompetensi (english)" name="nama_elemen_english" value="{{ $data['elemenkompetensi']->nama_elemen_english }}">
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Update</button>&nbsp;
		            <a href="{{ route('dashboard.master.elemenkompetensi.kompetensi.index',['kompetensi_id' => $data['elemenkompetensi']->kompetensi_id]) }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    {!! Form::close() !!}
		</div>
	</div>
</div>		    
@stop		    