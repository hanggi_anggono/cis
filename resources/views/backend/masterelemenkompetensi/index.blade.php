@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_masterkompetensi'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.elemenkompetensi.kompetensi.create',['kompetensi_id'=>$data['kompetensi_id']]) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Elemen Kompetensi</a>
		      <a href="{{ route('dashboard.master.kompetensi.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
		    </div><!-- /.box-header -->
		@else
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.kompetensi.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
		    </div><!-- /.box-header -->
		@endif
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th class="col-md-1">No.</th>
				          	<th>Nama Elemen Kompetensi</th>
				          	<th class="col-md-3 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @forelse($data['elemenkompetensi'] as $elemenkompetensi)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $elemenkompetensi->nama_elemen }} {!! (! empty($elemenkompetensi->nama_elemen_english)) ? '(<em>'.$elemenkompetensi->nama_elemen_english.'</em>)' : '' !!}</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterkompetensi'))
				          		<a href="{{ route('dashboard.master.kompetensi.elemenkompetensi.edit', ['id' => $elemenkompetensi->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.elemenkompetensi.delete', ['id' => $elemenkompetensi->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>
				      @empty
				      <tr>
				      	<td class="text-center" colspan="3">Tidak ada data elemen kompetensi</td>
				      </tr>    
			          @endforelse
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['elemenkompetensi']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop