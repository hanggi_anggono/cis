@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-aqua"><i class="fa fa-edit"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Permohonan</span>
	      <span class="info-box-number text-center"><h3><strong>{{ $data['summary']['permohonan'] }}</strong></h3></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-red"><i class="fa fa-pencil-square-o"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Pelaksanaan</span>
	      <span class="info-box-number text-center"><h3><strong>{{ $data['summary']['pelaksanaan'] }}</strong></h3></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-green"><i class="fa fa-certificate"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Sertifikat Kompeten</span>
	      <span class="info-box-number text-center"><h3><strong>{{ $data['summary']['kompeten'] }}</strong></h3></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-yellow"><i class="fa fa-group"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Peserta Hadir</span>
	      <span class="info-box-number text-center"><h3><strong>{{ $data['summary']['kehadiran'] }}</strong></h3></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
	</div><!-- /.row -->

	<div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">1 Bulan</a></li>
              <li><a href="#tab_2" data-toggle="tab">3 Bulan</a></li>
              <li><a href="#tab_3" data-toggle="tab">6 Bulan</a></li>
              <li><a href="#tab_4" data-toggle="tab">Expired</a></li>
               <li class="pull-right header"><i class="fa fa-certificate"></i> Expired Warning Certificate</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <div class="table-responsive">
                	<table class="table table-sm table-striped">
                		<thead class="bg-warning">
                			<tr>
                				<th>No.</th>
                				<th>No. Sertifikat</th>
                				<th>Kompetensi</th>
                				<th>LSK</th>
                				<th>NIP</th>
                				<th>Nama</th>
                				<th>Tanggal Berlaku</th>
                			</tr>
                		</thead>
                		<?php $no1 = 1; ?>
                		<tbody>
						@forelse($data['summary']['sertifikat']['1month'] as $value)
							<tr>
								<td>{{ $no1++ }}</td>
								<td>{{ $value->no_sertifikat }}</td>
								<td>{{ $value->kompetensi->nama_kompetensi }}</td>
								<td>{{ $value->lembagasertifikasi->nama_lembagasertifikasi }}</td>
								<td>{{ $value->peserta->nip }}</td>
								<td>{{ $value->peserta->nama }}</td>
								<td>{{ $value->tanggal_mulai_berlaku }} s/d {{ $value->tanggal_akhir_berlaku }}</td>
							</tr>
						@empty
							<tr>
								<td colspan="7" class="text-center">Tidak ada sertifikat expired dalam 1 bulan ke depan</td>
							</tr>
						@endforelse                			
                		</tbody>
                	</table>
                	@if(Auth::user()->can('can_read_sertifikatperpanjangan'))
                	<div class="pull-right">
                		<a href="{{ route('dashboard.sertifikat.perpanjangan.index') }}" class="btn btn-sm btn-default">Lihat Semua</a>
                	</div>
                	@endif
                </div>
              </div><!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <div class="table-responsive">
                	<table class="table table-sm table-striped">
                		<thead class="bg-warning">
                			<tr>
                				<th>No.</th>
                				<th>No. Sertifikat</th>
                				<th>Kompetensi</th>
                				<th>LSK</th>
                				<th>NIP</th>
                				<th>Nama</th>
                				<th>Tanggal Berlaku</th>
                			</tr>
                		</thead>
                		<tbody>
                		<?php $no2 = 1; ?>
						@forelse($data['summary']['sertifikat']['3month'] as $value)
							<tr>
								<td>{{ $no2++ }}</td>
								<td>{{ $value->no_sertifikat }}</td>
								<td>{{ $value->kompetensi->nama_kompetensi }}</td>
								<td>{{ $value->lembagasertifikasi->nama_lembagasertifikasi }}</td>
								<td>{{ $value->peserta->nip }}</td>
								<td>{{ $value->peserta->nama }}</td>
								<td>{{ $value->tanggal_mulai_berlaku }} s/d {{ $value->tanggal_akhir_berlaku }}</td>
							</tr>
						@empty
							<tr>
								<td colspan="7" class="text-center">Tidak ada sertifikat expired dalam 3 bulan ke depan</td>
							</tr>
						@endforelse                			
                		</tbody>
                	</table>
                	@if(Auth::user()->can('can_read_sertifikatperpanjangan'))
                	<div class="pull-right">
                		<a href="{{ route('dashboard.sertifikat.perpanjangan.index') }}" class="btn btn-sm btn-default">Lihat Semua</a>
                	</div>
                	@endif
                </div>
              </div><!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <div class="table-responsive">
                	<table class="table table-sm table-striped">
                		<thead class="bg-warning">
                			<tr>
                				<th>No.</th>
                				<th>No. Sertifikat</th>
                				<th>Kompetensi</th>
                				<th>LSK</th>
                				<th>NIP</th>
                				<th>Nama</th>
                				<th>Tanggal Berlaku</th>
                			</tr>
                		</thead>
                		<tbody>
                		<?php $no3 = 1; ?>
						@forelse($data['summary']['sertifikat']['6month'] as $value)
							<tr>
								<td>{{ $no3++ }}</td>
								<td>{{ $value->no_sertifikat }}</td>
								<td>{{ $value->kompetensi->nama_kompetensi }}</td>
								<td>{{ $value->lembagasertifikasi->nama_lembagasertifikasi }}</td>
								<td>{{ $value->peserta->nip }}</td>
								<td>{{ $value->peserta->nama }}</td>
								<td>{{ $value->tanggal_mulai_berlaku }} s/d {{ $value->tanggal_akhir_berlaku }}</td>
							</tr>
						@empty
							<tr>
								<td colspan="7" class="text-center">Tidak ada sertifikat expired dalam 6 bulan ke depan</td>
							</tr>
						@endforelse                			
                		</tbody>
                	</table>
                	@if(Auth::user()->can('can_read_sertifikatperpanjangan'))
                	<div class="pull-right">
                		<a href="{{ route('dashboard.sertifikat.perpanjangan.index') }}" class="btn btn-sm btn-default">Lihat Semua</a>
                	</div>
                	@endif
                </div>
              </div><!-- /.tab-pane -->
              <div class="tab-pane" id="tab_4">
                <div class="table-responsive">
                	<table class="table table-sm table-striped">
                		<thead class="bg-warning">
                			<tr>
                				<th>No.</th>
                				<th>No. Sertifikat</th>
                				<th>Kompetensi</th>
                				<th>LSK</th>
                				<th>NIP</th>
                				<th>Nama</th>
                				<th>Tanggal Berlaku</th>
                			</tr>
                		</thead>
                		<?php $no1 = 1; ?>
                		<tbody>
						@forelse($data['summary']['sertifikat']['expired'] as $value)
							<tr>
								<td>{{ $no1++ }}</td>
								<td>{{ $value->no_sertifikat }}</td>
								<td>{{ $value->kompetensi->nama_kompetensi }}</td>
								<td>{{ $value->lembagasertifikasi->nama_lembagasertifikasi }}</td>
								<td>{{ $value->peserta->nip }}</td>
								<td>{{ $value->peserta->nama }}</td>
								<td>{{ $value->tanggal_mulai_berlaku }} s/d {{ $value->tanggal_akhir_berlaku }}</td>
							</tr>
						@empty
							<tr>
								<td colspan="7" class="text-center">Tidak ada sertifikat expired</td>
							</tr>
						@endforelse                			
                		</tbody>
                	</table>
                	@if(Auth::user()->can('can_read_sertifikatperpanjangan'))
                	<div class="pull-right">
                		<a href="{{ route('dashboard.sertifikat.perpanjangan.index') }}" class="btn btn-sm btn-default">Lihat Semua</a>
                	</div>
                	@endif
                </div>
              </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
          </div><!-- nav-tabs-custom -->
        </div><!-- /.col -->
    </div>    

	<div class="row">
	<div class="col-md-12">
	  <div class="box box-info">
	      <div class="box-header with-border">
	        <h3 class="box-title">Penjadwalan Sertifikasi Terbaru</h3>
	        <div class="pull-right">
	          Total: <span class="badge alert-info">{{ $data['summary']['penjadwalan']['count'] }}</span> Jadwal
	        </div>
	      </div><!-- /.box-header -->
	      <div class="box-body">
	        <div class="table-responsive">
	          <table class="table table-condensed table-bordered table-hover">
	            <thead>
	                <tr class="bg-info">
	                  <th class="text-center">No.</th>
	                  <th class="text-center">Tanggal Mulai</th>
	                  <th class="text-center">Tanggal Selesai</th>
	                  <th class="text-center">TUK</th>
	                  <th class="text-center">LSK</th>
	                  <th class="text-center">Unit Induk</th>
	                  <th class="text-center">Jumlah Kompetensi</th>
	                  <th class="text-center">Jumlah Peserta</th>
	                  <th class="text-center">Aksi</th>
	                </tr>
	            </thead>
	            <tbody>
	            <?php $no = 1; ?>
	              @forelse($data['summary']['penjadwalan']['collection'] as $penjadwalan)
	              <tr>
	              	<td>{{ $no++ }}</td>
	              	<td>{{ $penjadwalan->tanggal_mulai }}</td>
	              	<td>{{ $penjadwalan->tanggal_selesai }}</td>
	              	<td>{{ $penjadwalan->udiklat->nama_udiklat }}</td>
	              	<td>{{ $penjadwalan->lembagasertifikasi->nama_lembagasertifikasi }}</td>
	              	<td>{{ $penjadwalan->unitinduk->nama_unitinduk }}</td>
	              	<td>{{ $penjadwalan->kompetensi->count() }} Kompetensi</td>
	              	<td class="text-center">{{ $penjadwalan->peserta->count() }} Peserta</td>
	              	<td class="text-center">
	              		<a href="{{ route('dashboard.penjadwalan.show', ['id' => $penjadwalan->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-search"></i> Detail</a>
	              	</td>
	              </tr>
	              @empty
	              <tr>
	              	<td colspan="9" class="text-center"> Tidak ada data penjadwalan</td>
	              </tr>
	              @endforelse
	            </tbody>
	          </table>
	          @if(Auth::user()->can('can_read_penjadwalan'))
	          <div class="pull-right">
	              <a href="{{ route('dashboard.penjadwalan.index') }}" class="btn btn-sm btn-default">Lihat Semua</a>
	          </div>
	          @endif
	        </div>
	      </div>
	      
	  </div>
	</div>      
</div> <!-- /.row -->
@stop