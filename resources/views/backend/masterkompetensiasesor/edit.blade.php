@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Edit Kompetensi Asesor</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	{!! Form::open(['route' => ['dashboard.master.kompetensiasesor.update', $data['kompetensiasesor']->id], 'class' => 'form-horizontal', 'method' => 'put']) !!}
		    		<input type="hidden" name="kompetensi_old" value="{{ $data['kompetensipicked'] }}">
		    		<input type="hidden" name="asesor_id" value="{{ $data['asesorpicked'] }}">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Asesor</label>
                      <div class="col-sm-7">
                  		<input type="text" class="form-control" value="{{ $data['kompetensiasesor']->nama }}" readonly="readonly">
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Kompetensi</label>
                      <div class="col-sm-7">
                  		{!! Form::select('kompetensi_id', $data['kompetensi'], $data['kompetensipicked'], ['class'=>'select2']) !!}
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Update</button>&nbsp;
		            <button class="btn btn-warning" onclick="history.back();">Kembali</button>
    			</div>
		    </div>
		    {!! Form::close() !!}
		</div>
	</div>
</div>		    
@stop		    