@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
			@if(Auth::user()->can('can_write_masterasesor'))
		      <a href="{{ route('dashboard.master.kompetensiasesor.asesor.create',['asesor_id'=>$data['asesor_id']]) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Kompetensi Asesor</a>
		    @endif
		      <span class="pull-right"><strong>Asesor: {{ $data['kompetensiasesor']->nama }}</strong></span>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th class="col-md-1">No.</th>
				          	<th>Nama Kompetensi Asesor</th>
				          	<th class="col-md-2 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @forelse($data['kompetensiasesor']->kompetensi as $kompetensiasesor)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $kompetensiasesor->nama_kompetensi }}</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterasesor'))
				          		<a href="{{ route('dashboard.master.asesor.kompetensiasesor.delete', ['kompetensi' => $kompetensiasesor->id,'asesor' => $kompetensiasesor->pivot->asesor_id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>   
				      @empty
				      <tr>
				      	<td colspan="3" class="text-center">Tidak ada data</td>
				      </tr> 
			          @endforelse
			          </tbody>
			        </table>
			    </div> <br>
			    <div class="text-center">
		        	<a href="{{ route('dashboard.master.asesor.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Kembali</a>
		        </div>     
		    </div>
		</div>
	</div>
</div>
@stop