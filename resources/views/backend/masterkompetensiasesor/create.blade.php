@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Tambah Kompetensi Asesor</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	<form class="form-horizontal" action="{{ route('dashboard.master.asesor.kompetensiasesor.store') }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Asesor</label>
                      <div class="col-sm-7">
                      	@if(isset($data['asesor_single']))
                      		<input type="text" class="form-control" value="{{ $data['asesor_single']->nama }}" readonly="readonly">
                      		<input type="hidden" class="form-control" name="asesor_id" value="{{ $data['asesor_single']->id }}">
                      	@else
                      		{!! Form::select('asesor_id', $data['asesor'], null,['class'=>'selectpicker form-control','data-live-search' => 'true'])!!}
                      	@endif          
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Kompetensi</label>
                      <div class="col-sm-7">
                      	@if(isset($data['kompetensi_single']))
                      		<input type="text" class="form-control" value="{{ $data['kompetensi_single']->nama_kompetensi }}" readonly="readonly">
                      		<input type="hidden" class="form-control" name="kompetensi_id" value="{{ $data['kompetensi_single']->id }}">
                      	@else
                      		{!! Form::select('kompetensi_id', $data['kompetensi'], null,['class'=>'selectpicker form-control','data-live-search' => 'true'])!!}
                      	@endif          
                      </div>
		    		</div>		    		
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		            <a href="{{ route('dashboard.master.asesor.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    