@extends('layouts.backend.master')
@section('content')
<!-- Your Page Content Here -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-aqua"><i class="fa fa-edit"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Permohonan</span>
	      <span class="info-box-number"><a href="#">3</a></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-red"><i class="fa fa-pencil-square-o"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Pelaksanaan</span>
	      <span class="info-box-number"><a href="#">20</a></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-green"><i class="fa fa-certificate"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Sertifikat <br>yang Kompeten</span>
	      <span class="info-box-number"><a href="#">150</a></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
	  <div class="info-box">
	    <span class="info-box-icon bg-yellow"><i class="fa fa-group"></i></span>
	    <div class="info-box-content">
	      <span class="info-box-text">Peserta Hadir</span>
	      <span class="info-box-number"><a href="#">500</a></span>
	    </div><!-- /.info-box-content -->
	  </div><!-- /.info-box -->
	</div><!-- /.col -->
	</div><!-- /.row -->

<div class="row">
	<div class="col-md-12">
	<div class="box box-info">
	    <div class="box-header with-border">
	      <h3 class="box-title">Administrator Area</h3>
	    </div><!-- /.box-header -->
	    <div class="box-body">
        Selamat datang di dashboard Administrator, silahkan akses menu di sebelah kiri untuk navigasi modul
        </div>
	      
	  </div>
	</div>      
</div> <!-- /.row -->
@stop