@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="box box-primary">
			<div class="box-header with-border">
		     <h3>Form Tambah Kompetensi</h3>
		    </div><!-- /.box-header -->
		    <div class="box-body">
		    	@if (Session::has('flash_notification.message'))
				    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				        {{ Session::get('flash_notification.message') }}
				    </div>
				@endif
		    	<form class="form-horizontal" action="{{ route('dashboard.master.kompetensi.store') }}" method="post">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Subbidang</label>
                      <div class="col-sm-7">
	              		{!! Form::select('subbidang_id', $data['subbidang'], null,['class'=>'form-control']) !!} 
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Nama Kompetensi</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama Kompetensi" name="nama_kompetensi" required>
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Nama Kompetensi (English)</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Nama Kompetensi (English)" name="nama_kompetensi_english">
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Kode DJK</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Kode DJK" name="kodedjk">
                      </div>
		    		</div>
		    		<div class="form-group">
                      <label for="nama" class="col-sm-3 control-label">Kode SKKNI</label>
                      <div class="col-sm-7">
                          <input type="text" class="form-control" placeholder="Kode SKKNI" name="kodeskkni">
                      </div>
		    		</div>
		    </div>
		    <div class="box-footer">
		    	<div class="col-sm-4 col-md-offset-3">
    				<button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
		            <a href="{{ route('dashboard.master.kompetensi.index') }}" class="btn btn-warning">Kembali</a>
    			</div>
		    </div>
		    </form>
		</div>
	</div>
</div>		    
@stop		    