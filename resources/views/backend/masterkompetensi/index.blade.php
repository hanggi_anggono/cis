@extends('layouts.backend.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
		@if(Auth::user()->can('can_write_masterkompetensi'))
			<div class="box-header with-border">
		      <a href="{{ route('dashboard.master.kompetensi.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Kompetensi</a>&nbsp; 
		      <a href="{{ route('dashboard.master.kompetensi.elemenkompetensi.create') }}" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Tambah Elemen Kompetensi</a>
		    </div><!-- /.box-header -->
		@endif
		    <div class="box-body">
		    @if (Session::has('flash_notification.message'))
			    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
			        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			        {{ Session::get('flash_notification.message') }}
			    </div>
			@endif
		    	 <div class="table-responsive">
			        <table class="table no-margin table-condensed table-bordered table-hover">
			          <thead class="bg-info">
				          <tr>
				          	<th>No.</th>
				          	<th class="col-md-4">Nama Kompetensi</th>
				          	<th class="col-md-1">Kode DJK</th>
				          	<th class="col-md-1">Kode SKKNI</th>
				          	<th>Elemen Kompetensi</th>
				          	<th>Sub Bidang</th>
				          	<th class="col-md-2 text-center">Aksi</th>
				          </tr>
			          </thead>
			          <tbody>
			          @foreach($data['kompetensi'] as $kompetensi)
				          <tr>
				          	<td>{{ $data['pagination_number'] }}</td>
				          	<td>{{ $kompetensi->nama_kompetensi }} {!! (! empty($kompetensi->nama_kompetensi_english)) ? '(<em>'.$kompetensi->nama_kompetensi_english.'</em>)' : '' !!}</td>
				          	<td>{{ $kompetensi->kodedjk }}</td>
				          	<td>{{ $kompetensi->kodeskkni }}</td>
				          	<td><a href="{{ route('dashboard.master.elemenkompetensi.kompetensi.index',['kompetensi_id' => $kompetensi->id]) }}">{{ $kompetensi->elemenKompetensi->count() }} Elemen</a></td>
				          	<td>{{ $kompetensi->subbidang->nama_subbidang }}</td>
				          	<td class="text-center">
				          	@if(Auth::user()->can('can_write_masterkompetensi'))
				          		<a href="{{ route('dashboard.master.kompetensi.edit', ['id' => $kompetensi->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
				          		<a href="{{ route('dashboard.master.kompetensi.delete', ['id' => $kompetensi->id]) }}" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data?'); "><i class="fa fa-trash"></i> Delete</a>
				          	@else
				          		-
				          	@endif
				          	</td>
				          </tr>
				      <?php $data['pagination_number']++; ?>    
			          @endforeach
			          </tbody>
			        </table><br>
			        <div class="text-center">
			        	{!! paginationHelper($data['kompetensi']->render()) !!}
			        </div>
			    </div>      
		    </div>
		</div>
	</div>
</div>
@stop