<table>
  <tr>
    <td colspan="5"><strong>Laporan Biaya Pelaksanaan</strong></td>
  </tr>
  <tr>
    <td colspan="5"><strong>Tanggal: {{ indonesianDate(date('Y-m-d')) }}</strong></td>
    <td colspan="5"></td>
  </tr>
</table>

<table>
	<thead>
		<tr>
			<th>No.</th>
			<th>Periode Pelaksanaan</th>
			<th>Komponen</th>
			<th>No. Tagihan</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; $total = 0;?>
	@forelse($data['collection'] as $biaya)
		<tr>
		<td>{{ $no++ }}</td>
		<td>{{ $biaya->penjadwalan->tanggal_mulai }} s/d {{ $biaya->penjadwalan->tanggal_selesai }}</td>
		<td>{{ $biaya->komponenbiaya->nama_komponenbiaya }}</td>
		<td>{{ (! empty($biaya->nosurat_tagihan)) ? $biaya->nosurat_tagihan : '-' }}</td>
		<td>{{ IDRFormatter($biaya->biaya) }}</td>
		</tr>
		<?php $total += $biaya->biaya; ?>
		@empty
		<tr>
			<td colspan="5"><center> Tidak ada data biaya</center></td>
		</tr>
		@endforelse
		<tr>
			<td colspan="4"><center><strong>Total Biaya Keseluruhan</strong></center></td>
			<td><center><strong>{{ IDRFormatter($total) }}</strong></center></td>
		</tr>
	</tbody>
</table>