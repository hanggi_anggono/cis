<table>
  <tr>
    <td colspan="7"><strong>Laporan Asesor</strong></td>
  </tr>
  <tr>
    <td colspan="7"><strong>Tanggal: {{ indonesianDate(date('Y-m-d')) }}</strong></td>
    <td colspan="7"></td>
  </tr>
</table>

<table>
  <thead>
        <tr>
          <th>No.</th>
          <th>NIP</th>
          <th>Nama</th>
          <th>Induk / Institusi</th>
          <th>Non PLN</th>
          <th>Lembaga Sertifikasi</th>
          <th>Subbidang Kompetensi</th>
        </tr>
    </thead>
    <tbody>
    <?php $no=1; ?>
    @forelse($data['collection'] as $asesor)
      <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $asesor->nip }}</td>
        <td>{{ $asesor->nama }}</td>
        <td>{{ (! is_null($asesor->unitinduk)) ? $asesor->unitinduk->nama_unitinduk : '-' }}</td>
        <td>{{ (! is_null($asesor->unitnonpln)) ? $asesor->unitnonpln->nama_unitnonpln : '-' }}</td>
        <td>
          @forelse($asesor->kelompok as $kelompok)
            - {{ $kelompok->nama_lembagasertifikasi }}<br>
          @empty
            -
          @endforelse
        </td>
        <td>
          @forelse($asesor->subbidang as $subbidang)
            - {{ $subbidang->nama_subbidang }}<br>
          @empty
            -
          @endforelse
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="7">Tidak ada data asesor</td>
      </tr>
    @endforelse
    </tbody>
</table>