<table>
  <tr>
    <td colspan="8"><strong>Laporan Sebaran Kompetensi Sertifikasi</strong></td>
  </tr>
  <tr>
    <td colspan="8"><strong>Tanggal: {{ indonesianDate(date('Y-m-d')) }}</strong></td>
    <td colspan="8"></td>
  </tr>
</table>

<table>
  <thead>
      <tr>
        <th>No.</th>
        <th>Kode DJK</th>
        <th>Kode SKKNI</th>
        <th>Nama Kompetensi</th>
        <th>Nama Bidang</th>
        <th>Nama Subbidang</th>
        <th>Kompeten</th>
        <th>Belum Kompeten</th>
      </tr>
  </thead>
  <tbody>
  <?php $no=1; ?>
  @forelse($data['collection'] as $sebaran)
    <tr>
      <td>{{ $no++ }}</td>
      <td>{{ (! empty($sebaran->kodedjk)) ? $sebaran->kodedjk : '-' }}</td>
      <td>{{ (! empty($sebaran->kodeskkni)) ? $sebaran->kodeskkni : '-' }}</td>
      <td>{{ $sebaran->nama_kompetensi }}</td>
      <td>{{ $sebaran->nama_bidang }}</td>
      <td>{{ $sebaran->nama_subbidang }}</td>
      <td>{{ $sebaran->passesCount }} Peserta</td>
      <td>{{ $sebaran->notpassesCount }} Peserta</td>
    </tr>
  @empty
    <tr>
      <td colspan="8">Tidak ada data kompetensi</td>
    </tr>
  @endforelse
  </tbody>
</table>