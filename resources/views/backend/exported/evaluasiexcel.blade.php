<table>
  <tr>
    <td colspan="9"><strong>Laporan Hasil Evaluasi Sertifikasi PLN</strong></td>
  </tr>  
  <tr>
    <td colspan="9"><strong>Tanggal: {{ indonesianDate(date('Y-m-d')) }}</strong></td>
  </tr>
   <tr>
    <td colspan="9"></td>
  </tr>
</table>
 <table class="table">
    <thead>
        <tr>
          <th>No.</th>
          <th>NIP</th>
          <th>Nama</th>
          <th>Unit Induk</th>
          <th>Kompetensi</th>
          <th>No. Surat</th>
          <th>Hasil</th>
          <th>Tgl. Evaluasi</th>
          <th>Status Penjadwalan</th>
        </tr>
    </thead>
    <tbody>
    <?php $no = 1; $hasilArr = ['ditolak','diterima'];; ?>
    @forelse($data['collection'] as $evaluasi)
      <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $evaluasi->nip }}</td>
        <td>{{ $evaluasi->nama }}</td>
        <td>{{ (! is_null($evaluasi->nama_unitinduk)) ? $evaluasi->nama_unitinduk : '-'}}</td>
        <td>{{ $evaluasi->nama_kompetensi }}</td>
        <td>{{ $evaluasi->nosurat }}</td>
        <td>
          @if($evaluasi->hasil != '')
            {{ $hasilArr[$evaluasi->hasil] }}
          @else
             Belum dievaluasi
          @endif
        </td>
        <td>{{ ($evaluasi->tanggal_evaluasi != '') ? $evaluasi->tanggal_evaluasi : '-' }}</td>
        <td class="text-center">
          @if($evaluasi->status_penjadwalan != '')
            <span class="label {{ ($evaluasi->status_penjadwalan == '1') ? 'label-success' : 'label-warning' }}">{{ ($evaluasi->status_penjadwalan == '1') ? 'Sudah dijadwalkan' : 'Belum dijadwalkan' }}</span>
          @else
            -
          @endif
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="9" class="text-center">Tidak ada data evaluasi</td>
      </tr>
    @endforelse
    </tbody>
</table>