<table>
  <tr>
    <td colspan="8"><strong>Laporan Hasil Kelulusan Sertifikasi</strong></td>
  </tr>
  <tr>
    <td colspan="8"><strong>Tanggal: {{ indonesianDate(date('Y-m-d')) }}</strong></td>
    <td colspan="8"></td>
  </tr>
</table>

<table>
  <thead>
      <tr>
        <th>No.</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>Tipe Peserta</th>
        <th>Kompetensi</th>
        <th>Kelulusan</th>
        <th>Tanggal Realisasi</th>
      </tr>
  </thead>
  <tbody>
  <?php $no=1; ?>
  @forelse($data['collection'] as $realisasi)
  <tr>
    <td>{{ $no++ }}</td>
    <td>{{ $realisasi->peserta->nip }}</td>
    <td>{{ $realisasi->peserta->nama }}</td>
    <td>{{ ($realisasi->peserta->tipe_peserta == 'pln') ? 'PLN' : 'NON PLN' }}</td>
    <td>{{ $realisasi->kompetensi->nama_kompetensi }}</td>
    <td>{{ ($realisasi->kelulusan == 'y') ? 'Kompeten' : 'Belum Kompeten' }}</td>
    <td>{{ $realisasi->created_at }}</td>
  </tr>
  @empty
  <tr>
    <td colspan="7">Tidak ada data kelulusan</td>
  </tr>
  @endforelse
  </tbody>
</table