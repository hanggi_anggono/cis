<table>
  <tr>
    <td colspan="11"><strong>Laporan Pelaksanaan Sertifikasi</strong></td>
  </tr>
  <tr>
    <td colspan="11"><strong>Tanggal: {{ indonesianDate(date('Y-m-d')) }}</strong></td>
    <td colspan="11"></td>
  </tr>
</table>

<table>
  <thead>
      <tr>
        <th>No.</th>
        <th>Tanggal Pelaksanaan</th>
        <th>TUK</th>
        <th>LSK</th>
        <th>Unit Induk</th>
        <th>Kompetensi</th>
        <th>Rencana</th>
        <th>Kehadiran</th>
        <th>Kompeten</th>
        <th>Belum Kompeten</th>
      </tr>
  </thead>
  <tbody>
    <?php $no=1; ?>
    @forelse($data['collection']['penjadwalan'] as $pelaksanaan)
    <tr>
      <td>{{ $no++ }}</td>
      <td>{{ $pelaksanaan->tanggal_mulai }} s/d {{ $pelaksanaan->tanggal_selesai }}</td>
      <td>{{ $pelaksanaan->udiklat->nama_udiklat }}</td>
      <td>{{ $pelaksanaan->lembagasertifikasi->nama_lembagasertifikasi }}</td>
      <td>{{ $pelaksanaan->unitinduk->nama_unitinduk }}</td>
      <td style="white-space:nowrap; wrap-text: true;">
        @forelse($pelaksanaan->kompetensi as $kompetensi)
          - {{ $kompetensi->nama_kompetensi }}<br>
        @empty
          -
        @endforelse
      </td>
      <td style="white-space:nowrap; wrap-text: true;">
        @forelse($data['collection']['pelaksanaan'] as $realisasi)
        @if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
          {{ $realisasi['total_peserta'] }}<br>
        @endif    
        @empty
          -
        @endforelse
      </td>
      <td style="white-space:nowrap; wrap-text: true;">
        @forelse($data['collection']['pelaksanaan'] as $realisasi)
          @if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
            {{ $realisasi['realisasi']['kehadiran'] }}<br>
          @endif  
        @empty
        -
        @endforelse
      </td>
      <td style="white-space:nowrap; wrap-text: true;">
        @forelse($data['collection']['pelaksanaan'] as $realisasi)
          @if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
            {{ $realisasi['realisasi']['kompeten'] }}<br>
          @endif  
        @empty
        -
        @endforelse
      </td>
      <td style="white-space:nowrap; wrap-text: true;">
        @forelse($data['collection']['pelaksanaan'] as $realisasi)
          @if($realisasi['penjadwalan_id'] == $pelaksanaan->id)
            {{ $realisasi['realisasi']['nonkompeten'] }}<br>
          @endif  
        @empty
        -
        @endforelse
      </td>
    </tr>
    @empty
    <tr>
      <td colspan="11" class="text-center">Tidak ada data pelaksanaan</td>
    </tr>
    @endforelse
  </tbody>
</table>