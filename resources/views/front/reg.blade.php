<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>PLN Certification Information System | Registration</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('assets/dist/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.5/css/bootstrap-select.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="register-page">
  <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li><a href="{{ url('/') }}" class="btn btn-primary">Sign In</a></li>
          </ul>
        </nav>
      </div>
   </div>   
    <div class="register-box">
      <div class="login-logo">
        <a href="{{ url('/') }}"><img src="{{ url().'/assets/dist/img/logo-pln-corpu.png'}}" style="width: 45%; height: 45% "><img src="{{ url().'/assets/dist/img/pln.jpg'}}" style="width: 10%; height: 10%; margin-left:20px; "><br>Your Global Personnel Certification Partner</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg"><b>Registrasi Permohonan Uji Kompetensi</b></p>
        @if($errors->any())
          <div class="alert alert-danger alert-dismissable">
            <ul>
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">x</button>
            @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <form class="form-horizontal" action="{{ route('register.post') }}" method="post" accept-charset="utf-8" id="registration-form" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Bidang Kompetensi</label>
                  <div class="col-sm-7">
                      {!! Form::select('bidang_id', ['0' => '-- Pilih Bidang --'] + $bidang, '', ['class' => 'selectpicker form-control bidang', 'data-live-search' => 'true', 'id' => 'bidang_id', 'autocomplete' => 'off']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Subbidang Kompetensi</label>
                  <div class="col-sm-7">
                      {!! Form::select('subbidang', [], '', ['class' => 'selectpicker form-control subbidang', 'data-live-search' => 'true', 'disabled' => 'true', 'id' => 'subbidang', 'autocomplete' => 'off']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Kompetensi</label>
                  <div class="col-sm-7">
                      {!! Form::select('kompetensi_id', [], '', ['class' => 'selectpicker form-control', 'data-live-search' => 'true', 'disabled' => 'true', 'id' => 'kompetensi', 'autocomplete' => 'off']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Tipe</label>
                  <div class="col-sm-3">
                      {!! Form::select('tipe_peserta', ['pln'=>'PLN','nonpln'=>'NON PLN'], (old('tipe_peserta') == 'pln') ? old('pln') : 'pln', ['class'=>'form-control', 'id' => 'tipe_peserta', 'autocomplete' => 'off'])!!}
                  </div>
                </div>
                <div class="form-group" id="info-nonpln" style="display:none;">
                  <label for="nip" class="col-sm-3 control-label">Unit Non PLN</label>
                  <div class="col-sm-5">
                      {!! Form::select('unitnonpln_id', $data['support']['nonpln'], '', ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitnonpln_id', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">NIP</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="NIP peserta" name="nip"  value="{{ old('nip') }}" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">No Identitas</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="No. KTP / SIM / passport" name="no_identitas" value="{{ old('no_identitas') }}" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Nama</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="Nama lengkap peserta" name="nama"  value="{{ old('nama') }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Gol Darah</label>
                  <div class="col-sm-7">
                      <label class="radio-inline">
                        <input type="radio" name="golongan_darah" id="inlineRadio1" value="O" {{ (old('golongan_darah') == 'O') ? 'checked' : '' }}> O
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="golongan_darah" id="inlineRadio2" value="A" {{ (old('golongan_darah') == 'A') ? 'checked' : '' }}> A
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="golongan_darah" id="inlineRadio1" value="B" {{ (old('golongan_darah') == 'B') ? 'checked' : '' }}> B
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="golongan_darah" id="inlineRadio2" value="AB" {{ (old('golongan_darah') == 'AB') ? 'checked' : '' }}> AB
                      </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Kewarganegaraan</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="Kewarganegaraan peserta" name="kewarganegaraan" value="{{ old('kewarganegaraan') }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Jenis Kelamin</label>
                  <div class="col-sm-7">
                      <label class="radio-inline">
                        <input type="radio" name="jenis_kelamin" id="inlineRadio1" value="m" {{ (old('jenis_kelamin') == 'm') ? 'checked' : '' }}> Laki-laki
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="jenis_kelamin" id="inlineRadio2" value="f" {{ (old('jenis_kelamin') == 'f') ? 'checked' : '' }}> Perempuan
                      </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-3 control-label">Email</label>
                  <div class="col-sm-7">
                      <input type="email" class="form-control" placeholder="Email peserta" name="email" value="{{ old('email') }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">No. Handphone</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="No. Handphone peserta" name="no_hp"  value="{{ old('no_hp') }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Telepon Rumah</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="Nama telepon rumah peserta" name="no_telp"  value="{{ old('no_telp') }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Tempat Lahir</label>
                   <div class="col-sm-7">
                    <input type="text" class="form-control" placeholder="Tempat lahir" name="tempat_lahir" value="{{ old('tempat_lahir') }}">
                   </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Tanggal Lahir</label>
                  <div class="col-sm-2">
                      {!! Form::selectRange('dd', 1, 31, old('dd'), ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off']) !!}
                  </div>
                  <div class="col-sm-3">
                      {!! Form::selectMonth('mm', old('mm'),['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off']) !!}
                  </div>
                  <div class="col-sm-2">
                      {!! Form::selectRange('yy', 1945, 2005, old('yy'), ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Alamat</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="Alamat sesuai dengan kartu identitas peserta" name="alamat" value="{{ old('alamat') }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Jenjang Jabatan</label>
                  <div class="col-sm-5">
                      {!! Form::select('jenjangjabatan_id', $data['support']['jenjang'], old('jenjangjabatan_id'), ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off'])!!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Jabatan</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="Jabatan peserta" name="jabatan" valu="{{ old('jabatan') }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Grade</label>
                  <div class="col-sm-5">
                      {!! Form::select('grade_id', $data['support']['grade'], old('grade_id'), ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off'])!!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Pendidikan</label>
                   <div class="col-sm-5">
                      {!! Form::select('pendidikan_id', $data['support']['pendidikan'], old('pendidikan_id'), ['class'=>'selectpicker form-control','data-live-search' => 'true', 'autocomplete' => 'off'])!!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Jurusan</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="Jurusan peserta" name="jurusan" value="{{ old('jurusan') }}">
                  </div>
                </div>
                <div id="units">
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Unit Induk</label>
                   <div class="col-sm-7">
                   <?php $data['support']['uinduk'] = array_add($data['support']['uinduk'],'','-- Pilih Unit Induk --'); ?>
                      {!! Form::select('unitinduk_id', $data['support']['uinduk'], '', ['class'=>'selectpicker form-control','data-live-search' => 'true','id'=>'unitinduk_id', 'autocomplete' => 'off'])!!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Unit Cabang</label>
                  <div class="col-sm-7">
                    <select name="unitcabang_id" class="selectpicker form-control" id="unitcabang" data-live-search="true" disabled="disabled" autocomplete="off">
                   </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Unit Ranting</label>
                   <div class="col-sm-7">
                      <select name="unitranting_id" class="selectpicker form-control" id="unitranting" disabled="disabled" data-placeholder="-- Pilih unit ranting --" data-live-search="true" autocomplete="off">
                     </select>
                  </div>
                </div>
                </div>
                <div class="form-group">
                  <label for="keterangan" class="col-sm-3 control-label">Kondisi Khusus</label>
                  <div class="col-sm-7">
                    <label class="radio-inline">
                        <input type="radio" name="kondisi_khusus" id="condition_no" value="n" {{ (old('kondisi_khusus') == 'n') ? 'checked' : '' }} class="condition" checked> Tidak
                      </label>
                    <label class="radio-inline">
                      <input type="radio" name="kondisi_khusus" id="condition_yes" value="y" {{ (old('kondisi_khusus') == 'y') ? 'checked' : '' }} class="condition"> Ya
                    </label>  
                    <textarea class="form-control" placeholder="Keterangan kondisi khusus yang dapat menghambat / mempengaruhi pelaksanaan uji. (diisi jika ada kondisi khusus)" style="margin-top: 10px; resize:none;" name="keterangan_kondisi_khusus" id="keterangan-kondisi-khusus" disabled></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Unggah Foto</label>
                  <div class="col-sm-8">
                      <input type="file" name="foto">
                      <p class="help-block">berkas unggahan tidak boleh berkapasitas lebih dari 200kb.</p>
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="col-md-offset-3 box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>&nbsp;
                <a href="{{ url('/') }}" class="btn btn-warning">Kembali</a>
              </div>
            </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap-select.min.js') }}"></script>
    <script type="text/javascript">
    $('.selectpicker').selectpicker();

    $(document).ready(function()
    {
      var form = $('form#registration-form'),
      uinduk      = form.find('select#unitinduk_id'),
      ucabang     = form.find('select#unitcabang'),
      uranting    = form.find('select#unitranting'),
      tipepeserta = form.find('select#tipe_peserta'),
      kondisi     = form.find('radio.kondisi_khusus');
      url         = "{{ route('api.get.cabang.byunitinduk', 'id') }}",
      urlranting  = "{{ route('api.get.ranting.byunitcabang', 'id') }}";
      
      uinduk.change(function() {
        urlWithId = url.replace('id', uinduk.val());  
        var options = '';
        $.getJSON(urlWithId, function(data){
          if(! $.isEmptyObject(data)) {
            options += '<option value="">-- Pilih Unit Cabang --</option>';
            $.each(data,function(index,value){
              options += '<option value="'+index+'">'+value+'</option>';
            });
            ucabang.removeAttr('disabled');
            uranting.removeAttr('disabled');
            ucabang.html(options).selectpicker('refresh');
          } else {
            ucabang.html('');
            uranting.html('');
            ucabang.attr('disabled',true).selectpicker('refresh');
            uranting.attr('disabled',true).selectpicker('refresh');
          }
          
        });
      });
      ucabang.change(function() {
        urlrantingWithId = urlranting.replace('id', ucabang.val()); 
        var options = '';
        $.getJSON(urlrantingWithId, function(data){
          if(! $.isEmptyObject(data)) {
            options += '<option value="">-- Pilih Unit Ranting --</option>';
              $.each(data,function(index,value){
                  options += '<option value="'+index+'">'+value+'</option>';
              });
              uranting.removeAttr('disabled');
              uranting.html(options).selectpicker('refresh');
          } else {
            uranting.html('');
            uranting.attr('disabled',true).selectpicker('refresh');
          }
          
        });
      });

      tipepeserta.change(function(event)
      {
        event.preventDefault();
        if($(this).val() == "pln") {
          $('div#info-nonpln').hide();
          $('div#units').show();
          $('#unitnonpln_id').attr('disabled', true).selectpicker('refresh');
          uinduk.removeAttr('disabled').selectpicker('refresh');
        } else {
          $('div#info-nonpln').show();
          $('div#units').hide();
          $('#unitnonpln_id').removeAttr('disabled').selectpicker('refresh');
          uinduk.attr('disabled', true).selectpicker('refresh');
          ucabang.attr('disabled', true).selectpicker('refresh');
          uranting.attr('disabled', true).selectpicker('refresh');
        }
      
      });

      // check kondisi khusus input
      $('.condition').change(function()
      {
        if ($(this).val() == 'y') {
          $('#keterangan-kondisi-khusus').val('').removeAttr('disabled');
        } else {
          $('#keterangan-kondisi-khusus').val('').attr('disabled', true);
        }
      });  

      urlSubbidang  = "{{ route('api.get.subbidang.byBidang', 'bidang_id') }}";
      urlKompetensi = "{{ route('api.get.kompetensi.getBySubbidang', 'subbidang_id') }}";
      
      $('select.bidang').change(function(event)
      {
        event.preventDefault();

        urlWithId   = urlSubbidang.replace('bidang_id', $(this).val());  
        var options = '';

        $.getJSON(urlWithId, function(data){
              if(! $.isEmptyObject(data)) {
                options += '<option value="0">-- Pilih Sub Bidang --</option>';
                $.each(data,function(index,value){
                  options += '<option value="'+index+'">'+value+'</option>';
                });

                $('#subbidang').html(options).removeAttr('disabled').selectpicker('refresh');
              } else {

                $('#subbidang').html('').attr('disabled', true).selectpicker('refresh');
                $('#kompetensi').html('').attr('disabled', true).selectpicker('refresh');
              }
            
          });
      });

      $('select.subbidang').change(function(event)
      {
        event.preventDefault();

        subbidang_id = $(this).val();
        urlWithId    = urlKompetensi.replace('subbidang_id', subbidang_id);  
        var options  = '';

        $.getJSON(urlWithId, function(data){
            if(! $.isEmptyObject(data)) {
              options += '<option>-- Pilih Kompetensi --</option>';
              $.each(data,function(index,value){
                options += '<option value="'+index+'">'+value+'</option>';
              });
              
              $('#kompetensi').html(options).removeAttr('disabled').selectpicker('refresh');
            } else {
              $('#kompetensi').html('').attr('disabled',true).selectpicker('refresh');
            }
        });
        
      });
    });
    </script>
  </body>
</html>