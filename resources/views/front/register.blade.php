@extends('layouts.front.master')
@section('content')
<div class="row marketing">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
            Data Diri Sertifikasi PLN
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="#" method="post" accept-charset="utf-8">
              <div class="box-body">
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Bidang Kompetensi</label>
                  <div class="col-sm-7">
                      {!! Form::select('bidang_id', ['0' => '-- Pilih Bidang --'] + $bidang, '', ['class' => 'selectpicker form-control bidang', 'data-live-search' => 'true', 'id' => 'bidang_id', 'autocomplete' => 'off']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Subbidang Kompetensi</label>
                  <div class="col-sm-7">
                      {!! Form::select('subbidang', [], '', ['class' => 'selectpicker form-control subbidang', 'data-live-search' => 'true', 'disabled' => 'true', 'id' => 'subbidang', 'autocomplete' => 'off']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Kompetensi</label>
                  <div class="col-sm-7">
                      {!! Form::select('kompetensi_id', [], '', ['class' => 'selectpicker form-control', 'data-live-search' => 'true', 'disabled' => 'true', 'id' => 'kompetensi', 'autocomplete' => 'off']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">No. Identitas</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="No. Identitas Peserta (KTP / SIM / Passport)">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Nama</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="Nama lengkap peserta">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Jenis Kelamin</label>
                  <div class="col-sm-7">
                      <label class="radio-inline">
                        <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Laki-laki
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Perempuan
                      </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-3 control-label">Email</label>
                  <div class="col-sm-7">
                      <input type="email" class="form-control" placeholder="Email peserta">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">No. Handphone</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="No. Handphone peserta">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Tempat Lahir</label>
                  <div class="col-sm-7">
                      <input type="text" class="form-control" placeholder="Kota kelahiran">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Tanggal Lahir</label>
                  <div class="col-sm-7">
                    <input type="date" class="form-control" placeholder="Tanggal lahir">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Kewarganegaraan</label>
                   <div class="col-sm-7">
                    <input type="date" class="form-control" placeholder="kewarganegaraan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Pendidikan</label>
                  <div class="col-sm-5">
                      <select name="pendidikan" class="form-control">
                        <option value="">SMA/SMK</option>
                        <option value="">D1</option>
                        <option value="">D2</option>
                        <option value="">D3</option>
                        <option value="">S1</option>
                        <option value="">S2</option>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Jurusan</label>
                  <div class="col-sm-5">
                      <input type="text" class="form-control" placeholder="Jurusan (opsional)">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Nama Perusahaan</label>
                   <div class="col-sm-7">
                    <input type="text" class="form-control" placeholder="Nama perusahaan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Nama Unit Kerja</label>
                   <div class="col-sm-7">
                    <input type="text" class="form-control" placeholder="Nama unit kerja">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Alamat perusahaan</label>
                   <div class="col-sm-7">
                    <input type="text" class="form-control" placeholder="Alamat perusahaan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nip" class="col-sm-3 control-label">Telepon Perusahaan</label>
                   <div class="col-sm-7">
                    <input type="text" class="form-control" placeholder="Telepon perusahaan">
                  </div>
                </div>
              </div><!-- /.box-body -->
              <div class="col-md-offset-3 box-footer">
                <a href="{{ url('/') }}" class="btn btn-warning">Kembali</a>&nbsp;
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
<script src="{{ asset('assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function()
{
  urlSubbidang  = "{{ route('api.get.subbidang.byBidang', 'bidang_id') }}";
  urlKompetensi = "{{ route('api.get.kompetensi.getBySubbidang', 'subbidang_id') }}";
  
  $('select.bidang').change(function(event)
  {
    event.preventDefault();

    urlWithId   = urlSubbidang.replace('bidang_id', $(this).val());  
    var options = '';

    $.getJSON(urlWithId, function(data){
          if(! $.isEmptyObject(data)) {
            options += '<option value="0">-- Pilih Sub Bidang --</option>';
            $.each(data,function(index,value){
              options += '<option value="'+index+'">'+value+'</option>';
            });

            $('#subbidang').html(options).removeAttr('disabled').selectpicker('refresh');
          } else {

            $('#subbidang').html('').attr('disabled', true).selectpicker('refresh');
            $('#kompetensi').html('').attr('disabled', true).selectpicker('refresh');
          }
        
      });
  });

  $('select.subbidang').change(function(event)
  {
    event.preventDefault();

    subbidang_id = $(this).val();
    urlWithId    = urlKompetensi.replace('subbidang_id', subbidang_id);  
    var options  = '';

    $.getJSON(urlWithId, function(data){
          if(! $.isEmptyObject(data)) {
            options += '<option value="0">-- Pilih Kompetensi --</option>';
            $.each(data,function(index,value){
              options += '<option value="'+index+'">'+value+'</option>';
            });
            
            $('#kompetensi').html(options).removeAttr('disabled').selectpicker('refresh');
          } else {
            $('#kompetensi').html('').attr('disabled',true).selectpicker('refresh');
          }
      });
    
  });
});
</script>
@stop