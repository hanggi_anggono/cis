<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header">MENU UTAMA</li>
      <!-- Optionally, you can add icons to the links -->
@if(Auth::user()->can('can_read_maindashboard'))
      <li class="{{ checkActiveMenu('dashboard') }}"><a href="{{ url('dashboard') }}"><i class='fa fa-dashboard'></i> <span>Home</span></a></li>
@endif

<!--////////////////////////////////////////////////////-->
<!--////////////////////////MASTER//////////////////////-->
<!--////////////////////////////////////////////////////-->
@if(isTreeMenuAccessible('master'))
      <li class="treeview {{ checkActiveMenu('dashboard/master/*') }}">
        <a href="#"><i class='fa fa-database'></i> <span>Data Master</span><i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">

<!--//////////////////////////-->
<!--///////ADMINISTRATOR//////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masteradministrator'))

          <li class="{{ checkActiveMenu('dashboard/master/administrator*') }}"><a href="{{ route('dashboard.master.administrator.index') }}"><i class='fa fa-user'></i> <span>Administrator</span></a></li>
@endif

<!--//////////////////////////-->
<!--//////////PESERTA/////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterpeserta'))   

          <li class="{{ checkActiveMenu('dashboard/master/peserta*') }}"><a href="{{ route('dashboard.master.peserta.index') }}"><i class='fa fa-user-plus'></i> <span>Peserta</span></a></li>
@endif

<!--//////////////////////////-->
<!--//////////ASESOR//////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterasesor'))
  
          <li class="{{ checkActiveMenu('dashboard/master/asesor*') }}"><a href="{{ route('dashboard.master.asesor.index') }}"><i class='fa fa-group'></i> <span>Asesor</span></a></li>
@endif

<!--//////////////////////////-->
<!--/////////AKREDITOR////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterakreditor'))

          <li class="{{ checkActiveMenu('dashboard/master/akreditor*') }}"><a href="{{ route('dashboard.master.akreditor.index') }}"><i class='fa fa-building'></i> <span>Akreditor</span></a></li>
@endif

<!--//////////////////////////-->
<!--////LEMBAGA SERTIFIKASI///-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterlembagasertifikasi'))

          <li class="{{ checkActiveMenu('dashboard/master/lembagasertifikasi*') }}"><a href="{{ route('dashboard.master.lembagasertifikasi.index') }}"><i class='fa fa-building'></i> <span>Lembaga Sertifikasi</span></a></li>
@endif

<!--//////////////////////////-->
<!--//////////BIDANG//////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterbidang'))

          <li class="{{ checkActiveMenu('dashboard/master/bidang*') }}"><a href="{{ route('dashboard.master.bidang.index') }}"><i class='fa fa-crosshairs'></i> <span>Bidang</span></a></li>
@endif

<!--//////////////////////////-->
<!--/////////KOMPETENSI///////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterkompetensi'))

          <li class="{{ checkActiveMenu('dashboard/master/kompetensi*') }}"><a href="{{ route('dashboard.master.kompetensi.index') }}"><i class='fa fa-crosshairs'></i> <span>Unit Kompetensi</span></a></li>
@endif

<!--//////////////////////////-->
<!--////////////UNIT//////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterunit'))

          <li class="{{ checkActiveMenu('dashboard/master/unit*') }}"><a href="{{ route('dashboard.master.unit.index') }}"><i class='fa fa-external-link'></i> <span>Unit</span></a></li>
@endif

<!--//////////////////////////-->
<!--//////////JABATAN/////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterjenjangjabatan'))

          <li class="{{ checkActiveMenu('dashboard/master/jenjang*') }}"><a href="{{ route('dashboard.master.jenjang.index') }}"><i class='fa fa-briefcase'></i> <span>Jenjang Jabatan</span></a></li>
@endif

<!--//////////////////////////-->
<!--////////PENDIDIKAN////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterpendidikan'))

          <li class="{{ checkActiveMenu('dashboard/master/pendidikan*') }}"><a href="{{ route('dashboard.master.pendidikan.index') }}"><i class='fa fa-institution'></i> <span>Pendidikan</span></a></li>
@endif

<!--//////////////////////////-->
<!--//////////GRADE///////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_mastergrade'))

          <li class="{{ checkActiveMenu('dashboard/master/grade*') }}"><a href="{{ route('dashboard.master.grade.index') }}"><i class='fa fa-graduation-cap'></i> <span>Jenjang Grade</span></a></li>
@endif

<!--//////////////////////////-->
<!--///////////BIAYA//////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterkomponenbiaya'))
          <li class="{{ checkActiveMenu('dashboard/master/komponenbiaya*') }}"><a href="{{ route('dashboard.master.komponenbiaya.index') }}"><i class='fa fa-money'></i> <span>Komponen Biaya</span></a></li>
@endif

<!--//////////////////////////-->
<!--////////////ROLE//////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_masterrole'))

          <li class="{{ checkActiveMenu('dashboard/master/userrole*') }}"><a href="{{ route('dashboard.master.userrole.index') }}"><i class="fa fa-users"></i> Role</a></li>
@endif
        </ul>
      </li>
@endif
{{-- end of tree data master --}}

<!--////////////////////////////////////////////////////-->
<!--///////////////////////REGISTRASI///////////////////-->
<!--////////////////////////////////////////////////////-->
@if(Auth::user()->hasRole('superadmin') )
<li class="{{ checkActiveMenu('dashboard/registration*') }}"><a href="{{ route('dashboard.registration.index') }}"><i class='fa fa-plus'></i> <span>Registrasi</span></a></li>
@endif

<!--////////////////////////////////////////////////////-->
<!--//////////////////////PERMOHONAN////////////////////-->
<!--////////////////////////////////////////////////////-->
@if(isTreeMenuAccessible('permohonan'))
      <li class="treeview {{ checkActiveMenu('dashboard/permohonan/*') }}">
        <a href="#"><i class='fa fa-edit'></i> <span>Permohonan</span><i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
@if(Auth::user()->can('can_read_permohonan'))
           <li class="{{ checkActiveMenu('dashboard/permohonan*') }}"><a href="{{ route('dashboard.permohonan.index') }}"><i class='fa fa-edit'></i> <span>Permohonan Surat</span></a></li>
@endif

@if(Auth::user()->can('can_read_permohonan'))
           <li class="{{ checkActiveMenu('dashboard/permohonansdm*') }}"><a href="{{ route('dashboard.permohonansdm.index') }}"><i class='fa fa-edit'></i> <span>Permohonan SDM</span></a></li>           
@endif
        </ul>
      </li>  
@endif
{{-- end of tree permohonan --}}

<!--////////////////////////////////////////////////////-->
<!--////////////////////////EVALUASI////////////////////-->
<!--////////////////////////////////////////////////////-->
@if(Auth::user()->can('can_read_evaluasi'))
      <li class="{{ checkActiveMenu('dashboard/evaluasi*') }}"><a href="{{ route('dashboard.evaluasi.index') }}"><i class='fa fa-check-square-o'></i> <span>Evaluasi</span></a></li>
@endif

<!--////////////////////////////////////////////////////-->
<!--//////////////////////PENJADWALAN///////////////////-->
<!--////////////////////////////////////////////////////-->
@if(Auth::user()->can('can_read_penjadwalan'))
      <li class="{{ checkActiveMenu('dashboard/penjadwalan*') }}"><a href="{{ route('dashboard.penjadwalan.index') }}"><i class='fa fa-clock-o'></i> <span>Penjadwalan</span></a></li>
@endif

<!--////////////////////////////////////////////////////-->
<!--//////////////////////PELAKSANAAN///////////////////-->
<!--////////////////////////////////////////////////////-->
@if(Auth::user()->can('can_read_pelaksanaan'))
      <li class="{{ checkActiveMenu('dashboard/pelaksanaan*') }}"><a href="{{ route('dashboard.pelaksanaan.index') }}"><i class='fa fa-pencil-square-o'></i> <span>Pelaksanaan</span></a></li>
@endif

<!--////////////////////////////////////////////////////-->
<!--///////////////////////SERTIFIKAT///////////////////-->
<!--////////////////////////////////////////////////////-->
@if(isTreeMenuAccessible('sertifikat'))
      <li class="treeview {{ checkActiveMenu('dashboard/sertifikat/*') }}">
        <a href="#"><i class='fa fa-certificate'></i> <span>Sertifikat</span><i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
<!--//////////////////////////-->
<!--//////////SER BARU////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_sertifikatbaru'))
           <li class="{{ checkActiveMenu('dashboard/sertifikat/baru*') }}"><a href="{{ route('dashboard.sertifikat.baru.index') }}"><i class='fa fa-certificate'></i> <span>Sertifikat Baru</span></a></li>
@endif

<!--//////////////////////////-->
<!--////SERT PERPANJANGAN/////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_sertifikatperpanjangan'))
           <li class="{{ checkActiveMenu('dashboard/sertifikat/perpanjangan*') }}"><a href="{{ route('dashboard.sertifikat.perpanjangan.index') }}"><i class='fa fa-certificate'></i> <span>Sertifikat Perpanjangan</span></a></li>
@endif

        </ul>
      </li>  
@endif
{{-- end of tree sertifikat --}}

<!--////////////////////////////////////////////////////-->
<!--////////////////////////LAPORAN/////////////////////-->
<!--////////////////////////////////////////////////////-->
@if(isTreeMenuAccessible('report'))
      <li class="treeview {{ checkActiveMenu('dashboard/report/*') }}">
        <a href="#"><i class='fa fa-stack-overflow'></i> <span>Laporan</span><i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">

<!--//////////////////////////-->
<!--/////////PELAKSANAAN//////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_laporanpelaksanaan'))
          <li class="{{ checkActiveMenu('dashboard/report/pelaksanaan*') }}"><a href="{{ route('report.pelaksanaan.index') }}"><i class='fa fa-bar-chart'></i> <span>Hasil Pelaksanaan</span></a></li>
@endif

<!--//////////////////////////-->
<!--/////////EVALUASI/////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_laporanevaluasi'))
          <li class="{{ checkActiveMenu('dashboard/report/evaluasi*') }}"><a href="{{ route('report.evaluasi.index') }}"><i class='fa fa-bar-chart'></i> <span>Hasil Evaluasi</span></a></li>
@endif

<!--//////////////////////////-->
<!--//////////ASESOR//////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_laporanasesor'))
          <li class="{{ checkActiveMenu('dashboard/report/asesor*') }}"><a href="{{ route('report.asesor.index') }}"><i class='fa fa-bar-chart'></i> <span>Assesor</span></a></li>
@endif

<!--//////////////////////////-->
<!--////////KOMPETENSI////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_laporansebarankompetensi'))
          <li class="{{ checkActiveMenu('dashboard/report/sebarankompetensi*') }}"><a href="{{ route('report.sebarankompetensi.index') }}"><i class='fa fa-bar-chart'></i> <span>Sebaran Kompetensi</span></a></li>
@endif

<!--//////////////////////////-->
<!--/////////KELULUSAN////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_laporanhasilkelulusan'))
          <li class="{{ checkActiveMenu('dashboard/report/kelulusan*') }}"><a href="{{ route('report.kelulusan.index') }}"><i class='fa fa-bar-chart'></i> <span>Hasil Kelulusan</span></a></li>
@endif

<!--//////////////////////////-->
<!--///////////BIAYA//////////-->
<!--//////////////////////////-->
@if(Auth::user()->can('can_read_laporanbiaya'))
          <li class="{{ checkActiveMenu('dashboard/report/biaya*') }}"><a href="{{ route('report.biaya.index') }}"><i class='fa fa-bar-chart'></i> <span>Biaya</span></a></li> 
@endif
        </ul>
      </li>
@endif
{{-- end of tree report --}}
     
      <li><a href="{{ route('user.signout') }}"><i class='fa fa-lock'></i> <span>Sign out</span></a></li>
    </ul><!-- /.sidebar-menu -->
    <center><img src="{{ asset('assets/dist/img/pln.jpg') }}" style="width: 50%; height: 50%; margin-top:200px;"></center>
  </section>
  <!-- /.sidebar -->
</aside>