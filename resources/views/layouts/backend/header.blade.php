<!-- Main Header -->
<header class="main-header">

  <!-- Logo -->
  <a href="{{ route('dashboard.index') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>PLN</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><img src="{{ url().'/assets/dist/img/logo-pln-corpu.png'}}" style="width: 60%; height: 60% "></span>
  </a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
      
        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span class="hidden-xs">{{ Auth::user()->nip }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- The user image in the menu -->
            <li class="user-header">
              <p>
                {{ Auth::user()->nip }} - {{ (Auth::user()->roles->count() > 0) ? Auth::user()->roles[0]->display_name : '-' }}
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{ route('dashboard.password.change') }}" class="btn btn-default btn-flat">Ganti Password</a>
              </div>
              <div class="pull-right">
                <a href="{{ route('user.signout') }}" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>