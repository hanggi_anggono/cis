<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<style type="text/css">
		@page {
		    size: A4;
		    margin: 0;
		}
		@media print {
		    .page {
		        margin: 0;
		        border: initial;
		        border-radius: initial;
		        width: initial;
		        min-height: initial;
		        box-shadow: initial;
		        background: initial;
		        page-break-after: always;
		    }
		}
		body {
			font-size: medium;
			font-family: 'Helvetica';
		}
		
		td {
			text-align: center;
		}
		.title {
			font-size: x-large;
			padding-bottom: 10px;
			padding-top: 10px;
		}
		.en{
			font-style: italic;
		}
		.page-break {
			page-break-after: always;
		}
		.block {
			display: inline-block;
			width: 100%;
		}
		.blue {
			color: blue;
		}
	</style>
</head>
<body>
<?php $count = count($collection); $i = 1; ?>
@foreach($collection as $key => $data)
<center>
	<img src="{{ asset('assets/dist/img/garuda.png') }}" width="150" height="150" style="margin-top:20px;"><br>
	<div class="title">
		<b>BADAN NASIONAL<br>
		SERTIFIKASI NASIONAL</b>
	</div>
	<div class="en">
		<b>INDONESIAN PROFESSIONAL<br>
		CERTIFICATION AUTHORITY</b>
	</div><br>
	<div class="title blue">
		<b>SERTIFIKAT KOMPETENSI
	</div>
	<div class="en blue">CERTIFICATE OF COMPETENCE</div><br>
	<div class="title">
		<b>No. {{ $data['sertifikat']->no_sertifikat }}</b>
	</div><br>
	Dengan ini menyatakan bahwa,<br>
	<span class="en">This is certify that,</span><br>
	<div class="title">
		<b>{{ $data['sertifikat']->peserta->nama }}</b>
	</div>
	Telah kompeten pada Bidang :<br>
	<span class="en">Is competent in area of</span> :<br><br>
	<b>{{ $data['kompetensi']->kodeskkni }}</b>
	<br><br>
	Dengan Kualifikasi/Kompetensi :<br>
	<span class="en">with Qualification/Competency</span> :<br>
	<div class="title">
		<b>{{ $data['kompetensi']->nama_kompetensi }}</b>
	</div>
	<span class="en">{{ $data['kompetensi']->nama_kompetensi_english }}</span><br>
	Sertifikat ini berlaku untuk: 3 (tiga) tahun<br>
	<span class="en">This certificate is valid for : 3 (three) years</span><br>
	<br>
	Jakarta, {{ indonesianDate($data['sertifikat']->tanggal_mulai_berlaku) }}<br>
	Atas Nama<br>
	<span class="en">On Behalf of</span><br>
	<b>Badan Nasional Sertifikasi Profesi</b><br>
	<span class="en">Indonesian Professional Certification Authority</span><br>
	<b>Lembaga Sertifikasi Profesi USER PLN</b><br>
	<span class="en">USER PLN Professional Certification Body</span></em>
	<br><br><br><br>
	Okto Rinaldi Sagala<br>
	___________________________<br>
	Ketua Dewan Pengarah<br>
	<b>(Governing Board)</b>
</center>

	<div class="page-break"></div>
	<center>
	<div class="title">
		<b>Daftar Unit Kompetensi</b>
	</div>
	<div class="en">
		<b>Lists of Unit(s) of Competency</b>
	</div>
	</center>

	<br><br>
	
	<table style="margin-left: auto; margin-right: auto; width: 80%;" border="1" cellspacing="0" cellpadding="5">
		<tr>
			<th>NO</th>
			<th>Kode Unit Kompetensi<br><span class="en">Code of Competency Unit</span></th>
			<th>Judul Unit Kompetensi<br><span class="en">Title of Competency Unit</span></th>
		</tr>
		<tr>
			<td>1</td>
			<td>{{ $data['kompetensi']->kodeskkni }}</td>
			<td>{{ $data['kompetensi']->nama_kompetensi }}<br><span class="en">{{ $data['kompetensi']->nama_kompetensi_english }}</span></td>
		</tr>
	</table>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	
	@if(! empty($data['sertifikat']->peserta->photo))
		<img src="{{ asset('resources/pesertaphotos/'.$data['sertifikat']->peserta->photo) }}" width="150" height="170" style="margin-left:25px;">
	@endif
	<table style="margin-left: auto; margin-right: auto; width: 100%;" class="table">
		<tr>
			<td></td>
			<td></td>
			<td>
				<b>Jakarta, {{ indonesianDate($data['sertifikat']->tanggal_mulai_berlaku) }}<br>
				Lembaga Sertifikasi Profesi USER PLN</b><br>
				<span class="en">USER PLN Professional Certification Body</span>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<br>
				<br>
				<br>
				<br>
			</td>
		</tr>
		<tr>
			<td><b>{{ $data['sertifikat']->peserta->nama }}</b></td>
			<td></td>
			<td><b>Nur Syamsu Safrillah</b></td>
		</tr>
		<tr>
			<td>
				Tanda tangan pemilik<br>
				<span class="en">(Signature of holder)</span>
			</td>
			<td></td>
			<td>
				<b>Kepala</b>
			</td>
		</tr>
	</table>
	@if($i++ !== $count)
		<div class="page-break"></div>
	@endif
@endforeach
</body>
</html>