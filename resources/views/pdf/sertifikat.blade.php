<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<style type="text/css">
		body {
			background-color: #5AC5E8;
			font-family: 'Helvetica';
		}
		@page {
		    size: A4 landscape;
		    margin: 0;
		}
		@media print {
		    .page {
		        margin: 0;
		        border: initial;
		        border-radius: initial;
		        width: initial;
		        min-height: initial;
		        box-shadow: initial;
		        background: initial;
		        page-break-after: always;
		    }
		}
		.page-break {
			page-break-after: always;
		}
		.row:before, 
	    .row:after {
	        content:"";
	        display: table ;
	        clear:both;
	    }
	     [class*='col-'] {
	     	/*float: left;*/
	        min-height: 1px; 
	        width: 16.66%; 
	    }
	    .col-1{
	        width: 16.66%; 
	    }
	    .col-2{
	        width: 33.33%; 
	    }
	    .col-3{
	        width: 50%; 
	    }
	    .col-4{
	        width: 66.664%;
	    }
	    .col-5{
	        width: 83.33%;
	    }
	    .col-6{
	        width: 100%;
	    }
	    td {
	    	padding: 3px;
	    }
	</style>
</head>
<body>
<?php $count = count($collection); $i = 1; ?>
@foreach($collection as $key => $data)
	<div style="width:100%;">
	<br>
		<div class="row">
			<div class="col-2" style="text-align:center; position:absolute; left:-100px;">
				<img src="{{ asset('assets/dist/img/pln.jpg') }}" width="20%" height="20%">
			</div>
			<div class="col-2" style="text-align:center; position:absolute; right:-70px;">
				<img src="{{ asset('assets/dist/img/LOGO-KAN.png') }}" width="50%" height="50%"><br>
				Lembaga Sertifikasi Personel<br>
				LSP - 006 - IDN
			</div>
		</div><br><br><br><br>
		<div class="row">
			<div class="col-6">
				<h1 style="text-align:center;">Sertifikat Kompetensi</h1>
				<h4 style="text-align:center;">Certificate of Competency</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<table style="margin-left: auto; margin-right: auto">
				<tr>
					<td>Nomor Registrasi / <em>Registration</em></td>
					<td>:</td>
					<td>{{ $data['sertifikat']->no_sertifikat }}</td>
				</tr>
				<tr>
					<td>Nama / <em>Name</em></td>
					<td>:</td>
					<td><b>{{ $data['sertifikat']->peserta->nama }}</b></td>
				</tr>
				<tr>
					<td>Tempat, Tanggal Lahir / <em>Place, Date of Birth</em></td>
					<td>:</td>
					<td>{{ $data['sertifikat']->peserta->tempat_lahir }}, {{ $data['sertifikat']->peserta->tanggal_lahir }}</td>
				</tr>
				<tr>
					<td>No. Identitas / <em>Id. Number</em></td>
					<td>:</td>
					<td>{{ $data['sertifikat']->peserta->nip }}</td>
				</tr>
			</table><br>
		</div>
		
		<div class="row">
			<div style="text-align:center; padding:12px;">
				Dinyatakan kompeten pada {{ $data['bidang']->nama_bidang }} sub bidang {{ $data['kompetensi']->subbidang->nama_subbidang }}<br>
				Paket Kompetensi: {{ $data['kompetensi']->nama_kompetensi }}
			</div>
			<div style="text-align:center; padding:12px; font-style: italic;">
				Has been declared that competent in the {{ $data['bidang']->nama_bidang_english }} sub-field {{ $data['kompetensi']->subbidang->nama_subbidang_english }}<br>
				Cluster Competency: {{ $data['kompetensi']->nama_kompetensi_english }}
			</div>
		</div>
		<div class="row" width="100%" style="margin-top: 20px;">
			<div class="col-2" style="text-align:center; position:absolute; left:0px;">
				Mengetahui,<br>
				Governing Board<br>
				Ketua
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				Okto Rinaldi Sagala
			</div>
			<div class="col-2" style="text-align:center; position:absolute;">
				@if(! empty($data['sertifikat']->peserta->photo))
				<img src="{{ asset('resources/pesertaphotos/'.$data['sertifikat']->peserta->photo) }}" width="150" height="170" style="position:absolute; left:450px;">
				@endif
			</div>
			<div class="col-2" style="text-align:center; position:absolute; right:0px;">
				Jakarta, {{ indonesianDate($data['sertifikat']->tanggal_mulai_berlaku) }}<br>
				LSP User PLN<br>
				Kepala
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				Nur Syamsu Safrillah
			</div>
		</div>
		<div class="row">
			<div class="col-6" style="text-align:center; bottom:20px; position:absolute;">
				Sertifikat ini berlaku mulai tanggal {{ indonesianDate($data['sertifikat']->tanggal_mulai_berlaku) }} sampai dengan tanggal {{ indonesianDate($data['sertifikat']->tanggal_akhir_berlaku) }}<br>
				<em>The validity of this certificate from {{ indonesianDate($data['sertifikat']->tanggal_mulai_berlaku) }} until {{ indonesianDate($data['sertifikat']->tanggal_akhir_berlaku) }}</em>
			</div>
		</div>
	</div>
	
	<div class="page-break"></div>
	
	<div style="width:100%;">
		<div class="row">
			<div class="col-6">
				<h1 style="text-align:center;">PAKET KOMPETENSI</h1>
				<h1 style="text-align:center;">CLUSTER COMPETENCY</h1>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-6">
				<center><b>{{ $data['kompetensi']->nama_kompetensi }}</b><br>
				<b><i>{{ $data['kompetensi']->nama_kompetensi_english }}</i></b><br>
				<b>Kode Paket: {{ $data['kompetensi']->kodeskkni }}</b></center>
			</div>
		</div>
		<br><br>
		<div class="row">
			<table style="margin-left: auto; margin-right: auto">
				@foreach($data['kompetensi']->elemenkompetensi as $elemenkompetensi)
				<tr>
					<td>Judul Unit</td>
					<td>:</td>
					<td>{{ $elemenkompetensi->nama_elemen }}</td>
				</tr>
				<tr>
					<td><em>Unit Title</em></td>
					<td>:</td>
					<td><em>{{ $elemenkompetensi->nama_elemen_english }}</em></td>
				</tr>
				<tr>
					<td colspan="3"></td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
	@if($i++ !== $count)
		<div class="page-break"></div>
	@endif
	
@endforeach
</body>
</html>
