-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2015 at 07:13 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbcispln`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pendidikan_id` int(10) unsigned NOT NULL,
  `jenjangjabatan_id` int(10) unsigned NOT NULL,
  `grade_id` int(10) unsigned NOT NULL,
  `unitinduk_id` int(10) unsigned NOT NULL,
  `unitcabang_id` int(10) unsigned DEFAULT NULL,
  `unitranting_id` int(10) unsigned DEFAULT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `no_identitas` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_kelamin` enum('m','f') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'm',
  `kewarganegaraan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `golongan_darah` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_hp` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_telp` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jabatan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `edited_by` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `administrator_pendidikan_id_foreign` (`pendidikan_id`),
  KEY `administrator_jenjangjabatan_id_foreign` (`jenjangjabatan_id`),
  KEY `administrator_grade_id_foreign` (`grade_id`),
  KEY `administrator_unitinduk_id_foreign` (`unitinduk_id`),
  KEY `administrator_unitcabang_id_foreign` (`unitcabang_id`),
  KEY `administrator_unitranting_id_foreign` (`unitranting_id`),
  KEY `administrator_nip_index` (`nip`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `nip`, `pendidikan_id`, `jenjangjabatan_id`, `grade_id`, `unitinduk_id`, `unitcabang_id`, `unitranting_id`, `nama`, `no_identitas`, `jenis_kelamin`, `kewarganegaraan`, `tempat_lahir`, `tanggal_lahir`, `golongan_darah`, `email`, `no_hp`, `no_telp`, `jabatan`, `photo`, `status`, `created_by`, `edited_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '8711161Z', 3, 3, 3, 3, 3, 3, 'PRANESTI NOVITASARI', '123', 'f', 'hawai', 'hawai', '1945-01-01', 'O', 'dia@gmail.com', '098987867578', '210938038', 'kepsek', 'photo_123456.jpg', 1, 0, 0, NULL, '2015-09-22 06:19:28', '2015-11-03 08:08:50'),
(3, '89112248ZY', 8, 3, 3, 3, 3, 3, 'Fulan 1', '89112248Z1', 'm', 'Indonesia', 'Blitar', '1989-01-01', 'A', 'benny.adhetya@yahoo.com', '087788801511', '6287788801511', 'JO Pengolahan Data', NULL, 1, 0, 0, NULL, '2015-09-23 05:53:30', '2015-11-03 10:41:49'),
(4, '89112248Z2', 3, 10, 3, 9, 3, 3, 'Fulan 2', '89112248Z2', 'm', 'Indonesia', 'Blitar', '1950-01-01', 'O', 'benny.adhetya@yahoo.com', '087788801511', '6287788801511', 'Supervisor Pelaksanaan Sertifikasi', NULL, 1, 0, 0, NULL, '2015-09-23 06:16:21', '2015-11-03 10:43:45'),
(18, '89112248Z2', 3, 3, 3, 4, NULL, NULL, 'niiniasd', '98797', 'm', 'inindd', '', '1945-01-01', 'O', 'rezd14@gmail.com', '1234445', '', '', NULL, 1, 0, 0, NULL, '2015-11-03 11:13:51', '2015-11-03 13:22:22'),
(19, '8711161ZY', 3, 3, 8, 4, NULL, NULL, 'novi', '132', 'f', 'Indonesia', '', '1945-01-01', 'A', 'dianingratri@yahoo.com', '53466', '576', '', NULL, 1, 0, 0, NULL, '2015-11-03 13:10:13', '2015-11-03 13:10:13');

-- --------------------------------------------------------

--
-- Table structure for table `asesor`
--

CREATE TABLE IF NOT EXISTS `asesor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pendidikan_id` int(10) unsigned NOT NULL,
  `jenjangjabatan_id` int(10) unsigned NOT NULL,
  `grade_id` int(10) unsigned NOT NULL,
  `unitinduk_id` int(10) unsigned DEFAULT NULL,
  `unitcabang_id` int(10) unsigned DEFAULT NULL,
  `unitranting_id` int(10) unsigned DEFAULT NULL,
  `unitnonpln_id` int(10) unsigned DEFAULT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `no_identitas` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_kelamin` enum('m','f') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'm',
  `alamat_sekarang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat_identitas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kewarganegaraan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `golongan_darah` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_hp` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_telp` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_telp_perusahaan` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jabatan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `tipe_asesor` enum('pln','nonpln') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pln',
  `created_by` int(10) unsigned NOT NULL,
  `edited_by` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `asesor_pendidikan_id_foreign` (`pendidikan_id`),
  KEY `asesor_jenjangjabatan_id_foreign` (`jenjangjabatan_id`),
  KEY `asesor_grade_id_foreign` (`grade_id`),
  KEY `asesor_unitinduk_id_foreign` (`unitinduk_id`),
  KEY `asesor_unitcabang_id_foreign` (`unitcabang_id`),
  KEY `asesor_unitranting_id_foreign` (`unitranting_id`),
  KEY `asesor_nip_index` (`nip`),
  KEY `asesor_unitnonpln_id_foreign` (`unitnonpln_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `asesor`
--

INSERT INTO `asesor` (`id`, `nip`, `pendidikan_id`, `jenjangjabatan_id`, `grade_id`, `unitinduk_id`, `unitcabang_id`, `unitranting_id`, `unitnonpln_id`, `nama`, `no_identitas`, `jenis_kelamin`, `alamat_sekarang`, `alamat_identitas`, `kewarganegaraan`, `tempat_lahir`, `tanggal_lahir`, `golongan_darah`, `email`, `no_hp`, `no_telp`, `no_telp_perusahaan`, `jabatan`, `photo`, `status`, `tipe_asesor`, `created_by`, `edited_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '123456', 3, 3, 3, 20, 3, NULL, NULL, 'jonatan', '123', 'm', '', '', 'disana', 'disana', '1945-01-01', 'A', 'jon@natan.com', '098987867578', '089620709396', '9098786667', 'kepsek', 'photo_123456.jpg', 1, 'pln', 0, 0, NULL, '2015-09-22 06:24:04', '2015-11-03 13:18:28'),
(3, '86465', 8, 3, 5, 9, NULL, NULL, NULL, 'Asesor1', '1223', 'm', NULL, NULL, 'INDONESIA', '', '1945-01-01', 'A', '', '', '', '', '', NULL, 1, 'pln', 0, 0, NULL, '2015-09-23 07:09:05', '2015-09-23 07:09:05'),
(4, '456', 3, 10, 8, NULL, NULL, NULL, 3, 'Asesor2', '123', 'f', NULL, NULL, 'INDONESIA', '', '1945-01-01', 'B', '', '', '', '', '', NULL, 1, 'nonpln', 0, 0, NULL, '2015-09-23 07:09:48', '2015-09-23 07:09:48'),
(5, '123', 3, 3, 3, 7, NULL, NULL, NULL, 'Asesor1', '1331', 'f', NULL, NULL, 'Indonesia', '', '1945-01-01', 'B', '', '', '', '', '', NULL, 1, 'pln', 0, 0, NULL, '2015-11-03 07:43:01', '2015-11-03 07:43:01'),
(6, '123', 3, 3, 3, 23, 5, 10, NULL, 'Asesor3', '346', 'm', '', '', '', '', '1945-01-01', NULL, '', '', '', '', '', NULL, 1, 'pln', 0, 0, NULL, '2015-11-03 07:45:44', '2015-11-03 12:56:06');

-- --------------------------------------------------------

--
-- Table structure for table `biayapelaksanaan`
--

CREATE TABLE IF NOT EXISTS `biayapelaksanaan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `penjadwalan_id` int(10) unsigned NOT NULL,
  `komponenbiaya_id` int(10) unsigned DEFAULT NULL,
  `nosurat_tagihan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biaya` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `biayapelaksanaan_penjadwalan_id_foreign` (`penjadwalan_id`),
  KEY `biayapelaksanaan_komponenbiaya_id_foreign` (`komponenbiaya_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `biayapelaksanaan`
--

INSERT INTO `biayapelaksanaan` (`id`, `penjadwalan_id`, `komponenbiaya_id`, `nosurat_tagihan`, `biaya`, `created_at`, `updated_at`) VALUES
(2, 6, 1, '', 70000, '2015-11-03 09:44:06', '2015-11-03 09:44:29'),
(3, 6, 1, '', 80000, '2015-11-03 09:44:49', '2015-11-03 09:44:49');

-- --------------------------------------------------------

--
-- Table structure for table `bidang`
--

CREATE TABLE IF NOT EXISTS `bidang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_bidang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nama_bidang_english` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bidang`
--

INSERT INTO `bidang` (`id`, `nama_bidang`, `created_at`, `updated_at`, `nama_bidang_english`) VALUES
(2, 'Distribusi', '2015-09-22 06:08:30', '2015-09-23 04:37:59', 'Distribution'),
(3, 'Pembangkit', '2015-09-23 04:32:19', '2015-09-23 04:32:19', 'Power Generation'),
(4, 'Transmisi', '2015-09-23 04:33:21', '2015-09-23 04:33:21', 'Transmission'),
(5, 'Niaga', '2015-09-23 04:33:35', '2015-09-23 04:33:35', 'Niaga'),
(6, 'Supervisi Konstruksi', '2015-09-23 04:48:31', '2015-09-23 04:48:31', 'Supervisi Konstruksi');

-- --------------------------------------------------------

--
-- Table structure for table `elemenkompetensi`
--

CREATE TABLE IF NOT EXISTS `elemenkompetensi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_elemen` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nama_elemen_english` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `elemenkompetensi_kompetensi_id_foreign` (`kompetensi_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `elemenkompetensi`
--

INSERT INTO `elemenkompetensi` (`id`, `nama_elemen`, `kompetensi_id`, `created_at`, `updated_at`, `nama_elemen_english`) VALUES
(1, 'coba', 1, '2015-09-22 06:10:42', '2015-11-03 09:05:48', 'test'),
(2, 'elemen2', 1, '2015-09-23 07:10:36', '2015-09-23 07:10:36', ''),
(3, 'Elemen1', 2, '2015-09-23 07:10:50', '2015-09-23 07:10:50', ''),
(4, 'elemen2', 2, '2015-09-23 07:11:01', '2015-09-23 07:11:01', '');

-- --------------------------------------------------------

--
-- Table structure for table `evaluasi`
--

CREATE TABLE IF NOT EXISTS `evaluasi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompetensipermohonan_id` int(10) unsigned NOT NULL,
  `peserta_id` int(10) unsigned NOT NULL,
  `tanggal_evaluasi` date DEFAULT NULL,
  `hasil` tinyint(4) NOT NULL DEFAULT '0',
  `info` text COLLATE utf8_unicode_ci,
  `status_penjadwalan` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `evaluasi_kompetensipermohonan_id_foreign` (`kompetensipermohonan_id`),
  KEY `evaluasi_peserta_id_foreign` (`peserta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `evaluasi`
--

INSERT INTO `evaluasi` (`id`, `kompetensipermohonan_id`, `peserta_id`, `tanggal_evaluasi`, `hasil`, `info`, `status_penjadwalan`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2015-09-22', 1, 'dia anak yang baik', '1', 1, '2015-09-22 06:28:14', '2015-09-22 06:56:04'),
(3, 2, 7, '2015-09-23', 0, 'kurang ktp', '0', 5, '2015-09-23 07:33:46', '2015-09-23 07:33:46'),
(4, 2, 1, '2015-09-23', 1, '', '1', 5, '2015-09-23 07:34:07', '2015-09-23 07:50:05'),
(5, 2, 3, '2015-09-23', 1, '', '1', 5, '2015-09-23 07:34:20', '2015-09-23 07:50:05'),
(6, 1, 3, '2015-09-23', 1, '', '1', 5, '2015-09-23 07:42:48', '2015-09-29 03:40:13'),
(7, 2, 4, '2015-09-23', 1, '', '1', 5, '2015-09-23 07:58:15', '2015-09-25 11:53:33'),
(8, 4, 3, '2015-09-25', 1, '', '1', 1, '2015-09-25 04:31:28', '2015-09-25 11:46:40'),
(9, 4, 1, '2015-09-25', 1, '', '1', 1, '2015-09-25 04:31:39', '2015-09-25 11:53:33'),
(10, 4, 5, '2015-09-25', 1, '', '1', 1, '2015-09-25 04:31:52', '2015-09-25 11:53:33'),
(11, 5, 8, '2015-09-29', 1, '', '1', 1, '2015-09-29 03:32:39', '2015-09-29 03:36:30'),
(12, 5, 5, '2015-09-29', 1, '', '1', 1, '2015-09-29 03:32:48', '2015-09-29 03:36:30'),
(13, 5, 7, '2015-09-29', 0, '', '0', 1, '2015-09-29 03:33:03', '2015-09-29 03:33:03'),
(14, 5, 9, '2015-09-29', 1, '', '1', 1, '2015-09-29 03:33:16', '2015-09-29 03:36:30'),
(15, 4, 4, '2015-10-19', 0, '', '0', 1, '2015-10-19 03:27:56', '2015-10-19 03:27:56'),
(16, 6, 16, '2015-11-03', 1, '', '1', 1, '2015-11-03 09:47:38', '2015-11-03 13:29:36'),
(17, 7, 15, '2015-11-03', 1, '', '1', 1, '2015-11-03 09:47:59', '2015-11-03 13:30:42'),
(18, 6, 15, '2015-11-03', 1, '', '1', 1, '2015-11-03 09:48:11', '2015-11-03 13:29:36'),
(19, 7, 16, '2015-11-03', 1, '', '1', 1, '2015-11-03 09:48:19', '2015-11-03 13:30:42');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE IF NOT EXISTS `grade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_grade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `nama_grade`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'apah', '2015-09-22 06:06:50', '2015-09-22 06:04:56', '2015-09-22 06:06:50'),
(2, 'a', '2015-09-22 06:05:18', '2015-09-22 06:05:14', '2015-09-22 06:05:18'),
(3, 'Basic 1', NULL, '2015-09-22 06:13:21', '2015-09-23 04:51:51'),
(4, 'Basic 2', NULL, '2015-09-23 04:52:00', '2015-09-23 04:52:00'),
(5, 'Basic 3', NULL, '2015-09-23 04:52:14', '2015-09-23 04:52:14'),
(6, 'Basic 4', NULL, '2015-09-23 04:52:25', '2015-09-23 04:52:25'),
(7, 'Specific 4', NULL, '2015-09-23 04:52:57', '2015-09-23 04:52:57'),
(8, 'Specific 3', NULL, '2015-09-23 04:53:30', '2015-09-23 04:53:30'),
(9, 'Specific 2', NULL, '2015-09-23 04:53:40', '2015-09-23 04:53:40'),
(10, 'Specific 1', NULL, '2015-09-23 05:51:34', '2015-09-23 05:51:34'),
(11, 'System 1', NULL, '2015-11-03 12:59:34', '2015-11-03 12:59:53'),
(12, 'System 2', NULL, '2015-11-03 13:00:06', '2015-11-03 13:00:06'),
(13, 'System 3', NULL, '2015-11-03 13:00:16', '2015-11-03 13:00:16'),
(14, 'System 4', NULL, '2015-11-03 13:01:03', '2015-11-03 13:01:03');

-- --------------------------------------------------------

--
-- Table structure for table `jenjangjabatan`
--

CREATE TABLE IF NOT EXISTS `jenjangjabatan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_jenjangjabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `jenjangjabatan`
--

INSERT INTO `jenjangjabatan` (`id`, `nama_jenjangjabatan`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 'Fungsional VI', NULL, '2015-09-22 06:13:07', '2015-09-23 03:50:11'),
(4, 'Fungsional V', NULL, '2015-09-23 03:50:24', '2015-09-23 03:50:24'),
(5, 'Fungsional IV', NULL, '2015-09-23 03:50:42', '2015-09-23 03:50:42'),
(6, 'Fungsional III', NULL, '2015-09-23 03:50:55', '2015-09-23 03:50:55'),
(7, 'Fungsional II', NULL, '2015-09-23 03:51:24', '2015-09-23 03:51:24'),
(8, 'Fungsional bla', NULL, '2015-09-23 04:10:43', '2015-11-03 09:04:20'),
(9, 'Supervisory Bawah', NULL, '2015-09-23 04:10:59', '2015-09-23 04:10:59'),
(10, 'Supervisory Atas', NULL, '2015-09-23 04:12:30', '2015-09-23 04:12:30'),
(11, 'Manajemen Dasar', NULL, '2015-09-23 04:14:11', '2015-09-23 04:14:11'),
(12, 'Manajemen Menengah', NULL, '2015-09-23 04:15:07', '2015-09-23 04:15:07'),
(13, 'Manajemen Atas', NULL, '2015-09-23 04:16:04', '2015-09-23 04:16:04'),
(15, 'Fungsional I', NULL, '2015-11-03 09:02:53', '2015-11-03 09:02:53');

-- --------------------------------------------------------

--
-- Table structure for table `kelompokadministrator`
--

CREATE TABLE IF NOT EXISTS `kelompokadministrator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `administrator_id` int(10) unsigned NOT NULL,
  `lembagasertifikasi_id` int(10) unsigned NOT NULL,
  `tanggalpenetapan` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `kelompokadministrator_administrator_id_index` (`administrator_id`),
  KEY `kelompokadministrator_lembagasertifikasi_id_index` (`lembagasertifikasi_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `kelompokadministrator`
--

INSERT INTO `kelompokadministrator` (`id`, `administrator_id`, `lembagasertifikasi_id`, `tanggalpenetapan`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2015-09-17', '2015-09-22 06:19:47', '2015-09-22 06:19:47'),
(2, 4, 4, '2015-10-25', '2015-11-03 13:20:53', '2015-11-03 13:20:53'),
(3, 4, 6, '2015-11-04', '2015-11-03 13:21:15', '2015-11-03 13:21:15'),
(4, 4, 5, '0000-00-00', '2015-11-03 13:21:30', '2015-11-03 13:21:30'),
(7, 4, 2, '2015-11-10', '2015-11-03 13:23:28', '2015-11-03 13:23:28'),
(8, 4, 3, '0000-00-00', '2015-11-03 13:24:46', '2015-11-03 13:24:46');

-- --------------------------------------------------------

--
-- Table structure for table `kelompokasesor`
--

CREATE TABLE IF NOT EXISTS `kelompokasesor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asesor_id` int(10) unsigned NOT NULL,
  `lembagasertifikasi_id` int(10) unsigned NOT NULL,
  `tanggalpenetapan` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `kelompokasesor_asesor_id_index` (`asesor_id`),
  KEY `kelompokasesor_lembagasertifikasi_id_index` (`lembagasertifikasi_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `kelompokasesor`
--

INSERT INTO `kelompokasesor` (`id`, `asesor_id`, `lembagasertifikasi_id`, `tanggalpenetapan`, `created_at`, `updated_at`) VALUES
(2, 1, 5, '0000-00-00', '2015-11-03 09:57:44', '2015-11-03 09:57:44'),
(4, 1, 6, '2015-11-04', '2015-11-03 09:59:08', '2015-11-03 09:59:08'),
(5, 1, 3, '2015-11-03', '2015-11-03 09:59:22', '2015-11-03 09:59:22'),
(9, 6, 4, '2015-11-04', '2015-11-03 10:03:33', '2015-11-03 10:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensi`
--

CREATE TABLE IF NOT EXISTS `kompetensi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subbidang_id` int(10) unsigned NOT NULL,
  `nama_kompetensi` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `kodedjk` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kodeskkni` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nama_kompetensi_english` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kompetensi_subbidang_id_foreign` (`subbidang_id`),
  KEY `kompetensi_kodedjk_index` (`kodedjk`),
  KEY `kompetensi_kodeskkni_index` (`kodeskkni`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kompetensi`
--

INSERT INTO `kompetensi` (`id`, `subbidang_id`, `nama_kompetensi`, `kodedjk`, `kodeskkni`, `created_at`, `updated_at`, `nama_kompetensi_english`) VALUES
(1, 1, 'Memelihara Gardu Distribusi dan Peralatan Hubung Bagi Tegangan Rendah (PHB-TR / LV-PANEL)', '1111', '111', '2015-09-22 06:10:26', '2015-09-23 04:43:11', 'Maintenance of Distribution Substation and Low Voltage Panel'),
(2, 1, 'Memelihara Jaringan Tegangan Menengah (JTM)', 'DJKD1', 'KTL.DHR.41.2.01.K1.12 ', '2015-09-23 04:44:17', '2015-09-23 04:44:17', 'Maintenance of Medium Voltage Network');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensiadministrator`
--

CREATE TABLE IF NOT EXISTS `kompetensiadministrator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `administrator_id` int(10) unsigned NOT NULL,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `kompetensiadministrator_administrator_id_index` (`administrator_id`),
  KEY `kompetensiadministrator_kompetensi_id_index` (`kompetensi_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kompetensiadministrator`
--

INSERT INTO `kompetensiadministrator` (`id`, `administrator_id`, `kompetensi_id`, `created_at`, `updated_at`) VALUES
(3, 1, 1, '2015-09-22 06:49:35', '2015-09-22 06:49:35');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensiasesor`
--

CREATE TABLE IF NOT EXISTS `kompetensiasesor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `asesor_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `kompetensiasesor_kompetensi_id_foreign` (`kompetensi_id`),
  KEY `kompetensiasesor_asesor_id_foreign` (`asesor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kompetensiasesor`
--

INSERT INTO `kompetensiasesor` (`id`, `kompetensi_id`, `asesor_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2015-09-22 06:25:51', '2015-09-22 06:25:51');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensipenjadwalan`
--

CREATE TABLE IF NOT EXISTS `kompetensipenjadwalan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `penjadwalan_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `kompetensipenjadwalan_kompetensi_id_foreign` (`kompetensi_id`),
  KEY `kompetensipenjadwalan_penjadwalan_id_foreign` (`penjadwalan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `kompetensipenjadwalan`
--

INSERT INTO `kompetensipenjadwalan` (`id`, `kompetensi_id`, `penjadwalan_id`, `created_at`, `updated_at`) VALUES
(4, 2, 5, '2015-09-25 11:52:11', '2015-09-25 11:52:11'),
(5, 1, 4, '2015-09-25 11:52:36', '2015-09-25 11:52:36'),
(6, 1, 6, '2015-09-29 03:34:47', '2015-09-29 03:34:47'),
(7, 1, 7, '2015-11-03 13:28:58', '2015-11-03 13:28:58'),
(8, 2, 7, '2015-11-03 13:28:58', '2015-11-03 13:28:58');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensipermohonan`
--

CREATE TABLE IF NOT EXISTS `kompetensipermohonan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `permohonan_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `kompetensipermohonan_kompetensi_id_foreign` (`kompetensi_id`),
  KEY `kompetensipermohonan_permohonan_id_foreign` (`permohonan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `kompetensipermohonan`
--

INSERT INTO `kompetensipermohonan` (`id`, `kompetensi_id`, `permohonan_id`, `created_at`, `updated_at`) VALUES
(1, 1, 4, '2015-09-22 06:27:11', '2015-09-22 06:27:11'),
(2, 2, 6, '2015-09-23 07:26:27', '2015-09-23 07:26:27'),
(3, 1, 7, '2015-09-25 04:27:46', '2015-09-25 04:27:46'),
(4, 2, 7, '2015-09-25 04:28:32', '2015-09-25 04:28:32'),
(5, 1, 8, '2015-09-29 03:31:38', '2015-09-29 03:31:38'),
(6, 1, 11, '2015-11-03 09:46:12', '2015-11-03 09:46:12'),
(7, 2, 11, '2015-11-03 09:46:12', '2015-11-03 09:46:12');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensipermohonanpeserta`
--

CREATE TABLE IF NOT EXISTS `kompetensipermohonanpeserta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permohonan_id` int(10) unsigned NOT NULL,
  `peserta_id` int(10) unsigned NOT NULL,
  `kompetensipermohonan_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `kompetensipermohonanpeserta_kompetensipermohonan_id_foreign` (`kompetensipermohonan_id`),
  KEY `kompetensipermohonanpeserta_peserta_id_foreign` (`peserta_id`),
  KEY `kompetensipermohonanpeserta_permohonan_id_foreign` (`permohonan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `kompetensipermohonanpeserta`
--

INSERT INTO `kompetensipermohonanpeserta` (`id`, `permohonan_id`, `peserta_id`, `kompetensipermohonan_id`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1, '2015-09-22 06:27:29', '2015-09-22 06:27:29'),
(2, 4, 3, 1, '2015-09-23 07:12:37', '2015-09-23 07:12:37'),
(3, 6, 4, 2, '2015-09-23 07:28:31', '2015-09-23 07:28:31'),
(4, 6, 7, 2, '2015-09-23 07:28:55', '2015-09-23 07:28:55'),
(5, 6, 1, 2, '2015-09-23 07:28:55', '2015-09-23 07:28:55'),
(6, 6, 3, 2, '2015-09-23 07:28:55', '2015-09-23 07:28:55'),
(7, 7, 3, 4, '2015-09-25 04:29:34', '2015-09-25 04:29:34'),
(8, 7, 1, 4, '2015-09-25 04:29:34', '2015-09-25 04:29:34'),
(9, 7, 5, 4, '2015-09-25 04:29:34', '2015-09-25 04:29:34'),
(10, 7, 4, 4, '2015-09-25 04:29:34', '2015-09-25 04:29:34'),
(11, 8, 8, 5, '2015-09-29 03:32:13', '2015-09-29 03:32:13'),
(12, 8, 9, 5, '2015-09-29 03:32:13', '2015-09-29 03:32:13'),
(13, 8, 5, 5, '2015-09-29 03:32:13', '2015-09-29 03:32:13'),
(15, 8, 7, 5, '2015-09-29 03:32:13', '2015-09-29 03:32:13'),
(16, 11, 15, 6, '2015-11-03 09:46:49', '2015-11-03 09:46:49'),
(17, 11, 16, 6, '2015-11-03 09:46:49', '2015-11-03 09:46:49'),
(18, 11, 15, 7, '2015-11-03 09:47:13', '2015-11-03 09:47:13'),
(19, 11, 16, 7, '2015-11-03 09:47:13', '2015-11-03 09:47:13');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensipermohonansdm`
--

CREATE TABLE IF NOT EXISTS `kompetensipermohonansdm` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `permohonansdm_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `kompetensipermohonansdm_kompetensi_id_foreign` (`kompetensi_id`),
  KEY `kompetensipermohonansdm_permohonansdm_id_foreign` (`permohonansdm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kompetensipermohonansdmpeserta`
--

CREATE TABLE IF NOT EXISTS `kompetensipermohonansdmpeserta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permohonansdm_id` int(10) unsigned NOT NULL,
  `peserta_id` int(10) unsigned NOT NULL,
  `kompetensipermohonansdm_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `komponenbiaya`
--

CREATE TABLE IF NOT EXISTS `komponenbiaya` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_komponenbiaya` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `komponenbiaya`
--

INSERT INTO `komponenbiaya` (`id`, `nama_komponenbiaya`, `created_at`, `updated_at`) VALUES
(1, 'komponen biaya 0', '2015-09-22 08:02:12', '2015-09-22 08:02:12'),
(2, 'komponen biaya 1', '2015-09-22 08:02:12', '2015-09-22 08:02:12'),
(3, 'komponen biaya 2', '2015-09-22 08:02:12', '2015-09-22 08:02:12');

-- --------------------------------------------------------

--
-- Table structure for table `lembaga`
--

CREATE TABLE IF NOT EXISTS `lembaga` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_lembaga` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `lembaga`
--

INSERT INTO `lembaga` (`id`, `nama_lembaga`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'abd', '2015-09-22 05:49:26', '2015-09-22 05:48:29', '2015-09-22 05:49:26'),
(2, 'KAN', NULL, '2015-09-22 06:07:44', '2015-09-23 04:26:38'),
(3, 'w', '2015-09-22 06:54:18', '2015-09-22 06:54:14', '2015-09-22 06:54:18'),
(4, 'w', '2015-09-22 08:17:04', '2015-09-22 08:16:53', '2015-09-22 08:17:04'),
(5, 'BNSP', NULL, '2015-09-23 04:26:50', '2015-09-23 04:26:50'),
(6, 'GEMA PDKB', NULL, '2015-11-03 08:54:31', '2015-11-03 10:41:14');

-- --------------------------------------------------------

--
-- Table structure for table `lembagasertifikasi`
--

CREATE TABLE IF NOT EXISTS `lembagasertifikasi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_lembagasertifikasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `koderegistrasilsp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grup` enum('pln','kerjasama') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pln',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jenis_sertifikat` enum('bnsp','pln') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pln',
  PRIMARY KEY (`id`),
  KEY `lembagasertifikasi_nama_lembagasertifikasi_index` (`nama_lembagasertifikasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `lembagasertifikasi`
--

INSERT INTO `lembagasertifikasi` (`id`, `nama_lembagasertifikasi`, `koderegistrasilsp`, `grup`, `deleted_at`, `created_at`, `updated_at`, `jenis_sertifikat`) VALUES
(2, 'LSP USER PLN - BNSP', '111', 'pln', NULL, '2015-09-22 06:08:08', '2015-10-06 00:12:20', 'bnsp'),
(3, 'LSP USER PLN - KAN', 'LSPUSER1', 'pln', NULL, '2015-09-23 04:27:38', '2015-09-23 04:27:38', 'pln'),
(4, 'ELESKA GEMA PDKB', 'LSK4', 'kerjasama', NULL, '2015-09-23 04:28:47', '2015-11-03 10:09:08', 'pln'),
(5, 'ELESKA IATKI', 'LSK2', 'kerjasama', NULL, '2015-09-23 04:29:18', '2015-09-23 04:29:18', 'pln'),
(6, 'ELESKA HAKIT', 'LSK3', 'kerjasama', NULL, '2015-09-23 04:29:39', '2015-09-23 04:29:39', 'pln'),
(8, 'ELESKA HAKIT', '', 'pln', NULL, '2015-11-03 08:57:40', '2015-11-03 08:57:40', 'pln');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_06_19_100952_create_table_lembaga', 1),
('2015_06_19_221655_create_table_pendidikan', 1),
('2015_06_20_013600_create_grade_table', 1),
('2015_06_22_164212_create_bidang_table', 1),
('2015_06_22_164457_create_subbidang_table', 1),
('2015_06_23_162818_create_kompetensi_table', 1),
('2015_06_23_165434_create_elemenkompetensis_table', 1),
('2015_06_25_170340_create_jenjangs_table', 1),
('2015_07_01_091845_create_udiklats_table', 1),
('2015_07_01_091855_create_unitinduks_table', 1),
('2015_07_01_091922_create_unitcabangs_table', 1),
('2015_07_01_091928_create_unitrantings_table', 1),
('2015_07_06_162853_create_timasesors_table', 1),
('2015_07_06_202315_create_asesors_table', 1),
('2015_07_09_123611_create_kompetensiasesors_table', 1),
('2015_07_13_130023_create_administrators_table', 1),
('2015_07_28_115033_create_lembagasertifikasis_table', 1),
('2015_07_28_165527_create_table_kelompokasesor', 1),
('2015_07_30_121416_create_kompetensiadministrators_table', 1),
('2015_07_30_133203_create_kelompokadministrator_table', 1),
('2015_07_30_171627_create_pesertas_table', 1),
('2015_08_03_065224_create_users_table', 1),
('2015_08_03_111219_create_permohonans_table', 1),
('2015_08_03_144842_create_permohonankompetensi_table', 1),
('2015_08_03_161333_create_kompetensipermohonanpeserta_table', 1),
('2015_08_04_132846_create_evaluasis_table', 1),
('2015_08_11_163356_create_unitnonplns_table', 1),
('2015_08_12_141557_create_sertifikatasesor_table', 1),
('2015_08_24_163728_create_models_penjadwalans_table', 1),
('2015_08_24_165544_create_models_penjadwalanasesors_table', 1),
('2015_08_26_115552_create_table_kompetensipenjadwalan', 1),
('2015_08_26_164833_create_pesertakompetensipenjadwalan_table', 1),
('2015_08_28_100242_create_realisasipesertas_table', 1),
('2015_09_04_145159_add_field_type_sertifikat_lembaga_sertifikasi', 1),
('2015_09_04_182843_add_field_english_bidang_to_elemenkompetensi', 1),
('2015_09_06_132059_add_evaluasiid_field_pesertapenjadwalan', 1),
('2015_09_08_143025_create_komponenbiayas_table', 1),
('2015_09_08_143049_create_biayapelaksanaans_table', 1),
('2015_09_09_000556_create_table_sertifikat', 1),
('2015_09_09_000629_create_table_sertifikathistory', 1),
('2015_09_09_103826_entrust_setup_tables', 1),
('2015_09_29_102103_create_subbidangasesors_table', 2),
('2015_10_05_154434_add_subbidang_id_penjadwalan', 3),
('2015_11_05_085454_create_registers_table', 4),
('2015_11_04_090341_create_permohonansdm_table', 5),
('2015_11_12_102342_create_kompetensipermohonansdm_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE IF NOT EXISTS `pendidikan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_pendidikan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id`, `nama_pendidikan`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Kewarganegaraan', '2015-09-22 06:06:45', '2015-09-22 06:02:20', '2015-09-22 06:06:45'),
(2, 's', '2015-09-22 06:02:39', '2015-09-22 06:02:35', '2015-09-22 06:02:39'),
(3, 'SLTA - Sederajat', NULL, '2015-09-22 06:13:15', '2015-09-23 04:16:43'),
(4, 'SLTP - Sederajat', NULL, '2015-09-23 04:24:38', '2015-09-23 04:24:38'),
(5, 'SD - Sederajat', NULL, '2015-09-23 04:24:58', '2015-09-23 04:24:58'),
(6, 'D1', NULL, '2015-09-23 04:25:14', '2015-09-23 04:25:47'),
(7, 'D3', NULL, '2015-09-23 04:25:23', '2015-09-23 04:25:23'),
(8, 'S1/D4', NULL, '2015-09-23 04:25:34', '2015-09-23 04:26:19'),
(9, 'S2', NULL, '2015-09-23 04:26:01', '2015-09-23 04:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `penjadwalan`
--

CREATE TABLE IF NOT EXISTS `penjadwalan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `udiklat_id` int(10) unsigned DEFAULT NULL,
  `unitinduk_id` int(10) unsigned DEFAULT NULL,
  `unitcabang_id` int(10) unsigned DEFAULT NULL,
  `unitranting_id` int(10) unsigned DEFAULT NULL,
  `lembagasertifikasi_id` int(10) unsigned DEFAULT NULL,
  `administrator_id` int(10) unsigned NOT NULL,
  `no_undanganpeserta` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subbidang_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `penjadwalan_udiklat_id_foreign` (`udiklat_id`),
  KEY `penjadwalan_unitinduk_id_foreign` (`unitinduk_id`),
  KEY `penjadwalan_unitcabang_id_foreign` (`unitcabang_id`),
  KEY `penjadwalan_unitranting_id_foreign` (`unitranting_id`),
  KEY `penjadwalan_lembagasertifikasi_id_foreign` (`lembagasertifikasi_id`),
  KEY `penjadwalan_administrator_id_index` (`administrator_id`),
  KEY `penjadwalan_subbidang_id_foreign` (`subbidang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `penjadwalan`
--

INSERT INTO `penjadwalan` (`id`, `tanggal_mulai`, `tanggal_selesai`, `udiklat_id`, `unitinduk_id`, `unitcabang_id`, `unitranting_id`, `lembagasertifikasi_id`, `administrator_id`, `no_undanganpeserta`, `status`, `created_at`, `updated_at`, `subbidang_id`) VALUES
(4, '2015-09-08', '2015-09-10', 10, 20, 4, 8, 2, 1, NULL, 1, '2015-09-25 11:51:01', '2015-09-25 11:51:01', NULL),
(5, '2015-09-16', '2015-09-18', 8, 18, 5, 10, 3, 1, NULL, 1, '2015-09-25 11:51:45', '2015-09-25 11:51:45', NULL),
(6, '2015-09-28', '2015-09-30', 10, 22, 7, 4, 2, 1, NULL, 1, '2015-09-29 03:34:17', '2015-09-29 03:35:15', NULL),
(7, '2015-11-10', '2015-11-12', 7, 19, NULL, NULL, 2, 1, NULL, 1, '2015-11-03 13:28:08', '2015-11-03 13:28:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjadwalanasesor`
--

CREATE TABLE IF NOT EXISTS `penjadwalanasesor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `penjadwalan_id` int(10) unsigned NOT NULL,
  `asesor_id` int(10) unsigned NOT NULL,
  `posisi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `penjadwalanasesor_penjadwalan_id_foreign` (`penjadwalan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `penjadwalanasesor`
--

INSERT INTO `penjadwalanasesor` (`id`, `penjadwalan_id`, `asesor_id`, `posisi`, `created_at`, `updated_at`) VALUES
(6, 4, 3, 'ketua', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 4, 1, 'anggota', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 4, 4, 'anggota', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 5, 1, 'ketua', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 5, 3, 'anggota', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 5, 4, 'anggota', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 6, 1, 'ketua', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 6, 3, 'anggota', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 6, 0, 'anggota', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 7, 1, 'ketua', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 7, 0, 'anggota', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=44 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'can_read_maindashboard', 'view dashboard utama', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(2, 'can_read_masteradministrator', 'view master administrator', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(3, 'can_write_masteradministrator', 'Write master administrator', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(4, 'can_read_masterasesor', 'view master asesor', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(5, 'can_write_masterasesor', 'Write master asesor', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(6, 'can_read_masterpeserta', 'view master peserta', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(7, 'can_write_masterpeserta', 'Write master peserta', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(8, 'can_read_masterakreditor', 'view master akreditor', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(9, 'can_write_masterakreditor', 'Write master akreditor', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(10, 'can_read_masterlembagasertifikasi', 'view master lembaga sertifikasi', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(11, 'can_write_masterlembagasertifikasi', 'Write master lembaga sertifikasi', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(12, 'can_read_masterbidang', 'view master bidang', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(13, 'can_write_masterbidang', 'Write master bidang', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(14, 'can_read_masterkompetensi', 'view master kompetensi', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(15, 'can_write_masterkompetensi', 'Write master kompetensi', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(16, 'can_read_masterunit', 'view master unit', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(17, 'can_write_masterunit', 'Write master unit', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(18, 'can_read_masterjenjangjabatan', 'view master jenjang jabatan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(19, 'can_write_masterjenjangjabatan', 'Write master jenjang jabatan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(20, 'can_read_masterpendidikan', 'view master pendidikan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(21, 'can_write_masterpendidikan', 'Write master pendidikan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(22, 'can_read_mastergrade', 'view master jenjang grade', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(23, 'can_write_mastergrade', 'Write master jenjang grade', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(24, 'can_read_masterrole', 'view user role', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(25, 'can_write_masterrole', 'Write User Role', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(26, 'can_read_permohonan', 'view permohonan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(27, 'can_write_permohonan', 'Write permohonan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(28, 'can_read_evaluasi', 'view evaluasi', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(29, 'can_write_evaluasi', 'Write evaluasi', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(30, 'can_read_penjadwalan', 'view penjadwalan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(31, 'can_write_penjadwalan', 'Write penjadwalan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(32, 'can_read_pelaksanaan', 'view pelaksanaan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(33, 'can_write_pelaksanaan', 'Write pelaksanaan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(34, 'can_read_sertifikatbaru', 'view sertifikat baru', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(35, 'can_read_sertifikatperpanjangan', 'view sertifikat perpanjangan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(36, 'can_write_sertifikatbaru', 'Write sertifikat baru', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(37, 'can_write_sertifikatperpanjangan', 'Write sertifikat perpanjangan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(38, 'can_read_laporanpelaksanaan', 'view laporan pelaksanaan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(39, 'can_read_laporanevaluasi', 'view laporan evaluasi', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(40, 'can_read_laporanasesor', 'view laporan asesor', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(41, 'can_read_laporansebarankompetensi', 'view laporan sebaran kompetensi', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(42, 'can_read_laporanhasilkelulusan', 'view laporan hasil kelulusan', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(43, 'can_read_laporanbiaya', 'view laporan biaya', NULL, '2015-09-22 05:32:19', '2015-09-22 05:32:19');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(1, 2),
(12, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(1, 3),
(8, 3),
(10, 3),
(12, 3),
(14, 3),
(16, 3),
(18, 3),
(20, 3),
(22, 3),
(26, 3),
(28, 3),
(30, 3),
(32, 3),
(34, 3),
(35, 3),
(38, 3),
(39, 3),
(40, 3),
(41, 3),
(42, 3),
(43, 3),
(1, 4),
(4, 4),
(6, 4),
(8, 4),
(10, 4),
(12, 4),
(14, 4),
(16, 4),
(18, 4),
(20, 4),
(22, 4),
(26, 4),
(27, 4),
(28, 4),
(29, 4),
(30, 4),
(31, 4),
(32, 4),
(33, 4),
(34, 4),
(35, 4),
(36, 4),
(37, 4),
(38, 4),
(39, 4),
(40, 4),
(41, 4),
(42, 4),
(43, 4),
(38, 5),
(40, 5);

-- --------------------------------------------------------

--
-- Table structure for table `permohonan`
--

CREATE TABLE IF NOT EXISTS `permohonan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nosurat` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `unitinduk_id` int(10) unsigned NOT NULL,
  `tanggal_surat` date DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `permohonan_unitinduk_id_foreign` (`unitinduk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `permohonan`
--

INSERT INTO `permohonan` (`id`, `nosurat`, `unitinduk_id`, `tanggal_surat`, `keterangan`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, '123', 3, '1900-12-01', 'itu', 1, NULL, '2015-09-22 06:11:42', '2015-11-03 09:07:54'),
(6, '010/SDM03.02/WRKR/2015', 6, '2015-09-01', 'belum dilengkapi dengan form permohonan', 1, NULL, '2015-09-23 07:22:50', '2015-09-23 07:22:50'),
(7, '010/SDM03.02/DJBB/2015', 19, '2015-09-22', 'Sipp', 1, NULL, '2015-09-25 04:26:33', '2015-09-25 04:26:33'),
(8, '0123/SDM.03.02/DISBALI/2015', 22, '2015-09-01', 'TES', 1, NULL, '2015-09-29 03:29:20', '2015-11-03 09:42:49'),
(11, '010/SDM03.02/DISJATIM/2015', 20, '2015-11-05', 'Coba', 1, NULL, '2015-11-03 09:45:35', '2015-11-03 09:45:35');

-- --------------------------------------------------------

--
-- Table structure for table `permohonansdm`
--

CREATE TABLE IF NOT EXISTS `permohonansdm` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sdm_id` int(10) unsigned NOT NULL,
  `unitinduk_id` int(10) unsigned NOT NULL,
  `tanggal_mohon` date DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_mohon` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `permohonansdm_unitinduk_id_foreign` (`unitinduk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `permohonansdm`
--

INSERT INTO `permohonansdm` (`id`, `sdm_id`, `unitinduk_id`, `tanggal_mohon`, `keterangan`, `status_mohon`, `deleted_at`, `created_at`, `updated_at`) VALUES
(8, 0, 22, '2015-11-11', 'coba', 1, NULL, '2015-11-12 03:59:44', '2015-11-12 03:59:44'),
(9, 0, 22, '2015-11-11', 'coba', 1, NULL, '2015-11-12 03:59:44', '2015-11-12 03:59:44');

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE IF NOT EXISTS `peserta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pendidikan_id` int(10) unsigned NOT NULL,
  `jenjangjabatan_id` int(10) unsigned NOT NULL,
  `grade_id` int(10) unsigned NOT NULL,
  `unitinduk_id` int(10) unsigned DEFAULT NULL,
  `unitcabang_id` int(10) unsigned DEFAULT NULL,
  `unitranting_id` int(10) unsigned DEFAULT NULL,
  `unitnonpln_id` int(10) unsigned DEFAULT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `no_identitas` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_kelamin` enum('m','f') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'm',
  `kewarganegaraan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `alamat` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `golongan_darah` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_hp` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_telp` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jabatan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jurusan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipe_peserta` enum('pln','nonpln') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pln',
  `non_pln_info` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `atasan_id` int(10) unsigned DEFAULT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `edited_by` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `peserta_pendidikan_id_foreign` (`pendidikan_id`),
  KEY `peserta_jenjangjabatan_id_foreign` (`jenjangjabatan_id`),
  KEY `peserta_grade_id_foreign` (`grade_id`),
  KEY `peserta_unitinduk_id_foreign` (`unitinduk_id`),
  KEY `peserta_unitcabang_id_foreign` (`unitcabang_id`),
  KEY `peserta_unitranting_id_foreign` (`unitranting_id`),
  KEY `peserta_nip_index` (`nip`),
  KEY `peserta_unitnonpln_id_foreign` (`unitnonpln_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`id`, `nip`, `pendidikan_id`, `jenjangjabatan_id`, `grade_id`, `unitinduk_id`, `unitcabang_id`, `unitranting_id`, `unitnonpln_id`, `nama`, `no_identitas`, `jenis_kelamin`, `kewarganegaraan`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `golongan_darah`, `email`, `no_hp`, `no_telp`, `jabatan`, `jurusan`, `tipe_peserta`, `non_pln_info`, `atasan_id`, `photo`, `status`, `created_by`, `edited_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1234561', 3, 3, 3, 3, 3, 3, NULL, ' siapa ', '123', 'm', 'nigeria', 'nigeria', '1945-01-01', '', 'O', 'siapa@namaku.com', '098987867578', '089620709396', 'kepsek', 'RPL', 'pln', NULL, NULL, 'photo_123456.jpg', 1, 0, 0, NULL, '2015-09-22 06:22:13', '2015-09-22 06:51:07'),
(3, '12345621', 3, 3, 3, 3, NULL, NULL, 3, 'aku', '123', 'm', 'suriah', 'jerman', '1945-05-01', 'di sana', 'A', 'aku@suriah.com', '098987867578', '089620709396', 'kepsek', 'RPL', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-09-22 08:20:26', '2015-09-22 08:20:26'),
(4, '1123', 3, 3, 3, 15, NULL, NULL, 3, 'Coba', '1234', 'm', 'INDONESIA', '', '1945-01-01', '', 'O', '', '085', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-09-23 07:04:29', '2015-09-23 07:04:29'),
(5, '86465', 3, 3, 3, 3, NULL, NULL, 3, 'Coba1', '1516', 'f', 'INDONESIA', '', '1945-01-01', '', 'A', '', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-09-23 07:05:58', '2015-09-23 07:05:58'),
(7, '5464', 3, 8, 4, NULL, NULL, NULL, 3, 'Coba3', '4564', 'f', 'INDONESIA', '', '1945-01-01', '', 'A', '', '', '', '', '', 'nonpln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-09-23 07:08:01', '2015-09-23 07:08:01'),
(8, '123456', 6, 4, 3, 22, NULL, NULL, 3, 'TES1', '456658', 'f', 'INDONESIA', '', '1945-01-01', '', 'A', '', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-09-29 03:27:16', '2015-09-29 03:27:16'),
(9, '545646', 3, 9, 9, NULL, NULL, NULL, 3, 'TES2', '45868', 'm', '', '', '1945-01-07', '', 'O', '', '', '', '', '', 'nonpln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-09-29 03:28:20', '2015-09-29 03:28:20'),
(11, '4312010052', 3, 3, 3, NULL, NULL, NULL, 3, 'Yuniarty', '12345', 'f', 'Indonesia', 'Jakarta', '1956-06-05', '', 'O', 'yuniartyhan@gmail.com', '087888707172', '77214151', 'Peserta', 'IPA', 'nonpln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-10-07 01:56:51', '2015-10-07 01:58:20'),
(12, '001', 3, 3, 3, 3, NULL, NULL, 3, 'SDM Sumatera Utara', '010101', 'f', 'INDONESIA', '', '1945-01-01', '', 'A', '', '', '', '', 'S1', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-10-22 04:06:27', '2015-10-22 04:06:27'),
(13, 'peg1', 3, 5, 3, 18, 5, 10, NULL, 'Pegawai 1', '145611', 'f', 'Indonesia', '', '1945-01-01', '', 'O', 'peg1@gmail.com', '087888707172', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:29:17', '2015-11-03 07:30:53'),
(14, 'peg11', 3, 3, 3, 18, NULL, NULL, 3, 'pegawai11', '344t4', 'f', 'Indonesia', '', '1945-01-01', '', 'B', '', '53466', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:30:01', '2015-11-03 07:30:01'),
(15, 'peg2', 7, 3, 3, 18, 7, NULL, 3, 'Pegawai 2', '145612', 'm', 'Indonesia', '', '1953-07-03', '', 'B', 'peg2@gmail.com', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:30:17', '2015-11-03 07:30:17'),
(16, 'peg3', 8, 3, 3, 18, 5, NULL, 3, 'Pegawai 3', '145613', 'f', 'Indonesia', '', '1953-05-05', '', 'B', 'peg3@gmail.com', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:32:11', '2015-11-03 07:32:11'),
(17, 'peg4', 9, 3, 7, 18, 5, 10, 3, 'Pegawai 4', '145614', 'f', 'Indonesia', '', '1962-09-14', '', 'AB', 'peg4@gmail.com', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:33:00', '2015-11-03 07:33:00'),
(18, 'peg5', 8, 7, 9, 18, 5, 10, 3, 'Pegawai 5', '145615', 'm', 'Indonesia', '', '1958-08-19', '', 'B', 'peg5@gmail.com', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:33:52', '2015-11-03 07:33:52'),
(19, 'peg6', 6, 12, 9, 18, 7, NULL, 3, 'Pegawai 6', '145616', 'm', 'Indonesia', '', '1961-12-17', '', 'A', 'peg6@gmail.com', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:34:49', '2015-11-03 07:34:49'),
(20, 'peg7', 7, 11, 3, 18, 7, NULL, 3, 'Pegawai 7', '145617', 'm', 'Indonesia', '', '1949-02-07', '', 'B', 'peg7@gmail.com', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:35:46', '2015-11-03 07:35:46'),
(21, 'peg11', 8, 5, 9, 18, NULL, NULL, 3, 'pegawai11', '989876', 'm', '', '', '1945-01-01', '', 'AB', '', '', '576', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:36:15', '2015-11-03 07:36:15'),
(22, 'peg8', 8, 8, 3, 11, 7, NULL, NULL, 'Pegawai 8', '145618', 'm', 'Indonesia', '', '1951-02-09', '', 'A', 'peg8@gmail.com', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:37:30', '2015-11-03 10:44:38'),
(23, 'peg9', 3, 8, 9, 18, 5, 10, 3, 'Pegawai 9', '145619', 'm', 'Indonesia', '', '1945-01-01', '', 'O', 'peg9@gmail.com', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:38:31', '2015-11-03 07:38:31'),
(24, 'peg10', 7, 3, 8, 18, 7, NULL, NULL, 'Pegawai 10', '145621', 'f', 'Indonesia', '', '1953-09-14', '', 'B', 'peg10@gmail.com', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 07:39:45', '2015-11-03 09:49:32'),
(26, '1234567P', 3, 3, 3, 4, NULL, NULL, 3, 'Test Test', '12345568877994', 'm', 'Indonesia', '', '1945-01-01', '', 'A', '', '', '', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 10:59:39', '2015-11-03 10:59:39'),
(27, 'peg12', 3, 7, 8, 20, 4, 8, NULL, 'pegawai12', '123', 'm', 'Indonesia', '', '1945-01-01', '', 'B', 'dianingratri@yahoo.com', '53466', '576', '', '', 'pln', NULL, NULL, NULL, 1, 0, 0, NULL, '2015-11-03 12:45:52', '2015-11-03 12:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `pesertakompetensipenjadwalan`
--

CREATE TABLE IF NOT EXISTS `pesertakompetensipenjadwalan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `penjadwalan_id` int(10) unsigned NOT NULL,
  `kompetensipenjadwalan_id` int(10) unsigned NOT NULL,
  `peserta_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `evaluasi_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pesertakompetensipenjadwalan_penjadwalan_id_foreign` (`penjadwalan_id`),
  KEY `pesertakompetensipenjadwalan_kompetensipenjadwalan_id_foreign` (`kompetensipenjadwalan_id`),
  KEY `pesertakompetensipenjadwalan_peserta_id_foreign` (`peserta_id`),
  KEY `pesertakompetensipenjadwalan_evaluasi_id_foreign` (`evaluasi_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `pesertakompetensipenjadwalan`
--

INSERT INTO `pesertakompetensipenjadwalan` (`id`, `penjadwalan_id`, `kompetensipenjadwalan_id`, `peserta_id`, `created_at`, `updated_at`, `evaluasi_id`) VALUES
(5, 5, 4, 4, '2015-09-25 11:53:33', '2015-09-25 11:53:33', 7),
(6, 5, 4, 5, '2015-09-25 11:53:33', '2015-09-25 11:53:33', 10),
(7, 5, 4, 1, '2015-09-25 11:53:33', '2015-09-25 11:53:33', 9),
(8, 6, 6, 8, '2015-09-29 03:36:30', '2015-09-29 03:36:30', 11),
(9, 6, 6, 9, '2015-09-29 03:36:30', '2015-09-29 03:36:30', 14),
(10, 6, 6, 5, '2015-09-29 03:36:30', '2015-09-29 03:36:30', 12),
(12, 4, 5, 3, '2015-09-29 03:40:13', '2015-09-29 03:40:13', 6),
(13, 7, 7, 16, '2015-11-03 13:29:36', '2015-11-03 13:29:36', 16),
(14, 7, 7, 15, '2015-11-03 13:29:36', '2015-11-03 13:29:36', 18),
(15, 7, 8, 15, '2015-11-03 13:30:42', '2015-11-03 13:30:42', 17),
(16, 7, 8, 16, '2015-11-03 13:30:42', '2015-11-03 13:30:42', 19);

-- --------------------------------------------------------

--
-- Table structure for table `realisasipeserta`
--

CREATE TABLE IF NOT EXISTS `realisasipeserta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `peserta_id` int(10) unsigned NOT NULL,
  `kompetensipenjadwalan_id` int(10) unsigned NOT NULL,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `kehadiran` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `kelulusan` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `information` text COLLATE utf8_unicode_ci,
  `no_sertifikat` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_cetak` date DEFAULT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `tanggal_keluar` date DEFAULT NULL,
  `status_sertifikat` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `realisasipeserta_peserta_id_foreign` (`peserta_id`),
  KEY `realisasipeserta_kompetensipenjadwalan_id_foreign` (`kompetensipenjadwalan_id`),
  KEY `realisasipeserta_kompetensi_id_foreign` (`kompetensi_id`),
  KEY `realisasipeserta_no_sertifikat_index` (`no_sertifikat`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `realisasipeserta`
--

INSERT INTO `realisasipeserta` (`id`, `peserta_id`, `kompetensipenjadwalan_id`, `kompetensi_id`, `kehadiran`, `kelulusan`, `information`, `no_sertifikat`, `tanggal_cetak`, `tanggal_awal`, `tanggal_akhir`, `tanggal_keluar`, `status_sertifikat`, `created_at`, `updated_at`) VALUES
(2, 5, 4, 2, 'y', 'n', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-09-25 11:59:24', '2015-09-29 03:38:26'),
(4, 7, 4, 2, 'y', 'n', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-09-25 11:59:24', '2015-09-25 11:59:24'),
(5, 8, 6, 1, 'y', 'y', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-09-29 07:29:39', '2015-09-29 07:29:39'),
(6, 9, 6, 1, 'n', 'n', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-09-29 07:29:39', '2015-10-19 04:00:16'),
(7, 5, 6, 1, 'y', 'y', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-09-29 07:29:39', '2015-09-29 07:30:06'),
(8, 3, 5, 1, 'y', 'y', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-10-06 00:06:54', '2015-10-06 00:06:54'),
(9, 16, 7, 1, 'y', 'y', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-11-03 13:32:12', '2015-11-03 13:32:12'),
(10, 15, 7, 1, 'y', 'y', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-11-03 13:32:12', '2015-11-03 13:32:12'),
(11, 15, 8, 2, 'y', 'y', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-11-03 13:32:43', '2015-11-03 13:32:43'),
(12, 16, 8, 2, 'y', 'y', '', '', NULL, NULL, NULL, NULL, 'baru', '2015-11-03 13:32:43', '2015-11-03 13:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `registers`
--

CREATE TABLE IF NOT EXISTS `registers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `nip` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pendidikan_id` int(10) unsigned NOT NULL,
  `jenjangjabatan_id` int(10) unsigned NOT NULL,
  `grade_id` int(10) unsigned NOT NULL,
  `unitinduk_id` int(10) unsigned DEFAULT NULL,
  `unitcabang_id` int(10) unsigned DEFAULT NULL,
  `unitranting_id` int(10) unsigned DEFAULT NULL,
  `unitnonpln_id` int(10) unsigned DEFAULT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `no_identitas` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_kelamin` enum('m','f') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'm',
  `kewarganegaraan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `alamat` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `golongan_darah` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_hp` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_telp` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jabatan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jurusan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipe_peserta` enum('pln','nonpln') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pln',
  `non_pln_info` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kondisi_khusus` enum('y','n') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `keterangan_kondisi_khusus` text COLLATE utf8_unicode_ci,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_valid` enum('y','n') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `validated_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `registers_pendidikan_id_foreign` (`pendidikan_id`),
  KEY `registers_jenjangjabatan_id_foreign` (`jenjangjabatan_id`),
  KEY `registers_grade_id_foreign` (`grade_id`),
  KEY `registers_unitinduk_id_foreign` (`unitinduk_id`),
  KEY `registers_unitcabang_id_foreign` (`unitcabang_id`),
  KEY `registers_unitranting_id_foreign` (`unitranting_id`),
  KEY `registers_kompetensi_id_foreign` (`kompetensi_id`),
  KEY `registers_nip_index` (`nip`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `registers`
--

INSERT INTO `registers` (`id`, `kompetensi_id`, `nip`, `pendidikan_id`, `jenjangjabatan_id`, `grade_id`, `unitinduk_id`, `unitcabang_id`, `unitranting_id`, `unitnonpln_id`, `nama`, `no_identitas`, `jenis_kelamin`, `kewarganegaraan`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `golongan_darah`, `email`, `no_hp`, `no_telp`, `jabatan`, `jurusan`, `tipe_peserta`, `non_pln_info`, `kondisi_khusus`, `keterangan_kondisi_khusus`, `photo`, `is_valid`, `validated_by`, `created_at`, `updated_at`) VALUES
(1, 2, '07650076', 8, 3, 3, 4, NULL, NULL, NULL, 'John Doe', '123456', 'f', 'Indonesia', 'Surabaya', '1958-07-08', 'Jalan Tlogomas Sengkaling', 'B', 'rezd14@gmail.com', '098839817', '03198123123', 'Manager', 'Teknik Elektro', 'pln', NULL, 'y', 'Punya asma, membutuhkan obat-obatan sesuai petunjuk dokter, tidak boleh terlalu lelah', NULL, 'n', NULL, '2015-11-11 09:15:09', '2015-11-11 09:15:09');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super Admin', 'Super administrator', '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(2, 'employee', 'Employee', 'Pegawai', '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(3, 'sdm', 'SDM Unit', 'SDM Unit', '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(4, 'unitsertifikasi', 'Unit Sertifikasi', 'Unit Sertifikasi', '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(5, 'asesor', 'Asesor', 'Asesor', '2015-09-22 05:32:19', '2015-09-22 05:32:19'),
(6, 'lsk', 'LSK', 'Lembaga Sertifikasi', '2015-09-22 05:32:19', '2015-09-22 05:32:19');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(6, 1),
(4, 2),
(3, 3),
(8, 3),
(5, 4),
(7, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sertifikat`
--

CREATE TABLE IF NOT EXISTS `sertifikat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `peserta_id` int(10) unsigned NOT NULL,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `lembagasertifikasi_id` int(10) unsigned NOT NULL,
  `penjadwalan_id` int(10) unsigned NOT NULL,
  `no_sertifikat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_mulai_berlaku` date DEFAULT NULL,
  `tanggal_akhir_berlaku` date DEFAULT NULL,
  `tanggal_cetak` date DEFAULT NULL,
  `status` enum('baru','perpanjangan') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'baru',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `sertifikat_peserta_id_foreign` (`peserta_id`),
  KEY `sertifikat_kompetensi_id_foreign` (`kompetensi_id`),
  KEY `sertifikat_lembagasertifikasi_id_foreign` (`lembagasertifikasi_id`),
  KEY `sertifikat_penjadwalan_id_foreign` (`penjadwalan_id`),
  KEY `sertifikat_no_sertifikat_index` (`no_sertifikat`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `sertifikat`
--

INSERT INTO `sertifikat` (`id`, `peserta_id`, `kompetensi_id`, `lembagasertifikasi_id`, `penjadwalan_id`, `no_sertifikat`, `tanggal_mulai_berlaku`, `tanggal_akhir_berlaku`, `tanggal_cetak`, `status`, `created_at`, `updated_at`) VALUES
(2, 5, 2, 3, 5, 'KTL.DHR.41.2.01.K1.12 .2015.00012', '2015-09-25', '2018-09-25', NULL, 'baru', '2015-09-25 12:00:07', '2015-09-25 12:00:07'),
(4, 8, 1, 2, 6, '111.2015.00014', '2015-09-29', '2018-09-29', NULL, 'baru', '2015-09-29 07:30:30', '2015-09-29 07:30:30'),
(5, 5, 1, 2, 6, '111.2015.00015', '2015-09-29', '2018-09-29', NULL, 'baru', '2015-09-29 07:30:43', '2015-09-29 07:30:43'),
(6, 16, 1, 2, 7, '111.2015.00016', '2015-11-03', '2018-11-03', NULL, 'baru', '2015-11-03 13:33:01', '2015-11-03 13:33:01'),
(7, 15, 1, 2, 7, '111.2015.00017', '2015-11-03', '2018-11-03', NULL, 'baru', '2015-11-03 13:33:10', '2015-11-03 13:33:10');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikatasesor`
--

CREATE TABLE IF NOT EXISTS `sertifikatasesor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no_registrasi` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `no_sertifikat` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_terbit` date DEFAULT NULL,
  `masa_berlaku` date DEFAULT NULL,
  `lembaga_id` int(10) unsigned NOT NULL,
  `asesor_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `sertifikatasesor_lembaga_id_foreign` (`lembaga_id`),
  KEY `sertifikatasesor_asesor_id_foreign` (`asesor_id`),
  KEY `sertifikatasesor_no_registrasi_index` (`no_registrasi`),
  KEY `sertifikatasesor_no_sertifikat_index` (`no_sertifikat`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sertifikatasesor`
--

INSERT INTO `sertifikatasesor` (`id`, `no_registrasi`, `no_sertifikat`, `tanggal_terbit`, `masa_berlaku`, `lembaga_id`, `asesor_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, '12345', '12', '2015-09-14', '2015-09-21', 2, 1, NULL, '2015-09-22 06:53:39', '2015-09-22 06:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikathistory`
--

CREATE TABLE IF NOT EXISTS `sertifikathistory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sertifikat_id` int(10) unsigned NOT NULL,
  `peserta_id` int(10) unsigned NOT NULL,
  `kompetensi_id` int(10) unsigned NOT NULL,
  `lembagasertifikasi_id` int(10) unsigned NOT NULL,
  `penjadwalan_id` int(10) unsigned NOT NULL,
  `no_sertifikat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_mulai_berlaku` date DEFAULT NULL,
  `tanggal_akhir_berlaku` date DEFAULT NULL,
  `tanggal_cetak` date DEFAULT NULL,
  `status` enum('baru','perpanjangan') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'baru',
  `information` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `sertifikathistory_sertifikat_id_foreign` (`sertifikat_id`),
  KEY `sertifikathistory_peserta_id_foreign` (`peserta_id`),
  KEY `sertifikathistory_kompetensi_id_foreign` (`kompetensi_id`),
  KEY `sertifikathistory_lembagasertifikasi_id_foreign` (`lembagasertifikasi_id`),
  KEY `sertifikathistory_penjadwalan_id_foreign` (`penjadwalan_id`),
  KEY `sertifikathistory_no_sertifikat_index` (`no_sertifikat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subbidang`
--

CREATE TABLE IF NOT EXISTS `subbidang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bidang_id` int(10) unsigned NOT NULL,
  `nama_subbidang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nama_subbidang_english` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subbidang_bidang_id_foreign` (`bidang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `subbidang`
--

INSERT INTO `subbidang` (`id`, `bidang_id`, `nama_subbidang`, `created_at`, `updated_at`, `nama_subbidang_english`) VALUES
(1, 2, 'Pemeliharaan Distribusi', '2015-09-22 06:08:50', '2015-10-07 02:09:28', 'Distribution Maintenance'),
(2, 2, 'Pengoperasian Distribusi', '2015-09-23 04:37:39', '2015-10-07 02:09:48', 'Distribution Operation'),
(3, 3, 'Pemeliharaan Pembangkit', '2015-09-23 04:38:41', '2015-10-07 02:10:32', 'Power Generation Maintenance '),
(4, 3, 'Pengoperasian Pembangkit', '2015-09-23 04:39:19', '2015-10-07 02:10:41', 'Power Generation Operation '),
(5, 6, 'Supervisi Konstruksi Sipil', '2015-09-23 04:48:58', '2015-10-07 02:12:13', 'Civil Construction Supervision'),
(6, 6, 'Supervisi Konstruksi Elektrikal', '2015-09-23 04:49:28', '2015-11-03 10:07:20', 'Electrical Construction Supervision'),
(7, 6, 'Supervisi Konstruksi Mekanikal', '2015-09-23 04:49:47', '2015-10-07 02:11:24', 'Mechanical Construction Supervision');

-- --------------------------------------------------------

--
-- Table structure for table `subbidangasesor`
--

CREATE TABLE IF NOT EXISTS `subbidangasesor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subbidang_id` int(10) unsigned NOT NULL,
  `asesor_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `subbidangasesor_subbidang_id_foreign` (`subbidang_id`),
  KEY `subbidangasesor_asesor_id_foreign` (`asesor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `subbidangasesor`
--

INSERT INTO `subbidangasesor` (`id`, `subbidang_id`, `asesor_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2015-11-03 11:22:45', '2015-11-03 11:22:45');

-- --------------------------------------------------------

--
-- Table structure for table `timasesor`
--

CREATE TABLE IF NOT EXISTS `timasesor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jabatantim` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `timasesor_jabatantim_index` (`jabatantim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `udiklat`
--

CREATE TABLE IF NOT EXISTS `udiklat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_udiklat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `udiklat`
--

INSERT INTO `udiklat` (`id`, `nama_udiklat`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, 'PLN Udiklat Tuntungan', NULL, '2015-09-22 06:10:59', '2015-09-23 06:17:10'),
(5, 'PLN Udiklat Palembang', NULL, '2015-09-23 06:17:28', '2015-09-23 06:17:28'),
(6, 'PLN Udiklat Padang', NULL, '2015-09-23 06:17:54', '2015-09-23 06:17:54'),
(7, 'PLN Udiklat Bogor', NULL, '2015-09-23 06:18:17', '2015-09-23 06:18:17'),
(8, 'PLN Udiklat Jakarta', NULL, '2015-09-23 06:18:29', '2015-09-23 06:18:29'),
(9, 'PLN Udiklat Semarang', NULL, '2015-09-23 06:22:25', '2015-09-23 06:22:25'),
(10, 'PLN Udiklat Pandaan', NULL, '2015-09-23 06:22:42', '2015-09-23 06:22:42'),
(11, 'PLN Udiklat Banjarbaru', NULL, '2015-09-23 06:23:06', '2015-09-23 06:23:06'),
(12, 'PLN Udiklat Makassar', NULL, '2015-09-23 06:23:30', '2015-10-07 02:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `unitcabang`
--

CREATE TABLE IF NOT EXISTS `unitcabang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unitinduk_id` int(10) unsigned NOT NULL,
  `nama_unitcabang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `edited_by` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `unitcabang_unitinduk_id_foreign` (`unitinduk_id`),
  KEY `unitcabang_nama_unitcabang_index` (`nama_unitcabang`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `unitcabang`
--

INSERT INTO `unitcabang` (`id`, `unitinduk_id`, `nama_unitcabang`, `created_by`, `edited_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 20, 'PT PLN (Persero) Area Malang', 0, 0, NULL, '2015-09-22 06:12:09', '2015-09-25 04:12:56'),
(4, 20, 'PT PLN (Persero) Area Madiun', 0, 0, NULL, '2015-09-25 04:12:42', '2015-09-25 04:12:42'),
(5, 18, 'PT PLN (Persero) Area Lenteng Agung', 0, 0, NULL, '2015-09-25 04:13:16', '2015-11-03 12:58:48'),
(6, 20, 'PT PLN (Persero) Area Kediri', 0, 0, NULL, '2015-09-25 04:13:33', '2015-09-25 04:13:33'),
(7, 18, 'PT PLN (Persero) Area Bintaro', 0, 0, NULL, '2015-09-25 04:13:59', '2015-11-03 12:58:34'),
(8, 20, 'PT PLN (Persero) Area Surabaya Utara', 0, 0, NULL, '2015-09-25 04:15:37', '2015-09-25 04:15:37'),
(9, 21, 'PT PLN (Persero) Area Semarang', 0, 0, NULL, '2015-09-25 04:16:26', '2015-09-25 04:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `unitinduk`
--

CREATE TABLE IF NOT EXISTS `unitinduk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `udiklat_id` int(10) unsigned NOT NULL,
  `nama_unitinduk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wilayah_unitinduk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nonpln` mediumtext COLLATE utf8_unicode_ci,
  `edited_by` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `unitinduk_udiklat_id_foreign` (`udiklat_id`),
  KEY `unitinduk_nama_unitinduk_index` (`nama_unitinduk`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `unitinduk`
--

INSERT INTO `unitinduk` (`id`, `udiklat_id`, `nama_unitinduk`, `wilayah_unitinduk`, `nonpln`, `edited_by`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 4, 'PT PLN (Persero) Wilayah Sumatera Utara', NULL, NULL, 0, 0, NULL, '2015-09-22 06:11:11', '2015-09-23 06:26:27'),
(4, 4, 'PT PLN (Persero) Wilayah Aceh', NULL, NULL, 0, 0, NULL, '2015-09-23 06:25:59', '2015-09-23 06:25:59'),
(5, 6, 'PT PLN (Persero) Wilayah Sumatera Barat', NULL, NULL, 0, 0, NULL, '2015-09-23 06:38:14', '2015-09-23 06:38:14'),
(6, 6, 'PT PLN (Persero) Wilayah Riau dan Kepulauan Riau', NULL, NULL, 0, 0, NULL, '2015-09-23 06:39:22', '2015-09-23 06:39:22'),
(7, 6, 'PT PLN (Persero) Wilayah Bangka Belitung', NULL, NULL, 0, 0, NULL, '2015-09-23 06:39:53', '2015-09-23 06:39:53'),
(8, 5, 'PT PLN (Persero) Wilayah Sumatra Selatan, Jambi, dan Bengkulu', NULL, NULL, 0, 0, NULL, '2015-09-23 06:40:16', '2015-09-23 06:40:16'),
(9, 7, 'PT PLN (Persero) Wilayah Kalimantan Barat', NULL, NULL, 0, 0, NULL, '2015-09-23 06:40:37', '2015-09-23 06:40:37'),
(10, 11, 'PT PLN (Persero) Wilayah Kalimantan Selatan dan Tengah', NULL, NULL, 0, 0, NULL, '2015-09-23 06:40:53', '2015-09-23 06:40:53'),
(11, 11, 'PT PLN (Persero) Wilayah Kalimantan Timur', NULL, NULL, 0, 0, NULL, '2015-09-23 06:41:18', '2015-09-23 06:41:18'),
(12, 12, 'PT PLN (Persero) Wilayah Sulawesi Utara, Tengah, dan Gorontalo', NULL, NULL, 0, 0, NULL, '2015-09-23 06:41:46', '2015-09-23 06:41:46'),
(13, 12, 'PT PLN (Persero) Wilayah Sulawesi Selatan, Tenggara, dan Barat', NULL, NULL, 0, 0, NULL, '2015-09-23 06:42:04', '2015-09-23 06:42:04'),
(14, 12, 'PT PLN (Persero) Wilayah Maluku dan Maluku Utara', NULL, NULL, 0, 0, NULL, '2015-09-23 06:42:21', '2015-09-23 06:42:21'),
(15, 10, 'PT PLN (Persero) Wilayah Nusa Tenggara Barat', NULL, NULL, 0, 0, NULL, '2015-09-23 06:43:12', '2015-09-23 06:43:12'),
(16, 10, 'PT PLN (Persero) Wilayah Nusa Tenggara Timur', NULL, NULL, 0, 0, NULL, '2015-09-23 06:43:33', '2015-09-23 06:43:33'),
(17, 12, 'PT PLN (Persero) Wilayah Papua dan Papua Barat', NULL, NULL, 0, 0, NULL, '2015-09-25 04:06:07', '2015-09-25 04:06:07'),
(18, 8, 'PT PLN (Persero) Distribusi DKI Jaya & Tangerang', NULL, NULL, 0, 0, NULL, '2015-09-25 04:06:39', '2015-09-25 04:06:39'),
(19, 7, 'PT PLN (Persero) Distribusi Jawa Barat dan Banten', NULL, NULL, 0, 0, NULL, '2015-09-25 04:06:51', '2015-09-25 04:06:51'),
(20, 10, 'PT PLN (Persero) Distribusi Jawa Timur', NULL, NULL, 0, 0, NULL, '2015-09-25 04:07:11', '2015-09-25 04:07:11'),
(21, 9, 'PT PLN (Persero) Distribusi Jawa Tengah dan DI Yogyakarta', NULL, NULL, 0, 0, NULL, '2015-09-25 04:07:28', '2015-09-25 04:07:28'),
(22, 10, 'PT PLN (Persero) Distribusi Bali', NULL, NULL, 0, 0, NULL, '2015-09-25 04:07:44', '2015-09-25 04:07:44'),
(23, 5, 'PT PLN (Persero) Distribusi Lampung', NULL, NULL, 0, 0, NULL, '2015-09-25 04:08:00', '2015-09-25 04:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `unitnonpln`
--

CREATE TABLE IF NOT EXISTS `unitnonpln` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_unitnonpln` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `edited_by` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `unitnonpln_nama_unitnonpln_index` (`nama_unitnonpln`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `unitnonpln`
--

INSERT INTO `unitnonpln` (`id`, `nama_unitnonpln`, `alamat`, `created_by`, `edited_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 'itu', 'di sana', 0, 0, NULL, '2015-09-22 06:12:54', '2015-09-22 06:12:54');

-- --------------------------------------------------------

--
-- Table structure for table `unitranting`
--

CREATE TABLE IF NOT EXISTS `unitranting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unitcabang_id` int(10) unsigned NOT NULL,
  `nama_unitranting` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `edited_by` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `unitranting_unitcabang_id_foreign` (`unitcabang_id`),
  KEY `unitranting_nama_unitranting_index` (`nama_unitranting`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `unitranting`
--

INSERT INTO `unitranting` (`id`, `unitcabang_id`, `nama_unitranting`, `created_by`, `edited_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 3, 'PT PLN (Persero) Rayon Kepanjen', 0, 0, NULL, '2015-09-22 06:12:38', '2015-09-25 04:17:13'),
(4, 6, 'PT PLN (Persero) Rayon Blitar', 0, 0, NULL, '2015-09-25 04:17:31', '2015-09-25 04:17:31'),
(5, 6, 'PT PLN (Persero) Rayon Tulung Agung', 0, 0, NULL, '2015-09-25 04:17:51', '2015-09-25 04:17:51'),
(6, 6, 'PT PLN (Persero) Rayon Pare', 0, 0, NULL, '2015-09-25 04:18:07', '2015-09-25 04:18:07'),
(7, 6, 'PT PLN (Persero) Rayon Trenggalek', 0, 0, NULL, '2015-09-25 04:19:07', '2015-09-25 04:19:07'),
(8, 4, 'PT PLN (Persero) Rayon Ponorogo', 0, 0, NULL, '2015-09-25 04:19:34', '2015-09-25 04:19:34'),
(9, 4, 'PT PLN (Persero) Rayon Madiun', 0, 0, NULL, '2015-09-25 04:19:52', '2015-09-25 04:19:52'),
(10, 5, 'PT PLN (Persero) Rayon Pasar Minggu', 0, 0, NULL, '2015-09-25 04:20:10', '2015-09-25 04:20:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_nip_unique` (`nip`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nip`, `password`, `role`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '123456', '$2y$10$8LVFqWTEruIH4Oo7arXUM.qHQ.krX22awtKvjmt7rmst1IfYDnAyW', 'admin', 'active', '66E7sl7EX1ab1Ow6AgVJCNUzN7ADONqfVngGrzkkS4Czvil87JH56rLQ7My1', '2015-09-22 05:32:02', '2015-11-12 01:53:04'),
(3, '8711161Z', '$2y$10$8sFKW4V6HEDQDq9qWh6ER.RUmB3wXafrsYzdQcZUdjQ.4ksOn3dT6', '', 'active', 'p7VwHQXdBbd3iRk6bBrUZQSKCky6oBJJN3GvUzPXDTSg2sHFZ6OvbqV8kDAD', '2015-09-23 01:10:48', '2015-11-03 08:07:39'),
(4, 'novi', '$2y$10$zX0jBqZPnjDcCsEsgDTSf.PvpBlZ4j2PQSwLD9wNDjI4W3qpaI/S6', '', 'active', 'qruFJiqyP7BdSP5J1MoatlWE8vNJqWkiBrwm7BdkH60jPw0dOKigEpkpVKD3', '2015-09-23 01:21:01', '2015-11-03 08:39:00'),
(5, 'ratih', '$2y$10$UzxVIgoPXDSbFmejqKr8F.0swjbwZD6UJkRCbRf77mFYc2cBgyTEa', '', 'active', 'gxNNWnMLoRncMa0SL4ouIlAXndH78jh1LIWyIMkt3r16Zdp2OZLW1JLVFyHt', '2015-09-23 07:03:16', '2015-10-22 04:04:16'),
(6, 'yuniarty', '$2y$10$wkTEmuACkUmRV2OY6xoo3ujb6STreTHyQUF0s/thb790Bv9ByEaW2', '', 'active', NULL, '2015-10-07 02:53:20', '2015-10-19 04:35:37'),
(7, 'cobauser', '$2y$10$KognWDIODVIfiT83CykeTu4L6Q5j2WPdS3Z2wJZLQ8rfAL6k8O6tG', '', 'active', 'zq4RhJZZbOgugwNs9T6sSifhzYqKc2Eq7YUImHR36HANtcrPM0W21oNu08mp', '2015-10-07 02:53:55', '2015-10-07 03:00:16'),
(8, '001', '$2y$10$TAIa8jEMXyWNTIOZyxJu5OyKldIw1J8L4alpgLvHt4qHuTevR1mOC', '', 'active', 'agOMLJnxiVl4kAweeJq2YMUV4QyYvY205i9auS210Prhi5rOJCkwpwHsuqoq', '2015-10-22 04:07:03', '2015-11-04 06:15:15');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `administrator`
--
ALTER TABLE `administrator`
  ADD CONSTRAINT `administrator_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grade` (`id`),
  ADD CONSTRAINT `administrator_jenjangjabatan_id_foreign` FOREIGN KEY (`jenjangjabatan_id`) REFERENCES `jenjangjabatan` (`id`),
  ADD CONSTRAINT `administrator_pendidikan_id_foreign` FOREIGN KEY (`pendidikan_id`) REFERENCES `pendidikan` (`id`),
  ADD CONSTRAINT `administrator_unitcabang_id_foreign` FOREIGN KEY (`unitcabang_id`) REFERENCES `unitcabang` (`id`),
  ADD CONSTRAINT `administrator_unitinduk_id_foreign` FOREIGN KEY (`unitinduk_id`) REFERENCES `unitinduk` (`id`),
  ADD CONSTRAINT `administrator_unitranting_id_foreign` FOREIGN KEY (`unitranting_id`) REFERENCES `unitranting` (`id`);

--
-- Constraints for table `asesor`
--
ALTER TABLE `asesor`
  ADD CONSTRAINT `asesor_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grade` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asesor_jenjangjabatan_id_foreign` FOREIGN KEY (`jenjangjabatan_id`) REFERENCES `jenjangjabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asesor_pendidikan_id_foreign` FOREIGN KEY (`pendidikan_id`) REFERENCES `pendidikan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asesor_unitcabang_id_foreign` FOREIGN KEY (`unitcabang_id`) REFERENCES `unitcabang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asesor_unitinduk_id_foreign` FOREIGN KEY (`unitinduk_id`) REFERENCES `unitinduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asesor_unitnonpln_id_foreign` FOREIGN KEY (`unitnonpln_id`) REFERENCES `unitnonpln` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asesor_unitranting_id_foreign` FOREIGN KEY (`unitranting_id`) REFERENCES `unitranting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `biayapelaksanaan`
--
ALTER TABLE `biayapelaksanaan`
  ADD CONSTRAINT `biayapelaksanaan_komponenbiaya_id_foreign` FOREIGN KEY (`komponenbiaya_id`) REFERENCES `komponenbiaya` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `biayapelaksanaan_penjadwalan_id_foreign` FOREIGN KEY (`penjadwalan_id`) REFERENCES `penjadwalan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `elemenkompetensi`
--
ALTER TABLE `elemenkompetensi`
  ADD CONSTRAINT `elemenkompetensi_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluasi`
--
ALTER TABLE `evaluasi`
  ADD CONSTRAINT `evaluasi_kompetensipermohonan_id_foreign` FOREIGN KEY (`kompetensipermohonan_id`) REFERENCES `kompetensipermohonan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `evaluasi_peserta_id_foreign` FOREIGN KEY (`peserta_id`) REFERENCES `peserta` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kelompokadministrator`
--
ALTER TABLE `kelompokadministrator`
  ADD CONSTRAINT `kelompokadministrator_administrator_id_foreign` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kelompokadministrator_lembagasertifikasi_id_foreign` FOREIGN KEY (`lembagasertifikasi_id`) REFERENCES `lembagasertifikasi` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kelompokasesor`
--
ALTER TABLE `kelompokasesor`
  ADD CONSTRAINT `kelompokasesor_ibfk_1` FOREIGN KEY (`lembagasertifikasi_id`) REFERENCES `lembagasertifikasi` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kelompokasesor_asesor_id_foreign` FOREIGN KEY (`asesor_id`) REFERENCES `asesor` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kompetensi`
--
ALTER TABLE `kompetensi`
  ADD CONSTRAINT `kompetensi_subbidang_id_foreign` FOREIGN KEY (`subbidang_id`) REFERENCES `subbidang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kompetensiadministrator`
--
ALTER TABLE `kompetensiadministrator`
  ADD CONSTRAINT `kompetensiadministrator_administrator_id_foreign` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kompetensiadministrator_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kompetensiasesor`
--
ALTER TABLE `kompetensiasesor`
  ADD CONSTRAINT `kompetensiasesor_asesor_id_foreign` FOREIGN KEY (`asesor_id`) REFERENCES `asesor` (`id`),
  ADD CONSTRAINT `kompetensiasesor_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`);

--
-- Constraints for table `kompetensipenjadwalan`
--
ALTER TABLE `kompetensipenjadwalan`
  ADD CONSTRAINT `kompetensipenjadwalan_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kompetensipenjadwalan_penjadwalan_id_foreign` FOREIGN KEY (`penjadwalan_id`) REFERENCES `penjadwalan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kompetensipermohonan`
--
ALTER TABLE `kompetensipermohonan`
  ADD CONSTRAINT `kompetensipermohonan_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kompetensipermohonan_permohonan_id_foreign` FOREIGN KEY (`permohonan_id`) REFERENCES `permohonan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kompetensipermohonanpeserta`
--
ALTER TABLE `kompetensipermohonanpeserta`
  ADD CONSTRAINT `kompetensipermohonanpeserta_kompetensipermohonan_id_foreign` FOREIGN KEY (`kompetensipermohonan_id`) REFERENCES `kompetensipermohonan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kompetensipermohonanpeserta_permohonan_id_foreign` FOREIGN KEY (`permohonan_id`) REFERENCES `permohonan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kompetensipermohonanpeserta_peserta_id_foreign` FOREIGN KEY (`peserta_id`) REFERENCES `peserta` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `kompetensipermohonansdm`
--
ALTER TABLE `kompetensipermohonansdm`
  ADD CONSTRAINT `kompetensipermohonansdm_permohonansdm_id_foreign` FOREIGN KEY (`permohonansdm_id`) REFERENCES `permohonansdm` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kompetensipermohonansdm_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `penjadwalan`
--
ALTER TABLE `penjadwalan`
  ADD CONSTRAINT `penjadwalan_subbidang_id_foreign` FOREIGN KEY (`subbidang_id`) REFERENCES `subbidang` (`id`),
  ADD CONSTRAINT `penjadwalan_administrator_id_foreign` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penjadwalan_lembagasertifikasi_id_foreign` FOREIGN KEY (`lembagasertifikasi_id`) REFERENCES `lembagasertifikasi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penjadwalan_udiklat_id_foreign` FOREIGN KEY (`udiklat_id`) REFERENCES `udiklat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penjadwalan_unitcabang_id_foreign` FOREIGN KEY (`unitcabang_id`) REFERENCES `unitcabang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penjadwalan_unitinduk_id_foreign` FOREIGN KEY (`unitinduk_id`) REFERENCES `unitinduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penjadwalan_unitranting_id_foreign` FOREIGN KEY (`unitranting_id`) REFERENCES `unitranting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penjadwalanasesor`
--
ALTER TABLE `penjadwalanasesor`
  ADD CONSTRAINT `penjadwalanasesor_penjadwalan_id_foreign` FOREIGN KEY (`penjadwalan_id`) REFERENCES `penjadwalan` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permohonan`
--
ALTER TABLE `permohonan`
  ADD CONSTRAINT `permohonan_unitinduk_id_foreign` FOREIGN KEY (`unitinduk_id`) REFERENCES `unitinduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permohonansdm`
--
ALTER TABLE `permohonansdm`
  ADD CONSTRAINT `permohonansdm_unitinduk_id_foreign` FOREIGN KEY (`unitinduk_id`) REFERENCES `unitinduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `peserta`
--
ALTER TABLE `peserta`
  ADD CONSTRAINT `peserta_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grade` (`id`),
  ADD CONSTRAINT `peserta_jenjangjabatan_id_foreign` FOREIGN KEY (`jenjangjabatan_id`) REFERENCES `jenjangjabatan` (`id`),
  ADD CONSTRAINT `peserta_pendidikan_id_foreign` FOREIGN KEY (`pendidikan_id`) REFERENCES `pendidikan` (`id`),
  ADD CONSTRAINT `peserta_unitcabang_id_foreign` FOREIGN KEY (`unitcabang_id`) REFERENCES `unitcabang` (`id`),
  ADD CONSTRAINT `peserta_unitinduk_id_foreign` FOREIGN KEY (`unitinduk_id`) REFERENCES `unitinduk` (`id`),
  ADD CONSTRAINT `peserta_unitnonpln_id_foreign` FOREIGN KEY (`unitnonpln_id`) REFERENCES `unitnonpln` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `peserta_unitranting_id_foreign` FOREIGN KEY (`unitranting_id`) REFERENCES `unitranting` (`id`);

--
-- Constraints for table `pesertakompetensipenjadwalan`
--
ALTER TABLE `pesertakompetensipenjadwalan`
  ADD CONSTRAINT `pesertakompetensipenjadwalan_evaluasi_id_foreign` FOREIGN KEY (`evaluasi_id`) REFERENCES `evaluasi` (`id`),
  ADD CONSTRAINT `pesertakompetensipenjadwalan_kompetensipenjadwalan_id_foreign` FOREIGN KEY (`kompetensipenjadwalan_id`) REFERENCES `kompetensipenjadwalan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pesertakompetensipenjadwalan_penjadwalan_id_foreign` FOREIGN KEY (`penjadwalan_id`) REFERENCES `penjadwalan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pesertakompetensipenjadwalan_peserta_id_foreign` FOREIGN KEY (`peserta_id`) REFERENCES `peserta` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `realisasipeserta`
--
ALTER TABLE `realisasipeserta`
  ADD CONSTRAINT `realisasipeserta_kompetensipenjadwalan_id_foreign` FOREIGN KEY (`kompetensipenjadwalan_id`) REFERENCES `kompetensipenjadwalan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `realisasipeserta_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `realisasipeserta_peserta_id_foreign` FOREIGN KEY (`peserta_id`) REFERENCES `peserta` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `registers`
--
ALTER TABLE `registers`
  ADD CONSTRAINT `registers_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`),
  ADD CONSTRAINT `registers_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grade` (`id`),
  ADD CONSTRAINT `registers_jenjangjabatan_id_foreign` FOREIGN KEY (`jenjangjabatan_id`) REFERENCES `jenjangjabatan` (`id`),
  ADD CONSTRAINT `registers_pendidikan_id_foreign` FOREIGN KEY (`pendidikan_id`) REFERENCES `pendidikan` (`id`),
  ADD CONSTRAINT `registers_unitcabang_id_foreign` FOREIGN KEY (`unitcabang_id`) REFERENCES `unitcabang` (`id`),
  ADD CONSTRAINT `registers_unitinduk_id_foreign` FOREIGN KEY (`unitinduk_id`) REFERENCES `unitinduk` (`id`),
  ADD CONSTRAINT `registers_unitranting_id_foreign` FOREIGN KEY (`unitranting_id`) REFERENCES `unitranting` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sertifikat`
--
ALTER TABLE `sertifikat`
  ADD CONSTRAINT `sertifikat_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sertifikat_lembagasertifikasi_id_foreign` FOREIGN KEY (`lembagasertifikasi_id`) REFERENCES `lembagasertifikasi` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sertifikat_penjadwalan_id_foreign` FOREIGN KEY (`penjadwalan_id`) REFERENCES `penjadwalan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sertifikat_peserta_id_foreign` FOREIGN KEY (`peserta_id`) REFERENCES `peserta` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sertifikatasesor`
--
ALTER TABLE `sertifikatasesor`
  ADD CONSTRAINT `sertifikatasesor_asesor_id_foreign` FOREIGN KEY (`asesor_id`) REFERENCES `asesor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sertifikatasesor_lembaga_id_foreign` FOREIGN KEY (`lembaga_id`) REFERENCES `lembaga` (`id`);

--
-- Constraints for table `sertifikathistory`
--
ALTER TABLE `sertifikathistory`
  ADD CONSTRAINT `sertifikathistory_kompetensi_id_foreign` FOREIGN KEY (`kompetensi_id`) REFERENCES `kompetensi` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sertifikathistory_lembagasertifikasi_id_foreign` FOREIGN KEY (`lembagasertifikasi_id`) REFERENCES `lembagasertifikasi` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sertifikathistory_penjadwalan_id_foreign` FOREIGN KEY (`penjadwalan_id`) REFERENCES `penjadwalan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sertifikathistory_peserta_id_foreign` FOREIGN KEY (`peserta_id`) REFERENCES `peserta` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sertifikathistory_sertifikat_id_foreign` FOREIGN KEY (`sertifikat_id`) REFERENCES `sertifikat` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subbidang`
--
ALTER TABLE `subbidang`
  ADD CONSTRAINT `subbidang_bidang_id_foreign` FOREIGN KEY (`bidang_id`) REFERENCES `bidang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subbidangasesor`
--
ALTER TABLE `subbidangasesor`
  ADD CONSTRAINT `subbidangasesor_asesor_id_foreign` FOREIGN KEY (`asesor_id`) REFERENCES `asesor` (`id`),
  ADD CONSTRAINT `subbidangasesor_subbidang_id_foreign` FOREIGN KEY (`subbidang_id`) REFERENCES `subbidang` (`id`);

--
-- Constraints for table `unitcabang`
--
ALTER TABLE `unitcabang`
  ADD CONSTRAINT `unitcabang_unitinduk_id_foreign` FOREIGN KEY (`unitinduk_id`) REFERENCES `unitinduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `unitinduk`
--
ALTER TABLE `unitinduk`
  ADD CONSTRAINT `unitinduk_udiklat_id_foreign` FOREIGN KEY (`udiklat_id`) REFERENCES `udiklat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `unitranting`
--
ALTER TABLE `unitranting`
  ADD CONSTRAINT `unitranting_unitcabang_id_foreign` FOREIGN KEY (`unitcabang_id`) REFERENCES `unitcabang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
