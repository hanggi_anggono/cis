<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubbidangIdPenjadwalan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('penjadwalan', function(Blueprint $table)
		{
			$table->integer('subbidang_id')->nullable()->unsigned();

			$table->foreign('subbidang_id')->references('id')->on('subbidang')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('penjadwalan', function(Blueprint $table)
		{
			$table->dropForeign('penjadwalan_subbidang_id_foreign');
			$table->dropColumn('subbidang_id');
		});
	}

}
