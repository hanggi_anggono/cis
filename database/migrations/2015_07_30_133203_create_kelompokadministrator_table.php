<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelompokadministratorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kelompokadministrator', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('administrator_id')->unsigned()->index();
			$table->integer('lembagasertifikasi_id')->unsigned()->index();
			$table->date('tanggalpenetapan')->nullable();
			$table->timestamps();

			$table->foreign('administrator_id')->references('id')->on('administrator')->onDelete('cascade');
			$table->foreign('lembagasertifikasi_id')->references('id')->on('lembagasertifikasi')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kelompokadministrator');
	}

}
