<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateEvaluasisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('evaluasi', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('kompetensipermohonan_id')->unsigned();
			$table->integer('peserta_id')->unsigned();
			$table->date('tanggal_evaluasi')->nullable();
			$table->tinyInteger('hasil')->default(0);
			$table->text('info')->nullable();
			$table->string('status_penjadwalan')->nullable()->default(0);
			$table->integer('updated_by')->nullable();
			$table->timestamps();

			$table->foreign('kompetensipermohonan_id')->references('id')->on('kompetensipermohonan')->onDelete('cascade');
			$table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('evaluasi');
	}

}
