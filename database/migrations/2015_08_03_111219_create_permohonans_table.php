<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermohonansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permohonan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nosurat',50);
			$table->integer('unitinduk_id')->unsigned();
			$table->date('tanggal_surat')->nullable();
			$table->string('keterangan')->nullable();
			$table->tinyInteger('status')->default(1);
			$table->softDeletes();
			$table->timestamps();

			$table->foreign('unitinduk_id')->references('id')->on('unitinduk')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permohonan');
	}

}
