<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubbidangasesorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subbidangasesor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('subbidang_id')->unsigned();
			$table->integer('asesor_id')->unsigned();
			$table->timestamps();

			// define foreign keys
			$table->foreign('subbidang_id')->references('id')->on('subbidang')->onDelete('restrict')->onUpdate('restrict');			
			$table->foreign('asesor_id')->references('id')->on('asesor')->onDelete('restrict')->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subbidangasesor');
	}

}
