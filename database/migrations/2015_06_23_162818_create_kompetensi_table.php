<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKompetensiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensi', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('subbidang_id')->unsigned();
			$table->string('nama_kompetensi',100);
			$table->string('kodedjk', 100)->nullable()->index();
			$table->string('kodeskkni', 100)->nullable()->index();
			$table->timestamps();

			$table->foreign('subbidang_id')->references('id')->on('subbidang')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensi');
	}

}
