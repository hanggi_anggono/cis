<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsPenjadwalansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('penjadwalan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('tanggal_mulai');
			$table->date('tanggal_selesai');
			$table->integer('udiklat_id')->unsigned()->nullable();
			$table->integer('unitinduk_id')->unsigned()->nullable();
			$table->integer('unitcabang_id')->unsigned()->nullable();
			$table->integer('unitranting_id')->unsigned()->nullable();
			$table->integer('lembagasertifikasi_id')->unsigned()->nullable();
			$table->integer('administrator_id')->unsigned()->index();
			$table->string('no_undanganpeserta', 50)->nullable();
			$table->tinyInteger('status')->default(0);
			$table->timestamps();

			$table->foreign('udiklat_id')->references('id')->on('udiklat')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('unitinduk_id')->references('id')->on('unitinduk')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('unitcabang_id')->references('id')->on('unitcabang')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('unitranting_id')->references('id')->on('unitranting')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('lembagasertifikasi_id')->references('id')->on('lembagasertifikasi')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('administrator_id')->references('id')->on('administrator')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('penjadwalan');
	}

}
