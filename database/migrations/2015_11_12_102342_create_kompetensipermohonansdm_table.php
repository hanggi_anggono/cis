<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKompetensipermohonansdmTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensipermohonansdm', function(Blueprint $table)
		{

			$table->increments('id');
			$table->integer('kompetensi_id')->unsigned();
			$table->integer('permohonansdm_id')->unsigned();
			$table->timestamps();

			$table->foreign('kompetensi_id')->references('id')->on('kompetensi')->onDelete('cascade');
			$table->foreign('permohonansdm_id')->references('id')->on('permohonansdm')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensipermohonansdm');
	}

}
