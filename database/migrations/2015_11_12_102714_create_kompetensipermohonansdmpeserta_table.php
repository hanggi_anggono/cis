<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKompetensipermohonansdmpesertaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensipermohonansdmpeserta', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('permohonansdm_id')->unsigned();
			$table->integer('peserta_id')->unsigned();
			$table->integer('kompetensipermohonansdm_id')->unsigned();
			$table->timestamps();

			$table->foreign('kompetensipermohonansdm_id')->references('id')->on('kompetensipermohonansdm')->onDelete('cascade');
			$table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade');
			$table->foreign('permohonansdm_id')->references('id')->on('permohonansdm')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensipermohonansdmpeserta');
	}

}
