<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitrantingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unitranting', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('unitcabang_id')->unsigned();
			$table->string('nama_unitranting')->index();
			$table->integer('created_by')->unsigned();
			$table->integer('edited_by')->unsigned();
			$table->softDeletes();
			$table->timestamps();

			$table->foreign('unitcabang_id')->references('id')->on('unitcabang')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unitranting');
	}

}
