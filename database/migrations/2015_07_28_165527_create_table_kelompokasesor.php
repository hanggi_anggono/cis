<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKelompokasesor extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kelompokasesor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('asesor_id')->unsigned()->index();
			$table->integer('lembagasertifikasi_id')->unsigned()->index();
			$table->date('tanggalpenetapan')->nullable();
			$table->timestamps();

			$table->foreign('asesor_id')->references('id')->on('asesor')->onDelete('cascade');
			$table->foreign('lembagasertifikasi_id')->references('id')->on('lembagasertifikasi')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kelompokasesor');
	}

}
