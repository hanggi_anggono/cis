<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsesorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asesor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nip',30)->index();
			$table->integer('pendidikan_id')->unsigned();
			$table->integer('jenjangjabatan_id')->unsigned();
			$table->integer('grade_id')->unsigned();
			$table->integer('unitinduk_id')->unsigned()->nullable();
			$table->integer('unitcabang_id')->unsigned()->nullable();
			$table->integer('unitranting_id')->unsigned()->nullable();
			$table->integer('unitnonpln_id')->unsigned()->nullable();
			$table->string('nama',100);
			$table->string('no_identitas',25);
			$table->enum('jenis_kelamin',['m','f'])->default('m');
			$table->string('alamat_sekarang',255)->nullable();
			$table->string('alamat_identitas',255)->nullable();
			$table->string('kewarganegaraan',100)->nullable();
			$table->string('tempat_lahir',100)->nullable();
			$table->date('tanggal_lahir')->nullable();
			$table->char('golongan_darah',2)->nullable();
			$table->string('email',50)->nullable();
			$table->string('no_hp',15)->nullable();
			$table->string('no_telp',15)->nullable();
			$table->string('no_telp_perusahaan',15)->nullable();
			$table->string('jabatan',50)->nullable();
			$table->string('photo',50)->nullable();
			$table->tinyInteger('status')->default(1);
			$table->enum('tipe_asesor', ['pln', 'nonpln'])->default('pln');
			$table->integer('created_by')->unsigned();
			$table->integer('edited_by')->unsigned();
			$table->softDeletes();
			$table->timestamps();

			// define foreign keys
			$table->foreign('pendidikan_id')->references('id')->on('pendidikan')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('jenjangjabatan_id')->references('id')->on('jenjangjabatan')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('grade_id')->references('id')->on('grade')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('unitinduk_id')->references('id')->on('unitinduk')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('unitcabang_id')->references('id')->on('unitcabang')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('unitranting_id')->references('id')->on('unitranting')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('asesor');
	}

}
