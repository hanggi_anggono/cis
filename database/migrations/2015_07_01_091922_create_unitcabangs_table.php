<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitcabangsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unitcabang', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('unitinduk_id')->unsigned();
			$table->string('nama_unitcabang')->index();
			$table->integer('created_by')->unsigned();
			$table->integer('edited_by')->unsigned();
			$table->softDeletes();
			$table->timestamps();

			$table->foreign('unitinduk_id')->references('id')->on('unitinduk')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unitcabang');
	}

}
