<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermohonankompetensiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensipermohonan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('kompetensi_id')->unsigned();
			$table->integer('permohonan_id')->unsigned();
			$table->timestamps();

			$table->foreign('kompetensi_id')->references('id')->on('kompetensi')->onDelete('cascade');
			$table->foreign('permohonan_id')->references('id')->on('permohonan')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensipermohonan');
	}

}
