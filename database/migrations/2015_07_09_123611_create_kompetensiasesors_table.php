<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKompetensiasesorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensiasesor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('kompetensi_id')->unsigned();
			$table->integer('asesor_id')->unsigned();
			$table->timestamps();

			// define foreign keys
			$table->foreign('kompetensi_id')->references('id')->on('kompetensi')->onDelete('restrict')->onUpdate('restrict');			
			$table->foreign('asesor_id')->references('id')->on('asesor')->onDelete('restrict')->onUpdate('restrict');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensiasesor');
	}

}
