<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKompetensipenjadwalan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensipenjadwalan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('kompetensi_id')->unsigned();
			$table->integer('penjadwalan_id')->unsigned();
			$table->timestamps();

			$table->foreign('kompetensi_id')->references('id')->on('kompetensi')->onDelete('cascade');
			$table->foreign('penjadwalan_id')->references('id')->on('penjadwalan')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensipenjadwalan');
	}

}
