<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSertifikatasesorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sertifikatasesor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('no_registrasi',30)->index();
			$table->string('no_sertifikat',30)->index();
			$table->date('tanggal_terbit')->nullable();
			$table->date('masa_berlaku')->nullable();
			$table->integer('lembaga_id')->unsigned();
			$table->integer('asesor_id')->unsigned();
			$table->softDeletes();
			$table->timestamps();

			// define foreign keys
			$table->foreign('lembaga_id')->references('id')->on('lembaga')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('asesor_id')->references('id')->on('asesor')->onDelete('cascade')->onUpdate('cascade');			
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sertifikatasesor');
	}

}
