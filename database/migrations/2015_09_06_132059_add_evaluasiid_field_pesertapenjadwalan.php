<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEvaluasiidFieldPesertapenjadwalan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pesertakompetensipenjadwalan', function(Blueprint $table)
		{
			$table->integer('evaluasi_id')->unsigned();

			$table->foreign('evaluasi_id')->references('id')->on('evaluasi')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pesertakompetensipenjadwalan', function(Blueprint $table)
		{
			$table->dropForeign('pesertakompetensipenjadwalan_evaluasi_id_foreign');
			$table->dropColumn('evaluasi_id');
		});
	}

}
