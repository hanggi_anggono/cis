<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealisasipesertasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('realisasipeserta', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('peserta_id')->unsigned();
			$table->integer('kompetensipenjadwalan_id')->unsigned();
			$table->integer('kompetensi_id')->unsigned();
			$table->enum('kehadiran', ['n', 'y'])->default('n');
			$table->enum('kelulusan', ['n', 'y'])->default('n');
			$table->text('information')->nullable();
			$table->string('no_sertifikat', 50)->index();
			$table->date('tanggal_cetak')->nullable();
			$table->date('tanggal_awal')->nullable();
			$table->date('tanggal_akhir')->nullable();
			$table->date('tanggal_keluar')->nullable();
			$table->string('status_sertifikat', 20)->nullable();
			$table->timestamps();

			$table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade');
			$table->foreign('kompetensipenjadwalan_id')->references('id')->on('kompetensipenjadwalan')->onDelete('cascade');
			$table->foreign('kompetensi_id')->references('id')->on('kompetensi')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('realisasipeserta');
	}

}
