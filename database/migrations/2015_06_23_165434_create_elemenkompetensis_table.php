<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElemenkompetensisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('elemenkompetensi', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama_elemen', 100);
			$table->integer('kompetensi_id')->unsigned();
			$table->timestamps();

			$table->foreign('kompetensi_id')->references('id')->on('kompetensi')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('elemenkompetensi');
	}

}
