<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsPenjadwalanasesorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('penjadwalanasesor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('penjadwalan_id')->unsigned();
			$table->integer('asesor_id')->unsigned();
			$table->string('posisi')->nullable(); // ketua, anggota1, anggota2
			$table->timestamps();

			$table->foreign('penjadwalan_id')->references('id')->on('penjadwalan')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('penjadwalanasesor');
	}

}
