<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitinduksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unitinduk', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('udiklat_id')->unsigned();
			$table->string('nama_unitinduk')->index();
			$table->string('wilayah_unitinduk')->nullable();
			$table->mediumText('nonpln')->nullable();
			$table->integer('edited_by')->unsigned();
			$table->integer('created_by')->unsigned();
			$table->softDeletes();
			$table->timestamps();

			$table->foreign('udiklat_id')->references('id')->on('udiklat')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unitinduk');
	}

}
