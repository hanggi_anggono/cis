<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKompetensipermohonanpesertaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensipermohonanpeserta', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('permohonan_id')->unsigned();
			$table->integer('peserta_id')->unsigned();
			$table->integer('kompetensipermohonan_id')->unsigned();
			$table->timestamps();

			$table->foreign('kompetensipermohonan_id')->references('id')->on('kompetensipermohonan')->onDelete('cascade');
			$table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade');
			$table->foreign('permohonan_id')->references('id')->on('permohonan')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensipermohonanpeserta');
	}

}
