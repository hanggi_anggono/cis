<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSertifikathistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sertifikathistory', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sertifikat_id')->unsigned();
			$table->integer('peserta_id')->unsigned();
			$table->integer('kompetensi_id')->unsigned();
			$table->integer('lembagasertifikasi_id')->unsigned();
			$table->integer('penjadwalan_id')->unsigned();
			$table->string('no_sertifikat')->nullable()->index();
			$table->date('tanggal_mulai_berlaku')->nullable();
			$table->date('tanggal_akhir_berlaku')->nullable();
			$table->date('tanggal_cetak')->nullable();
			$table->enum('status', ['baru', 'perpanjangan'])->default('baru');
			$table->string('information')->nullable();
			$table->timestamps();

			$table->foreign('sertifikat_id')->references('id')->on('sertifikat')->onDelete('cascade');
			$table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade');
			$table->foreign('kompetensi_id')->references('id')->on('kompetensi')->onDelete('cascade');
			$table->foreign('lembagasertifikasi_id')->references('id')->on('lembagasertifikasi')->onDelete('cascade');
			$table->foreign('penjadwalan_id')->references('id')->on('penjadwalan')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sertifikathistory');
	}

}
