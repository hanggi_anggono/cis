<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermohonansdmTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permohonansdm', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sdm_id')->unsigned();
			$table->integer('unitinduk_id')->unsigned();
			$table->date('tanggal_mohon')->nullable();
			$table->string('keterangan')->nullable();
			$table->tinyInteger('status_mohon')->default(1);
			$table->softDeletes();
			$table->timestamps();

			$table->foreign('unitinduk_id')->references('id')->on('unitinduk')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permohonansdm');
	}

}
