<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLembagasertifikasisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lembagasertifikasi', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama_lembagasertifikasi')->index();
			$table->string('koderegistrasilsp')->nullable();
			$table->enum('grup', ['pln', 'kerjasama'])->default('pln');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lembagasertifikasi');
	}

}
