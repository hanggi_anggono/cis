<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitnonplnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unitnonpln', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama_unitnonpln')->index();
			$table->string('alamat');
			$table->integer('created_by')->unsigned();
			$table->integer('edited_by')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});

		Schema::table('asesor', function(Blueprint $table)
		{
			$table->foreign('unitnonpln_id')->references('id')->on('unitnonpln')->onDelete('cascade')->onUpdate('cascade');
		});

		Schema::table('peserta', function(Blueprint $table)
		{
			$table->foreign('unitnonpln_id')->references('id')->on('unitnonpln')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('unitnonpln');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

	}

}
