<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubbidangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subbidang', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('bidang_id')->unsigned();
			$table->string('nama_subbidang');
			$table->timestamps();

			$table->foreign('bidang_id')->references('id')->on('bidang')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subbidang');
	}

}
