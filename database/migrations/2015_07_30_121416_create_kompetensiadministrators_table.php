<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKompetensiadministratorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kompetensiadministrator', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('administrator_id')->unsigned()->index();
			$table->integer('kompetensi_id')->unsigned()->index();
			$table->timestamps();

			$table->foreign('administrator_id')->references('id')->on('administrator')->onDelete('cascade');
			$table->foreign('kompetensi_id')->references('id')->on('kompetensi')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kompetensiadministrator');
	}

}
