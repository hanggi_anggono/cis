<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('kompetensi_id')->unsigned();
			$table->string('nip',30)->index();
			$table->integer('pendidikan_id')->unsigned();
			$table->integer('jenjangjabatan_id')->unsigned();
			$table->integer('grade_id')->unsigned();
			$table->integer('unitinduk_id')->unsigned()->nullable();
			$table->integer('unitcabang_id')->unsigned()->nullable();
			$table->integer('unitranting_id')->unsigned()->nullable();
			$table->integer('unitnonpln_id')->unsigned()->nullable();
			$table->string('nama',100);
			$table->string('no_identitas',25);
			$table->enum('jenis_kelamin',['m','f'])->default('m');
			$table->string('kewarganegaraan',100)->nullable();
			$table->string('tempat_lahir',100)->nullable();
			$table->date('tanggal_lahir')->nullable();
			$table->string('alamat',100)->nullable();
			$table->char('golongan_darah',2)->nullable();
			$table->string('email',50)->nullable();
			$table->string('no_hp',15)->nullable();
			$table->string('no_telp',15)->nullable();
			$table->string('jabatan',50)->nullable();
			$table->string('jurusan',100)->nullable();
			$table->enum('tipe_peserta',['pln','nonpln'])->default('pln');
			$table->string('non_pln_info',100)->nullable();
			$table->enum('kondisi_khusus', ['y', 'n'])->default('y');
			$table->text('keterangan_kondisi_khusus')->nullable();
			$table->string('photo',50)->nullable();
			$table->enum('is_valid', ['y', 'n'])->default('n');
			$table->string('validated_by', 100)->nullable();
			$table->timestamps();

			// define foreign keys
			$table->foreign('pendidikan_id')->references('id')->on('pendidikan')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('jenjangjabatan_id')->references('id')->on('jenjangjabatan')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('grade_id')->references('id')->on('grade')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('unitinduk_id')->references('id')->on('unitinduk')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('unitcabang_id')->references('id')->on('unitcabang')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('unitranting_id')->references('id')->on('unitranting')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('kompetensi_id')->references('id')->on('kompetensi')->onDelete('restrict')->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registers');
	}

}
