<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertakompetensipenjadwalanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pesertakompetensipenjadwalan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('penjadwalan_id')->unsigned();
			$table->integer('kompetensipenjadwalan_id')->unsigned();
			$table->integer('peserta_id')->unsigned();
			$table->timestamps();

			$table->foreign('penjadwalan_id')->references('id')->on('penjadwalan')->onDelete('cascade');
			$table->foreign('kompetensipenjadwalan_id')->references('id')->on('kompetensipenjadwalan')->onDelete('cascade');
			$table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pesertakompetensipenjadwalan');
	}

}
