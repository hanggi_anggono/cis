<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldEnglishBidangToElemenkompetensi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bidang', function(Blueprint $table)
		{
			$table->string('nama_bidang_english')->nullable();
		});
		Schema::table('subbidang', function(Blueprint $table)
		{
			$table->string('nama_subbidang_english')->nullable();
		});
		Schema::table('kompetensi', function(Blueprint $table)
		{
			$table->string('nama_kompetensi_english')->nullable();
		});
		Schema::table('elemenkompetensi', function(Blueprint $table)
		{
			$table->string('nama_elemen_english')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bidang', function(Blueprint $table)
		{
			$table->dropColumn('nama_bidang_english');
		});
		Schema::table('subbidang', function(Blueprint $table)
		{
			$table->dropColumn('nama_subbidang_english');
		});
		Schema::table('kompetensi', function(Blueprint $table)
		{
			$table->dropColumn('nama_kompetensi_english');
		});
		Schema::table('elemenkompetensi', function(Blueprint $table)
		{
			$table->dropColumn('nama_elemen_english');
		});
	}

}
