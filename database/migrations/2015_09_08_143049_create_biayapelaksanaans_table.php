<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiayapelaksanaansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('biayapelaksanaan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('penjadwalan_id')->unsigned();
			$table->integer('komponenbiaya_id')->unsigned()->nullable();
			$table->string('nosurat_tagihan')->nullable();
			$table->integer('biaya')->nullable()->default(0);
			$table->timestamps();

			$table->foreign('penjadwalan_id')->references('id')->on('penjadwalan')->onDelete('cascade');
			$table->foreign('komponenbiaya_id')->references('id')->on('komponenbiaya')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('biayapelaksanaan');
	}

}
