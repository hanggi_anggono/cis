<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTypeSertifikatLembagaSertifikasi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('lembagasertifikasi', function(Blueprint $table)
		{
			$table->enum('jenis_sertifikat', ['bnsp', 'pln'])->default('pln');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('lembagasertifikasi', function(Blueprint $table)
		{
			$table->dropColumn('jenis_sertifikat');
		});
	}

}
