<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		// $this->call('TableLembagaSeeder');
		// $this->call('LembagasertifikasiTableSeeder');
		// $this->call('PendidikanTableSeeder');
		// $this->call('GradeTableSeeder');
		// $this->call('BidangTableSeeder');
		// $this->call('SubbidangTableSeeder');
		// $this->call('KompetensiTableSeeder');
		// $this->call('ElemenkompetensiTableSeeder');
		// $this->call('JenjangTableSeeder');
		// $this->call('UdiklatTableSeeder');
		// $this->call('UnitindukTableSeeder');
		// $this->call('UcabangTableSeeder');
		// $this->call('UrantingTableSeeder');
		// $this->call('UnitnonplnTableSeeder');
		// $this->call('TimasesorTableSeeder');
		// $this->call('AsesorTableSeeder');
		// $this->call('AdministratorTableSeeder');
		// $this->call('PesertaTableSeeder');
		// $this->call('PermohonanTableSeeder');
		// $this->call('KomponenbiayaTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('EntrustTableSeeder');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
