<?php 

use Illuminate\Database\Seeder;
use Certification\Models\Role;
use Certification\Models\Permission;
use Certification\Models\User;
use \DB;

class EntrustTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('roles')->truncate();
		DB::table('permissions')->truncate();
		DB::table('permission_role')->truncate();
		DB::table('role_user')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		$admin           = Role::create(['name' => 'superadmin', 'display_name' => 'Super Admin', 'description' => 'Super administrator']);
		$pegawai         = Role::create(['name' => 'employee', 'display_name' => 'Employee', 'description' => 'Pegawai']);
		$sdm             = Role::create(['name' => 'sdm', 'display_name' => 'SDM Unit', 'description' => 'SDM Unit']);
		$unitsertifikasi = Role::create(['name' => 'unitsertifikasi', 'display_name' => 'Unit Sertifikasi', 'description' => 'Unit Sertifikasi']);
		$asesor          = Role::create(['name' => 'asesor', 'display_name' => 'Asesor', 'description' => 'Asesor']);
		$lsk             = Role::create(['name' => 'lsk', 'display_name' => 'LSK', 'description' => 'Lembaga Sertifikasi']);

		$permArr = [
		
			/**
			 * define dashboard action
			 */
			['can_read_maindashboard', 'view dashboard utama'],
			
			/**
			 *  define master administrator action
			 */
			['can_read_masteradministrator', 'view master administrator'],
			['can_write_masteradministrator', 'Write master administrator'],

			/**
			 *  define master asesor action
			 */
			['can_read_masterasesor', 'view master asesor'],
			['can_write_masterasesor', 'Write master asesor'],

			/**
			 *  define master peserta action
			 */
			['can_read_masterpeserta', 'view master peserta'],
			['can_write_masterpeserta', 'Write master peserta'],

			/**
			 *  define master akreditor action
			 */
			['can_read_masterakreditor', 'view master akreditor'],
			['can_write_masterakreditor', 'Write master akreditor'],

			/**
			 *  define master LSK action
			 */
			['can_read_masterlembagasertifikasi', 'view master lembaga sertifikasi'],
			['can_write_masterlembagasertifikasi', 'Write master lembaga sertifikasi'],

			/**
			 *  define master Bidang action
			 */
			['can_read_masterbidang', 'view master bidang'],
			['can_write_masterbidang', 'Write master bidang'],

			/**
			 *  define master kompetensi action
			 */
			['can_read_masterkompetensi', 'view master kompetensi'],
			['can_write_masterkompetensi', 'Write master kompetensi'],

			/**
			 *  define master unit action
			 */
			['can_read_masterunit', 'view master unit'],
			['can_write_masterunit', 'Write master unit'],

			/**
			 *  define master kompetensi action
			 */
			['can_read_masterjenjangjabatan', 'view master jenjang jabatan'],
			['can_write_masterjenjangjabatan', 'Write master jenjang jabatan'],

			/**
			 *  define master pendidikan action
			 */
			['can_read_masterpendidikan', 'view master pendidikan'],
			['can_write_masterpendidikan', 'Write master pendidikan'],

			/**
			 *  define master grade action
			 */
			['can_read_mastergrade', 'view master jenjang grade'],
			['can_write_mastergrade', 'Write master jenjang grade'],

			/**
			 *  define master komponen biaya action
			 */
			['can_read_masterkomponenbiaya', 'view master komponen biaya'],
			['can_write_masterkomponenbiaya', 'Write master komponen biaya grade'],

			/**
			 *  define master user role action
			 */
			['can_read_masterrole', 'view user role'],
			['can_write_masterrole', 'Write User Role'],

			/**
			 *  define permohonan action
			 */
			['can_read_permohonan', 'view permohonan'],
			['can_write_permohonan', 'Write permohonan'],

			/**
			 *  define evaluasi action
			 */
			['can_read_evaluasi', 'view evaluasi'],
			['can_write_evaluasi', 'Write evaluasi'],

			/**
			 *  define penjadwalan action
			 */
			['can_read_penjadwalan', 'view penjadwalan'],
			['can_write_penjadwalan', 'Write penjadwalan'],

			/**
			 *  define pelaksanaan action
			 */
			
			['can_read_pelaksanaan', 'view pelaksanaan'],
			['can_write_pelaksanaan', 'Write pelaksanaan'],

			/**
			 *  define sertifikat action
			 */
			['can_read_sertifikatbaru', 'view sertifikat baru'],
			['can_read_sertifikatperpanjangan', 'view sertifikat perpanjangan'],
			['can_write_sertifikatbaru', 'Write sertifikat baru'],
			['can_write_sertifikatperpanjangan', 'Write sertifikat perpanjangan'],

			/**
			 * define report action
			 */
			['can_read_laporanpelaksanaan', 'view laporan pelaksanaan'],
			['can_read_laporanevaluasi', 'view laporan evaluasi'],
			['can_read_laporanasesor', 'view laporan asesor'],
			['can_read_laporansebarankompetensi', 'view laporan sebaran kompetensi'],
			['can_read_laporanhasilkelulusan', 'view laporan hasil kelulusan'],
			['can_read_laporanbiaya', 'view laporan biaya'],

		];

		foreach ($permArr as $value) {
			$permission = new Permission();
			$permission->name = $value[0];
			$permission->display_name = $value[1];
			$permission->save();

			$admin->attachPermission($permission);
		}
		
		$adminRole = DB::table('roles')->where('name', '=', 'superadmin')->pluck('id');

		$user = User::where('id', '=', 1)->first();
		$user->roles()->attach($adminRole);
	}
}
