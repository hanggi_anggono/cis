<?php

use Illuminate\Database\Seeder;
use Certification\Models\Unitranting;
use \DB;

class UrantingTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('unitranting')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		Unitranting::create(['nama_unitranting' => 'Unit ranting 1','unitcabang_id'=>1,'created_by'=>1,'edited_by'=>1]);
		Unitranting::create(['nama_unitranting' => 'Unit ranting 2','unitcabang_id'=>2,'created_by'=>1,'edited_by'=>1]);
	}
}
