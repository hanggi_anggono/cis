<?php

use Illuminate\Database\Seeder;
use Certification\Models\Kompetensiasesor;
use \DB;

class KompetensiasesorTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('kompetensiasesor')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

			Kompetensiasesor::create([
					'kompetensi_id'     => 1,
					'asesor_id' => 1
			]);
			Kompetensiasesor::create([
					'kompetensi_id'     => 2,
					'asesor_id' => 1
			]);
			Kompetensiasesor::create([
					'kompetensi_id'     => 1,
					'asesor_id' => 2
			]);						
	}

}
