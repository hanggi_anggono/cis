<?php

use Illuminate\Database\Seeder;
use Certification\Models\Udiklat;
use \DB;

class UdiklatTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('udiklat')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 2; $i++) { 
			Udiklat::create(['nama_udiklat' => 'Nama TUK '.$i]);
		}
	}
}
