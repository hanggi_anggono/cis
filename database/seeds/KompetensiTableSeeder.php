<?php

use Illuminate\Database\Seeder;
use Certification\Models\Kompetensi;
use \DB;

class KompetensiTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('kompetensi')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		// for ($i=0; $i < 21; $i++) { 
			Kompetensi::create([
							'nama_kompetensi' => 'Memelihara JTR',
							'subbidang_id' => 1
							]);
			Kompetensi::create([
							'nama_kompetensi' => 'Memelihara JTM',
							'subbidang_id' => 1
							]);
			Kompetensi::create([
							'nama_kompetensi' => 'Pengoperasian APP satu fase',
							'subbidang_id' => 2
							]);
			Kompetensi::create([
							'nama_kompetensi' => 'Pengoperasian APP tiga fase',
							'subbidang_id' => 2
							]);

		// }
	}

}
