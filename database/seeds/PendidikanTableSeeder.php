<?php

use Illuminate\Database\Seeder;
use Certification\Models\Pendidikan;
use \DB;

class PendidikanTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('pendidikan')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 20; $i++) { 
			Pendidikan::create(['nama_pendidikan' => 'Pendidikan '.$i]);
		}
	}
}
