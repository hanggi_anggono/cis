<?php

use Illuminate\Database\Seeder;
use Certification\Models\User;
use \DB, \Hash;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('users')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

			User::create([
					'nip' => '123456',
					'password' => Hash::make('rahasia123'),
					'role' => 'admin'
				]);
	}

}
