<?php

use Illuminate\Database\Seeder;
use Certification\Models\Subbidang;
use \DB;

class SubbidangTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('subbidang')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		// for ($i=0; $i < 25; $i++) { 
			Subbidang::create([
				'bidang_id' => 1,
				'nama_subbidang' => 'Pemeliharaan Distribusi'
				]);

			Subbidang::create([
				'bidang_id' => 1,
				'nama_subbidang' => 'Pengoperasian Distribusi'
				]);
		// }
	}

}
