<?php

use Illuminate\Database\Seeder;
use Certification\Models\Sertifikatasesor;

class SertifikatasesorTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('sertifikatasesor')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

			Sertifikatasesor::create([
					'no_registrasi' => '1000-1000-1',
					'no_sertifikat' => '1000-1000-1',
					'lembaga_id'     => 1,
					'asesor_id' => 1
			]);
			Sertifikatasesor::create([
					'no_registrasi' => '1000-1000-2',
					'no_sertifikat' => '1000-1000-2',
					'lembaga_id'     => 4,
					'asesor_id' => 1
			]);
			Sertifikatasesor::create([
					'no_registrasi' => '1000-1000-3',
					'no_sertifikat' => '1000-1000-3',
					'lembaga_id'     => 3,
					'asesor_id' => 2
			]);
			Sertifikatasesor::create([
					'no_registrasi' => '1000-1000-4',
					'no_sertifikat' => '1000-1000-4',
					'lembaga_id'     => 2,
					'asesor_id' => 3
			]);									
	}

}
