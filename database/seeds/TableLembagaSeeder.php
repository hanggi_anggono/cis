<?php

use Illuminate\Database\Seeder;
use Certification\Models\Lembaga;
use \DB;

class TableLembagaSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('lembaga')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 20; $i++) { 
			Lembaga::create(['nama_lembaga' => 'Lembaga '.$i]);
		}
	}
}
