<?php

use Illuminate\Database\Seeder;
use Certification\Models\Peserta;
use \DB;

class PesertaTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('peserta')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
		
		$tipePeserta = ['pln','nonpln'];

		for ($i=0; $i < 50; $i++) { 
			Peserta::create([
					'nip'               => '1000-1000-'.$i,
					'pendidikan_id'     => 1,
					'jenjangjabatan_id' => 1,
					'grade_id'          => 1,
					'nama'              => 'Nama Peserta ke-'.$i,
					'no_identitas'      => '100000'.$i,
					'kewarganegaraan'   => 'Indonesia',
					'tempat_lahir'      => 'Surabaya',
					'tanggal_lahir'     => '1945-08-17',
					'alamat'			=> 'Jl. Dewandaru Dalam No. 5 Malang',
					'golongan_darah'    => 'B',
					'email'             => 'peserta'.$i.'@pln.co.id',
					'no_hp'             => '0852580000'.$i,
					'jabatan'           => 'Staff',
					'status'            => 1,
					'created_at'        => 1,
					'edited_by'         => 1,
					'tipe_peserta'		=> $tipePeserta[rand(0,1)],
					'jurusan'			=> 'Teknik Industri',
					'non_pln_info'		=> 'lorem ipsum'
				]);
		}
	}

}
