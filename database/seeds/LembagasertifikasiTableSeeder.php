<?php

use Illuminate\Database\Seeder;
use Certification\Models\Lembagasertifikasi;
use \DB;

class LembagasertifikasiTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('lembagasertifikasi')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 10; $i++) { 
			Lembagasertifikasi::create(['nama_lembagasertifikasi' => 'Lembaga sertifikasi'.$i, 'koderegistrasilsp' => 'xxxxx']);
		}
	}
}
