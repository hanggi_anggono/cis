<?php

use Illuminate\Database\Seeder;
use Certification\Models\Bidang;
use \DB;

class BidangTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('bidang')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		// for ($i=0; $i < 21; $i++) { 
			Bidang::create(['nama_bidang' => 'Bidang Distribusi']);
		// }
	}

}
