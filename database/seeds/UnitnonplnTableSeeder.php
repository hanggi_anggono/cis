<?php

use Illuminate\Database\Seeder;
use Certification\Models\Unitnonpln;
use \DB;

class UnitnonplnTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('unitnonpln')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 10; $i++) { 
			Unitnonpln::create(['nama_unitnonpln' => 'Unit Non PLN  '.$i,'alamat' => 'Alamatnya di sana ', 'created_by'=>1,'edited_by'=>1]);
		}
	}
}
