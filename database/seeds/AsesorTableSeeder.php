<?php

use Illuminate\Database\Seeder;
use Certification\Models\Asesor;
use \DB;

class AsesorTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('asesor')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
		$tipeAsesor = ['pln', 'nonpln'];

		for ($i=0; $i < 15; $i++) { 
			Asesor::create([
					'nip'               => '1000-1000-'.$i,
					'pendidikan_id'     => 1,
					'jenjangjabatan_id' => 1,
					'grade_id'          => 1,
					'tipe_asesor'		=> $tipeAsesor[mt_rand(0,1)],
					'nama'              => 'Nama asesor ke-'.$i,
					'no_identitas'      => '100000'.$i,
					'kewarganegaraan'   => 'Indonesia',
					'tempat_lahir'      => 'Surabaya',
					'tanggal_lahir'     => '1945-08-17',
					'golongan_darah'    => 'B',
					'alamat_sekarang'   => 'Jl. Dewandaru dalam No.'.$i.' Malang',
					'email'             => 'asesor'.$i.'@pln.co.id',
					'no_hp'             => '0852580000'.$i,
					'jabatan'           => 'Direktur',
					'status'            => 1,
					'created_at'        => 1,
					'edited_by'         => 1
				]);
		}
	}

}
