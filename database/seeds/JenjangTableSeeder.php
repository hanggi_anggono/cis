<?php

use Illuminate\Database\Seeder;
use Certification\Models\Jenjang;
use \DB;

class JenjangTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('jenjangjabatan')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 22; $i++) { 
			Jenjang::create(['nama_jenjangjabatan' => 'Jenjang Jabatan '.$i]);
		}
	}
}
