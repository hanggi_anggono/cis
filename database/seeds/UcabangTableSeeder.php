<?php

use Illuminate\Database\Seeder;
use Certification\Models\Unitcabang;
use \DB;

class UcabangTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('unitcabang')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 2; $i++) { 
			Unitcabang::create(['nama_unitcabang' => 'Unit cabang '.$i,'unitinduk_id'=>mt_rand(1,2),'created_by'=>1,'edited_by'=>1]);
		}
	}
}
