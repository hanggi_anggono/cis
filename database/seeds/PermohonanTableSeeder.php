<?php

use Illuminate\Database\Seeder;
use Certification\Models\Permohonan;
use \DB;

class PermohonanTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('permohonan')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		// reset pivots
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('kompetensipermohonan')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('kompetensipermohonanpeserta')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 5; $i++) { 
			Permohonan::create([
					'nosurat' => 'SK000-'.$i,
					'unitinduk_id' => rand(1,2),
					'tanggal_surat' => date('Y-m-d'),
					'keterangan' => 'lorem ipsum sit dolor amet'
				]);
		}
	}

}
