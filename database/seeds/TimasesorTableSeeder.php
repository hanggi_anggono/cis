<?php

use Illuminate\Database\Seeder;
use Certification\Models\Timasesor;
use \DB;

class TimasesorTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('timasesor')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 11; $i++) { 
			Timasesor::create(['jabatantim' => 'jabatan tim '.$i]);
		}
	}
}
