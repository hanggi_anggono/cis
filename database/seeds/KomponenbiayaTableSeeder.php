<?php

use Illuminate\Database\Seeder;
use Certification\Models\Komponenbiaya;
use \DB;

class KomponenbiayaTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('komponenbiaya')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 3; $i++) { 
			Komponenbiaya::create(['nama_komponenbiaya' => 'komponen biaya '.$i]);
		}
	}

}
