<?php

use Illuminate\Database\Seeder;
use Certification\Models\Administrator;
use \DB;

class AdministratorTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('administrator')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 15; $i++) { 
			Administrator::create([
					'nip'               => '1000-1000-'.$i,
					'pendidikan_id'     => 1,
					'jenjangjabatan_id' => 1,
					'grade_id'          => 1,
					'unitinduk_id'      => 1,
					'unitcabang_id'     => 1,
					'unitranting_id'    => 1,
					'nama'              => 'Nama administrator ke-'.$i,
					'no_identitas'      => '100000'.$i,
					'kewarganegaraan'   => 'Indonesia',
					'tempat_lahir'      => 'Surabaya',
					'tanggal_lahir'     => '1945-08-17',
					'golongan_darah'    => 'B',
					'email'             => 'administrator'.$i.'@pln.co.id',
					'no_hp'             => '0852580000'.$i,
					'jabatan'           => 'Direktur',
					'status'            => 1,
					'created_at'        => 1,
					'edited_by'         => 1
				]);
		}
	}

}
