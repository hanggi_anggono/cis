<?php

use Illuminate\Database\Seeder;
use Certification\Models\Unitinduk;
use \DB;

class UnitindukTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('unitinduk')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 2; $i++) { 
			Unitinduk::create(['nama_unitinduk' => 'Unit induk '.$i, 'udiklat_id'=>mt_rand(1,2),'created_by'=>1,'edited_by'=>1]);
		}
	}
}
