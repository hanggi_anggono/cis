<?php

use Illuminate\Database\Seeder;
use Certification\Models\Grade;
use \DB;

class GradeTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::table('grade')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		for ($i=0; $i < 21; $i++) { 
			Grade::create(['nama_grade' => 'Grade '.$i]);
		}
	}

}
